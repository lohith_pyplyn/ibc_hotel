$(document).ready(function() {

	$("#menuIcon").click(function(e) {
		// pr(e);
		$("#menu").fadeIn(300);
	});

	$("#close").click(function() {
		$("#menu").fadeOut(300);
	});

	function closeMenu(){
	  $('#menu').fadeOut(500);
	}

});
