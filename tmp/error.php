<div class="content-page">
  <div class="content">
      <div class="container">
          <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Error</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="#/dashboard">BOS</a>
                        </li>
                        <li>
                            <a href="#/">Error</a>
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
          </div>
          <div class="row">
              <div class="col-sm-12">
                  <div class="card-box">
                    <h1>Page Not Found</h1>
                    <h4><a href="#/dashboard" class="app-green-text">Go Back</a></h4>
                  </div>
              </div>
          </div>
          <!-- End row -->
      </div>
      <!-- <div ng-include="'tmp/tmp/footer.php'"></div> -->
  </div>
</div>
