<div class="content-page">
  <div class="content">
    <div class="container" style="margin-top: 100px;">
    <div class="row">
              <div class="col-sm-12">
                <div class="portlet">
                    <div class="portlet-heading card-box-green">
                        <h3 class="portlet-title">
                            <i class="fa fa-user fa-lg"></i> Profile
                        </h3>
                        <div class="clearfix"></div>
                    </div>
                    <div id="bg-primary" class="panel-collapse collapse in">
                        <div class="portlet-body">
                          <span class="message col-sm-6 col-sm-offset-3"></span>
                          <form class="form-horizontal" name="add_agent_form" method="post" ng-repeat="agent in agent_details">
                            <div class="row">
                              <div class="col-md-3 col-md-offset-1">
                                <div class="form-group">
                                    <img ng-src="./assets/images/users/{{agent.image}}" alt="" class="img-circle" width="100%" height="80%"><br><br><br>
                                    <label for="field-1" class="control-label">Profile Image</label>
                                    <input type="file" name="profile_pic" class="form-control" ngf-select="" accept="*" ng-model="profile_pic" />
                                </div>
                              </div>
                              <div class="col-md-4 col-md-offset-2">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Name</label>
                                    <input type="text" name="name" ng-model="agent.name" class="form-control" required>
                                    <div ng-show="add_agent_form.name.$touched">
                                      <p class="error" ng-show="add_agent_form.name.$error.required">First name is required.</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Email</label>
                                    <input type="text" name="email" ng-model="agent.email" class="form-control" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Phone</label>
                                    <input type="text" name="mobile_no" ng-model="agent.mobile_no" class="form-control" ng-pattern="/^[0-9]+$/">
                                    <div ng-show="add_agent_form.mobile_no.$touched">
                                      <p class="error" ng-show="add_agent_form.mobile_no.$error.pattern">Invalid Phone number.</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Address</label>
                                    <textarea type="text" name="address" ng-model="agent.address" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4">
                                      <label for="field-1" class="control-label">City</label>
                                      <input type="text" name="city" ng-model="agent.city" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="field-1" class="control-label">State</label>
                                        <input type="text" name="state" ng-model="agent.state" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="field-1" class="control-label">Country</label>
                                        <input type="text" name="country" ng-model="agent.country" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Zip</label>
                                    <input type="text" name="zip" ng-model="agent.zip" ng-pattern="/^[0-9]+$/" class="form-control">
                                    <div ng-show="add_agent_form.zip.$touched">
                                      <p class="error" ng-show="add_agent_form.zip.$error.pattern">Invalid Pincode.</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Expiry Date : </label>
                                    <label for="field-1" class="control-label">{{ agent.expire_date | date : 'dd MMM, yyyy' }}</label>

                                    <!-- <input type="text" name="expire_date" ng-model="" class="form-control" readonly> -->
                                </div>
                              </div>
                            </div>

                            <div class="modal-footer">
                                <!-- <a type="button" class="btn btn-default waves-effect" href="#/dashboard">Cancel</a> -->
                                <button type="submit" class="btn btn-primary waves-effect waves-light" ng-disabled="add_agent_form.$invalid" ng-click="updateProfile(agent,profile_pic);">Update</button>
                            </div>
                          </form>
                        </div>
                    </div>
                </div>
              </div>
          </div>
          <!-- End row -->
    </div>
    <div ng-include="'tmp/footer.php'"></div>
  </div>
</div>
