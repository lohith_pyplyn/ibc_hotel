
<section id="main-slider" class="no-margin text-center">
    <div class="carousel slide">
        <div class="carousel-inner">
            <div class="item active col-sm-12" style=" opacity: 1.5; background-repeat: no-repeat; background-size: cover; height:500px; background-image: url(assets/images/banner-2-bedroom-superior.jpg)">
                <div class="container">
                    <div class="row slide-margin">
                        <div class="col-sm-12">
                            <div class="carousel-content" style= "width: fit-content; padding: 0% 2%;   margin-left: auto; margin-right: auto;margin-top: 15%;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><br/><br/>
<div class="availability_form" style="width:100%; text-align:center;">
  <form class="form-inline" name="availabilityForm" method="post" enctype="multipart/form-data">
    <div class="form-group availibility_form_group">
      <!-- <label>Check In:</label> -->
      <div class="input-group">
        <span class="input-group-addon">
        <span class="glyphicon glyphicon-calendar fa-lg" style="color: #99e265"></span>
        </span>
        <input  type="text" name="check_in" ng-model="avlbty.check_in" class="form-control datetimepicker checkIn" placeholder="Check In Date" ng-value="check_in">
      </div>
   </div>
    <div class="form-group availibility_form_group">
      <!-- <label>Check Out:</label> -->
      <div class="input-group  " >
        <span class="input-group-addon">
        <span class="glyphicon glyphicon-calendar fa-lg"  style="color: #99e265"></span>
        </span>
        <input type="text" name="check_out" ng-model="avlbty.check_out" class="form-control datetimepicker"  placeholder="Check Out Date" ng-value="check_out">
        </div>
     </div>
    <div class="form-group ">
      <button  type="button" class="btn btn-default availibility_btn" ng-click="get_availability(avlbty)" name="button">Search</button>
    </div>
  </form>
</div>
<div class="container-fluid">
    <div ng-repeat="data in availability">
        <div class="col-md-4">
            <div class="property-card">
                <div class="property-image" style="background: url('uploads/room_images/{{data.image}}') center center / cover no-repeat;">
                    <span class="property-label label label-danger" style="font-size: 13px;">
                        <span ng-if="data.available_rooms != 0">{{data.available_rooms}} Room Available</span>
                        <span ng-if="data.available_rooms == 0">SOLD OUT</span>
                    </span>
                </div>
                <div class="property-content">
                    <div class="listingInfo">
                        <h4 class="text-success m-t-0">&#8377; {{data.price}}/-</h4>
                        <h3 class="text-overflow"><p class="text-dark">{{data.room_type}}</p></h3>
                        <p class="text-muted"><i class="mdi mdi-map-marker-radius m-r-5"></i>Diamond District Outer Podium,Block- G,HAL Old Airport Rd,ISRO Colony,Domlur,Bengaluru,Karnataka 560008</p>
                        <div class="m-t-20">
                            <button type="button" style="width:100%;" class="btn w-md btn-bordered btn-primary waves-effect waves-light  btndisable" data-toggle="modal" ng-if="data.available_rooms != 0" ng-click="storeBookingTempData(data)"><i class=""></i> Add Room</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4" ng-if="grand_total != 0">
        <div class="property-card">
            <div class="property-content">
                <div ng-repeat="roomtotal in availability">
                    <div class="portlet" ng-if="roomtotal.is_room_added == true">
                        <div class="portlet-heading bg-success">
                            <h3 class="portlet-title">
                                {{roomtotal.room_type}}
                            </h3>
                            <div class="portlet-widgets">
                                <a ng-click="remove_room(roomtotal.id);" data-toggle="dismiss"><i class="ion-close-round"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div id="bg-success" class="panel-collapse collapse in">
                            <div class="row text-center">
                                <div class="col-xs-4">
                                    <p class="text-overflow" title="Square Feet">No. of Rooms</p>
                                    <h4>{{roomtotal.selected_no_of_rooms}}</h4>
                                </div>
                                <div class="col-xs-4">
                                    <p class="text-overflow" title="Bedrooms">Adults</p>
                                    <h4>{{roomtotal.selected_adults}}</h4>
                                </div>
                                <div class="col-xs-4" ng-show="roomtotal.selected_children">
                                    <p class="text-overflow" title="Bedrooms">Children</p>
                                    <h4>{{roomtotal.selected_children}}</h4>
                                </div>
                                <div class="col-xs-4" ng-show="roomtotal.selected_extra_beds">
                                    <p class="text-overflow" title="Bedrooms">Extra Beds</p>
                                    <h4>{{roomtotal.selected_extra_beds}}</h4>
                                </div>
                                <div class="col-xs-4">
                                    <p class="text-overflow" title="Parking Space">Price</p>
                                    <h4>&#8377; {{roomtotal.selected_price}}/-</h4>
                                </div>
                                <div class="col-md-4">
                                    <p class="text-overflow" title="Square Feet">Guest</p>
                                    <h5>{{roomtotal.guest_name}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <a class="pointer" data-toggle="modal" data-target="#promocode-modal">Have a promo code ?</a>
        </div><br/>
        <div class="card-box">
            <h4>
                <p><span>Total Price :</span><span class="pull-right"> &#8377; {{grand_total}}/-</span></p>
                <p ng-if="promocode_applied"><span>Promocode Discount ({{discount_percent}}%) :</span><span class="pull-right"> - &#8377; {{discounted_price}}/-</span></p>
                <p><span>GST (12%) :</span><span class="pull-right"> &#8377; {{tax_price}}/-</span></p>
                <p><strong>Grand Total :</strong><strong class="pull-right"> &#8377; {{final_price}}/-</strong></p>
            </h4>
        </div>
        <!-- End property item -->

        <div class="col-md-12 pull-right">
            <span><input type="checkbox" name="terms_condn" ng-model="terms_condn" id="terms_condn" ng-click="termsConditions(terms_condn);" > &nbsp;
                <span>I have read and agree to the Terms of Service</span></span><br/><br/>
            <button type="button" class="btn btn-lg btn-success btn-rounded waves-effect w-md waves-light col-md-12 text-uppercase" ng-click="confirmBooking(final_price);"><i class=""></i><strong> Confirm Booking </strong></button>
        </div>
    </div>
</div>


<div class="modal fade" id="userbooking-modal" data-backdrop="static" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="col-sm-12">
            <div class="m-t-30 account-pages">
                <div class="text-center account-logo-box header-bar">
                    <h3 class="text-uppercase">
                        <span style="color:white;">Add Rooms</span>
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">x</button>
                    </h3>
                    <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
                </div>
                <div class="account-content">
                    <form class="form-horizontal" name="addroom_form" enctype="multipart/form-data" method="post">
                        <div class="row">
                            <span class="message col-sm-10 col-sm-offset-1"></span>
                            <div class="col-md-12">
                                <input type="hidden" name="id" ng-model="addroom.id">
                                <div class="form-group">
                                    <label for="no_of_rooms">Guest Name</span></label>
                                    <input type="text" class="form-control" name="guest_name" ng-model="addroom.guest_name" required>
                                    <div ng-show="addroom_form.guest_name.$touched">
                                        <p ng-show="addroom_form.guest_name.$error.required" class="error">This field is required!</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="no_of_rooms">No. of Rooms</span></label>
                                    <select class="form-control" name="selected_no_of_rooms" ng-model="addroom.selected_no_of_rooms" ng-change="get_room_max_counts(addroom.id, addroom.selected_no_of_rooms);" required>
                                        <option value="">Select</option>
                                        <option ng-repeat="no_ofrooms in available_rooms" ng-value="no_ofrooms">{{no_ofrooms}}</option>
                                    </select>
                                    <div ng-show="addroom_form.selected_no_of_rooms.$touched">
                                        <p ng-show="addroom_form.selected_no_of_rooms.$error.required" class="error">This field is required!</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="no_of_adults">No. of Adults</span></label>
                                    <select class="form-control" name="no_of_adults1" ng-model="addroom.no_of_adults1" required>
                                        <option value="">Select</option>
                                        <option ng-repeat="no_adult in no_of_adults" value="{{no_adult.id}}">{{no_adult.id}}</option>
                                    </select>
                                    <div ng-show="addroom_form.no_of_adults1.$touched">
                                        <p ng-show="addroom_form.no_of_adults1.$error.required" class="error">This field is required!</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="no_of_children">No. of Children</span></label>
                                    <select class="form-control" name="no_of_children1" ng-model="addroom.no_of_children1">
                                        <option value="">Select</option>
                                        <option ng-repeat="no_child in no_of_children" value="{{no_child.id}}">{{no_child.id}}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="extra_beds1">Extra Beds</span></label>
                                    <select class="form-control" name="extra_beds1" ng-model="addroom.extra_beds1">
                                        <option value="">Select</option>
                                        <option ng-repeat="ex_bd in no_of_extras" value="{{ex_bd.id}}">{{ex_bd.id}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-primary waves-effect waves-light" ng-disabled="addroom_form.$invalid" ng-click="saveRooms(addroom)">
                                Save
                            </button>
                        </div>
                    </form>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- end wrapper -->
        </div>
    </div><!-- /.modal-dialog -->
</div><!-- End of modal -->


<div class="modal fade" id="promocode-modal" data-backdrop="static" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="col-sm-12">
            <div class="m-t-30 account-pages">
                <div class="text-center account-logo-box header-bar">
                    <h4 class="text-uppercase">
                        <span style="color:white;">Apply Promo Code</span>
                        <button type="button" class="btn btn-xs btn-default pull-right" data-dismiss="modal">x</button>
                    </h4>
                    <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
                </div>
                <div class="account-content">
                    <form class="form-horizontal" name="promocode_form" enctype="multipart/form-data" method="post">
                        <div class="row">
                            <span class="message col-sm-10 col-sm-offset-1"></span>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="promo_code">Enter Promo Code</span></label>
                                    <input type="text" class="form-control" name="promo_code" ng-model="promo_code" required>
                                    <div ng-show="promocode_form.promo_code.$touched">
                                        <p ng-show="promocode_form.promo_code.$error.required" class="error">This field is required!</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-primary waves-effect waves-light" ng-disabled="promocode_form.$invalid" ng-click="applyPromoCode(promo_code)">
                                Save
                            </button>
                        </div>
                    </form>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- end wrapper -->
        </div>
    </div><!-- /.modal-dialog -->
</div><!-- End of modal -->


<!-- Modal -->
<div class="modal fade" id="loginSignup" role="dialog">
      <div class="modal-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="wrapper-page">
                    <div class="m-t-40 account-pages">
                        <div class="text-center account-logo-box" style="background:#000000;">
                            <h2 class="text-uppercase" style="color:#ffffff!important;">
                              login
                              <button type="button" class="btn btn-default pull-right" data-dismiss="modal">&times;</button>
                            </h2>
                        </div>
                        <div class="account-content" style="border-radius: 0 0 5px 5px">
                            <form class="form-horizontal" name="loginUserForm">
                                <span id="success" class="col-sm-12 success"></span>
                                <span id="error" class="col-sm-12 error"></span>
                                <div class="form-group ">
                                  <div class="col-xs-12">
                                    <input class="form-control" type="text" required="" placeholder="Username" name="username" ng-model="user.username">
                                    <div ng-show="loginUserForm.username.$touched">
                                      <p class="error" ng-show="loginUserForm.username.$error.required">This field is required.</p>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <input class="form-control" type="password" name="password" ng-model="user.password" required="" placeholder="Password">
                                        <div ng-show="loginUserForm.password.$touched">
                                          <p class="error" ng-show="loginUserForm.password.$error.required">This field is required.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group text-center m-t-30">
                                    <div class="col-sm-12">
                                        <a id="forgotPass" class="text-muted" data-toggle="modal" data-target="#forgotModal"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
                                    </div>
                                </div>
                                <div class="form-group text-center m-t-10">
                                    <div class="col-xs-12">
                                        <button class="btn w-md btn-bordered btn-primary" type="submit"  ng-click="login(user);">Log In</button>
                                    </div>
                                </div>
                            </form>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <!-- end card-box-->
                </div>
                <!-- end wrapper -->
            </div>
        </div>
      </div>
</div>
<script>
  $(function() {
    $(window).on("scroll", function() {
        if($(window).scrollTop() >=100) {
            $(".headerIBC").addClass("scroDown");
        } else {
            //remove the background property so it comes transparent again (defined in your css)
           $(".headerIBC").removeClass("scroDown");
        }
    });
});
  </script>


  <script>

    $.datetimepicker.setLocale('en');

    $('.datetimepicker').datetimepicker({
        timepicker: false,
        format:'Y-m-d',
        minDate: 0
    });


  </script>
