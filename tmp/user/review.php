<div class="content-page">
  <div class="content">
      <div class="container">
          <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Review</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="#/dashboard">IBC</a>
                        </li>
                        <li>
                            <a>Review </a>
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
          </div>
          <!-- End row -->
      </div>
      <div ng-include="'tmp/footer.php'"></div>
  </div>
</div>
