<div ng-controller="NavigationController">
  <div id="menuLoaderDiv" ng-show="isLoggedIn" style="display: none;">
    <div ng-controller="HeaderController">
        <div class="topbar">
            <div class="topbar-left">
              <a href="#/dashboard" class="logo">
                  <span>
                      <img src="assets/images/logo.png" height="100%" alt="" style="margin-top: 35px;">
                  </span>
                  <i>
                      <img src="assets/images/logo.png" alt="" height="50" style="margin-top:30px;">
                  </i>
              </a>
            </div>
            <div class="navbar navbar-default" role="navigation">
              <div class="container">
                <ul class="nav navbar-nav navbar-left">
                      <li>
                          <button class="button-menu-mobile open-left waves-effect">
                              <i class="mdi mdi-menu"></i>
                          </button>
                      </li>
                  </ul>
                    <ul class="nav navbar-nav app-text-center">
                      <li class="app-name">BOOKING <span class="app-green-text">Operations</span> SYSTEM</li>
                    </ul>
                  <ul class="nav navbar-nav navbar-right">
                      <li class="dropdown user-box">
                          <a href="" class="dropdown-toggle waves-effect user-link" data-toggle="dropdown" aria-expanded="true">
                              <img ng-src="assets/images/users/avatars/avatar_default2.png" alt="user-img" class="img-circle user-img">
                          </a>
                          <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list" >
                              <li>
                                  <h5>Hi, User</h5>
                              </li>
                              <li><a href="#/profile"><i class="glyphicon glyphicon-user m-r-5"></i> Profile</a></li>
                              <li><a href="#/change-password"><i class="glyphicon glyphicon-cog"></i> Change Password</a></li>
                              <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0);" ng-click="logout();"><i class="glyphicon glyphicon-log-out"></i> &nbsp;Logout</a></li>
                          </ul>
                      </li>
                  </ul>

              </div>
            </div>
        </div>
        <div class="left side-menu">
            <div class="sidebar-inner slimscrollleft">
                <div id="sidebar-menu">
                    <ul>
                        <li class="has_sub">
                            <a href="#/dashboard" class="waves-effect"><i class="fa fa-tachometer app-green-text"></i> <span> Dashboard </span> </a>
                        </li>
                        <li class="has_sub">
                            <a href="#/booking" class="waves-effect"><i class="fa fa-book app-green-text"></i> <span> Booking </span> </a>
                        </li>
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money app-green-text"></i> <span>Payments</span> <span class="menu-arrow"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="#/booking-payment"><span> Booking Payment </span> </a></li>
                                <li><a href="#/service-payment"><span> Service Payment </span> </a></li>
                            </ul>
                        </li>
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money app-green-text"></i> <span>Expenses</span> <span class="menu-arrow"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="#/expense-category">Expense Types</a></li>
                                <li><a href="#/expense-vouchers">Expense Vouchers</a></li>
                            </ul>
                        </li>
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bus app-green-text"></i> <span>Services</span> <span class="menu-arrow"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="#/service" class="waves-effect"> <span> Add Service </span> </a></li>
                                <li><a href="#/book-service" class="waves-effect"><span> Book Service </span> </a></li>
                            </ul>
                        </li>
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file app-green-text"></i><span> Reports </span><span class="menu-arrow"></span> </a>
                            <ul class="list-unstyled">
                                <li><a href="#/customer-report" class="waves-effect"> <span> Customer </span> </a></li>
                                <!-- <li><a href="#/book-service" class="waves-effect"><span> Book Service </span> </a></li> -->
                            </ul>
                        </li>
                        <li class="has_sub">
                            <a href="#/banks" class="waves-effect"><i class="fa fa-university app-green-text"></i><span> Banks </span> </a>
                        </li>
                        <li class="has_sub">
                            <a href="#/supplier" class="waves-effect"><i class="fa fa-industry app-green-text"></i> <span> Supplier </span> </a>
                        </li>
                        <li class="has_sub">
                            <a href="#/hotel" class="waves-effect"><i class="fa fa-building app-green-text"></i> <span> Hotel </span> </a>
                        </li>
                        <li class="has_sub">
                            <a href="#/room_type" class="waves-effect"><i class="fa fa-bed app-green-text"></i> <span> Room Type </span> </a>
                        </li>
                        <li class="has_sub">
                            <a href="#/meal" class="waves-effect"><i class="fa fa-cutlery app-green-text"></i> <span> Meal </span> </a>
                        </li>
                        <li class="has_sub">
                            <a href="#/customer" class="waves-effect"><i class="fa fa-group app-green-text"></i> <span> Customers </span> </a>
                        </li>
                        <li class="has_sub">
                            <a href="#/add_details" class="waves-effect"><i class="fa fa-plus-circle fa-lg app-green-text"></i> <span> Add Details </span> </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="waves-effect" ng-click="logout();"><i class="mdi mdi-logout app-green-text"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
