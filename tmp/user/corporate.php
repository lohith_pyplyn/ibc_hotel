<section id="main-slider" class="no-margin text-center">
        <div class="carousel slide">
            <div class="carousel-inner">
                <div class="item active col-sm-12 "  class="img-responsive img1" style="  opacity: 1.5; background-repeat: no-repeat; background-size: cover; height:500px; background-image: url(assets/images/adventure-backlit-community-207896.jpg)">
                    <div class="container">
                        <div class="row slide-margin">
                            <div class="col-sm-12">
                                <div class="carousel-content" style= "width: fit-content; padding: 0% 2%;   margin-left: auto; margin-right: auto;margin-top: 15%;">

                                    <!-- <div class="centered">Centered</div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="availability_form" style="width:100%; text-align:center;">
      <form class="form-inline" name="availabilityForm" method="post" enctype="multipart/form-data">
        <div class="form-group availibility_form_group">
          <!-- <label>Check In:</label> -->
          <div class="input-group">
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar fa-lg" style="color: #99e265"></span>
            </span>
            <input  type="text" name="check_in" ng-model="avlbty.check_in" class="form-control datetimepicker checkIn" name="check_in" placeholder="Check In Date">
          </div>
       </div>
        <div class="form-group availibility_form_group">
          <!-- <label>Check Out:</label> -->
          <div class="input-group  " >
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar fa-lg"  style="color: #99e265"></span>
            </span>
            <input type="text" name="check_out" ng-model="avlbty.check_out" class="form-control datetimepicker checkIn" name="check_out" placeholder="Check Out Date">
            </div>
         </div>
        <!-- <div class="form-group availibility_form_group">
         <label>Adult:</label>
          <select class="form-control checkIn" ng-model="avlbty.no_of_person_allowed" name="no_of_person_allowed">
            <option value="" disabled selected>Guests</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
          </select>
        </div> -->

        <!-- <label>Children:</label> -->
        <!-- <div class="form-group availibility_form_group">

          <select class="form-control" ng-model="availability.no_of_children_allowed" name="no_of_children_allowed">
            <option value="" disabled selected>Select Childeren</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
          </select>
        </div> -->

        <div class="form-group ">
          <button  type="button" class="btn btn-default availibility_btn" ng-click="availability(avlbty)" name="button">Search</button>
        </div>
      </form>
    </div>


<div class="container">

<div class="row marg_top_100">
        <div class="col-md-10 col-md-offset-1">
<div class="text-justify">
<h4>Corporate Travel</h4>
<h5>
<p>We understand how business trips can be tiresome for companies as well as employees. We know
how to make business trips a hassle-free experience.IBC Hotels &amp; Resorts offers personalized
services to corporate customers, our apartment hotel ensures the luxuries of a hotel with the peace
of a home. This programme is designed solely keeping companies in mind with special rates and
other exclusive services on the offer.</p></h5>
<h5>
<p>Our Corporate Travel Programme is available to companies of every size to minimize costs and
maximize value of business trips for your employees, matching all your company’s business needs.</p></h5>

<h4>Why Choose IBC Hotels &amp; Resorts for Corporate Accommodation?</h4>
<h5>
<p>We have a decade of experience in hosting corporate guests and we&#39;re committed towards
providing spacious, modern and conveniently located apartments for your employees. With business
facilities on the offer, we make sure that the needs of your employees are well taken care of.</p></h5>


<h4>Business Services</h4>
<h5>
<p>IBC Hotels &amp; Resorts offers a range of business services to help you remain calm even on a business
trip. Whether you wish to conduct work calls, conduct quick meeting, fax or even a print, we&#39;ve got
you covered.</p></h5>
<h4>An Ideal Location</h4>
<h5>
<p>Conveniently located in the vibrant gated society Diamond District, on the Old Airport road, IBC
Hotels &amp; Resorts is only minutes away from business hubs, hospitals, and shopping arenas.<p></h5>
  <h5>
<p>To inquire about corporate travel plans, please contact marketing@ibchotels.co.in <p></h5>

</div>
</div>

<script>

  $.datetimepicker.setLocale('en');

  // var currentdate = new Date();
  // var datetime = "Now: " + currentdate.getDate() + "-"
  //             + (currentdate.getMonth()+1)  + "-"
  //             + currentdate.getFullYear() + "  "
  //             + currentdate.getHours() + ":"
  //             + currentdate.getMinutes() + ":"
  //             + currentdate.getSeconds();

  $('.datetimepicker').datetimepicker({
      timepicker: false,
      format:'Y-m-d',
      minDate: 0
  });
  // $('#check_in').datetimepicker({
  //   timepicker: false,
  //     minDate: 0
  // });
  // $('#check_in').blur(function(){
  //   var check_in = document.getElementById('check_in').value;
  // })

  // $('#check_out').datetimepicker({

  //   timepicker : false,
  //   minDate: check_in
  // })


</script>
