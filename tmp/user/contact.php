<section id="main-slider" class="no-margin text-center">
    <div class="carousel slide">
        <div class="carousel-inner">
            <div class="item active col-sm-12" style=" opacity: 1.5; background-repeat: no-repeat; background-size: cover; height:500px; background-image: url(assets/images/banner4.jpg)">
                <div class="container">
                    <div class="row slide-margin">
                        <div class="col-sm-12">
                            <div class="carousel-content" style= "width: fit-content; padding: 0% 2%;   margin-left: auto; margin-right: auto;margin-top: 15%;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><
<div class="container-fluid contact">
	<!-- <img src="assets/images/apartment.jpg" class="img-responsive"> -->
	<h1 class="text-center animated zoomIn">Contact Us</h1>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.212926991894!2d77.6421778143751!3d12.958222590864684!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae123840cb9b3d%3A0xa7354ed5f00a568d!2sIBC+Hotels+%26+Resorts!5e0!3m2!1sen!2sin!4v1533213551656" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	    </div>
    	<div class="col-md-4">
        <div class="container">
          <form id="contact" action="" method="post">
            <h3>Contact Us</h3>
            <!-- <h4>Contact us today, and get reply with in 24 hours!</h4> -->
            <fieldset>
              <input placeholder="Your name" type="text" tabindex="1" required autofocus>
            </fieldset>
            <fieldset>
              <input placeholder="Your Email Address" type="email" tabindex="2" required>
            </fieldset>
            <fieldset>
              <input placeholder="Your Phone Number" type="tel" tabindex="3" required>
            </fieldset>
            <!-- <fieldset>
              <input placeholder="Your Web Site starts with http://" type="url" tabindex="4" required>
            </fieldset> -->
            <fieldset>
              <textarea placeholder="Type your Message Here...." tabindex="5" required></textarea>
            </fieldset>
            <fieldset>
              <button name="submit" type="submit" id="contact-submit" data-submit="...Sending">Submit</button>
            </fieldset>
          </form>


        </div>
	    </div>
    </div>
</div>
