<section id="main-slider" class="no-margin text-center">
        <div class="carousel slide">
            <div class="carousel-inner">
                <div class="item active col-sm-12" style=" opacity: 1.5; background-repeat: no-repeat; background-size: cover; height:500px; background-image: url(assets/images/banner-2-bedroom-superior.jpg)">
                    <div class="container">
                        <div class="row slide-margin">
                            <div class="col-sm-12">
                                <div class="carousel-content" style= "width: fit-content; padding: 0% 2%;   margin-left: auto; margin-right: auto;margin-top: 15%;">

                                    <!-- <div class="centered">Centered</div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<div class="container">
<div class="row marg_top_100">
        <div class="col-md-10 col-md-offset-1">
<h4 class="text-center">Terms & Conditions</h4>
<div class="text-justify">
<p>IBC Hotels & Resorts pvt.ltd owns and manages IBC Hotel website, this site may contain links
to other websites (“Linked Web Sites”). Those links are provided for convenience only. You
acknowledge and agree that IBC Hotel does not have any control over the content or
availability of Linked Web Sites and accepts no responsibility for the content, privacy
policies or any other views of Linked Websites.</p>

<p>The booking/reservation feature of the Site is provided merely to assist customers in
determining the availability of travel-related services and products and to make genuine
bookings and for no other purposes.</p>

<p>You certify that you are at least 18 years of age, possess the legal authority to enter into the
legal agreement constituted by your acceptance of these Conditions and to use the Site in
accordance with such Conditions.</p>

<p>You agree to be financially accountable for your use of the Site including without limitation
for all bookings/reservations made by you or on your account for you, whether authorised
by you or not. For any reservations or other services for which fees may be charged you
consent to abide by the terms or conditions including without limitation payment of all
money due under such terms and conditions.</p>

<p>The Site contains details of hotel charges and room rates (including any available special
offers) for hotels owned or managed by IBC Hotels. Hotel reservation terms and conditions
of booking are set out on the Site and payment will be in accordance with the policies set
out in such terms and conditions.</p>

<p>No contract will be valid between you and IBC Hotels in respect of any services or products
offered through the Site unless IBC Hotels accepts your order by e-mail or automated
confirmation through the Site declaring that it has accepted your reservation, booking or
order and any such contract shall be deemed to incorporate the hotel reservation terms and
conditions of booking.</p>

<p>You acknowledge that all details you provide to in connection with any services or products
which may be offered by IBC Hotels on the Site (including hotel room reservations) will be
correct and, wherever applicable, the credit card which you use is your own and that there
are sufficient funds to cover the cost of any services or products which you wish to
purchase. IBC Hotels reserves the right to obtain verification of your credit card details
before providing you with any services or products.</p>

<p>As per the government regulations a valid photo id has to be carried by every person above
the age of 18 staying at the hotel. It is mandatory for guest to furnish a valid photo id
proof(original) at the time of check in.</p>

<p>Please note that all the taxes mentioned in our package and final negotiated rates have
been in accordance with the Government regulations. Any revision in tax structure due to
Government policies or changes in rules are applicable at the time of billing.</p>

<h4>Refund Policy</h4>
<p>Confirmed bookings are non-amendable & non-refundable.</p>

<h4>Cancellation policy<h4>

<p>A No-show or non-cancellation of a confirmed reservation atleast 48 hours prior to arrival will attract
a one night retention charge.</p>
</div>
</div>
</div>
</div>