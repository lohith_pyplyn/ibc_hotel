<section id="main-slider" class="no-margin text-center">
    <div class="carousel slide">
        <div class="carousel-inner">
            <div class="item active col-sm-12" style=" opacity: 1.5; background-repeat: no-repeat; background-size: cover; height:500px; background-image: url(assets/images/banner-2-bedroom-superior.jpg)">
                <div class="container">
                    <div class="row slide-margin">
                        <div class="col-sm-12">
                            <div class="carousel-content" style= "width: fit-content; padding: 0% 2%;   margin-left: auto; margin-right: auto;margin-top: 15%;">

                                <!-- <div class="centered">Centered</div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    <div class="availability_form" style="width:100%; text-align:center;">
      <form class="form-inline" name="availabilityForm" method="post" enctype="multipart/form-data">
        <div class="form-group availibility_form_group">
          <!-- <label>Check In:</label> -->
          <div class="input-group">
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar fa-lg" style="color: #99e265"></span>
            </span>
            <input  type="text" name="check_in" ng-model="avlbty.check_in" class="form-control datetimepicker checkIn" name="check_in" placeholder="Check In Date">
          </div>
       </div>
        <div class="form-group availibility_form_group">
          <!-- <label>Check Out:</label> -->
          <div class="input-group  " >
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar fa-lg"  style="color: #99e265"></span>
            </span>
            <input type="text" name="check_out" ng-model="avlbty.check_out" class="form-control datetimepicker checkIn" name="check_out" placeholder="Check Out Date">
            </div>
         </div>

        <div class="form-group ">
          <button  type="button" class="btn btn-default availibility_btn" ng-click="availability(avlbty)" name="button">Search</button>
        </div>
      </form>
    </div><br/><br/>
<h3 class="text-center">YOUR FAVORITE DESTINATIONS AT OUR ATTRACTIVE PRICES</h3>
  <br></br>
  <div class="container-fluid">
    <!-- end row -->
    <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class="property-card">
                <div class="property-image" style="background: url('assets/images/studio/singlerrom_featured.jpg') center center / cover no-repeat;">
                </div>
                <div class="property-content">
                    <div class="listingInfo">
                        <div class="">
                            <h5 class="text-success m-t-0">&#8377; {{ studio_price }}/-</h5>
                        </div>
                        <div class="">
                            <h3 class="text-overflow"><class="text-dark">Studio Apartment</h3>
                            <!-- <p class="text-muted text-overflow"><i class="mdi mdi-map-marker-radius m-r-5"></i>245 E 20th St, New York, NY 201609</p> -->
                            <!-- <div class="row text-center">
                              <p>With combined living area and bedroom, our modern and sleek studio apartment is perfect for those
                             looking for comfortable stays. The Apartment offers Queen size bed with en suite bathroom and a
                             fully- equipped kitchenette.</p>
                            </div> -->
                            <div class="m-t-20">
                                 <a href="#/studio_apartment" class="btn btn-success btn-block waves-effect waves-light" >View Detail</a>
                            </div>
                        </div>
                    </div>
                    <!-- end. Card actions -->
                </div>
                <!-- /inner row -->
            </div>
            <!-- End property item -->
        </div>
        <!-- end col -->

        <div class="col-md-4 col-sm-6">
            <div class="property-card">
                <div class="property-image" style="background: url('assets/images/single-room/Room.jpg') center center / cover no-repeat;">
                </div>
                <div class="property-content">
                    <div class="listingInfo">
                        <div class="">
                            <h5 class="text-success m-t-0">&#8377; {{ one_bedroom_apartment_price }}/-</h5>
                        </div>
                        <div class="">
                            <h3 class="text-overflow"><class="text-dark">One Bedroom Apartment</h3>
                            <div class="m-t-20">
                                 <a href="#/apartment_one" class="btn btn-success btn-block waves-effect waves-light" >View Detail</a>
                            </div>
                        </div>
                    </div>
                    <!-- end. Card actions -->
                </div>
                <!-- /inner row -->
            </div>
            <!-- End property item -->
        </div>
        <!-- end col -->
        <div class="col-md-4 col-sm-6">
            <div class="property-card">
                <div class="property-image" style="background: url('assets/images/double-room/Lobby_Two_Bedroom.jpg') center center / cover no-repeat;">
                </div>
                <div class="property-content">
                    <div class="listingInfo">
                        <div class="">
                            <h5 class="text-success m-t-0">&#8377; {{ two_bedroom_apartment_price }}/-</h5>
                        </div>
                        <div class="">
                            <h3 class="text-overflow"><a href="#" class="text-dark">Two Bedroom Apartment</a></h3>
                            <!-- <p class="text-muted text-overflow"><i class="mdi mdi-map-marker-radius m-r-5"></i>245 E 20th St, New York, NY 201609</p> -->
                            <!-- <div class="row text-center">
                              <p>Home-style living even on a holiday, for those discerning guests who don’t like crammed hotel rooms. Two bedroom apartments are ideal if you’re travelling with your family or friends, en entire apartment at your disposal ensure privacy and more room to relax. The apartment comes with two bedrooms, fully-equipped kitchen, dining area, living area and a balcony as well.</p>
                            </div> -->
                            <div class="m-t-20">
                                 <a href="#/apartment_two" class="btn btn-success btn-block waves-effect waves-light" >View Detail</a>
                            </div>
                        </div>
                    </div>
                    <!-- end. Card actions -->
                </div>
                <!-- /inner row -->
            </div>
            <!-- End property item -->
        </div>
        <!-- end col -->
        <div class="col-md-4 col-sm-6">
            <div class="property-card">
                <div class="property-image" style="background: url('assets/images/triple-room/Lobby_3_Bedroom.jpg') center center / cover no-repeat;">
                </div>
                <div class="property-content">
                    <div class="listingInfo">
                        <div class="">
                            <h5 class="text-success m-t-0">&#8377; {{ three_bedroom_apartment_price }}/-</h5>
                        </div>
                        <div class="">
                            <h3 class="text-overflow"><class="text-dark">Three Bedroom Apartment</h3>
                            <!-- <p class="text-muted text-overflow"><i class="mdi mdi-map-marker-radius m-r-5"></i>245 E 20th St, New York, NY 201609</p> -->
                            <!-- <div class="row text-center">
                              <p>Live like a local in our three bedroom apartment, fitted with all the necessary amenities of a home, this is perfect for large groups or friends. Sleeping four to six people, this popular apartment offers en-suite bathroom, fully-equipped kitchen, dining hall and a spacious living room.</p>
                            </div> -->
                            <div class="m-t-20">
                                 <a href="#/apartment_Three" class="btn btn-success btn-block waves-effect waves-light" >View Detail</a>
                            </div>

                        </div>
                    </div>
                    <!-- end. Card actions -->
                </div>
                <!-- /inner row -->
            </div>
            <!-- End property item -->
        </div>
        <!-- end col -->
        <div class="col-md-4 col-sm-6">
            <div class="property-card">
                <div class="property-image" style="background: url('assets/images/Pent_house_Lobby.jpg') center center / cover no-repeat;">
                </div>
                <div class="property-content">
                    <div class="listingInfo">
                        <div class="">
                            <h5 class="text-success m-t-0">&#8377; {{ penthouse__price }}/-</h5>
                        </div>
                        <div class="">
                            <h3 class="text-overflow"><a href="#" class="text-dark">Three/Four Bedroom Penthouse</a></h3>
                            <!-- <p class="text-muted text-overflow"><i class="mdi mdi-map-marker-radius m-r-5"></i>245 E 20th St, New York, NY 201609</p> -->
                            <!-- <div class="row text-center">
                              <p>Luxuriously expansive for the well-travelled guests to make them feel right at home, this 3/4 Bedroom Penthouse is perfect for families. The entire penthouse at your disposal ensures privacy along with a comfortable stay. This spacious penthouse is well-furnished to accommodate six to eight guests. The apartment comes with separate bedding, dining and living areas along with balconies with a view.</p>
                            </div> -->
                            <div class="m-t-20">
                                 <a href="#/apartment_four" class="btn btn-success btn-block waves-effect waves-light" >View Detail</a>
                            </div>
                        </div>
                    </div>
                    <!-- end. Card actions -->
                </div>
                <!-- /inner row -->
            </div>
            <!-- End property item -->
        </div>
        <!-- end col -->
    </div>
  </div>
  <script>
    $(function() {
      $(window).on("scroll", function() {
          if($(window).scrollTop() >=100) {
              $(".headerIBC").addClass("scroDown");
          } else {
              //remove the background property so it comes transparent again (defined in your css)
             $(".headerIBC").removeClass("scroDown");
          }
      });
  });
    </script>
  <script>

    $.datetimepicker.setLocale('en');
    $('.datetimepicker').datetimepicker({
        timepicker: false,
        format:'Y-m-d',
        minDate: 0
    });
  </script>
