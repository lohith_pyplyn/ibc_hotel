<br/ >
<div class="property-detail-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="">
                <ul class="bxslider property-slider">
                    <li><img style="width: 100%" src="assets/images/single-room/Master Bedroom Pent house.jpg" /></li>
                    <li><img style="width: 100%" src="assets/images/single-room/Room.jpg" /></li>
                    <li><img style="width: 100%" src="assets/images/single-room/balcony.jpg" /></li>
                    <li><img style="width: 100%" src="assets/images/single-room/bathroom.jpg" /></li>
                </ul>

                <div id="bx-pager" class="text-center hide-phone">
                    <a data-slide-index="0" href="#"><img src="assets/images/single-room/Master Bedroom Pent house.jpg" height="70" /></a>
                    <a data-slide-index="1" href="#"><img src="assets/images/single-room/Room.jpg" height="70" /></a>
                    <a data-slide-index="2" href="#"><img src="assets/images/single-room/balcony.jpg" height="70" /></a>
                    <a data-slide-index="3" href="#"><img src="assets/images/single-room/bathroom.jpg" height="70" /></a>
                </div>
            </div>
            <!-- end slider -->
            <br/ >
          </div>
    </div> <!-- end row -->
    <div class="row">
      <div class="col-md-3">
          <div class="card-box" style="margin-top: 130px;">
              <div class="table-responsive">
                  <table class="table table-bordered m-b-0">
                      <tbody>
                      <tr>
                              <th scope="row"><a class="txt-colour" href="#/studio_apartment">Studio Apartment:</a></th>
                          </tr>
                          <tr>
                              <th scope="row"><a class="txt-colour" href="#/apartment_one">One Bedroom Apartment</a></th>
                          </tr>
                          <tr>
                              <th scope="row"><a class="txt-colour" href="#/apartment_two">Two Bedroom Apartment:</a></th>
                          </tr>
                          <tr>
                              <th scope="row"><a class="txt-colour" href="#/apartment_Three">Three Bedroom Apartment:</a></th>
                          </tr>
                          <tr>
                              <th scope="row"><a class="txt-colour" href="#/apartment_four">Three/Four Bedroom Penthouse:</a></th>
                          </tr>

                      </tbody>

                  </table>
                  <ul class="social-links list-inline m-t-30">
                      <li>
                          <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="#" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
                      </li>
                      <li>
                          <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="#" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
                      </li>
                      <li>
                          <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="#" data-original-title="Skype"><i class="fa fa-skype"></i></a>
                      </li>
                  </ul>
              </div>
          </div>
          <!-- end card-box -->
      </div> <!-- end col -->
      <div class="col-md-9">
          <h2 style="padding-left: 81px;">One Bedroom Apartment</h2><br><br>
          <h4>In-Suite Facilities</h4>
          <div class="container-fluid  apartmentthree" style="  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
    padding: 22px;
    margin-right: 143px;
    width: 116%;
    margin-left: -2px;
    padding-right: 0px;
    font-size: initial;">
 <div class="column ">

  <div class="col-sm-3" style="background-color:white;  margin-right: 11px;">
    <h4 style="color:black;">One Bedroom Apartment</h4>
<h5>
    <li>Number of bedrooms:1</li>
    <li>Number of bathrooms: 1</li>
    <li>Number of private toilets: 1</li>
</h5>
    <hr>

   <h6> Nearby Areas</h6>
   <h5>
   <li>Manipal Hospital- 2min</li>
   <li>Indiranagar- 5min</li>
   <li>Koramangla- 15min</li>
 </h5>
<br>
     <!-- <select class="btn btn" style="width:100%; ">
    <option value="volvo">HOW TO GET HERE</option>
    <option value="saab">2</option>
    <option value="mercedes">3</option>
    <option value="audi">4</option>
</select>
<br><br>
 <select class="btn btn" style="width:100%;">
    <option value="volvo">CHECK RATES</option>
    <option value="saab">2</option>
    <option value="mercedes">3</option>
    <option value="audi">4</option>
</select> -->
</div>


 <div class="col-sm-6 aboutusContent " style="background-color:white; width: 35%; height:100%; margin-right: 12px;">
 <h4 style="color:black;">ABOUT US</h4>
 <div class="text-justify">
   <h5>
 <p>This apartment is fitted with all the modern facilities and sleeps up to two persons in either king or
queen size beds. The apartment has a separate bedroom, kitchenette, dining area and a balcony.
The fully equipped modern kitchen includes a microwave oven, full size refrigerator, toaster and
more. Perfect if you&#39;re travelling solo or as a couple.</p></h5>
  </div>
  </div>



  <div class="col-sm-3" style="background-color:white; margin-right: 12px; padding-left: 75px;">
  <h4 style="color:black;">Facilities</h4>
  <h6>Fully Equipped Kitchen</h6>
<h5>
    <li>Electric Kettle</li>
    <li>Microwave</li>
    <li> Refrigerator</li>
    <li> Tea/Coffee Maker</li>
    <li>Toaster</li>
    <li> Serveware</li>

</h5>
<hr>

  <h6>Suite Comforts</h6>
<h5>
<li>  LED Television</li>
<li> Iron</li>
<li>Ironing Board</li>
<li> AC in Bedroom</li>
<li>In-room Safe</li>
</h5>

    </div>



  <div class="col-sm-3" style="background-color:white;">


    </div>
  </div>

</div>
<br><br>
<br><br>

<div class="row marg_top_100 apartmentCaousel" style="">
  <div class="col-md-10 col-md-offset-1">
    <div id="imageCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="item active">
              <div class="row">
                 <div class="col-sm-3">
                    <center><img src="assets/images/apartmentcoursal/Master_Bedroom_Pent_house.jpg" class="img-responsive img1" /></center><br>
                    <h4 class="text-center">Bedroom</h4>
                 </div>
                 <div class="col-sm-3">
                    <center><img src="assets/images/apartmentcoursal/dining.jpg" class="img-responsive img1" /></center><br>
                    <h4 class="text-center">dining</h4>
                 </div>
                 <div class="col-sm-3">
                    <center><img src="assets/images/apartmentcoursal/gym.jpg" class="img-responsive img1" /></center><br>
                    <h4 class="text-center">Gym</h4>
                 </div>

                  <div class="col-sm-3">
                    <center><img src="assets/images/apartmentcoursal/Golf_Course3.jpg" class="img-responsive img1" /></center><br>
                    <h4 class="text-center">Golf</h4>
                 </div>

              </div>
          </div>
          <div class="item">
              <div class="row">
                <div class="col-sm-3">
                <center><img src="assets/images/apartmentcoursal/Pool_View.jpg" class="img-responsive img1" /></center><br>
                    <h4 class="text-center">Swimming Pool</h4>
                 </div>
                 <div class="col-sm-3">
                    <center><img src="assets/images/apartmentcoursal/Upper_Lobby_Penthouse.jpg" class="img-responsive img1" /></center><br>
                    <h4 class="text-center">Room Lobby</h4>
                 </div>
                 <div class="col-sm-3">
                 <center><img src="assets/images/apartmentcoursal/Atrium.jpg" class="img-responsive img1" /></center><br>
                    <h4 class="text-center">Atrium</h4>
                 </div>
                 <div class="col-sm-3">
                 <center><img src="assets/images/apartmentcoursal/Reception.jpg" class="img-responsive img1" /></center><br>
                    <h4 class="text-center">Reception</h4>
                 </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
      </div><br><br>


      </div> <!-- end m-t-30 -->

  </div> <!-- end col -->
<br><br>

    </div>
  </div>
  <!-- end property-detail-wrapper -->

  <script>
      $(document).ready(function () {
          $('.property-slider').bxSlider({
              pagerCustom: '#bx-pager'
          });
      });
      var map = new GMaps({
          el: '#map-property',
          lat: 40.712784,
          lng: -74.005941,
          mapTypeControlOptions: {
              mapTypeIds : ["hybrid", "roadmap", "satellite", "terrain", "osm"]
          }
      });
      map.addMarker({
          lat:  40.725015 ,
          lng: -73.881452,
          title: 'Im your custom marker',
          icon: 'assets/images/map-marker.png'
      });
  </script>
