<br /><br /><br /><br /><br /><br />
<div id="foot" ng-controller="NavigationController">
  <div id="menuLoaderDiv">
    <div ng-controller="HeaderController">
      <footer id="footer" class="fh5co-bg-color">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="section-title text-center">
                <h1>REACH US</h1>
                <center><hr style="width: 50px; height: 2px; background-color: #fff; margin-top: -10px;"></center>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12"><center>
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.212926991894!2d77.6421778143751!3d12.958222590864684!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae123840cb9b3d%3A0xa7354ed5f00a568d!2sIBC+Hotels+%26+Resorts!5e0!3m2!1sen!2sin!4v1533213551656" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></center>
            </div>
          </div>
        </div>
        <br><br>
        <div class="container-fluid">
          <div class="container">
            <div class="row">
              <div class="col-md-2 col-md-offset-1" style="background-color:#000;">
                <h3 style="color: #fff; font-weight: lighter; font-size: 20px;">Company</h3>
                <ul class="link">
                  <li><a href="#">Home</a></li>
                  <li><a href="#/aboutus">About Us</a></li>
                  <li><a href="#">Hotels</a></li>
                  <li><a href="#">Customer Care</a></li>
                  <li><a href="#">Contact Us</a></li>
                </ul>
              </div>
              <div class="col-md-2"  style="background-color:#000;">
                <h3 style="color: #fff; font-weight: lighter; font-size: 20px;">Useful Links</h3>
                <ul class="link">
                  <li><a href="#/TermsAndConditions">Terms & Conditions</a></li>
                  <li><a href="#/privacypolicy">Privacy Policy</a></li>
                  <li><a href="#">FAQs</a></li>
                  <li><a href="#">Dining Program</a></li>
                  <li><a href="#">IBC for Buisness</a></li>

                </ul>
              </div>
              <div class="col-md-3"  style="background-color:#000;">
                <ul class="social-icons" style="list-style-type:none;">

                  <li><i class="glyphicon glyphicon-map-marker" style="color: #fff;"></i>
                    &nbsp Diamond District Outer Podium,<br>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    Block- G,HAL Old Airport Rd,&nbsp&nbsp&nbsp&nbsp<br>
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ISRO Colony,Domlur,Bengaluru,<br>
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Karnataka 560008</li>

                    <li><i class="glyphicon glyphicon-envelope" style="color: #fff;"></i>&nbsp&nbsp info@ibchotels.co.in</li>
                    <li><i class="glyphicon glyphicon-earphone" style="color: #fff;"></i>&nbsp&nbsp 08041670733</li><br>
                  </div>
                  <div class="col-md-3"  style="background-color:#000;">
                    <div class="copyright">
                      <p style="color: #fff; font-weight: bold;"><small><span style="font-size: 15px; color: #fff;">© 2018 IBC Hotels & Resorts</span> All Rights Reserved.</small></p>
                      <ul class="social-icons" style="list-style-type:none;">
                        <li>
                          <a href="#"><i class="icon-twitter-with-circle"></i></a>
                          <a href="#"><i class="icon-facebook-with-circle"></i></a>
                          <a href="#"><i class="icon-instagram-with-circle"></i></a>
                          <a href="#"><i class="icon-linkedin-with-circle"></i></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="row"  style="background-color:#000;">
                  <div class="col-md-3 col-sm-offset-9">
                    <span style="color: #fff;">Designed & Developed by</span><a href="http://pyplyn.co" target="_blank" style="font-size: 12px; color: #fff; font-family: Times New Roman;" class="text-right">&nbsp&nbsp<span>Pyplyn</span></a>
                  </div>
                </div>
              </div>
            </div>
          </footer>
        </div>
      </div>
    </div>
