<div class="content-page">
  <div class="content">
    <div class="container" style="margin-top: 100px;">
      <div class="row">
        <div class="col-md-12">
          <div class="portlet">
              <div class="portlet-heading card-box-green">
                  <h3 class="portlet-title">
                      Room Types
                  </h3>
                  <a data-toggle="modal" data-target="#add_Roomtype" class="btn w-md btn-rounded btn-custom waves-effect waves-light pull-right"><i class="fa fa-plus-circle fa-lg"></i> &nbsp;ADD ROOMTYPE</a>
                  <div class="clearfix"></div>
              </div>
              <div id="bg-primary" class="panel-collapse collapse in">
                  <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row app-green-text">
                              <div class="col-md-2">PageSize:
                                <select ng-model="entryLimit" class="form-control">
                                  <option>5</option>
                                  <option>10</option>
                                  <option>20</option>
                                  <option>50</option>
                                  <option>100</option>
                                </select>
                              </div>
                              <div class="col-md-3">Filter:
                                <input type="text" ng-model="search" ng-change="filter()" placeholder="Search" class="form-control" />
                              </div>
                            </div>
                            <br/>
                            <div class="panel panel-default price-panel">
                                <div class="table-responsive">
                                  <table class="table table-bordered">
                                    <thead class="thead-light">
                                      <tr class="pointer">
                                        <th><span ng-click="sort_by('id');">ID</span></th>
                                        <th><span ng-click="sort_by('Room_name');">Room Name</span></th>
                                        <th><span ng-click="sort_by('No._of_guest');">No. of guest</span></th>
                                        <th><span ng-click="sort_by('purchase_price')">Purchase Price</span></th>
                                        <th><span ng-click="sort_by('sale_price')">Sale Price</span></th>
                                        <th><span ng-click="sort_by('tax_price')">Tax Price</span></th>
                                        <th><span ng-click="sort_by('Description');">Description</span></th>
                                        <th></th>
                                        <th></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr ng-repeat="room in filtered = (allRoomTypes | filter:search | orderBy : predicate :reverse) | startFrom:(currentPage-1)*entryLimit | limitTo:entryLimit">
                                        <td>{{$index+1}}</td>
                                        <td><strong>{{room.room_name}}</strong></td>
                                        <td>{{room.no_of_pax}}</td>
                                        <td>{{room.purchase_price}}</td>
                                        <td>{{room.sale_price}}</td>
                                        <td>{{room.tax_price}}</td>
                                        <td>{{room.description}}</td>
                                        <td>
                                          <a class="app-green-text" data-toggle="modal" data-target="#update_Roomtype" ng-click="getRoomTypeById(room.id)" style="font-size: 25px;"><i class="fa fa-pencil"></i></a>
                                        </td>
                                        <td>
                                          <a class="" data-toggle="modal" data-target="#delete_roomtype" style="font-size: 25px; color: red;" ng-click="storeRoomTypeId(room.id)"><i class="fa fa-trash"></i></a>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                                <div class="col-md-12" ng-show="filteredItems == 0">
                                    <div class="col-md-12">
                                        <h4>Not  found</h4>
                                    </div>
                                </div>
                                <div class="col-md-12" ng-show="filteredItems > 0">
                                    <div pagination="" page="currentPage" on-select-page="setPage(page)" boundary-links="true" total-items="filteredItems" items-per-page="entryLimit" class="pagination-small" previous-text="&laquo;" next-text="&raquo;">
                                    </div>
                                </div>
                            </div> <!-- End of Panel -->
                        </div>
                    </div>  <!-- End row -->
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    <div ng-include="'tmp/footer.php'"></div>
  </div>
</div>
<div class="modal fade" id="add_Roomtype" data-backdrop="static">
  <div class="modal-dialog">
    <div class="col-sm-12">
      <div class="">
        <div class="m-t-30 account-pages">
          <div class="text-center account-logo-box header-bar">
            <h3 class="text-uppercase">
              <span>Add Room Type</span>
              <button type="button" class="btn btn-default pull-right" data-dismiss="modal">x</button>
            </h3>
          </div>
          <div class="account-content">
            <form class="form-horizontal " name="addRoomTypeForm" method="post">
              <span class="message" class="col-sm-12"></span>
              <div class="col-lg-6 col-md-6 nopadding">
                <div class="form-group ">
                  <div class="col-xs-12">
                    <input class="form-control" type="text" placeholder="Room Name" name="name" ng-model="room.name" required>
                    <div ng-show="addRoomTypeForm.name.$touched">
                      <p class="error" ng-show="addRoomTypeForm.name.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-1">
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group ">
                  <div class="col-xs-12">
                    <input class="form-control" type="text" placeholder="No. of Pax" name="no_of_pax" ng-model="room.no_of_pax" required>
                    <div ng-show="addRoomTypeForm.no_of_pax.$touched">
                      <p class="error" ng-show="addRoomTypeForm.no_of_pax.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-6 col-md-6 nopadding">
                <div class="form-group ">
                  <div class="col-xs-12">
                    <input class="form-control" type="text" placeholder="Purchase Price" name="purchase_price" ng-model="room.purchase_price" required>
                    <div ng-show="addRoomTypeForm.purchase_price.$touched">
                      <p class="error" ng-show="addRoomTypeForm.purchase_price.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-1">
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group ">
                  <div class="col-xs-12">
                    <input class="form-control" type="text" placeholder="Sale Price" name="sale_price" ng-model="room.sale_price" required>
                    <div ng-show="addRoomTypeForm.sale_price.$touched">
                      <p class="error" ng-show="addRoomTypeForm.sale_price.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group ">
                <div class="col-xs-12">
                  <input class="form-control" type="text" required="" placeholder="Tax Price" name="tax_price" ng-model="room.tax_price">
                  <div ng-show="addRoomTypeForm.tax_price.$touched">
                    <p class="error" ng-show="addRoomTypeForm.tax_price.$error.required">This field is required.</p>
                  </div>
                </div>
              </div>
              <div class="form-group ">
                <div class="col-xs-12">
                  <input class="form-control" type="text" required="" placeholder="Description" name="description" ng-model="room.description">
                  <div ng-show="addRoomTypeForm.description.$touched">
                    <p class="error" ng-show="addRoomTypeForm.description.$error.required">This field is required.</p>
                  </div>
                </div>
              </div>
              <div class="form-group text-center m-t-10">
                <div class="col-xs-12">
                  <br />
                  <button class="btn w-md btn-bordered btn-primary waves-effect waves-light pull-right" type="submit"  ng-click="addRoomType(room);">Add</button>
                </div>
              </div>
            </form>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <!-- end wrapper -->
    </div>
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="update_Roomtype" data-backdrop="static">
  <div class="modal-dialog">
    <div class="col-sm-12">
      <div class="">
        <div class="m-t-30 account-pages">
          <div class="text-center account-logo-box header-bar">
            <h3 class="text-uppercase">
              <span>Edit Room Type</span>
              <button type="button" class="btn btn-default pull-right" data-dismiss="modal">x</button>
            </h3>
          </div>
          <div class="account-content">
            <form class="form-horizontal " name="addRoomTypeForm" method="post">
              <span class="message" class="col-sm-12"></span>
              <div class="col-lg-6 col-md-6 nopadding">
                <div class="form-group ">
                  <div class="col-xs-12">
                    <input type="hidden" name="id" ng-model="rm.id">
                    <label>Room</label>
                    <input class="form-control" type="text" placeholder="Room Name" name="room_name" ng-model="rm.room_name" required>
                    <div ng-show="addRoomTypeForm.room_name.$touched">
                      <p class="error" ng-show="addRoomTypeForm.room_name.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-1">
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group ">
                  <div class="col-xs-12">
                    <label>No. of Pax</label>
                    <input class="form-control" type="text" placeholder="No. of Pax" name="no_of_pax" ng-model="rm.no_of_pax" required>
                    <div ng-show="addRoomTypeForm.no_of_pax.$touched">
                      <p class="error" ng-show="addRoomTypeForm.no_of_pax.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-6 col-md-6 nopadding">
                <div class="form-group ">
                  <div class="col-xs-12">
                    <label>Purchase Price</label>
                    <input class="form-control" type="text" placeholder="Purchase Price" name="purchase_price" ng-model="rm.purchase_price" required>
                    <div ng-show="addRoomTypeForm.purchase_price.$touched">
                      <p class="error" ng-show="addRoomTypeForm.purchase_price.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-1">
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group ">
                  <div class="col-xs-12">
                    <label>Sale Price</label>
                    <input class="form-control" type="text" placeholder="Sale Price" name="sale_price" ng-model="rm.sale_price" required>
                    <div ng-show="addRoomTypeForm.sale_price.$touched">
                      <p class="error" ng-show="addRoomTypeForm.sale_price.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group ">
                <div class="col-xs-12">
                  <input class="form-control" type="text" required="" placeholder="Tax Price" name="tax_price" ng-model="rm.tax_price">
                  <div ng-show="addRoomTypeForm.tax_price.$touched">
                    <p class="error" ng-show="addRoomTypeForm.tax_price.$error.required">This field is required.</p>
                  </div>
                </div>
              </div>
              <div class="form-group ">
                <div class="col-xs-12">
                  <label>Description</label>
                  <input class="form-control" type="text" required="" placeholder="Description" name="description" ng-model="rm.description">
                  <div ng-show="addRoomTypeForm.description.$touched">
                    <p class="error" ng-show="addRoomTypeForm.description.$error.required">This field is required.</p>
                  </div>
                </div>
              </div>
              <div class="form-group text-center m-t-10">
                <div class="col-xs-12">
                  <br />
                  <button class="btn w-md btn-bordered btn-primary waves-effect waves-light pull-right" type="submit"  ng-click="updateRoomType(rm);">Update</button>
                </div>
              </div>
            </form>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <!-- end wrapper -->
    </div>
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div id="delete_roomtype" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <p class="text-center" style="font-size:80px;"><i class="fa fa-exclamation-triangle"></i></p>
          <div class="row text-center">
              <div class="col-md-12">
                  <span class="col-sm-12 success text-center"></span>
                  <span class="col-sm-12 error text-center"></span>
                  <h3 class="color-black">Do you want to delete this Room Type?</h3>
                  <!-- <p class="text-size">This will delete all the properties of this hotel!</p> -->
              </div>
          </div>
          <div class="modal-footer">
            <div class="col-md-10 col-md-offset-2" style="top: 10px;">
              <button type="button" class="btn w-md btn-bordered btn-primary waves-effect waves-light" ng-click="deleteRoomType(selectedRoomTypeId)"></i> &nbsp;&nbsp;OK</button>
              <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
            </div>
          </div>
      </div>
    </div>
  </div>
</div><!-- /.modal -->
