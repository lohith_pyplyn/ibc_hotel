  <section id="main-slider" class="no-margin text-center">
    <div class="carousel slide">
        <div class="carousel-inner">
            <div class="item active col-sm-12" style=" opacity: 1.5; background-repeat: no-repeat; background-size: cover; height:500px; background-image: url(assets/images/banner-2-bedroom-superior.jpg)">
                <div class="container">
                    <div class="row slide-margin">
                        <div class="col-sm-12">
                            <div class="carousel-content" style= "width: fit-content; padding: 0% 2%;   margin-left: auto; margin-right: auto;margin-top: 15%;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<br />
<div class="container">
  <div class="col-md-5">
    <div class="row">
      <div class="portlet">
          <div class="portlet-heading bg-success">
              <h3 class="portlet-title">
                  Stay Information
              </h3>
              <div class="clearfix"></div>
          </div>
          <div id="bg-success" class="portlet-body">
              <div class="row"><br />
                <div class="col-md-12">
                  <div class="col-md-5 col-md-offset-1">
                      <label for="field-1" class="control-label"> Check-In : {{ check_in | date }}</label>
                  </div>
                  <div class="col-md-5">
                    <label for="field-1" class="control-label"> Check-Out : {{ check_out | date }}</label>
                  </div>
                </div>
              </div><br/><br/>
              <div class="property-content">
                <div ng-repeat="roomtotal in stay_details">
                  <div class="portlet" ng-if="roomtotal.is_room_added == true">
                    <div class="portlet-heading bg-success">
                      <h3 class="portlet-title">
                          {{roomtotal.room_type}}
                      </h3>
                      <div class="clearfix"></div>
                    </div>
                    <div id="bg-success" class="panel-collapse collapse in">
                      <div class="row text-center">
                          <div class="col-xs-4">
                              <p class="text-overflow" title="Square Feet">No. of Rooms</p>
                              <h4 ng-if="roomtotal.selected_no_of_rooms != 0">{{roomtotal.selected_no_of_rooms}}</h4>
                          </div>
                          <div class="col-xs-4">
                              <p class="text-overflow" title="Bedrooms">Adults</p>
                              <h4 ng-if="roomtotal.selected_adults != 0">{{roomtotal.selected_adults}}</h4>
                          </div>
                          <div class="col-xs-4" ng-show="roomtotal.selected_children">
                              <p class="text-overflow" title="Bedrooms">Children</p>
                              <h4>{{roomtotal.selected_children}}</h4>
                          </div>
                          <div class="col-xs-4" ng-show="roomtotal.selected_extra_beds">
                              <p class="text-overflow" title="Bedrooms">Extra Beds</p>
                              <h4>{{roomtotal.selected_extra_beds}}</h4>
                          </div>
                          <div class="col-xs-4">
                              <p class="text-overflow" title="Parking Space">Price</p>
                              <h4>&#8377; {{roomtotal.selected_price}}/-</h4>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-box text-center">
                  <h4>Total Price : &#8377; {{grand_total}}/-</h4>
              </div>
          </div>
      </div>
    </div>
  </div>
  <div class="col-md-6 col-md-offset-1">
    <div class="row">
      <div class="portlet">
          <div class="portlet-heading bg-success">
              <h3 class="portlet-title">
                  Guest Information
              </h3>
              <div class="clearfix"></div>
          </div>
          <div id="bg-success" class="portlet-body">
            <div class="row"><br />
              <div class="col-md-12">
                <form class="form-horizontal" name="userform">
                  <div class="form-group">
                    <div class="col-md-5 col-md-offset-1">
                        <label for="field-1" class="control-label"> First Name</label>
                        <input type="text" name="name" ng-model="customer.name" class="form-control" required>
                    </div>
                    <div class="col-md-5">
                      <label for="field-1" class="control-label"> Last Name</label>
                      <input type="text" name="lastname" ng-model="customer.lastname" class="form-control" required>
                    </div>
                  </div>
                  <br/><br/>
                  <div class="form-group">
                    <div class="col-md-4 col-md-offset-4">
                      <button type="button" class="btn btn-lg btn-success btn-rounded waves-effect w-md waves-light text-uppercase pull-right"  ng-click="fianlbooking(data)"><i class=""></i><strong> Confirm Booking </strong></button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
  
</div>

<script>
  $(function() {
    $(window).on("scroll", function() {
        if($(window).scrollTop() >=100) {
            $(".headerIBC").addClass("scroDown");
        } else {
           $(".headerIBC").removeClass("scroDown");
        }
    });
});
  </script>
