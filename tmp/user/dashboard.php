<div class="content-page">
  <div class="content"  style="margin-top: -38px;">
      <div class="container">
          <div class="row" style="margin-top: 72px;">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Dashboard</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="#/dashboard">IBC</a>
                        </li>

                        <li class="active">
                            <a>Dashboard</a>
                        </li>

                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
          </div> <!-- end of row -->
          <div class="row">
            <div class="col-sm-12">
              <div class="row">
                <div class="card-box card-box-bg">
                  </h3>
                  <div class="row">
                    <div class="col-md-12">
                      <br/>
                      <div class="panel panel-default price-panel">
                        <div class="table-responsive">
                          <table class="table table-bordered text-center">
                            <thead class="thead-light">
                              <tr class="pointer">
                              <th><span ng-click="sort_by('property_type');">reservation no</span></th>
                                  <th><span ng-click="sort_by('city');">check in</span></th>
                                  <th><span ng-click="sort_by('state');">check out</span></th>
                                  <th><span ng-click="sort_by('country');">no of adults</span></th>
                                  <th><span ng-click="sort_by('status');">no of children</span></th>
                                  <th><span ng-click="sort_by('state');">extra bed </span></th>
                                  <th><span ng-click="sort_by('status');">room type</span></th>
			                         	  <th><span ng-click="sort_by('city');">no of rooms</span></th>
                                  <th><span ng-click="sort_by('state');">price</span></th>
                                  <th><span ng-click="sort_by('country');">status</span></th>
                              </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="data in filtered = (booking_details | filter:search1 | orderBy : predicate1 :reverse1) | startFrom:(currentPage1-1)*entryLimit1 | limitTo:entryLimit1">
                                <td align="center">{{data.resrervation_no}}</td>
                                <td align="center">{{data.check_in | date}}</td>
                                <td align="center" >{{data.check_out | date}}</td>
                                <td align="center" >{{data.no_of_adults}}</td>
                                <td align="center" >{{data.no_of_children}}</td>
                                <td align="center" >{{data.extra_beds}}</td>
                                <td align="center" >{{data.room_type_name}}</td>
                                <td align="center" >{{data.no_of_rooms}}</td>
                                <td align="center" >{{data.price}}</td>
                                <td align="center" >{{data.status}}</td>
                                </tr>
                            </tbody>
                          </table>
                        </div>
                        <div class="col-md-12" ng-show="filteredItems1 == 0">
                            <div class="col-md-12">
                                <h4>Not  found</h4>
                            </div>
                        </div>
                        <div class="col-md-12" ng-show="filteredItems1 > 0">
                            <div pagination="" page="currentPage1" on-select-page="setPage1(page)" boundary-links="true" total-items="filteredItems1" items-per-page="entryLimit1" class="pagination-small" previous-text="&laquo;" next-text="&raquo;">
                            </div>
                        </div>
                      </div> <!-- End of Panel -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
            <div ng-include="'tmp/footer.php'"></div>

        <div class="modal fade" id="userbooking-modal" data-backdrop="static" role="dialog">
          <div class="modal-dialog modal-lg">
            <div class="col-sm-12">
              <div class="m-t-30 account-pages">
                <div class="text-center account-logo-box header-bar">
                    <h3 class="text-uppercase">
                        <span>Booking Form</span>
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">x</button>
                    </h3>
                    <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
                </div>
                <div class="account-content">
                  <form class="form-horizontal" name="userbooking_form" enctype="multipart/form-data" method="post">
                    <div class="row">
                      <span class="message col-sm-10 col-sm-offset-1"></span>
                      <div class="col-md-12">
                        <div class="col-md-5 col-md-offset-1">
                          <div class="form-group">
                            <label for="start_from">From</label>
                            <input type="text" class="form-control" name="start_from" pickadate ng-model="userbooking.start_from" required >
                            <div ng-show="userbooking_form.start_from.$touched">
                              <p class="error" ng-show="userbooking_form.start_from.$error.required">This field is required.</p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="end">To</label>
                            <input type="text" class="form-control" name="end" pickadate ng-model="userbooking.end" required >
                            <div ng-show="userbooking_form.end.$touched">
                              <p class="error" ng-show="userbooking_form.end.$error.required">This field is required.</p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="roomtype">Room Type</span></label>
                            <select class="form-control" name="bank" ng-model="userbooking.roomtype">
                              <option value="" disabled>Room Type</option>
                              <option ng-repeat="data in room_types" value="{{data.id}}">{{data.room_type}}</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="noofrooms">No. of Rooms</span></label>
                            <input type="text" class="form-control" name="noofrooms" ng-model="userbooking.noofrooms" required>
                            <div ng-show="userbooking_form.noofrooms.$touched">
                              <p class="error" ng-show="userbooking_form.noofrooms.$error.required">This field is required.</p>
                            </div>
                          </div>
                        </div> <!-- End of col-md-5 -->
                        <div class="col-md-5 col-md-offset-1">
                          <div class="form-group">
                            <label for="noofadults">No. of Adults</span></label>
                            <input type="text" id="noofadults" class="form-control" name="noofadults" ng-model="userbooking.noofadults" required>
                            <div ng-show="userbooking_form.noofadults.$touched">
                              <p class="error" ng-show="userbooking_form.noofadults.$error.required">This field is required.</p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="children">No. of children</span></label>
                            <input type="text" class="form-control" name="children" ng-model="userbooking.children" required>
                            <div ng-show="userbooking_form.children.$touched">
                              <p class="error" ng-show="userbooking_form.children.$error.required">This field is required.</p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="promocode">Promo Code</span></label>
                            <input type="text" class="form-control" name="promocode" ng-model="userbooking.promocode" required>
                            <div ng-show="userbooking_form.promocode.$touched">
                              <p class="error" ng-show="userbooking_form.promocode.$error.required">This field is required.</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <br>
                    <br><br>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">
                        Cancel
                      </button>
                      <button type="submit" class="btn btn-primary waves-effect waves-light" ng-click="userbooking(userbooking)">
                        Submit
                      </button>
                    </div>
                  </form>
                  <div class="clearfix"></div>
                </div>
            </div>
            </div>
          </div>
        </div>
