<section id="main-slider" class="no-margin text-center">
        <div class="carousel slide">
            <div class="carousel-inner">
                <div class="item active col-sm-12" style=" opacity: 1.5; background-repeat: no-repeat; background-size: cover; height:500px; background-image: url(assets/images/banner-2-bedroom-superior.jpg)">
                    <div class="container">
                        <div class="row slide-margin">
                            <div class="col-sm-12">
                                <div class="carousel-content" style= "width: fit-content; padding: 0% 2%;   margin-left: auto; margin-right: auto;margin-top: 15%;">

                                    <!-- <div class="centered">Centered</div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<div class="container">

<div class="row marg_top_100">
        <div class="col-md-10 col-md-offset-1">
<div class="text-justify">
<h4>Privacy Policy</h4>

<p>The IBC Hotels &amp; Resorts respects your privacy and is committed towards protecting the security and
confidentiality of any personal information that you provide to us or that we collect about you when
you visit our website www.ibchotels.co.in or when you contact guest services or when you
otherwise communicate with us.</p>
<p>This privacy policy statement outlines the type of personal information we collect and the policies
and procedure, we have established regarding its use and storage and serves as a guideline to be
observed by all our properties, affiliates, joint ventures and group companies.
You will be asked to agree to the terms of this Privacy policy while making a reservation, joining our
rewards programme or corresponding with us via the site or otherwise wherever required under
applicable law. Or your continued use of the site will be considered as your consent to the terms of
this Privacy Policy.</p>

<h4>Personal Information We Collect About You</h4>

<p>Personal data or information related to an individual through which that person can be identified,
such as First name/Last name, Gender, Date of Birth.</p>
<p>Personal contact details such as Phone Number, Address, Email Address.</p>
<p>We may also collect other information such as guest preferences and usage, when such information
is supplied or collected to or IBC Hotels &amp; Resorts in the course of transacting business with an
individual.</p>
<p>The Privacy Policy does not apply to information regarding IBC Hotels &amp; Resorts corporate
customers. However, such information is protected by other IBC Hotels &amp; Resorts policies and
practices and through contractual arrangements.</p>

<h4>How You May Provide Us Your Personal Information</h4>

<p>We gather your personal information while monitoring our technology tools and services, including
feedback forms and email communications sent to and from IBC Hotels &amp; Resorts.
Alternatively, you provide us with information when you interact with us, for example:
-By creating a profile or signing in to access our website;
-By making a reservation through our website;
-During your stay at our hotel, including information provided during check-in.
-We also receive information about you from other sources, such as our partners and information
available in the public domain.
-By visiting our website, we may also gather information from you through the use of cookies.
-We collate information that we have about you from various sources, including the information that
you have provided to us.</p>

<h4>How We May Use Your Personal Information</h4>

<p>We may use your personal information for the following specified purposes
- To determine your identity through the account you hold with us
- To provide excellent customer service to you
- To facilitate the use our website and services
- To help you make a reservation and payment through our website
- To assist you in inquiry related to your transaction, booking etc.
- For billing purposes during your stay at our property
- To inform you about matters related to your stay
- To conduct surveys or ask for a feedback about our property, our service and your stay with us
- To send you newsletters regarding our services, promotional campaigns, new offers that may be of
your interest
- To respond to your queries about our properties, special requests or any other correspondence
regarding your stay
- To analyse consumer behaviour and trends
- For internal purposes such as audit, data analysis, statistical &amp; research purposes and other
processes to improve the quality of our services.</p>


<h4>Why Do We Collect Your Personal Information</h4>
<p>- To comply with the legal and regulatory obligations, including financial reporting requirements
imposed by the government and regulatory authorities
- For legitimate business purposes.</p>


<h4>Principles</h4>

<p>-IBC Hotels &amp; Resorts will not obtain, use or disclose your personal information for any other
purpose than those identified above, except with your consent.</p>
<p>-IBC Hotels &amp; Resorts is committed to protect your personal information with necessary security
safeguards.</p>
<p>-IBC Hotels &amp; Resorts will take appropriate steps to protect the confidentiality of your personal
information when dealing with third parties.</p>
<p>-IBC Hotels &amp; Resorts will strive towards keeping your personal information as accurate and up to
date as is required for the purposes described above.</p>
<p>-IBC Hotels &amp; Resorts will respect your request to access your personal information in as promptly as
is fairly possible.<p>
<p>You can always choose to decline to furnish any of your personal details to us.</p>
<p>You can also choose to withdraw your approval with regards to the use of your personal details for
marketing purposes at any point of time, subject to legal or contractual limitations and reasonable
notice e-mailing us at marketing@ibchotels.co.in, using &#39;Unsubscribe&#39; as the subject line, and
providing us sufficient personal identifiers so we can act effectively on your request.</p>

<p>However, in either case, this might affect our ability to serve you.</p>
<p>If you have questions or concerns about our privacy policies or wish to make a request with respect
to your personal information please write to us at:</p>
<p>IBC Hotels &amp; Resorts Pvt. Ltd.</p>
<p>Block- G, Diamond District Outer Podium,
ISRO Colony, Old Airport Road,
Bengaluru-560008
Karnataka</p>
</div>