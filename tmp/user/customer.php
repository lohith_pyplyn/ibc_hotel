<div class="content-page">
  <div class="content">
    <div class="container" style="margin-top: 100px;">
      <div class="row">
        <div class="col-md-12">
          <div class="portlet">
              <div class="portlet-heading card-box-green">
                  <h3 class="portlet-title">
                    <i class="fa fa-group fa-lg"></i>  Customers
                  </h3>
                  <a data-toggle="modal" data-target="#add_customer" class="btn w-md btn-rounded btn-custom waves-effect waves-light pull-right"><i class="fa fa-user-plus"></i> &nbsp;ADD CUSTOMER</a>
                  <div class="clearfix"></div>
              </div>
              <div id="bg-primary" class="panel-collapse collapse in">
                  <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row app-green-text">
                              <div class="col-md-2">PageSize:
                                <select ng-model="entryLimit" class="form-control">
                                  <option>5</option>
                                  <option>10</option>
                                  <option>20</option>
                                  <option>50</option>
                                  <option>100</option>
                                </select>
                              </div>
                              <div class="col-md-3">Filter:
                                <input type="text" ng-model="search" ng-change="filter()" placeholder="Search" class="form-control" />
                              </div>
                            </div>
                            <br/>
                            <div class="panel panel-default price-panel">
                                <div class="table-responsive">
                                  <table class="table table-bordered">
                                    <thead class="thead-light">
                                      <tr class="pointer">
                                        <th><span ng-click="sort_by('id');">ID</span></th>
                                        <th><span ng-click="sort_by('Customer_name');">Customer Name</span></th>
                                        <th><span ng-click="sort_by('email');">Email</span></th>
                                        <th><span ng-click="sort_by('phone');">Phone</span></th>
                                        <th><span ng-click="sort_by('proof_id');">Proof Id</span></th>
                                        <th><span ng-click="sort_by('Credit_Limit');">Credit Limit</span></th>
                                        <th><span ng-click="sort_by('address');">Address</span></th>
                                        <th><span ng-click="sort_by('city');">City</span></th>
                                        <th><span ng-click="sort_by('city');">State</span></th>
                                        <th><span ng-click="sort_by('city');">Country</span></th>
                                        <th><span ng-click="sort_by('pin_code');">Zipcode</span></th>
                                        <th></th>
                                        <th></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr ng-repeat="cust in filtered = (allCustomers | filter:search | orderBy : predicate :reverse) | startFrom:(currentPage-1)*entryLimit | limitTo:entryLimit">
                                        <td>{{$index+1}}</td>
                                        <td><strong>{{cust.name}}</strong></td>
                                        <td>{{cust.email}}</td>
                                        <td>{{cust.phone}}</td>
                                        <td>{{cust.proof_id}}</td>
                                        <td>{{cust.credit_limit}}</td>
                                        <td>{{cust.address}}</td>
                                        <td>{{cust.city}}</td>
                                        <td>{{cust.state}}</td>
                                        <td>{{cust.country}}</td>
                                        <td>{{cust.zipcode}}</td>
                                        <td>
                                          <a class="app-green-text" data-toggle="modal" data-target="#update_customer" ng-click="getCustomerById(cust.id)" style="font-size: 25px;"><i class="fa fa-pencil"></i></a>
                                        </td>
                                        <td>
                                          <a class="" data-toggle="modal" data-target="#delete_customer" style="font-size: 25px; color: red;" ng-click="storeCustomerId(cust.id)"><i class="fa fa-trash"></i></a>
                                        </td>

                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                                <div class="col-md-12" ng-show="filteredItems == 0">
                                    <div class="col-md-12">
                                        <h4>Not  found</h4>
                                    </div>
                                </div>
                                <div class="col-md-12" ng-show="filteredItems > 0">
                                    <div pagination="" page="currentPage" on-select-page="setPage(page)" boundary-links="true" total-items="filteredItems" items-per-page="entryLimit" class="pagination-small" previous-text="&laquo;" next-text="&raquo;">
                                    </div>
                                </div>
                            </div> <!-- End of Panel -->
                        </div>
                    </div>  <!-- End row -->
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    <div ng-include="'tmp/footer.php'"></div>
  </div>
</div>
<div class="modal fade" id="add_customer" data-backdrop="static">
  <div class="modal-dialog">
    <div class="col-sm-12">
      <div class="">
        <div class="m-t-30 account-pages">
          <div class="text-center account-logo-box header-bar">
            <h3 class="text-uppercase">
              <span>add Customer</span>
              <button type="button" class="btn btn-default pull-right" data-dismiss="modal">x</button>
            </h3>
          </div>
          <div class="account-content">
            <form class="form-horizontal " name="addCustomerForm" method="post">
              <span class="message" class="col-sm-12"></span>
              <div class="col-lg-5 col-md-5">
                <div class="form-group ">
                  <div class="col-xs-12">
                    <div class="">
                      <select class="form-control" name="customer_type" ng-model="customer.customer_type">
                        <option value="" disabled>Select Customer Type</option>
                        <option value="1">Agent</option>
                        <option value="2">Overseas Agent</option>
                        <option value="3">Online Agent</option>
                        <option value="4">Travel Agent</option>
                        <option value="5">Walk inn</option>
                      </select>
                    </div>
                    <div ng-show="addCustomerForm.customer_type.$touched">
                      <p class="error" ng-show="addCustomerForm.customer_type.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
                <div class="form-group ">
                  <div class="col-xs-12">
                    <input class="form-control" type="text" placeholder= "Customer Name" name="name" ng-model="customer.name" required>
                    <div ng-show="addCustomerForm.C_name.$touched">
                      <p class="error" ng-show="addCustomerForm.C_name.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
                <div class="form-group ">
                  <div class="col-xs-12">
                    <input class="form-control" type="email" required="" placeholder="Email" name="email" ng-model="customer.email" ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/"/>
                    <div ng-show="addCustomerForm.email.$touched">
                      <p class="error" ng-show="addCustomerForm.email.$error.pattern">This is invalid email.</p>
                      <p class="error" ng-show="addCustomerForm.email.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
                <div class="form-group ">
                  <div class="col-xs-12">
                    <input class="form-control" type="text" required="" placeholder="Phone" name="phone" ng-model="customer.phone">
                    <div ng-show="addCustomerForm.phone.$touched">
                      <p class="error" ng-show="addCustomerForm.phone.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
                <div class="form-group ">
                  <div class="col-xs-12">
                    <input class="form-control" type="text" placeholder="Proof Id" name="proof_id" ng-model="customer.proof_id" required>
                    <div ng-show="addCustomerForm.proof_id.$touched">
                      <p class="error" ng-show="addCustomerForm.proof_id.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-1">
              </div>
              <div class="col-lg-5 col-md-5 pull-right">
                <div class="form-group ">
                  <div class="col-xs-12">
                    <input class="form-control" type="text" placeholder="Credit Limit" name="credit_limit" ng-model="customer.credit_limit" required>
                    <div ng-show="addCustomerForm.credit_limit.$touched">
                      <p class="error" ng-show="addCustomerForm.credit_limit.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
                <div class="form-group ">
                  <div class="col-xs-12">
                    <input class="form-control" type="text" required="" placeholder="Address" name="address" ng-model="customer.address">
                    <div ng-show="addCustomerForm.address.$touched">
                      <p class="error" ng-show="addCustomerForm.address.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
                <div class="form-group ">
                  <div class="col-xs-12">
                    <input class="form-control" type="text" required="" placeholder="City" name="city" ng-model="customer.city">
                    <div ng-show="addCustomerForm.city.$touched">
                      <p class="error" ng-show="addCustomerForm.city.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
                <div class="form-group ">
                  <div class="col-xs-12">
                    <input class="form-control" type="text" required="" placeholder="State" name="state" ng-model="customer.state">
                    <div ng-show="addCustomerForm.state.$touched">
                      <p class="error" ng-show="addCustomerForm.state.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
                <div class="form-group ">
                  <div class="col-xs-12">
                    <input class="form-control" type="text" required="" placeholder="Country" name="country" ng-model="customer.country">
                    <div ng-show="addCustomerForm.country.$touched">
                      <p class="error" ng-show="addCustomerForm.country.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
                <div class="form-group ">
                  <div class="col-xs-12">
                    <input class="form-control" type="text" required="" placeholder="Zipcode" name="zipcode" ng-model="customer.zipcode">
                    <div ng-show="addCustomerForm.zipcode.$touched">
                      <p class="error" ng-show="addCustomerForm.zipcode.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group text-center m-t-10">
                <div class="col-xs-12">
                  <br />
                  <button class="btn w-md btn-bordered btn-primary waves-effect waves-light pull-right" type="submit"  ng-click="addCustomer(customer);">Add</button>
                </div>
              </div>
            </form>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <!-- end wrapper -->
    </div>
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="update_customer" data-backdrop="static">
  <div class="modal-dialog">
    <div class="col-sm-12">
      <div class="">
        <div class="m-t-30 account-pages">
          <div class="text-center account-logo-box header-bar">
            <h3 class="text-uppercase">
              <span>Edit Customer</span>
              <button type="button" class="btn btn-default pull-right" data-dismiss="modal">x</button>
            </h3>
          </div>
          <div class="account-content">
            <form class="form-horizontal " name="addCustomerForm" method="post">
              <span class="message" class="col-sm-12"></span>
              <div class="col-lg-5 col-md-5">
                <div class="form-group ">
                  <div class="col-xs-12">
                    <div class="">
                      <label>Customer Type</label>
                      <select class="form-control" name="customer_type" ng-model="cstmr.customer_type">
                        <option value="" disabled>Select Customer Type</option>
                        <option value="1">Agent</option>
                        <option value="2">Overseas Agent</option>
                        <option value="3">Online Agent</option>
                        <option value="4">Travel Agent</option>
                        <option value="5">Walk inn</option>
                      </select>
                    </div>
                    <div ng-show="addCustomerForm.customer_type.$touched">
                      <p class="error" ng-show="addCustomerForm.customer_type.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
                <div class="form-group ">
                  <div class="col-xs-12">
                    <label>Name</label>
                    <input class="form-control" type="text" placeholder= "Customer Name" name="name" ng-model="cstmr.name" required>
                    <div ng-show="addCustomerForm.C_name.$touched">
                      <p class="error" ng-show="addCustomerForm.C_name.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
                <div class="form-group ">
                  <div class="col-xs-12">
                    <label>Email</label>
                    <input class="form-control" type="email" required="" placeholder="Email" name="email" ng-model="cstmr.email" ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/"/>
                    <div ng-show="addCustomerForm.email.$touched">
                      <p class="error" ng-show="addCustomerForm.email.$error.pattern">This is invalid email.</p>
                      <p class="error" ng-show="addCustomerForm.email.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
                <div class="form-group ">
                  <div class="col-xs-12">
                    <label>Phone</label>
                    <input class="form-control" type="text" required="" placeholder="Phone" name="phone" ng-model="cstmr.phone">
                    <div ng-show="addCustomerForm.phone.$touched">
                      <p class="error" ng-show="addCustomerForm.phone.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
                <div class="form-group ">
                  <div class="col-xs-12">
                    <label>Proof Id.</label>
                    <input class="form-control" type="text" placeholder="Proof Id" name="proof_id" ng-model="cstmr.proof_id" required>
                    <div ng-show="addCustomerForm.proof_id.$touched">
                      <p class="error" ng-show="addCustomerForm.proof_id.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-1">
              </div>
              <div class="col-lg-5 col-md-5 pull-right">
                <div class="form-group ">
                  <div class="col-xs-12">
                    <label>Credit Limit</label>
                    <input class="form-control" type="text" placeholder="Credit Limit" name="credit_limit" ng-model="cstmr.credit_limit" required>
                    <div ng-show="addCustomerForm.credit_limit.$touched">
                      <p class="error" ng-show="addCustomerForm.credit_limit.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
                <div class="form-group ">
                  <div class="col-xs-12">
                    <label>Address</label>
                    <input class="form-control" type="text" required="" placeholder="Address" name="address" ng-model="cstmr.address">
                    <div ng-show="addCustomerForm.address.$touched">
                      <p class="error" ng-show="addCustomerForm.address.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
                <div class="form-group ">
                  <div class="col-xs-12">
                    <label>City</label>
                    <input class="form-control" type="text" required="" placeholder="City" name="city" ng-model="cstmr.city">
                    <div ng-show="addCustomerForm.city.$touched">
                      <p class="error" ng-show="addCustomerForm.city.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
                <div class="form-group ">
                  <div class="col-xs-12">
                    <label>State</label>
                    <input class="form-control" type="text" required="" placeholder="State" name="state" ng-model="cstmr.state">
                    <div ng-show="addCustomerForm.state.$touched">
                      <p class="error" ng-show="addCustomerForm.state.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
                <div class="form-group ">
                  <div class="col-xs-12">
                    <label>Country</label>
                    <input class="form-control" type="text" required="" placeholder="Country" name="country" ng-model="cstmr.country">
                    <div ng-show="addCustomerForm.country.$touched">
                      <p class="error" ng-show="addCustomerForm.country.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
                <div class="form-group ">
                  <div class="col-xs-12">
                    <label>Zipcode</label>
                    <input class="form-control" type="text" required="" placeholder="Zipcode" name="zipcode" ng-model="cstmr.zipcode">
                    <div ng-show="addCustomerForm.zipcode.$touched">
                      <p class="error" ng-show="addCustomerForm.zipcode.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group text-center m-t-10">
                <div class="col-xs-12">
                  <br />
                  <button class="btn w-md btn-bordered btn-primary waves-effect waves-light pull-right" type="submit"  ng-click="updateCustomer(cstmr);">Update</button>
                </div>
              </div>
            </form>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <!-- end wrapper -->
    </div>
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div id="delete_customer" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <p class="text-center" style="font-size:80px;"><i class="fa fa-exclamation-triangle"></i></p>
          <div class="row text-center">
              <div class="col-md-12">
                  <span class="col-sm-12 success text-center"></span>
                  <span class="col-sm-12 error text-center"></span>
                  <h3 class="color-black">Do you want to delete this Customer?</h3>
                  <!-- <p class="text-size">This will delete all the properties of this hotel!</p> -->
              </div>
          </div>
          <div class="modal-footer">
            <div class="col-md-10 col-md-offset-2" style="top: 10px;">
              <button type="button" class="btn w-md btn-bordered btn-primary waves-effect waves-light" ng-click="deleteCustomer(selectedCustomerId)"></i> &nbsp;&nbsp;OK</button>
              <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
            </div>
          </div>
      </div>
    </div>
  </div>
</div><!-- /.modal -->
