<section id="login_bg">
    <div class="container-alt">
        <div class="row">
            <div class="col-sm-12">
                <div class="wrapper-page">
                    <div class="m-t-40 account-pages">
                        <div class="text-center account-logo-box" style="background:white;">
                            <h2 class="text-uppercase">
                              login
                            </h2>
                        </div>
                        <div class="account-content">
                            <form class="form-horizontal" name="loginUserForm">
                                <span id="success" class="col-sm-12 success"></span>
                                <span id="error" class="col-sm-12 error"></span>
                                <div class="form-group ">
                                    <div class="col-xs-12">
                                        <input class="form-control" type="text" required="" placeholder="Username or Email" name="username" ng-model="user.username">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <input class="form-control" type="password" name="password" ng-model="user.password" required="" placeholder="Password">
                                    </div>
                                </div>
                                <div class="form-group text-center m-t-30">
                                    <div class="col-sm-12">
                                        <a id="forgotPass" class="text-muted" data-toggle="modal" data-target="#forgotModal"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
                                    </div>
                                </div>
                                <div class="form-group text-center m-t-10">
                                    <div class="col-xs-12">
                                        <button class="btn w-md btn-bordered btn-primary" type="submit" ng-disabled="!loginUserForm.$valid" ng-click="login(user);">Log In</button>
                                    </div>
                                </div>

                            </form>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <!-- end card-box-->
                </div>
                <!-- end wrapper -->
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="forgotModal" data-backdrop="static">
    <div class="modal-dialog modal-md">
        <div class="col-sm-12">
            <div class="wrapper-page">
                <div class="m-t-30 account-pages">
                    <div class="text-center account-logo-box header-bar">
                        <h3 class="text-uppercase">
                            <span>Recover Password</span>
                            <button type="button" class="btn btn-default pull-right" data-dismiss="modal">x</button>
                        </h3>
                        <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
                    </div>
                    <div class="account-content">
                        <form name="forgotPassWordForm" method="post" class="form-horizontal" ng-controller="ForgotPasswordController">
                            <span id="message-forgot" class="col-sm-12"></span>
                            <div class="form-group ">
                                <div class="col-xs-12">
                                    <input type="text" class="form-control" id="user_email" ng-model="user.user_email" ng-unique2="user_master.user_email" class="form-control" name="user_email" placeholder="Email" required />
                                    <div ng-show="forgotPassWordForm.user_email.$touched">
                                      <p class="error" ng-show="forgotPassWordForm.user_email.$error.unique2">Email not exist !</p>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group text-center m-t-10">
                                <div class="col-xs-12">
                                    <button class="btn w-md btn-bordered btn-primary waves-effect waves-light" type="submit" ng-disabled="!forgotPassWordForm.$valid" ng-click="forgetPassword(user);">Submit</button>
                                </div>
                            </div>

                        </form>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end wrapper -->

        </div>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
