<section id="main-slider" class="no-margin text-center">
    <div class="carousel slide">
        <div class="carousel-inner">
            <div class="item active col-sm-12" style=" opacity: 1.5; background-repeat: no-repeat; background-size: cover; height:500px; background-image: url(assets/images/banner4.jpg)">
                <div class="container">
                    <div class="row slide-margin">
                        <div class="col-sm-12">
                            <div class="carousel-content" style= "width: fit-content; padding: 0% 2%;   margin-left: auto; margin-right: auto;margin-top: 15%;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    <div class="availability_form" style="width:100%; text-align:center;">
      <form class="form-inline" name="availabilityForm" method="post" enctype="multipart/form-data">
        <div class="form-group availibility_form_group">
          <!-- <label>Check In:</label> -->
          <div class="input-group">
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar fa-lg" style="color: #99e265"></span>
            </span>
            <input  type="text" name="check_in" ng-model="avlbty.check_in" class="form-control datetimepicker checkIn" name="check_in" placeholder="Check In Date">
          </div>
       </div>
        <div class="form-group availibility_form_group">
          <!-- <label>Check Out:</label> -->
          <div class="input-group  " >
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar fa-lg"  style="color: #99e265"></span>
            </span>
            <input type="text" name="check_out" ng-model="avlbty.check_out" class="form-control datetimepicker checkIn" name="check_out" placeholder="Check Out Date">
            </div>
         </div>
        <!-- <div class="form-group availibility_form_group">
         <label>Adult:</label>
          <select class="form-control checkIn" ng-model="avlbty.no_of_person_allowed" name="no_of_person_allowed">
            <option value="" disabled selected>Guests</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
          </select>
        </div> -->

        <!-- <label>Children:</label> -->
        <!-- <div class="form-group availibility_form_group">

          <select class="form-control" ng-model="availability.no_of_children_allowed" name="no_of_children_allowed">
            <option value="" disabled selected>Select Childeren</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
          </select>
        </div> -->

        <div class="form-group ">
          <button  type="button" class="btn btn-default availibility_btn" ng-click="availability(avlbty)" name="button">Search</button>
        </div>
      </form>
    </div>
<div class="everyday container">
		<h1 class="text-center" style="font-family: Calibri;">Special Offers</h1>
		<center><hr style="width: 50px; height: 3px; background-color: #179d67;"></center>
		<div class="row" style>

				<div class="col-md-4">
						<img src="assets/images/offers/Reception.jpg" class="img-responsive">
<div class="text-container">
						<h3 class="text-center">Stay 3-5 Nights – [5% OFF]</h3>
						<center><hr style="width: 50px; height: 3px; background-color: #179d67;"></center>
						<div class="text-justify">
                <p>Plan to drop by our property for 3-5 nights?</p>
                <p>We have a special offer for you!</p>
                <p>If you book 3-5 nights with us and prepay, you can save up to 5% if you book at least 10 days in
                advance.</p>
                </div>
						<center><a href="#/1-5" class="read-more">View Now</a></center>
						<center><hr style="width: 60px; height: 1px; margin-top: 0px; background-color: #179d67;"></center>
				</div>
			</div>
				<div class="col-md-4">
						<img src="assets/images/offers/Pool_View.jpg" class="img-responsive">
						<div class="text-container">
									<h3 class="text-center">STAY 7 – 10 NIGHTS – 10% OFF</h3>
									<center><hr style="width: 50px; height: 3px; background-color: #179d67;"></center>
									<p>If you’re coming to the Collingham Apartments for between 7 - 10 nights, book early and save up to 10% on your ...</p>
                  	<center><a href="#7-10" class="read-more">View Now</a></center>
									<center><hr style="width: 60px; height: 1px; margin-top: 0px; background-color: #179d67;"></center>
						</div>



				</div>
				<div class="col-md-4">
						<img src="assets/images/offers/Golf_Course3.jpg" class="img-responsive">
           <div class="text-container">
									<h3 class="text-center">STAY 11 – 28 NIGHTS – 15% OFF</h3>
									<center><hr style="width: 50px; height: 3px; background-color: #179d67;"></center>
									<p>If you’re coming to the Collingham Apartments for between 11 and 28 nights, book early and save up to 15% on your ...</p>
                  <center>	<a href="#11-30" class="read-more">View Now</a></center>
									<center><hr style="width: 60px; height: 1px; margin-top: 0px; background-color: #179d67;"></center>

					</div>
				</div>

		</div>

		</div>



<br>
<br><br>
<br><br>
<br><br>
<br>
<script>

  $.datetimepicker.setLocale('en');

  // var currentdate = new Date();
  // var datetime = "Now: " + currentdate.getDate() + "-"
  //             + (currentdate.getMonth()+1)  + "-"
  //             + currentdate.getFullYear() + "  "
  //             + currentdate.getHours() + ":"
  //             + currentdate.getMinutes() + ":"
  //             + currentdate.getSeconds();

  $('.datetimepicker').datetimepicker({
      timepicker: false,
      format:'Y-m-d',
      minDate: 0
  });
  // $('#check_in').datetimepicker({
  //   timepicker: false,
  //     minDate: 0
  // });
  // $('#check_in').blur(function(){
  //   var check_in = document.getElementById('check_in').value;
  // })

  // $('#check_out').datetimepicker({

  //   timepicker : false,
  //   minDate: check_in
  // })


</script>
