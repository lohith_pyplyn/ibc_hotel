<section id="main-slider" class="no-margin text-center">
        <div class="carousel slide">
            <div class="carousel-inner">
                <div class="item active col-sm-12" style=" opacity: 1.5; background-repeat: no-repeat; background-size: cover; height:500px; background-image: url(assets/images/adventure-backlit-community-207896.jpg)">
                    <div class="container">
                        <div class="row slide-margin">
                            <div class="col-sm-12">
                                <div class="carousel-content" style= "width: fit-content; padding: 0% 2%;   margin-left: auto; margin-right: auto;margin-top: 15%;">

                                    <!-- <div class="centered">Centered</div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</section>
	<br>
<h1 class="text-center text-uppercase" >About us</h1>

<div class="main">
		<div class="container aboutus">
			<!--<h1 class="text-center text-uppercase">About us</h1>-->
				<div class="row">
						<div class="col-md-8">
							<h1>Wellcome To Our Hotel</h1>
							<p class="text-justify">IBC Hotels &amp; Resorts is an apartment hotel situated inside Diamond District on Old Airport Road with
localities like Indiranagar, M.G. Road, Koramangala in the vicinity. It offers 61 well-appointed fully
furnished, spacious, modern apartments that come in studio, one, two, three bedroom and
penthouse configuration. Outfitted with a gourmet chef&#39;s kitchen/kitchenette, free WI-FI, 24*7
restaurant, gym, in-room laundry facilities, free parking, taxi service and more. Proximity to popular
restaurants, pubs, shopping complexes, hospitals and tech parks makes this a preferred choice for
leisure as well as business travellers. IBC Hotels &amp; Resorts is ideal if you want a comfortable and
luxurious stay at affordable prices.</p>
<p>Founded in 2006, IBC Hotels &amp; Resorts is the hospitality arm of the India Builders Corporation. With
over a decade of experience in this domain, we are one of the premium apartment hotels in
Bangalore. Carrying on the group’s passion for excellence, we are here to create a paradigm shift in
the hospitality industry with our customer centric approach.</p>
						</div>
						<div class="col-md-4">
							<img src="assets/images/b7.jpg" class="img-responsive">
						</div>
				</div>
		</div>
</div>
<div class="customer">
    <div class="container">
		<div class="stars text-center">
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<center><hr style="width: 55px; height: 1px; background-color: #179d67; margin-top: 0px;"></center>
		</div>
		<div class="heading">
				<h1 class="text-center">Our Satisfied Guests says</h1>
		</div>
		<div class="subheading">
			   <p class="text-center">We love to tell our successful far far away, behind the word mountains, far from<br> the countries Vokalia and Consonantia, there live the blind texts.</p>
		</div>
		<div class="row user">
				<div class="col-md-3 col-md-offset-1 coloum">
						<img src="assets/images/beach1.jpg" class="center-block img-responsive">
						<h3 class="text-center">Prafful Rajvaidya</h3>
						<h4 class="text-center">Satisfied Customer</h4>
						<p class="text-center">
							Dignissimos asperiores vitae velit<br> veniam totam fuga molestias<br> accusamus alias autem provident.<br> Odit ab aliquam dolor eius.
						</p>
				</div>
				<div class="col-md-3 col-md-offset-1 coloum">
						<img src="assets/images/beach1.jpg" class="center-block img-responsive">
						<h3 class="text-center">Prashant Rajvaidya</h3>
						<h4 class="text-center">Satisfied Customer</h4>
						<p class="text-center">
							Dignissimos asperiores vitae velit<br> veniam totam fuga molestias<br> accusamus alias autem provident.<br> Odit ab aliquam dolor eius.
						</p>
				</div>
				<div class="col-md-3 col-md-offset-1 coloum">
						<img src="assets/images/beach1.jpg" class="center-block img-responsive">
						<h3 class="text-center">Prafful Rajvaidya</h3>
						<h4 class="text-center">Satisfied Customer</h4>
						<p class="text-center">
							Dignissimos asperiores vitae velit<br> veniam totam fuga molestias<br> accusamus alias autem provident.<br> Odit ab aliquam dolor eius.
						</p>
				</div>
		</div>
	</div>
</div>
