<div class="content-page">
  <div class="content">
    <div class="container" style="margin-top: 100px;">
      <div class="row">
        <div class="col-md-12">
          <div class="portlet">
              <div class="portlet-heading card-box-green">
                  <h3 class="portlet-title">
                      Services
                  </h3>
                  <a data-toggle="modal" data-target="#add_service" class="btn w-md btn-rounded btn-custom waves-effect waves-light pull-right"><i class="fa fa-plus-circle fa-lg"></i> &nbsp;ADD SERVICE</a>
                  <div class="clearfix"></div>
              </div>
              <div id="bg-primary" class="panel-collapse collapse in">
                  <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row app-green-text">
                              <div class="col-md-2">PageSize:
                                <select ng-model="entryLimit" class="form-control">
                                  <option>5</option>
                                  <option>10</option>
                                  <option>20</option>
                                  <option>50</option>
                                  <option>100</option>
                                </select>
                              </div>
                              <div class="col-md-3">Filter:
                                <input type="text" ng-model="search" ng-change="filter()" placeholder="Search" class="form-control" />
                              </div>
                            </div>
                            <br/>
                            <div class="panel panel-default price-panel">
                                <div class="table-responsive">
                                  <table class="table table-bordered">
                                    <thead class="thead-light">
                                      <tr class="pointer">
                                        <th><span ng-click="sort_by('id');">ID</span></th>
                                        <th><span ng-click="sort_by('name');">Service Name</span></th>
                                        <th><span ng-click="sort_by('route_from');">Route From</span></th>
                                        <th><span ng-click="sort_by('route_to');">Route To</span></th>
                                        <!-- <th><span ng-click="sort_by('date_from');">Date From</span></th>
                                        <th><span ng-click="sort_by('date_to');">Date To</span></th> -->
                                        <th></th>
                                        <th></th>

                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr ng-repeat="serv in filtered = (allServices | filter:search | orderBy : predicate :reverse) | startFrom:(currentPage-1)*entryLimit | limitTo:entryLimit">
                                        <td>{{$index+1}}</td>
                                        <td><strong>{{serv.name}}</strong></td>
                                        <td>{{serv.route_from}}</td>
                                        <td>{{serv.route_to}}</td>
                                        <!-- <td>{{serv.date_from}}</td>
                                        <td>{{serv.date_to}}</td> -->
                                        <td>
                                          <a class="app-green-text" data-toggle="modal" data-target="#update_service" ng-click="getServiceById(serv.id)" style="font-size: 25px;"><i class="fa fa-pencil"></i></a>
                                        </td>
                                        <td>
                                          <a class="" data-toggle="modal" data-target="#delete_service" style="font-size: 25px; color: red;" ng-click="storeServiceId(serv.id)"><i class="fa fa-trash"></i></a>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                                <div class="col-md-12" ng-show="filteredItems == 0">
                                    <div class="col-md-12">
                                        <h4>Not  found</h4>
                                    </div>
                                </div>
                                <div class="col-md-12" ng-show="filteredItems > 0">
                                    <div pagination="" page="currentPage" on-select-page="setPage(page)" boundary-links="true" total-items="filteredItems" items-per-page="entryLimit" class="pagination-small" previous-text="&laquo;" next-text="&raquo;">
                                    </div>
                                </div>
                            </div> <!-- End of Panel -->
                        </div>
                    </div>  <!-- End row -->
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    <div ng-include="'tmp/footer.php'"></div>
  </div>
</div>
<div class="modal fade" id="add_service" data-backdrop="static">
  <div class="modal-dialog modal-md">
    <div class="col-sm-12">
      <div class="">
        <div class="m-t-30 account-pages">
          <div class="text-center account-logo-box header-bar">
            <h3 class="text-uppercase">
              <span>Add Service</span>
              <button type="button" class="btn btn-default pull-right" data-dismiss="modal">x</button>
            </h3>
          </div>
          <div class="account-content">
            <form class="form-horizontal" name="addServiceForm" method="post">
              <span class="message" class="col-sm-12"></span>
              <div class="form-group ">
                <div class="col-xs-12">
                  <input class="form-control" type="text" placeholder="Service Name" name="name" ng-model="service.name" required>
                  <div ng-show="addServiceForm.name.$touched">
                    <p class="error" ng-show="addServiceForm.name.$error.required">This field is required.</p>
                  </div>
                </div>
              </div>
              <div class=" nopadding">
                <div class="form-group">
                  <div class="col-xs-12">
                    <input class="form-control" type="text" placeholder="Route From" name="route_from" ng-model="service.route_from" required>
                    <div ng-show="addServiceForm.route_from.$touched">
                      <p class="error" ng-show="addServiceForm.route_from.$error.required">This field is required.</p>
                    </div>
                  </div>
                  </div>
                  <div class="form-group">
                    <div class="col-xs-12">
                      <input class="form-control" type="text" placeholder="Route To" name="route_to" ng-model="service.route_to" required>
                      <div ng-show="addServiceForm.route_to.$touched">
                        <p class="error" ng-show="addServiceForm.route_to.$error.required">This field is required.</p>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="col-md-1">
              </div>
              <div class="form-group text-center m-t-10">
                <div class="col-xs-12">
                  <br />
                  <button class="btn w-md btn-bordered btn-primary waves-effect waves-light pull-right" type="submit" ng-disabled="addServiceForm.$invalid" ng-click="addService(service);">Add</button>
                </div>
              </div>
            </form>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <!-- end wrapper -->
    </div>
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="update_service" data-backdrop="static">
  <div class="modal-dialog">
    <div class="col-sm-12">
      <div class="">
        <div class="m-t-30 account-pages">
          <div class="text-center account-logo-box header-bar">
            <h3 class="text-uppercase">
              <span>Edit Service</span>
              <button type="button" class="btn btn-default pull-right" data-dismiss="modal">x</button>
            </h3>
          </div>
          <div class="account-content">
            <form class="form-horizontal " name="editServiceForm" method="post">
              <span class="message" class="col-sm-12"></span>
              <div class="form-group ">
                <div class="col-xs-12">
                  <label>Name</label>
                  <input class="form-control" type="text" placeholder="Service Name" name="name" ng-model="srvc.name" required>
                  <div ng-show="editServiceForm.name.$touched">
                    <p class="error" ng-show="editServiceForm.name.$error.required">This field is required.</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-12 nopadding">
                <div class="form-group ">
                  <div class="col-xs-12">
                    <label>Route From</label>
                    <input class="form-control" type="text" placeholder="Route From" name="route_from" ng-model="srvc.route_from" required>
                    <div ng-show="editServiceForm.route_from.$touched">
                      <p class="error" ng-show="editServiceForm.route_from.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
                <div class="form-group ">
                  <div class="col-xs-12">
                    <label>Route To</label>
                    <input class="form-control" type="text" placeholder="Route To" name="route_to" ng-model="srvc.route_to" required>
                    <div ng-show="editServiceForm.route_to.$touched">
                      <p class="error" ng-show="editServiceForm.route_to.$error.required">This field is required.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-1">
              </div>
              <div class="form-group text-center m-t-10">
                <div class="col-xs-12">
                  <br />
                  <button class="btn w-md btn-bordered btn-primary waves-effect waves-light pull-right" type="submit" ng-disabled="editServiceForm.$invalid" ng-click="updateService(srvc);">Update</button>
                </div>
              </div>
            </form>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <!-- end wrapper -->
    </div>
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div id="delete_service" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <p class="text-center" style="font-size:80px;"><i class="fa fa-exclamation-triangle"></i></p>
          <div class="row text-center">
              <div class="col-md-12">
                  <span class="col-sm-12 success text-center"></span>
                  <span class="col-sm-12 error text-center"></span>
                  <h3 class="color-black">Do you want to delete this Service?</h3>
                  <!-- <p class="text-size">This will delete all the properties of this hotel!</p> -->
              </div>
          </div>
          <div class="modal-footer">
            <div class="col-md-10 col-md-offset-2" style="top: 10px;">
              <button type="button" class="btn w-md btn-bordered btn-primary waves-effect waves-light" ng-click="deleteService(selectedServiceId)"></i> &nbsp;&nbsp;OK</button>
              <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
            </div>
          </div>
      </div>
    </div>
  </div>
</div><!-- /.modal -->
<script>

$.datetimepicker.setLocale('en');

var currentdate = new Date();
var datetime = "Now: " + currentdate.getDate() + "-"
            + (currentdate.getMonth()+1)  + "-"
            + currentdate.getFullYear() + "  "
            + currentdate.getHours() + ":"
            + currentdate.getMinutes() + ":"
            + currentdate.getSeconds();

$('.datetimepicker').datetimepicker({
    value:datetime,
    format:'Y-m-d H:i:s'
});
</script>
