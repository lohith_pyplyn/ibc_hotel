<div ng-controller="NavigationController">
  <div id="menuLoaderDiv">
    <div ng-controller="HeaderController">
      <header id="" ng-hide="isDashboard">
        <nav class="navbar" style="margin-bottom: 0px;background-color: #f3f3f3;">
          <div class="container-fluid">
            <!-- <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style="float: left; margin: 10px; margin-top:20px">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div> -->
            <div class=" animated fadeInLeft" id="myNavbar">
              <ul class="nav navbar-nav display-hide" style="display: flex; justify-content: center;">
                <li><a href="#"><i class="fa fa-twitter-square fa-lg" style="color: #797979"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook-square fa-lg" style="color: blue !important;"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram fa-lg" style="color: #797979"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin-square fa-lg" style="color: #797979"></i></a></li>
              </ul>
              <ul class="nav navbar-nav" style="display:flex;float:right">
                <li>
                  <a class="display-hide" style="color: #797979;" href="#" ><i class="fa fa-envelope"style="color: #797979"></i> reservations@ibchotels.co.in</a>
                </li>
                <li>
                  <a class="display-hide" style="color: #797979;" href="#"><i class="fa fa-phone"style="color: #797979"></i> 08041670733</a>
                </li>
                <li ng-show="!isLoggedIn"><a href="" style="color: #797979;" data-toggle="modal" data-target="#Signup">Sign Up</a></li>
                <li ng-show="!isLoggedIn"><a href="" style="color: #797979;" data-toggle="modal" data-target="#loginSignup">Login</a></li>
                <li ng-show="isLoggedIn">
                  <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown user-box align-box">
                        <a href="" class="dropdown-toggle waves-effect user-link" data-toggle="dropdown" aria-expanded="true" style="padding:0px!important;">
                            <img ng-src="assets/images/users/avatars/avatar_default2.png" alt="user-img" class="img-circle user-img" style="margin-top: 5px!important;">
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list" >
                            <li>
                                <h5>Hi, Admin</h5>
                            </li>
                            <li ng-if="!isDashboard"><a href="#/dashboard"><i class="glyphicon glyphicon-user m-r-5"></i> Dashboard</a></li>
                            <li ng-if="isDashboard"><a href="#/"><i class="glyphicon glyphicon-user m-r-5"></i>Home</a></li>
                            <li><a href="#/userprofile"><i class="glyphicon glyphicon-cog"></i> User Profile</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0);" ng-click="logout();"><i class="glyphicon glyphicon-log-out"></i> &nbsp;Logout</a></li>
                        </ul>
                    </li>
                </ul>
                </li>
                <!-- <div  ng-show="!isLoggedIn" style="display:flex;">
                  <div style="display:flex !important;">
                    <li ></li>
                    <li ></li>
                  </div>
                </div> -->
              </ul>
            </div>
          </div>
        </nav>
        <nav class="navbar navbar-default navbar-inverse sec_navbar">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar1">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"><img class="img-responsive img-align" src="./assets/images/ibc_title.gif" alt="IBC Hotel"></img></a>
              <!-- <ul class="nav navbar-nav navbar-left">
                <li>
                    <button class="button-menu-mobile open-left waves-effect" style="margin-top:-35px">
                        <i class="mdi mdi-menu"></i>
                    </button>
                </li>
            </ul> -->
            </div>
            <div class="collapse navbar-collapse" id="myNavbar1">
              <ul class="nav navbar-nav navbar-right colour-change">
                <li class="active"><a href="#"><strong>Home</strong></a></li>
                <li class=""><a href="#/apartment">Apartment</a></li>
                <li class=""><a href="#">Amenities</a></li>
                <li class=""><a href="#/offers">Offers</a></li>
                <li class=""><a href="#">Corporate</a></li>
                <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="">About<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="#/aboutus">About Us</a></li>
                    <li><a href="#">Hotel Policy</a></li>
                    <li><a href="#">Feedback</a></li>
                    <li><a href="#/contact">Contact Us</a></li>
                  </ul>
                </li>
                <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">More<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="#/planevent">Plan and Events</a></li>
                    <li><a href="#">Dining Program</a></li>
                    <li><a href="#/lovebang">Love Benglore</a></li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
      <div class="topbar" ng-if="isDashboard">
        <div class="topbar-left">
          <a href="#/" class="logo">
            <span>
                <img src="assets/images/logo.png" alt="" style="width: 100%; height: 65px;">
            </span>
            <i>
                <img src="assets/images/logo.png" alt="" style="width: 100%;" >
            </i>
          </a>
        </div>
        <div class="navbar navbar-default" role="navigation">
          <div class="container">
            <ul class="nav navbar-nav navbar-left">
              <li>
                <button class="button-menu-mobile open-left waves-effect">
                  <i class="mdi mdi-menu"></i>
                </button>
              </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown user-box">
                <a href="#" class="dropdown-toggle waves-effect user-link" data-toggle="dropdown" aria-expanded="true">
                  <img ng-src="assets/images/users/avatars/avatar_default2.png" alt="user-img" class="img-circle user-img">
                </a>
                <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list" >
                  <li>
                      <h5>Hi, Admin</h5>
                  </li>
                  <li><a href="#/profile"><i class="glyphicon glyphicon-user m-r-5"></i> Profile</a></li>
                  <li><a href="#/change-password"><i class="glyphicon glyphicon-cog"></i> Change Password</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0);" ng-click="logout();" <i class="glyphicon glyphicon-log-out"> </i> Logout</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="left side-menu margin-top" ng-if="isDashboard" >
      <div class="sidebar-inner slimscrollleft">
        <div id="sidebar-menu">
          <ul>
            <li class="has_sub">
                <a href="#/dashboard" class="waves-effect"><i class="fa fa-tachometer"></i> <span> Dashboard </span> </a>
            </li>
            <li class="has_sub">
                <a href="#/upcomingbooking" class="waves-effect"><i class="fa fa-bookmark"></i> <span> Upcoming Booking </span> </a>
            </li>
            <li class="has_sub">
              <a href="#/bookinghistory" class="waves-effect"><i class="fa fa-history"></i><span> Booking History </span> </a>
            </li>
            <li>
              <a href="javascript:void(0);" class="waves-effect" ng-click="logout();"><i class="mdi mdi-logout"></i> Logout</a>
            </li>
          </ul>
            <!-- Sidebar -->
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal -->
  <div class="modal fade" id="loginSignup" role="dialog">
    <!-- <div class="modal-dialog"> -->
      <!-- Modal content-->
      <!-- <div class="modal-content"> -->
        <div class="modal-body">

          <div class="row">
              <div class="col-sm-12">
                  <div class="wrapper-page">
                      <div class="m-t-40 account-pages">
                          <div class="text-center account-logo-box" style="background:#000000;">
                              <h2 class="text-uppercase" style="color:#ffffff!important;">
                                login
                                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">&times;</button>
                              </h2>

                          </div>
                          <div class="account-content" style="border-radius: 0 0 5px 5px">
                              <form class="form-horizontal" name="loginUserForm">
                                  <span id="success" class="col-sm-12 success"></span>
                                  <span id="error" class="col-sm-12 error"></span>
                                  <div class="form-group ">
                                      <div class="col-xs-12">
                                          <input class="form-control" type="text" required="" placeholder="Username" name="username" ng-model="user.username">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <div class="col-xs-12">
                                          <input class="form-control" type="password" name="password" ng-model="user.password" required="" placeholder="Password">
                                      </div>
                                  </div>
                                  <div class="form-group text-center m-t-30">
                                      <div class="col-sm-12">
                                          <a id="forgotPass" class="text-muted" data-toggle="modal" data-target="#forgotModal"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
                                      </div>
                                  </div>
                                  <div class="form-group text-center m-t-10">
                                      <div class="col-xs-12">
                                          <button class="btn w-md btn-bordered btn-primary" type="submit" ng-disabled="!loginUserForm.$valid" ng-click="login(user);">Log In</button>
                                      </div>
                                  </div>
                              </form>
                              <div class="clearfix"></div>
                          </div>
                      </div>
                      <!-- end card-box-->
                  </div>
                  <!-- end wrapper -->
              </div>
          </div>
        </div>
        <!-- <div class="modal-footer">
          <div class="col-md-12">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div> -->
      <!-- </div> -->
    <!-- </div> -->
  </div>
  <div class="modal fade" id="Signup" role="dialog">
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->

          <div class="row">
              <div class="col-sm-12">
                  <!-- <div class="wrapper-page"> -->
                      <div class=" account-pages">
                          <div class="text-center account-logo-box" style="background:black;">
                              <h2 class="text-uppercase"style="color:white;">
                                Register
                                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">x</button>
                              </h2>

                          </div>
                          <div class="account-content" style="border-radius: 0 0 5px 5px">
                            <div class="account-content">
                    					<form class="form-horizontal" name="user_form" enctype="multipart/form-data" method="post">
                                <div class="row">
                    							<span class="message col-md-10 col-md-offset-1"></span>
                    							<div class="col-md-5 col-md-offset-1">
                    								<div class="form-group">
                    									<label for="field-1" class="control-label">First Name</label>
                    									<input type="text" class="form-control" name="firstname" ng-model="user.firstname" required>
                    									<div ng-show="user_form.firstname.$touched">
                    										<p class="error" ng-show="user_form.firstname.$error.required">This field is required.</p>
                    									</div>
                    								</div>
                    							</div>
                    							<div class="col-md-5 col-md-offset-1">
                                    <div class="form-group">
                    									<label for="field-1" class="control-label">Username</label>
                    									<input type="text" class="form-control" name="username" ng-model="user.username" required>
                    									<div ng-show="user_form.username.$touched">
                    										<p class="error" ng-show="user_form.username.$error.required">This field is required.</p>
                    									</div>
                    								</div>

                    							</div> <!-- End of col-md-4 -->
                    							<div class="col-md-1">
                    							</div>
                    							<div class="clearfix"></div>
                    						</div>
                    						<div class="row">
                    							<div class="col-md-5 col-md-offset-1">
                    								<div class="form-group">
                    									<label for="field-1" class="control-label">Last Name</label>
                    									<input type="text" class="form-control" name="lastname" ng-model="user.lastname" required>
                    									<div ng-show="user_form.lastname.$touched">
                    										<p class="error" ng-show="user_form.lastname.$error.required">This field is required.</p>
                    									</div>
                    								</div>
                                    <div class="form-group">
                    									<label for="field-1" class="control-label">E-Mail</label>
                    									<input type="text" class="form-control" name="email" ng-model="user.email" required>
                    									<div ng-show="user_form.email.$touched">
                    										<p class="error" ng-show="user_form.email.$error.required">This field is required.</p>
                    									</div>
                    								</div>
                                    <div class="form-group">
                    									<label for="field-1" class="control-label">Phone Number</label>
                    									<input type="text" class="form-control" name="phoneno" ng-model="user.phoneno" required>
                    									<div ng-show="user_form.phoneno.$touched">
                    										<p class="error" ng-show="user_form.phoneno.$error.required">This field is required.</p>
                    									</div>
                    								</div>
                    							</div>
                    							<div class="col-md-5 col-md-offset-1">
                                    <div class="form-group">
                                      <label  for="field-1" class="control-label">Password</label>
                                      <input type="password" class="form-control" name="password" ng-model="user.password" required id="pass">
                                      <div ng-show="user_form.password.$touched">
                                        <p class="error" ng-show="user_form.password.$error.required">This field is required.</p>
                                      </div>
                                    </div>
                                    <div class="form-group">
                    									<label for="field-1" class="control-label">Confirm Password</label>
                    									<input type="password" class="form-control" name="confirmpassword" ng-model="user.confirmpassword" required ng-pattern="user.password" id="cpass">
                    									<div ng-show="user_form.confirmpassword.$touched">
                    										<p class="error" ng-show="user_form.confirmpassword.$error.required">Password is Required.</p>
                                        <p class="error" ng-show="user_form.confirmpassword.$error.pattern">Password do not match.</p>
                    									</div>
                    								</div>

                    							</div> <!-- End of col-md-4 -->
                    							<div class="col-md-1">
                    							</div>
                    							<div class="clearfix"></div>
                    						</div>
                    						<div class="modal-footer">
                    							<div class="row pull-right">
                    								<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                    								<button type="submit" class="btn btn-primary waves-effect waves-light" ng-disabled="user_form.$invalid" ng-click="register(user);">Submit</button>
                    							</div>
                    						</div>
                    					</form>
                    				</div>
                              <div class="clearfix"></div>
                          </div>
                      </div>
                      <!-- end card-box-->
                  <!-- </div> -->
                  <!-- end wrapper -->
              </div>
          </div>
      </div>
        <!-- <div class="modal-footer">
          <div class="col-md-12">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div> -->
</div>
