<section>

  <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <!-- <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol> -->
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active">
          <img src="assets/images/slide1.jpg" alt="Los Angeles" style="width:100%;">
        </div>
        <div class="item">
          <img src="assets/images/slide2.jpg" alt="Chicago" style="width:100%;">
        </div>
        <div class="item">
          <img src="assets/images/Java_Resturant.jpg" alt="New york" style="width:100%;">
        </div>
        <div class="item">
          <img src="assets/images/Hotel_Reception.jpg" alt="New york" style="width:100%;">
        </div>
        <div class="item">
          <img src="assets/images/garder_view.jpg" alt="New york" style="width:100%;">
        </div>
        <div class="item">
          <img src="assets/images/Golf_Course3.jpg" alt="New york" style="width:100%;">
        </div>
      </div>
      <!-- Left and right controls -->
      <a class="left carousel-control" href="#myCarousel" data-slide="prev" >
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next" >
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    <div class="availability_form" style="width:100%; text-align:center;">
      <form class="form-inline" name="availabilityForm" method="post" enctype="multipart/form-data">
        <div class="form-group availibility_form_group">
          <!-- <label>Check In:</label> -->
          <div class="input-group">
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar fa-lg" style="color: #99e265"></span>
            </span>
            <input  type="text" name="check_in" ng-model="avlbty.check_in" class="form-control datetimepicker checkIn" name="check_in" placeholder="Check In Date">
          </div>
       </div>
        <div class="form-group availibility_form_group">
          <!-- <label>Check Out:</label> -->
          <div class="input-group  " >
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar fa-lg"  style="color: #99e265"></span>
            </span>
            <input type="text" name="check_out" ng-model="avlbty.check_out" class="form-control datetimepicker checkIn" onkeyup="success()" name="check_out" placeholder="Check Out Date">
            </div>
         </div>
         <div class="form-group ">
          <button  type="button" onkeyup="success()" class="btn btn-default availibility_btn" ng-click="availability(avlbty)" name="button">Search</button>
        </div>
      </form>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-10">
        </div>
      </div><br /><br />
      <!--list your property modal-->
      <div class="modal fade" id="feedbackform" data-backdrop="static" role="dialog">
      	<div class="modal-dialog modal-lg">
      		<div class="col-sm-12">
      			<div class="m-t-30 account-pages">
      				<div class="text-center account-logo-box header-bar">
      					<h3 class="text-uppercase">
      						<div class="row">
      							<div class="col-md-11">
      							<span class="txt-align" style="color:white;">Feedback Form</span>
      							</div>
      						<div class="col-md-1">
      							<button type="button" class="btn btn-default pull-right btn-margin" data-dismiss="modal">x</button>
      						</div>
      					</div>
      					</h3>
              </div>


      				<div class="account-content">
              <form name="user_feedback_form" enctype="multipart/form-data" method="post">
      							<!-- <span class="message col-md-10 col-md-offset-1 text-center"></span> -->
                    	<div class="main">
                        <div class="row">
                          <div class="col-md-5">

                            <div class="form-group">
                              <label for="field-1" class="control-label">Full name</label>
                              <input type="text" class="form-control" name="full_name" ng-model="property.full_name" required>
                              <div ng-show="user_feedback_form.full_name.$touched">
                              <p class="error" ng-show="user_feedback_form.full_name.$error.required">This field is required.</p>
                              </div>
                            </div>

                            <div class="form-group">
                              <label for="field-1" class="control-label">E-Mail</label>
                              <input type="text" class="form-control" name="email" ng-model="property.email" ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" required>
                              <div ng-show="user_feedback_form.email.$touched">
                                <p class="error" ng-show="user_feedback_form.email.$error.required">This field is required.</p>
                                <p class="error" ng-show="user_feedback_form.email.$error.pattern">Invalid email pattern.</p>
                              </div>
                            </div>

                            <div class="form-group">
                              <label for="field-1" class="control-label">Phone number</label>
                              <input type="text" class="form-control" name="phone_number" ng-model="property.phone_number" ng-pattern="/^[0-9]+$/" required>
                              <div ng-show="user_feedback_form.phone_number.$touched">
                                <p class="error" ng-show="user_feedback_form.phone_number.$error.required">This field is required.</p>
                                <p class="error" ng-show="user_feedback_form.phone_number.$error.pattern">Invalid phone number.</p>
                              </div>
                            </div>
                          </div>

                          <div class="col-md-5 col-md-offset-1">
                            <div class="form-group">
                              <label for="field-1" class="control-label">Apartment Number</label>
                              <input type="text" class="form-control" name="username" ng-model="property.Apartment_Number"            ng-unique="user_feedback_form.Apartment_Number" required>

                              <div ng-show="user_feedback_form.username.$touched">
                                <p class="error" ng-show="user_feedback_form.Apartment_Number.$error.required">This field is required.</p>
                                <p class="error" ng-show="user_feedback_form.Apartment_Number.$error.unique">Username already exist.</p>
                              </div>
                            </div>

                            <div class="form-group">
                              <label for="start_from">Check-in</label>
                              <input type="text" class="form-control" name="start_from" pickadate ng-model="property.start_from" required >
                              <div ng-show="user_feedback_form.start_from.$touched">
                                <p class="error" ng-show="user_feedback_form.start_from.$error.required">This field is required.</p>
                              </div>
                            </div>

                            <div class="form-group">
                              <label for="end">Check-out</label>
                              <input type="text" class="form-control" name="end" pickadate ng-model="property.end" required >
                              <div ng-show="user_feedback_form.end.$touched">
                                <p class="error" ng-show="user_feedback_form.end.$error.required">This field is required.</p>
                              </div>
                            </div>

                          <!-- End of col-md-4 -->
                          <!-- <div class="col-md-1"> -->
                          </div>
                        </div>

                        <h5>Timely Response</h5>
                        <div class="radio-btns">
                            <div class="swit">

                              <div class="check_box_one"> <div class="radio2"> <div class="radio radio-success radio-inline">
                              <input type="radio" id="inlineRadio1" value="Excellent" name="Timely_Response" ng-model="property.Timely_Response" checked>
                              <label for="inlineRadio1"> Excellent </label>
                            </div> </div></div>


                            <div class="check_box"> <div class="radio2"> <div class="radio radio-info radio-inline">
                              <input type="radio" id="inlineRadio2" value="Good" name="Timely_Response" ng-model="property.Timely_Response">
                              <label for="inlineRadio2"> Good </label>
                            </div> </div></div>

                              <div class="check_box"> <div class="radio2"> <div class="radio radio-warning radio-inline">
                              <input type="radio" id="inlineRadio3" value="Fair" name="Timely_Response" ng-model="property.Timely_Response">
                              <label for="inlineRadio3"> Fair </label>
                            </div> </div></div>

                              <div class="check_box"> <div class="radio2"> <div class="radio radio-danger radio-inline">
                              <input type="radio" id="inlineRadio4" value="Poor" name="Timely_Response" ng-model="property.Timely_Response">
                              <label for="inlineRadio4"> Poor </label>
                            </div> </div></div>

                              <div class="clear"></div>
                            </div>
                        </div>

                        <br/ ><br/ >
                    		<h5>Our Support</h5>
                    			<div class="radio-btns">
                    					<div class="swit">
                    						<div class="check_box_one"> <div class="radio2"> <div class="radio radio-success radio-inline">
                                <input type="radio" id="inlineRadio5" value="Excellent" name="Our_Support" ng-model="property.Our_Support" checked>
                                <label for="inlineRadio5"> Excellent </label>
                              </div> </div></div>
                              <div class="check_box"> <div class="radio2"> <div class="radio radio-info radio-inline">
                                <input type="radio" id="inlineRadio6" value="Good" name="Our_Support" ng-model="property.Our_Support">
                                <label for="inlineRadio6"> Good </label>
                              </div> </div></div>
                    					<div class="check_box"> <div class="radio2"> <div class="radio radio-warning radio-inline">
                                <input type="radio" id="inlineRadio7" value="Fair" name="Our_Support" ng-model="property.Our_Support">
                                <label for="inlineRadio7"> Fair </label>
                              </div> </div></div>
                    					<div class="check_box"> <div class="radio2"> <div class="radio radio-danger radio-inline">
                                <input type="radio" id="inlineRadio8" value="Poor" name="Our_Support" ng-model="property.Our_Support">
                                <label for="inlineRadio8"> Poor </label>
                              </div> </div></div>
                    						<div class="clear"></div>
                    					</div>
                    			</div><br/ ><br/ >
                    		<h5>Overall Satisfaction</h5>
                        <div class="radio-btns">
                            <div class="swit">
                              <div class="check_box_one active"> <div class="radio2"> <div class="radio radio-success radio-inline">
                              <input type="radio" id="inlineRadio9" value="Excellent" name="Overall_Satisfaction" ng-model="property.Overall_Satisfaction" checked>
                              <label for="inlineRadio9"> Excellent </label>
                            </div> </div></div>
                            <div class="check_box"> <div class="radio2"> <div class="radio radio-info radio-inline">
                              <input type="radio" id="inlineRadio10" value="Good" name="Overall_Satisfaction" ng-model="property.Overall_Satisfaction">
                              <label for="inlineRadio10"> Good </label>
                            </div> </div></div>
                            <div class="check_box"> <div class="radio2"> <div class="radio radio-warning radio-inline">
                              <input type="radio" id="inlineRadio11" value="Fair" name="Overall_Satisfaction" ng-model="property.Overall_Satisfaction">
                              <label for="inlineRadio11"> Fair </label>
                            </div> </div></div>
                            <div class="check_box"> <div class="radio2"> <div class="radio radio-danger radio-inline">
                              <input type="radio" id="inlineRadio12" value="Poor" name="Overall_Satisfaction" ng-model="property.Overall_Satisfaction">
                              <label for="inlineRadio12"> Poor </label>
                            </div> </div></div>
                              <div class="clear"></div>
                            </div>
                        </div><br/ ><br/ >

                    			<!-- <h5>Is there anything you would like to tell us?</h5>
                            <textarea onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Type here';}" required="">Type here</textarea> -->


                    			<h5>Is there anything you would like to tell us?</h5>
                    				<textarea onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Type here';}"  ng-model="property.anything_tell_us" required="">Type here</textarea>

                     	<button type="submit"  class="user_feedback_btn_color" ng-disabled="user_feedback_form.$invalid" ng-click="userFeedback(property);">Send Feedback </button>
                      </div>
                    </form>
      							<div class="col-md-1">
      							</div>
      							<div class="clearfix"></div>
      				</div>
      			</div>
      		</div>
      	</div><!-- /.modal-dialog -->
      </div><!-- End of modal -->



  <div class="modal fade" id="Hotel_policy" data-backdrop="static" role="dialog">
      	<div class="modal-dialog modal-lg">
      		<div class="col-sm-12">
      			<div class="m-t-30 account-pages">
      				<div class="text-center account-logo-box header-bar">
      					<h3 class="text-uppercase">
      						<div class="row">
      							<div class="col-md-11">
      							<span class="txt-align" style="color:white;">Hotel Policy</span>
      							</div>
      						<div class="col-md-1">
      							<button type="button" class="btn btn-default pull-right btn-margin" data-dismiss="modal">x</button>
      						</div>
      					</div>
      					</h3>
              </div>


      				<div class="account-content">

      							<!-- <span class="message col-md-10 col-md-offset-1 text-center"></span> -->
                    	<div class="main">
                        <div class="row">
                          <div class="col-md-5">


                          </div>

                          <div class="col-md-5 col-md-offset-1">

                          </div>
                        </div>

                        <h4>General Terms</h4>
                        <div class="text-justify textcolor">
                        <p><b>The Management reserves the right to request Photo ID in the name of the card holder and failure
                        to provide this may result in us deeming the booking Void. The primary guest must be at least 18
                        years of age with valid identification to be able to check-in at the hotel.</p><b>

                        <br>
                        <p><h4> Check-in Check-out</h4>
                        Check-in: 12:00 noon
                        Check-out: 12:00 noon</p>
                        <br>

                        <p><h4>Early Check-in and Late Check-out Policy</h4>
                        Early arrival before 12:00 noon or a late departure beyond 12:00 noon will be charged at 50% of the
                        daily room rate.
                        Early arrival before 9:00am or late departure after 6:00pm will be charged at 100% of the daily room
                        rate.</p>
                        <br>

                        <p><h4>Cancellation</h4>
                        A No-show or non-cancellation of a confirmed reservation at least 48 hours prior to arrival will
                        attract a one night retention charge.</p>

                       </div>
                        </div>

                    </form>
      							<div class="col-md-1">
      							</div>
      							<div class="clearfix"></div>
      				</div>
      			</div>
      		</div>
      	</div><!-- /.modal-dialog -->
      </div><!-- End of modal -->



  <div class="modal fade " id="gym" data-backdrop="static" role="dialog">
      	<div class="modal-dialog modal-lg">
      		<div class="col-sm-12">
      			<div class="m-t-30 account-pages">
      				<div class="text-center account-logo-box header-bar">
      					<h3 class="text-uppercase">
      						<div class="row">
      							<div class="col-md-11">
      							<span class="txt-align" style="color:white;">gym</span>
      							</div>
      						<div class="col-md-1">
      							<button type="button" class="btn btn-default pull-right btn-margin" data-dismiss="modal">x</button>
      						</div>
      					</div>
      					</h3>
              </div>
      				<div class="account-content">
      							<!-- <span class="message col-md-10 col-md-offset-1 text-center"></span> -->
                    	<div class="main">
                        <div class="row">
                          <div class="col-md-5">
                          </div>
                          <div class="col-md-5 col-md-offset-1">
                          </div>
                        </div>

                        <h4>GYM</h4>
                        <div class="text-justify textcolor">
                        <p>Your routine need not go for a toss if you’re on a holiday or a business trip. Whether you’re an early
                        bird and want an energetic start to the day or want to sweat it out after a long day at work, our GYM
                        is the place to be. Stacked with the best equipments, it is open 24 hours and completely free for our
                        guests.<p>
                        <p>With good lighting and large mirrors, you’re sure to enjoy your time at our gym. We have all the
                        equipments that you find at a multigym- Treadmill, Weights, Weight bench, Exercise Bike and more.
                        This gives you the option of varied training, you can pick what suits your needs.</p>
                       </div>
                   </div>
      							<div class="col-md-1">
      							</div>
      							<div class="clearfix"></div>
      				</div>
      			</div>
      		</div>
      	</div><!-- /.modal-dialog -->
      </div><!-- End of modal -->

      <div class="modal fade " id="privacy_policy" data-backdrop="static" role="dialog">
            <div class="modal-dialog modal-lg">
              <div class="col-sm-12">
                <div class="m-t-30 account-pages">
                  <div class="text-center account-logo-box header-bar">
                    <h3 class="text-uppercase">
                      <div class="row">
                        <div class="col-md-11">
                        <span class="txt-align" style="color:white;">Privacy Policy</span>
                        </div>
                      <div class="col-md-1">
                        <button type="button" class="btn btn-default pull-right btn-margin" data-dismiss="modal">x</button>
                      </div>
                    </div>
                    </h3>
                  </div>
                  <div class="account-content">
                        <!-- <span class="message col-md-10 col-md-offset-1 text-center"></span> -->
                          <div class="main">
                            <div class="row">
                              <div class="col-md-5">
                              </div>
                              <div class="col-md-5 col-md-offset-1">
                              </div>
                            </div>

                            <h4>Privacy Policy</h4>
                            <div class="text-justify textcolor">
                            <p>The IBC Hotels &amp; Resorts respects your privacy and is committed towards protecting the security and
                            confidentiality of any personal information that you provide to us or that we collect about you when
                            you visit our website www.ibchotels.co.in or when you contact guest services or when you
                            otherwise communicate with us.<p>
                            <p>This privacy policy statement outlines the type of personal information we collect and the policies
                            and procedure, we have established regarding its use and storage and serves as a guideline to be
                            observed by all our properties, affiliates, joint ventures and group companies.</p>
                            <p>You will be asked to agree to the terms of this Privacy policy while making a reservation, joining our
                            rewards programme or corresponding with us via the site or otherwise wherever required under
                            applicable law. Or your continued use of the site will be considered as your consent to the terms of
                            this Privacy Policy.</p>
                          </div><br/>
                           <h4>Personal Information We Collect About You</h4>
                           <div class="text-justify textcolor">
                           <p>Personal data or information related to an individual through which that person can be identified,
                            such as First name/Last name, Gender, Date of Birth.
                            Personal contact details such as Phone Number, Address, Email Address.<p>
                           <p>We may also collect other information such as guest preferences and usage, when such information
                            is supplied or collected to or IBC Hotels &amp; Resorts in the course of transacting business with an
                            individual.</p>
                           <p>The Privacy Policy does not apply to information regarding IBC Hotels &amp; Resorts corporate
                            customers. However, such information is protected by other IBC Hotels &amp; Resorts policies and
                            practices and through contractual arrangements.</p>
                          </div>
                       </div>
                        <div class="col-md-1">
                        </div>
                        <div class="clearfix"></div>
                  </div>
                </div>
              </div>
            </div><!-- /.modal-dialog -->
          </div><!-- End of modal -->



  <div class="modal fade" id="Airport_Transfers" data-backdrop="static" role="dialog">
      	<div class="modal-dialog modal-lg">
      		<div class="col-sm-12">
      			<div class="m-t-30 account-pages">
      				<div class="text-center account-logo-box header-bar">
      					<h3 class="text-uppercase">
      						<div class="row">
      							<div class="col-md-11">
      							<span class="txt-align" style="color:white;">Airport Transfers</span>
      							</div>
      						<div class="col-md-1">
      							<button type="button" class="btn btn-default pull-right btn-margin" data-dismiss="modal">x</button>
      						</div>
      					</div>
      					</h3>
              </div>
      				<div class="account-content">
      							<!-- <span class="message col-md-10 col-md-offset-1 text-center"></span> -->
                    	<div class="main">
                        <div class="row">
                          <div class="col-md-5">
                          </div>
                          <div class="col-md-5 col-md-offset-1">
                          </div>
                        </div>
                          <div class="text-justify textcolor">
                          <h4>Airport Transfers</h4>
                          <p>At IBC Hotels &amp; Resorts, we wish to make your stay a hassle-free experience, we offer Airport
                          transfer from our Airport to the hotel and vice versa.</p>

                          <h4>Transfer Rates</h4>

                          <p>SUV- 7 Seater – Price on Request</p>
                          <p>Kempegowda International Airport &lt;-&gt; IBC Hotels &amp; Resorts</p>

                          To place a booking, the following information would be required
                                <ul>
                                  <li>Arrival date and time</li>
                                  <li>Flight details (airline &amp; flight no)</li>
                                </ul>
                          <p>The airport transfer service needs to be reserved or cancelled at least 24 hours prior to the arrival
                          time and checkout time at the hotel.</p>
                          <p>Contact <a href="reservations@ibchotels.co.in">reservations@ibchotels.co.in </a> to reserve your booking.</p>

           </div>
                   </div>
      							<div class="col-md-1">
      							</div>
      							<div class="clearfix"></div>
      				</div>
      			</div>
      		</div>
      	</div><!-- /.modal-dialog -->
      </div><!-- End of modal -->

 <div class="modal fade" id="Business_Facilities" data-backdrop="static" role="dialog">
      	<div class="modal-dialog modal-lg">
      		<div class="col-sm-12">
      			<div class="m-t-30 account-pages">
      				<div class="text-center account-logo-box header-bar">
      					<h3 class="text-uppercase">
      						<div class="row">
      							<div class="col-md-11">
      							<span class="txt-align" style="color:white;">Business Facilities </span>
      							</div>
      						<div class="col-md-1">
      							<button type="button" class="btn btn-default pull-right btn-margin" data-dismiss="modal">x</button>
      						</div>
      					</div>
      					</h3>
              </div>
      				<div class="account-content">
      							<!-- <span class="message col-md-10 col-md-offset-1 text-center"></span> -->
                    	<div class="main">
                        <div class="row">
                          <div class="col-md-5">
                          </div>
                          <div class="col-md-5 col-md-offset-1">
                          </div>
                        </div>


                        <div class="text-justify textcolor">
                        <h4>Business Facilities</h4>
                        <p>IBC Hotels &amp; Resorts offers a range of business facilities to help you remain calm even on a business
                        trip. Whether you wish to conduct work calls, conduct quick meeting or even a print, we&#39;ve got you
                        covered.</p>


                        <li>Free Wi-Fi</li>
                        <li>Meeting Room</li>
                        <li>Arrival date and time</li>
                        <li>Copy Machine</li>
                        <li>Fax Machine</li>
                        <li>Printer</li>
                        <li>Audio Visual Equipment (On-request)</li>

                      <p>Catering options are also available, breakfast and lunches can be arranged. If you need any of these
                        services, inform at the reception desk and we’ll take care of the rest.</p>
                       </div>
                   </div>

      							<div class="clearfix"></div>
      				</div>
      			</div>
      		</div>
      	</div><!-- /.modal-dialog -->
      </div><!-- End of modal -->


<div class="modal fade" id="Conference_Hall" data-backdrop="static" role="dialog">
      	<div class="modal-dialog modal-lg">
      		<div class="col-sm-12">
      			<div class="m-t-30 account-pages">
      				<div class="text-center account-logo-box header-bar">
      					<h3 class="text-uppercase">
      						<div class="row">
      							<div class="col-md-11">
      							<span class="txt-align" style="color:white;">Conference Hall</span>
      							</div>
      						<div class="col-md-1">
      							<button type="button" class="btn btn-default pull-right btn-margin" data-dismiss="modal">x</button>
      						</div>
      					</div>
      					</h3>
              </div>
      				<div class="account-content">
      							<!-- <span class="message col-md-10 col-md-offset-1 text-center"></span> -->
                    	<div class="main">
                        <div class="row">
                          <div class="col-md-5">
                          </div>
                          <div class="col-md-5 col-md-offset-1">
                          </div>
                        </div>


                          <div class="text-justify textcolor">
                          <h4>Conference Hall</h4>

                          <p>Our meeting hall is perfect for quick meetings, conferences, presentations with a capacity of 50
                          guests. With the option of flexibility of our business facilities and the expertise of trained staff to
                          meet all your needs, rest assured you&#39;ll leave with a smooth and successful event.
                          <p>Catering options are available for breakfast, lunch, dinner, meeting breaks and parties.</p>
                       </div>
                   </div>

      							<div class="clearfix"></div>
      				</div>
      			</div>
      		</div>
      	</div><!-- /.modal-dialog -->
      </div><!-- End of modal -->





<div class="modal fade" id="Dining" data-backdrop="static" role="dialog">
      	<div class="modal-dialog modal-lg">
      		<div class="col-sm-12">
      			<div class="m-t-30 account-pages">
      				<div class="text-center account-logo-box header-bar">
      					<h3 class="text-uppercase">
      						<div class="row">
      							<div class="col-md-11">
      							<span class="txt-align" style="color:white;">Dining</span>
      							</div>
      						<div class="col-md-1">
      							<button type="button" class="btn btn-default pull-right btn-margin" data-dismiss="modal">x</button>
      						</div>
      					</div>
      					</h3>
              </div>
      				<div class="account-content">
      							<!-- <span class="message col-md-10 col-md-offset-1 text-center"></span> -->
                    	<div class="main">
                        <div class="row">
                          <div class="col-md-5">
                          </div>
                          <div class="col-md-5 col-md-offset-1">
                          </div>
                        </div>


                        <div class="text-justify textcolor">
                        <h4>Dining at ‘JAVA’</h4>


                        <p>Our multi-cuisine restaurant ‘Java’, offers all-day dining, we make sure you don’t have to step out of
                        the hotel for good food. Our chefs churn delectable preparation round the clock to whet your
                        appetite for great food. Whether you’re in a mood to explore the rich taste of Indian Cuisine or want
                        to try the popular Indo-Chinese cuisine or lay your hands on the continental fare, we’ve got it all.
                        Order your favourite pick from the comprehensive menu and we’re always at your service. Dine in at
                        ‘Java’or if you prefer in-room dining, we’ll have your order served hot right at your doorstep.
                        </p>

                        <h4>Buffet Breakfast</h4>
                        <p>Start the day with proper fuel. Our Buffet breakfast menu is carefully designed to give a wholesome
                        and healthy start to your day. The spread is comprehensive, you can pick from the classic English
                        breakfast or the power-packed South-Indian fare. Finish it off with a strong filter coffee or flavourful
                        tea, you&#39;re ready for the day.</p>

                        <h4>Buffet Lunch</h4>
                        <p>Our lunch spread is sure to get your mouth watering. If you’re up for Indian cuisine, our lavish buffet
                        spread would make you feel like a royal. Whether you want to host a party, have a lavish meal with
                        your colleagues or fill your stomach to your heart’s content, Java is the place to be.</p>
                       </div>
                   </div>

      							<div class="clearfix"></div>
      				</div>
      			</div>
      		</div>
      	</div><!-- /.modal-dialog -->
      </div><!-- End of modal -->





<div class="modal fade" id="Tourist_Attraction" data-backdrop="static" role="dialog">
      	<div class="modal-dialog modal-lg">
      		<div class="col-sm-12">
      			<div class="m-t-30 account-pages">
      				<div class="text-center account-logo-box header-bar">
      					<h3 class="text-uppercase">
      						<div class="row">
      							<div class="col-md-11">
      							<span class="txt-align" style="color:white;">Tourist Attraction</span>
      							</div>
      						<div class="col-md-1">
      							<button type="button" class="btn btn-default pull-right btn-margin" data-dismiss="modal">x</button>
      						</div>
      					</div>
      					</h3>
              </div>
      				<div class="account-content">
      							<!-- <span class="message col-md-10 col-md-offset-1 text-center"></span> -->
                    	<div class="main">
                        <div class="row">
                          <div class="col-md-5">
                          </div>
                          <div class="col-md-5 col-md-offset-1">
                          </div>
                        </div>


                        <div class="text-justify textcolor">
                        <h4>Tourist Attraction</h4>


                        <p>Bangalore has shifted from a quiet regional capital to a modern metropolis in no time, thanks to the
                        emergence of IT industry. Bangalore is one of the most developed cities of South-India, blessed with
                        a favourable climate and plenty of places to explore. The ethos of Bangalore lies in the rich cultural
                        heritage and diversity. It intrigues all kinds of travellers and Bangalore has something to offer to
                        each one of you.
                        </p>

                        <h4>Lal Bagh</h4>
                        <p>Lal Bagh is centuries old botanical garden, commissioned by Hyder Ali in 1760 as a private garden
                        spread across 40 acres. Today, Lal Bagh is home to 1854 species of native as well as exotic flora
                        spread over 240 acres. Landscaped gardens, open spaces, well laid out roads make for a great crowd
                        puller. Another distinct feature of the Lal Bagh is the glass-house where flower show is organized
                        annually, the event attracts several thousands of visitors. You’d also find a lake and aquarium inside
                        the botanical garden. With several species of birds like Myna, Crows, Parakeets, Bahminy Kite,
                        Purple Moor Hen and more, it is a delight for bird watchers.</p>

                        <h4>Cubbon Park</h4>
                        <p>Cubbon Park is officially known as Sri Chamarajendra Park, situated right at the heart of Bengaluru. A
                        walk along the park and the periphery will allow you to witness the history and landmark building of
                        Bengaluru. Lush green gardens covered across the labyrinth of pathways, dotted with monumental
                        statues that speak of the history of this park. Cubbon Park is spread over 300 acres, also referred to
                        as the ‘lungs’ of Bengaluru, boasts of a rich flora and the topography of the place continues to
                        fascinate the nature enthusiasts with flower beds, bamboos and groves of numerous trees.</p>


                        <h4>Bannerghatta National Park</h4>
                        <p>Bannerghatta National Park is some 22km away from Bengaluru, but it is totally worth your time and
                        one of the things you cannot afford to miss if you’re out to explore all that the city has to offer.
                        Established in 1970 and declared as a national park in 1974, it boasts of a rich flora and fauna and
                        you’d find animals like Leopard, Asiatic Lion, Spotted deer, Sloth Bear, Elephants, Royal Bengal Tiger,
                        Jackal, Fox and more. It’s a treat for nature enthusiasts and photographers as they can see the
                        animals up close on the safari. The jungle safari is one of the main attractions of the national park. It
                        has several other establishments in the park such as India’s first butterfly park, Bannerghatta
                        Biological Park, zoo, an aquarium, children’s park and more.</p>



                        <h4>Bangalore Palace</h4>
                        <p>The magnificent Bangalore Palace modelled around the Windsor castle stands tall with grandiose
                        amidst the rush of the city. The architectural marvel was built in the year 1887 by the Chamaraja
                        Wadiyar. The strong influence of the Tudor and the Scottish style architecture can be witnessed in
                        the palace. Paintings, wooden interiors and a great mix of traditional Indian décor offer a slice of
                        bygone royal splendour. Ornate cornices, huge pillars, arches, patterned walls and opulent
                        chandeliers adding to the grand setting make for a must visit attraction of Bengaluru.</p>



                        <h4>Wonderla</h4>
                        <p>Wonderla is counted among one of the best amusement parks in India and you could witness all the
                        fun on your Bangalore trip. Spread over 30 hectares on the outskirts of Bangalore, you’ll be spoilt for
                        choice with 60 thrilling rides ranging from Wave Pool, Watery Coasters, Sky Wheel, Fun Pools, Rain
                        Disco and more. You could also go for 3D shows such as virtual reality shows and laser shows.
                        Whether you want to get your adrenaline soaring or want a leisure time, Wonderla has something
                        for each one. The park also has plenty of restaurants offering a variety of cuisines to pick from.</p>

                       </div>
                   </div>
      							<div class="clearfix"></div>
      				</div>
      			</div>
      		</div>
      	</div><!-- /.modal-dialog -->
      </div><!-- End of modal -->




<div class="modal fade" id="Dining_and_Entertainment" data-backdrop="static" role="dialog">
      	<div class="modal-dialog modal-lg">
      		<div class="col-sm-12">
      			<div class="m-t-30 account-pages">
      				<div class="text-center account-logo-box header-bar">
      					<h3 class="text-uppercase">
      						<div class="row">
      							<div class="col-md-11">
      							<span class="txt-align" style="color:white;">Dining & Entertainment</span>
      							</div>
      						<div class="col-md-1">
      							<button type="button" class="btn btn-default pull-right btn-margin" data-dismiss="modal">x</button>
      						</div>
      					</div>
      					</h3>
              </div>
      				<div class="account-content">
      							<!-- <span class="message col-md-10 col-md-offset-1 text-center"></span> -->
                    	<div class="main">
                        <div class="row">
                          <div class="col-md-5">
                          </div>
                          <div class="col-md-5 col-md-offset-1">
                          </div>
                        </div>


                        <div class="text-justify textcolor">
                        <h4>Dining & Entertainment</h4>


                        <p>The silicon valley of India, Bangalore is not all work and no play. Bangalore is home to a thriving
                        culinary and drinking scene with a great demand for Theatre, performance arts and stand-up
                        comedy. So, while you&#39;re in the city, we&#39;ll tell you how to have a gala time and make the most of
                        your stay.
                        </p>

                        <h4>Flavour of Bangalore</h4>
                        <p>The cosmopolitan outlook of Bangalore has enabled cuisines from the all over India, especially
                        South-Indian states to make a place in the heart of residents. The food culture of Bangalore reflects
                        the diversity and some of the cuisines are not to be missed.</p>

                        <h4>South-Indian Breakfast</h4>
                        <p>The essence of South-Indian Cuisine lies in its breakfast, something you wouldn’t want to miss. Some
                        of the dishes that are popular in all the food joints are Idli, Vada, Dosa (this has several varieties), Bisi
                        Bele Bath, Pongal, Idiyappam, Kesari bath and much more. They say “Eat Breakfast like a king” and
                        with Buffet breakfast at our very own Java restaurant, you’d be in for a hearty feast.</p>


                        <h4>Mangalorean Cuisine</h4>
                        <p>Mangalorean cuisine draws its flavour from Mangalore and its surrounding regions. Coastal region of
                        Karnataka is popular for its seafood specialties, especially the Mangalore. Satiate your seafood
                        cravings with rich coconut based dishes made with fish, prawns, crabs and clams. You could also try
                        the famous Chicken Ghee Roast with Kori Roti or the Fish curry with Sannas. Some of the restaurants
                        where you can check out Mangalorean cuisine.</p>

                        <li>Mangalore Pearl</li>
                        <li>Coast II Coast</li>
                        <li>Sanadige</li>

                        <h4>Andhra Cuisine</h4>
                        <p>The cuisine of Andhra has a variety of delicious dishes and it is also famous for meals served in
                        typical style on a banana leaf. The meals are to die for with an array of dishes such as Pappu,
                        Vepudu, Gojju, Pachadi, Podi, Pulusu, Rasam, Sambhar, Ooragaya and curd arranged around a heap
                        of rice. It’s an experience of a lifetime if you haven’t had Andhra meals ever. Some of the Non-
                        vegetarian fare that are not to be missed are Andhra style Chili Chicken, Chicken Pepper Fry and
                        Mutton Roast. Some of the restaurants where you can check out Andhra cuisine.</p>


                        <li>Nagarjuna</li>
                        <li>Bhimas</li>

                        <h4>Kerala Cuisine</h4>
                        <p>Kerala Cuisine boasts of a rich culture and historical influence. Spices form an integral part of the
                        cuisine owing to its spice plantations of Chili, Cardamom, Cloves, Cinnamon and Black Peppercorns.
                        It has a myriad of vegetarian and Non-vegetarian dishes to offer with poultry, red meat and fish.
                        Some of the dishes that are a must have for breakfast are Pottu with Kadala curry, Idiyappam, Paal-
                        Appam, Pidiyan. Kerala is also famous for Sadya, a vegetarian feast with boiled rice and multitude
                        dishes as accompaniments. And, do not miss the desserts that the cuisine has to offer such as Pazham Pudding, Paal Payasam, Palada Payasam, Parippu Payasam, Chatti Pathiri, Mutta Maala and
                        Ari Unda. Some of the restaurants where you can check out Kerala cuisine.</p>

                        <li>Ente Keralam</li>
                        <li>Coconut Grove</li>

                        <h4>Dining</h4>
                        <p>Food connoisseurs are having a great time, thanks to the interesting restaurants, cafes and bars
                        springing up all the time. The dining arena is quite competitive in Bangalore and customer is the
                        winner. Indiranagr is famous for a great food &amp; party scene, you could have a fine dining experience
                        or eat at one of those dainty cafes or catch up with a friend for a drink. Koramangala is another area
                        where you&#39;ll be spoilt for choice for dining options or you could also explore the M.G road area.</p>

                        <h4>Entertainment</h4>
                        <p>Whether you love art or want to tickle your funny bone, Bangalore has a place for everyone. With
                        several theatres, auditoriums, art galleries and concerts, Bangalore is always up for something. Some
                        of the places you might want to check while you’re here.</p>

                        <p>National Gallery of Modern Art</p>
                        <p>Karnataka Chitrakala Parishat</p>
                        <p>Jagriti Theatre</p>
                        <p>Chowdiah Memorial Hall</p>


                       </div>
                   </div>

      							<div class="clearfix"></div>
      				</div>
      			</div>
      		</div>
      	</div><!-- /.modal-dialog -->
      </div><!-- End of modal -->



<div class="modal fade" id="Shopping_in_Bangalore" data-backdrop="static" role="dialog">
      	<div class="modal-dialog modal-lg">
      		<div class="col-sm-12">
      			<div class="m-t-30 account-pages">
      				<div class="text-center account-logo-box header-bar">
      					<h3 class="text-uppercase">
      						<div class="row">
      							<div class="col-md-11">
      							<span class="txt-align" style="color:white;">Shopping in Bangalore</span>
      							</div>
      						<div class="col-md-1">
      							<button type="button" class="btn btn-default pull-right btn-margin" data-dismiss="modal">x</button>
      						</div>
      					</div>
      					</h3>
              </div>
      				<div class="account-content">
                    	<div class="main">
                        <div class="row">
                          <div class="col-md-5">
                          </div>
                          <div class="col-md-5 col-md-offset-1">
                          </div>
                        </div>
                        <div class="text-justify textcolor">
                        <h4>Shopping in Bangalore</h4>


                        <p>Bangalore has a plethora of shopping destinations including malls and sprawling markets spread
                        across the city. Whether you want to do some budget shopping or in a mood to splurge, there is
                        something for every shopper. Bangalore is a demographically diverse with people from all over India
                        and the world visiting it or making it their home. Whether you want to take back a souvenir from
                        India, grab something to wear from those International brands or wish to own a rich Kanjeevaram
                        saree, Bangalore has it all. Let’s hop on and explore some of the best places to shop in Bangalore.
                        </p>

                        <h4>Indiranagar</h4>
                        <p>100Ft road Indiranagar is five minutes away from our property, it is best known for nightlife, but
                        there are options galore for shoppers alike. A stroll on the bustling streets of Indiranagar is enough
                        to give you all the retail therapy you’d ever need. Stop by one of the biggest international brands for
                        your staple or check out bespoke, sustainable fashion from clothing and lifestyle boutiques all over
                        the place.</p>

                        <h4>Brigade Road</h4>
                        <p>Brigade Road has its own charm with old buildings, giving way to modern looking stores, it is one of
                        the favourite shopping destinations especially for youngsters. Lined up with shops on both the sides
                        of the road, you’ll find brand showrooms, street stalls, multi-shop arcades and even some budget
                        shopping stores where you can get all the good stuff without burning a hole in your pocket. Brigade
                        road offers the best of both worlds, ideal place to be if you’re in a mood to splurge or even do some
                        street shopping.</p>


                        <h4>M. G. Road</h4>
                        <p>M. G. Road is one of the most iconic streets of Bengaluru, earlier known as South Parade. It was at
                        the heart of British settlement and even now remains an important commercial hub. It has always
                        been a favourite among the tourists, offering a multitude of buying choices be it handicrafts, books,
                        apparel or even jewellery. The area boasts of interesting shopping avenues, you’ll be spoilt for
                        choices. If you are a mall shopper, 1MG mall is sure to entice you with global as well as Indian
                        brands.</p>

                        <h4>Commercial Street</h4>
                        <p>Commercial Street is a shopper’s paradise, especially for bargainers, while there are brand stores as
                        well, but what steals the show is umpteen street stalls selling ethnic wear, accessories, footwear and
                        more . With shops lined on both sides, you’re sure to have a good time here shopping till you drop.
                        There are food outlets, juice shops, restaurants scattered all over the commercial street, just in case
                        you want to tend to your growling stomach while you shop.</p>

                        <h4>Chickpet</h4>
                        <p>The vibrant bylanes of Chickpet market is where you’d find some of the best buys in Bangalore. If
                        you’re one of those travellers who’d visit the local market and doesn’t mind some effort, chickpet is
                        the hot spot for wholesale shopping in Bangalore. You might want to check out chickpet, especially
                        for Mysore Silk, Kanjeevarams and other variety of Sarees at a reasonable price. Other stuff that you
                        can buy here is antiques, apparel, accessories, crockery, stationery and pretty much everything
                        under the sun.</p>
                       </div>
                   </div>
      							<div class="clearfix"></div>
      				</div>
      			</div>
      		</div>
      	</div><!-- /.modal-dialog -->
      </div><!-- End of modal -->




<div class="modal fade" id="Medical_Tourism" data-backdrop="static" role="dialog">
      	<div class="modal-dialog modal-lg">
      		<div class="col-sm-12">
      			<div class="m-t-30 account-pages">
      				<div class="text-center account-logo-box header-bar">
      					<h3 class="text-uppercase">
      						<div class="row">
      							<div class="col-md-11">
      							<span class="txt-align" style="color:white;">Medical Tourism</span>
      							</div>
      						<div class="col-md-1">
      							<button type="button" class="btn btn-default pull-right btn-margin" data-dismiss="modal">x</button>
      						</div>
      					</div>
      					</h3>
              </div>
      				<div class="account-content">
                    	<div class="main">
                        <div class="row">
                          <div class="col-md-5">
                          </div>
                          <div class="col-md-5 col-md-offset-1">
                          </div>
                        </div>
                    <div class="text-justify textcolor">
                    <h4>Medical Tourism</h4>
                    <p>Medical Tourism sector has witnessed tremendous growth in the recent years in India. With more
                    advanced facilities on offer along with low cost results has resulted in India being one of the most
                    preferred destination for foreign patients. The numbers have been rising each year, with medical
                    tourists flocking from countries like the Gulf, Afghanistan, Bangladesh and other African countries as
                    India offers advanced facilities with personalized care. Medical tourists from the west come here
                    mostly for dental and cosmetic surgeries as they aren’t covered under their medical insurance and it
                    is much cheaper in India.</p>

                    <p>The most popular areas of medical tourism are Cardiology, Ophthalmology, transplants,
                    Orthopedics, Dental and cosmetology. India has also seen a surge of tourists seeking treatment in
                    wellness, preventive and alternative medicine.</p>

                    <h4>Medical Tourism in Bangalore</h4>

                    <p>Bangalore is at the heart of the medical tourism scene in India owing to its social situation. It’s a
                    melting pot of culture with people from diverse background and ethnicities making it their home.
                    Bangalore has been an ever welcoming city with tourists from all over seeking treatment. English has
                    emerged as a common language with widespread acceptance and foreigners don’t find any
                    roadblocks in Bangalore. The numbers have proven that Bangalore is emerging as one of the most
                    sought after cities for people visiting India for medical tourism.</p>

                    <p>We have a sizeable ratio of guests each year staying at IBC Hotels &amp; Resorts due to our excellent
                    location. We have several top-notch hospitals and other healthcare centres that offer medical
                    tourism. We also offer services like Doctor on call and all our apartments and parking are wheelchair
                    accessible.</p>

                    <h4>Hospitals/Medical Services in the vicinity of IBC Hotels & Resorts</h4>
                    <ol>
                    <li>Manipal Hospital</li>
                    <li>Sakra World Hospital</li>
                    <li>NM Medicals</li>
                    <li>St. Johns Hospital</li>
                    <li>Cratis Hospital</li>
                    <li>Motherhood</li>
                    <li>Cloudnine Hospital</li>
                    <li>Victoria Hospital</li>
                    <li>CMH Hospital</li>
                    <li>Ojus Hospital</li>
                    <li>Shabeer Hospital</li>
                    <li>Asha Hospital</li>
                    <li>Lohitha Hospital</li>
                    </ol>




                       </div>
                   </div>

      							<div class="clearfix"></div>
      				</div>
      			</div>
      		</div>
      	</div><!-- /.modal-dialog -->
      </div><!-- End of modal -->


      <div class="row marg_top_100">
        <div class="col-md-12" style="margin-bottom: 50px;">
          <h1 class="text-center">WHY BOOK DIRECTLY?</h1>
        </div>
        <div class=" col-md-2 col-md-offset-1">
            <center><i class="fa fa-money fa-3x" style="color: #99e265" aria-hidden="true"></i></center><br>
            <h4 class="text-center">Best Rate Guaranted</h4>
        </div>

        <div class="col-md-2">
            <center><i class="fa fa-cutlery fa-3x"  style="color: #99e265" aria-hidden="true"></i></center><br>
            <h4 class="text-center">Buffet & Breakfast</h4>
        </div>
         <div class="col-md-2">
            <center><i class="fa fa-wifi fa-3x"  style="color: #99e265" aria-hidden="true"></i></center><br>
            <h4 class="text-center">Free Wifi</h4>
        </div>
        <div class="col-md-2">
            <center><i class="fa fa-globe fa-3x"  style="color: #99e265" aria-hidden="true"></i></center><br>
            <h4 class="text-center">Website Special Deal</h4>
        </div>
        <div class="col-md-2">
            <center><i class="glyphicon glyphicon-remove-sign fa-3x" style="color: #99e265" aria-hidden="true"></i></center><br>
            <h4 class="text-center">Free Cancelation</h4>
        </div>

      </div>
      <br><br>
      <div class="row marg_top_100">
        <div class="col-md-10 col-md-offset-1">
          <h1 class="text-center">About Us</h1>
          <h5 ><p class="text-justify textcolor">IBC Hotels &amp; Resorts is an apartment hotel situated inside Diamond District on Old Airport Road with
          localities like Indiranagar, M.G. Road, Koramangala in the vicinity. It offers 61 well-appointed fully
          furnished, spacious, modern apartments that come in studio, one, two, three bedroom and
          penthouse configuration. Outfitted with a gourmet chef&#39;s kitchen/kitchenette, free WI-FI, 24*7
          restaurant, gym, in-room laundry facilities, free parking, taxi service and more. Proximity to popular
          restaurants, pubs, shopping complexes, hospitals and tech parks makes this a preferred choice for
          leisure as well as business travellers. IBC Hotels &amp; Resorts is ideal if you want a comfortable and
          luxurious stay at affordable prices.</p>
          <p>Founded in 2006, IBC Hotels &amp; Resorts is the hospitality arm of the India Builders Corporation. With
          over a decade of experience in this domain, we are one of the premium apartment hotels in
          Bangalore. Carrying on the group’s passion for excellence, we are here to create a paradigm shift in
          the hospitality industry with our customer centric approach.</p></h5>
        </div>
      </div> <br><br>


      <div class="row marg_top_100">
        <div class="col-md-10 col-md-offset-1">
          <div id="imageCarousel" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                <div class="item active">
                    <div class="row">
                       <div class="col-sm-3">
                          <center><img src="assets/images/garder_view.jpg" class="img-responsive img1" /></center><br>
                          </h5>  <h4 class="text-center">Swimming Pool</h4>
                          <h5 class="text-justify">
                           <p>You routine need not go for a toss if you’re on a holiday or a business trip. Whether you’re an early
                           bird and want an energetic start to the day or want to sweat it out after a long day at work, our  GYM
                           is the place to be. Stacked with the best equipments, it is open 24 hours and completely free for  our
                           guests.</p>
                       </div>
                       <div class="col-sm-3">
                          <center><img src="assets/images/b8.jpg" class="img-responsive img1" /></center><br>
                          <h4 class="text-center">Conference/Banquet Hall</h4>
                          <h5 class="text-justify"> <p>Our meeting and conference hall can hold up to 50 guests, ideal for business meetings, conferences
                          and small parties. With the option of flexibility of our business facilities and the expertise of trained
                          staff to meet all your needs, rest assured you&#39;ll leave with a smooth and successful event.</p></h5>
                       </div>
                       <div class="col-sm-3">
                          <center><img src="assets/images/b9.jpg" class="img-responsive img1" /></center><br>
                          <h4 class="text-center">Gym</h4>
                          <h5 class="text-justify"> <p>You routine need not go for a toss if you’re on a holiday or a business trip. Whether you’re an early
                          bird and want an energetic start to the day or want to sweat it out after a long day at work, our GYM
                          is the place to be. Stacked with the best equipments, it is open 24 hours and completely free for our
                          guests.</p></h5>
                       </div>
                        <div class="col-sm-3">
                          <center><img src="assets/images/spacious.jpg" class="img-responsive img1" /></center><br>
                          <h4 class="text-center">Spacious Apartments</h4>
                          <h5 class="text-justify"> <p>You routine need not go for a toss if you’re on a holiday or a business trip. Whether you’re an early
                            bird and want an energetic start to the day or want to sweat it out after a long day at work, our GYM
                            is the place to be. Stacked with the best equipments, it is open 24 hours and completely free for our
                            guests.</p></h5>
                       </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row">
                      <div class="col-sm-3">
                          <center><img src="assets/images/b10.jpg" class="img-responsive img1" /></center><br>
                          <h4 class="text-center">Conference/Banquet Hall</h4>
                       </div>
                       <div class="col-sm-3">
                          <center><img src="assets/images/b7.jpg" class="img-responsive img1" /></center><br>
                          <h4 class="text-center">Spacious Apartments </h4>
                       </div>
                       <div class="col-sm-3">
                          <center><img src="assets/images/b8.jpg" class="img-responsive img1" /></center><br>
                          <h4 class="text-center">24*7 Restaurant</h4>
                       </div>
                       <div class="col-sm-3">
                          <center><img src="assets/images/b10.jpg" class="img-responsive img1" /></center><br>
                          <h4 class="text-center">24*7 Restaurant</h4>
                       </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row">
                      <div class="col-sm-3">
                          <center><img src="assets/images/b9.jpg" class="img-responsive img1" /></center><br>
                          <h4 class="text-center">Gym</h4>
                       </div>
                       <div class="col-sm-3">
                          <center><img src="assets/images/b10.jpg" class="img-responsive img1" /></center><br>
                          <h4 class="text-center">24*7 Restaurant</h4>
                       </div>
                       <div class="col-sm-3">
                          <center><img src="assets/images/b7.jpg" class="img-responsive img1" /></center><br>
                          <h4 class="text-center">Swimming Pool</h4>
                       </div>
                       <div class="col-sm-3">
                          <center><img src="assets/images/b9.jpg" class="img-responsive img1" /></center><br>
                          <h4 class="text-center">Gym</h4>
                       </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div><br><br>
      <div class="row marg_top_100">
        <div class="col-md-10 col-md-offset-1">
          <div id="client">
    				<div class="row">
    					<div class="col-md-12">
    						<div class="section-title text-center">
    							<h1>OUR CLIENTS</h1>
    							<center><hr style="width: 50px; height: 2px; background-color: #000; margin-top: -10px;"></center>
    						</div>
    					</div>
    				</div>
  					<div id="imageCarousel" ng-non-bindable class="carousel slide" data-ride="carousel">
  						  <div class="carousel-inner">
  								<div class="item active">
  								    <div class="row">
  								       <div class="col-md-4">
  								        	<center><img src="assets/images/team.jpg" class="img-responsive" /></center>
  								       </div>
  								       <div class="col-md-4">
  								         	<center><img src="assets/images/gojeck.png" class="img-responsive" /></center>
  								       </div>
  								       <div class="col-md-4">
  								        	<center> <img src="assets/images/indian.jpg" class="img-responsive" /></center>
  								       </div>
  								    </div>
  								</div>
  								<div class="item">
  								    <div class="row">
  								       <div class="col-md-4">
  								        	<center><img src="assets/images/vodaphone.png" class="img-responsive" /></center>
  								       </div>
  								       <div class="col-md-4">
  								         	<center><img src="assets/images/dhl.png" class="img-responsive" /></center>
  								       </div>
  								       <div class="col-md-4">
  								        	<center> <img src="assets/images/volvo.png" class="img-responsive" /></center>
  								       </div>
  								    </div>
  								</div>
  								<div class="item">
  								    <div class="row">
  								       <div class="col-md-4">
  								        	<center><img src="assets/images/rand.png" class="img-responsive" /></center>
  								       </div>
  								       <div class="col-md-4">
  								         	<center><img src="assets/images/cavium.png" class="img-responsive" /></center>
  								       </div>
  								       <div class="col-md-4">
  								        	<center> <img src="assets/images/nass.png" class="img-responsive" /></center>
  								       </div>
  								    </div>
  								</div>
  							</div>
   				    </div>
      		</div>
        </div>
    	</div>
      <div class="row marg_top_100">
        <div class="">
          <div id="testimonial">
            <div class="row">
               <div class="col-md-12">
                 <div class="section-title text-center">
                   <h1>HAPPY CUSTOMERS</h1>
                   <center><hr style="width: 50px; height: 2px; background-color: #fff; margin-top: -10px;"></center>
                 </div>
               </div>
               <div class="col-md-8 col-md-offset-2">
                 <div class="carousel slide" ng-non-bindable data-ride="carousel" id="quote-carousel">
                     <div class="carousel-inner text-center">
                       <div class="item active">
                         <blockquote>
                           <div class="row">
                             <div class="col-sm-8 col-sm-offset-2">
                               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. !</p>
                               <small>Someone famous</small>
                             </div>
                           </div>
                         </blockquote>
                       </div>
                       <div class="item">
                         <blockquote>
                           <div class="row">
                             <div class="col-sm-8 col-sm-offset-2">
                               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                               <small>Someone famous</small>
                             </div>
                           </div>
                         </blockquote>
                       </div>
                       <div class="item">
                         <blockquote>
                           <div class="row">
                             <div class="col-sm-8 col-sm-offset-2">
                               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. .</p>
                               <small>Someone famous</small>
                             </div>
                           </div>
                         </blockquote>
                       </div>
                     </div>
                     <a data-slide="prev" href="#quote-carousel" class="left carousel-control" style="background:none;"></a>
                     <a data-slide="next" href="#quote-carousel" class="right carousel-control" style="background:none;"></a>
                 </div>
               </div>
            </div>
          </div>
        </div>
      </div>
      <!-- <div class="row marg_top_100">
        <div class="col-md-12">
          <div id="fh5co-blog-section">
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <div class="section-title text-center">
                    <h1>LOVE BANGALORE</h1>
                    <center><hr style="width: 50px; height: 2px; background-color: #000; margin-top: -10px;"></center>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <div class="blog-grid" style="background-image: url(assets/images/bangalore_1.jpg);">
                    <div class="date text-center">
                      <span>09</span>
                      <small>Aug</small>
                    </div>
                  </div>
                  <div class="desc">
                    <h3 class="text-center"><a href="#">Tourist Attraction</a></h3>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="blog-grid" style="background-image: url(assets/images/bangalore_4.jpg);">
                    <div class="date text-center">
                      <span>09</span>
                      <small>Aug</small>
                    </div>
                  </div>
                  <div class="desc">
                    <h3  class="text-center"><a href="#">Dining & Entertainment</a></h3>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="blog-grid" style="background-image: url(assets/images/bangalore_2.jpg);">
                    <div class="date text-center">
                      <span>09</span>
                      <small>Aug</small>
                    </div>
                  </div>
                  <div class="desc">
                    <h3  class="text-center"><a href="#">Shopping</a></h3>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="blog-grid" style="background-image: url(assets/images/Manipal-Hospital-Bangalore-india.jpg);">
                    <div class="date text-center">
                      <span>09</span>
                      <small>Aug</small>
                    </div>
                  </div>
                  <div class="desc">
                    <h3  class="text-center"><a href="#">Medical Tourism</a></h3>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> -->
    </div>
</section>
<script>

  $.datetimepicker.setLocale('en');
  $('.datetimepicker').datetimepicker({
      timepicker: false,
      format:'Y-m-d',
      minDate: 0
  });

</script>

<script>
  $(function() {
    $(window).on("scroll", function() {
        if($(window).scrollTop() >=100) {
            $(".headerIBC").addClass("scroDown");
        } else {
            //remove the background property so it comes transparent again (defined in your css)
           $(".headerIBC").removeClass("scroDown");
        }
    });
});
  </script>
