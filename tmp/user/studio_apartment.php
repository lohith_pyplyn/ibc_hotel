<br/ >
<div class="property-detail-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="">
                <ul class="bxslider property-slider">
                    <li><img style="width: 100%" src="assets/images/studio/singlerrom_featured.jpg" /></li>
                    <li><img style="width: 100%" src="assets/images/studio/Kitchen.jpg" /></li>
                    <li><img style="width: 100%" src="assets/images/studio/Washroom.jpg" /></li>
                    <!-- <li><img style="width: 100%" src="assets/images/properties/4.jpg" /></li> -->
                </ul>

                <div id="bx-pager" class="text-center hide-phone">
                    <a data-slide-index="0" href="#"><img src="assets/images/studio/singlerrom_featured.jpg" height="70" /></a>
                    <a data-slide-index="1" href="#"><img src="assets/images/studio/Kitchen.jpg" height="70" /></a>
                    <a data-slide-index="2" href="#"><img src="assets/images/studio/Washroom.jpg" height="70" /></a>
                    <!-- <a data-slide-index="3" href="#"><img src="assets/images/properties/4.jpg" height="70" /></a> -->
                </div>
            </div>
            <!-- end slider -->
            <br/ >
          </div>
    </div> <!-- end row -->
    <div class="row">
      <div class="col-md-3">
          <div class="card-box" style="margin-top: 130px;">
              <div class="table-responsive">
                  <table class="table table-bordered m-b-0">
                      <tbody>
                      <tr>
                              <th scope="row"><a class="txt-colour" href="#/studio_apartment">Studio Apartment:</a></th>
                          </tr>
                          <tr>
                              <th scope="row"><a class="txt-colour" href="#/apartment_one">One Bedroom Apartment</a></th>
                          </tr>
                          <tr>
                              <th scope="row"><a class="txt-colour" href="#/apartment_two">Two Bedroom Apartment:</a></th>
                          </tr>
                          <tr>
                              <th scope="row"><a class="txt-colour" href="#/apartment_Three">Three Bedroom Apartment:</a></th>
                          </tr>
                          <tr>
                              <th scope="row"><a class="txt-colour" href="#/apartment_four">Three/Four Bedroom Penthouse:</a></th>
                          </tr>

                      </tbody>

                  </table>
                  <ul class="social-links list-inline m-t-30">
                      <li>
                          <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="#" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
                      </li>
                      <li>
                          <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="#" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
                      </li>
                      <li>
                          <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="#" data-original-title="Skype"><i class="fa fa-skype"></i></a>
                      </li>
                  </ul>
              </div>
          </div>
          <!-- end card-box -->
      </div> <!-- end col -->
      <div class="col-md-9">
          <h2 style="padding-left: 81px;">Studio Apartment</h2><br><br>
          <h4>In-Suite Facilities</h4>
          <div class="container-fluid  apartmentthree" style="  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
          padding: 22px;
          margin-right: 143px;
          width: 116%;
          margin-left: -2px;
          padding-right: 0px;
          font-size: initial;">
 <div class="column ">

  <div class="col-sm-3" style="background-color:white;  margin-right: 11px;">
    <h4 style="color:black;">Studio Apartment</h4>
<h5>
    <li>Number of bedrooms:1</li>
    <li>Number of bathrooms: 1</li>
    <li>Number of private toilets: 1</li>
    <hr>
</h5>
   <h6> Nearby Areas</h6>
   <h5>
   <li>Manipal Hospital- 2min</li>
   <li>Indiranagar- 5min</li>
   <li>Koramangla- 15min</li>
 </h5>
<br>
     <!-- <select class="btn btn" style="width:100%; ">
    <option value="volvo">HOW TO GET HERE</option>
    <option value="saab">2</option>
    <option value="mercedes">3</option>
    <option value="audi">4</option>
</select>
<br><br>
 <select class="btn btn" style="width:100%;">
    <option value="volvo">CHECK RATES</option>
    <option value="saab">2</option>
    <option value="mercedes">3</option>
    <option value="audi">4</option>
</select> -->
</div>


 <div class="col-sm-6 aboutusContent" style="background-color:white; width: 35%; height:100%; margin-right: 12px;">
 <h4 style="color:black;">ABOUT US</h4>
 <div class="text-justify">
   <h5>
 <p>With combined living area and bedroom, our modern and sleek studio apartment is perfect for those
looking for comfortable stays. The Apartment offers Queen size bed with en suite bathroom and a
fully- equipped kitchenette.</p></h5>
  </div>
  </div>



  <div class="col-sm-3" style="background-color:white; margin-right: 12px; padding-left: 75px;">
  <h4 style="color:black;">Facilities</h4>
  <h6>Fully Equipped Kitchen</h6>

  <h5>  <li>Electric Kettle</li>
    <li>Microwave</li>
    <li> Refrigerator</li>
    <li> Tea/Coffee Maker</li>
    <li>Toaster</li>
    <li> Serveware</li><br>

</h5>
<hr>

  <h6>Suite Comforts</h6>
<h5>
<li>  LED Television</li>
<li> AC in Bedroom</li>
<li> In-room Safe</li>
<li>Iron</li>
<li>Ironing Board</li>

</h5>


    </div>



  <div class="col-sm-3" style="background-color:white;">


    </div>
  </div>

</div>
<br><br>
<br><br>

<div class="row marg_top_100 apartmentCaousel" style="">
  <div class="col-md-10 col-md-offset-1">
    <div id="imageCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="item active">
              <div class="row">
                 <div class="col-sm-3">
                    <center><img src="assets/images/apartmentcoursal/Master_Bedroom_Pent_house.jpg" class="img-responsive img1" /></center><br>
                    <h4 class="text-center">Bedroom</h4>
                 </div>
                 <div class="col-sm-3">
                    <center><img src="assets/images/apartmentcoursal/dining.jpg" class="img-responsive img1" /></center><br>
                    <h4 class="text-center">dining</h4>
                 </div>
                 <div class="col-sm-3">
                    <center><img src="assets/images/apartmentcoursal/gym.jpg" class="img-responsive img1" /></center><br>
                    <h4 class="text-center">Gym</h4>
                 </div>

                  <div class="col-sm-3">
                    <center><img src="assets/images/apartmentcoursal/Golf_Course3.jpg" class="img-responsive img1" /></center><br>
                    <h4 class="text-center">Golf</h4>
                 </div>

              </div>
          </div>
          <div class="item">
              <div class="row">
                <div class="col-sm-3">
                <center><img src="assets/images/apartmentcoursal/Pool_View.jpg" class="img-responsive img1" /></center><br>
                    <h4 class="text-center">Swimming Pool</h4>
                 </div>
                 <div class="col-sm-3">
                    <center><img src="assets/images/apartmentcoursal/Upper_Lobby_Penthouse.jpg" class="img-responsive img1" /></center><br>
                    <h4 class="text-center">Room Lobby</h4>
                 </div>
                 <div class="col-sm-3">
                 <center><img src="assets/images/apartmentcoursal/Atrium.jpg" class="img-responsive img1" /></center><br>
                    <h4 class="text-center">Atrium</h4>
                 </div>
                 <div class="col-sm-3">
                 <center><img src="assets/images/apartmentcoursal/Reception.jpg" class="img-responsive img1" /></center><br>
                    <h4 class="text-center">Reception</h4>
                 </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
      </div><br><br>
      </div> <!-- end m-t-30 -->

  </div> <!-- end col -->
<br><br>

    </div>
  </div>
  <div class="modal fade" id="userbooking-modal" data-backdrop="static" role="dialog">
  <div class="modal-dialog modal-lg">
  <div class="col-sm-12">
      <div class="m-t-30 account-pages">
          <div class="text-center account-logo-box header-bar">
              <h3 class="text-uppercase">
                  <span>Booking Form</span>
                  <button type="button" class="btn btn-default pull-right" data-dismiss="modal">x</button>
              </h3>
              <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
          </div>
          <div class="account-content">
            <form class="form-horizontal" name="userbooking_form" enctype="multipart/form-data" method="post">
              <div class="row">
                <span class="message col-sm-10 col-sm-offset-1"></span>
                <div class="col-md-12">
                    <input type="hidden" name="price" ng-model="price" value="">
                    <!-- <input type="hidden" name="reservation_no" ng-model="reservation_no" value=""> -->
                  <div class="col-md-5 col-md-offset-1">
                    <div class="form-group">

                      <label for="check_in">Check_In</label>
                      <input type="text" class="form-control" name="check_in" pickadate ng-model="userbooking.check_in" required >
                      <div ng-show="userbooking_form.check_in.$touched">
                        <p class="error" ng-show="userbooking_form.check_in.$error.required">This field is required.</p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="check_out">Check_Out</label>
                      <input type="text" class="form-control" name="check_out" pickadate ng-model="userbooking.check_out" required >
                      <div ng-show="userbooking_form.check_out.$touched">
                        <p class="error" ng-show="userbooking_form.check_out.$error.required">This field is required.</p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="room_type">Room Type</span></label>
                      <input type="hidden" name="room_type_id" ng-model="userbooking.room_type_id">
                      <input type="text" class="form-control" name="room_type" ng-model="userbooking.room_type" required >
                      <div ng-show="userbooking_form.room_type.$touched">
                        <p class="error" ng-show="userbooking_form.room_type.$error.required">This field is required.</p>
                      </div>

                    </div>
                    <div class="form-group">
                      <label for="no_of_rooms">no_of_rooms</span></label>
                      <!-- <input type="text" class="form-control" name="no_of_rooms" ng-model="userbooking.no_of_rooms" required>
                      <div ng-show="userbooking_form.no_of_rooms.$touched">
                        <p class="error" ng-show="userbooking_form.no_of_rooms.$error.required">This field is required.</p>
                      </div> -->
                      <select class="form-control" name="available_rooms1" ng-model="userbooking.available_rooms1" ng-change="update_price(userbooking.available_rooms1,userbooking.extra_beds1);">
                        <option value="" disabled>Select</option>
                        <option ng-repeat="no_ofrooms in available_rooms" value="{{no_ofrooms.id}}">{{no_ofrooms.id}}</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="promo_code">Promo Code</span></label>
                      <input type="text" class="form-control" name="promo_code" ng-model="userbooking.promo_code" required>
                      <div ng-show="userbooking_form.promo_code.$touched">
                        <p class="error" ng-show="userbooking_form.promo_code.$error.required">This field is required.</p>
                      </div>
                    </div>
                  </div> <!-- End of col-md-5 -->
                  <div class="col-md-5 col-md-offset-1">
                    <div class="form-group">
                      <label for="no_of_adults">No. of Adults</span></label>
                      <!-- <input type="text" id="no_of_adults" class="form-control" name="no_of_adults" ng-model="userbooking.no_of_adults" required>
                      <div ng-show="userbooking_form.no_of_adults.$touched"> -->
                        <!-- <p class="error" ng-show="userbooking_form.no_of_adults.$error.required">This field is required.</p>
                      </div> -->
                      <select class="form-control" name="no_of_adults1" ng-model="userbooking.no_of_adults1">
                        <option value="" disabled>Select</option>
                        <option ng-repeat="no_adult in no_of_adults" value="{{no_adult.id}}">{{no_adult.id}}</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="no_of_children">No. of children</span></label>
                      <!-- <input type="text" class="form-control" name="no_of_children" ng-model="userbooking.no_of_children" required>
                      <div ng-show="userbooking_form.no_of_children.$touched">
                        <p class="error" ng-show="userbooking_form.no_of_children.$error.required">This field is required.</p>
                      </div> -->
                      <select class="form-control" name="no_of_children1" ng-model="userbooking.no_of_children1">
                        <option value="" disabled>Select</option>
                        <option ng-repeat="no_child in no_of_children" value="{{no_child.id}}">{{no_child.id}}</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="extra_beds1">Extra Beds</span></label>
                      <!-- <input type="number" class="form-control" name="extra_beds" ng-model="userbooking.extra_beds" min="0" max="{{userbooking.extra_beds1}}" required> -->
                      <select class="form-control" name="extra_beds1" ng-model="userbooking.extra_beds1" ng-change="update_price(userbooking.available_rooms1,userbooking.extra_beds1);">
                        <option value="" disabled>Select</option>
                        <option ng-repeat="ex_bd in no_of_extras" value="{{ex_bd.id}}">{{ex_bd.id}}</option>
                      </select>
                      <div ng-show="userbooking_form.extra_beds1.$touched">
                        <p class="error" ng-show="userbooking_form.extra_beds1.$error.required">This field is required.</p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="price">Price</span></label>
                      <input type="text" id="price" class="form-control" name="price" ng-model="userbooking.price" required>
                      <div ng-show="userbooking_form.price.$touched">
                        <p class="error" ng-show="userbooking_form.price.$error.required">This field is required.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <br>


              <br><br>
              <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">
                  Cancel
                </button>
                <button type="submit" class="btn btn-primary waves-effect waves-light" ng-click="addBooking(userbooking)">
                  Submit
                </button>
              </div>
            </form>
            <div class="clearfix"></div>
          </div>
      </div>
      <!-- end wrapper -->

  </div>
  </div><!-- /.modal-dialog -->
  </div><!-- End of modal -->
  <!-- end property-detail-wrapper -->

  <script>
      $(document).ready(function () {
          $('.property-slider').bxSlider({
              pagerCustom: '#bx-pager'
          });
      });
      var map = new GMaps({
          el: '#map-property',
          lat: 40.712784,
          lng: -74.005941,
          mapTypeControlOptions: {
              mapTypeIds : ["hybrid", "roadmap", "satellite", "terrain", "osm"]
          }
      });
      map.addMarker({
          lat:  40.725015 ,
          lng: -73.881452,
          title: 'Im your custom marker',
          icon: 'assets/images/map-marker.png'
      });
  </script>
