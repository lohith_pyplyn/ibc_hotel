<section id="main-slider" class="no-margin text-center">
    <div class="carousel slide">
        <div class="carousel-inner">
            <div class="item active col-sm-12" style=" opacity: 1.5; background-repeat: no-repeat; background-size: cover; height:500px; background-image: url(assets/images/big_wine.jpg)">
                <div class="container">
                    <div class="row slide-margin">
                        <div class="col-sm-12">
                            <div class="carousel-content" style= "width: fit-content; padding: 0% 2%;   margin-left: auto; margin-right: auto;margin-top: 15%;">
                                <h2 class="animation animated-item-1 text-center" style="color:white;">STAY 3 – 5 NIGHTS – 5% </h2>
                                <h1 class="animation animated-item-2 text-center" style="color:white;">OFF</h1>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="availability_form" style="width:100%; text-align:center;">
  <form class="form-inline" name="availabilityForm" method="post" enctype="multipart/form-data">
    <div class="form-group availibility_form_group">
      <div class="input-group">
        <span class="input-group-addon">
        <span class="glyphicon glyphicon-calendar fa-lg" style="color: #99e265"></span>
        </span>
        <input  type="text" name="check_in" ng-model="avlbty.check_in" class="form-control datetimepicker checkIn" name="check_in" placeholder="Check In Date">
      </div>
   </div>
    <div class="form-group availibility_form_group">
      <div class="input-group  " >
        <span class="input-group-addon">
        <span class="glyphicon glyphicon-calendar fa-lg"  style="color: #99e265"></span>
        </span>
        <input type="text" name="check_out" ng-model="avlbty.check_out" class="form-control datetimepicker checkIn" name="check_out" placeholder="Check Out Date">
        </div>
     </div>
    <div class="form-group ">
      <button  type="button" class="btn btn-default availibility_btn" ng-click="availability(avlbty)" name="button">Search</button>
    </div>
  </form>
</div>
<div class="col-md-8 col-sm-8  wow fadeInDown col-xs-12" data-wow-duration="1000ms" data-wow-delay="300ms" ></div>
<br/ >
<div class="property-detail-wrapper">
    <div class="row">
      <div class="col-md-8">
        <h3>STAY 3 – 5 NIGHTS – 5%OFF</h3>
        <h5><p>Plan to drop by our property for 3-5 nights?</h5></p><br/ >
        <h5><p>We have a special offer for you!</h5></p><br/ >
        <ul style="margin-left: 37px;">
          <h5>
           <li>Up to [5%] off our online rates</li>
           <li>Free Breakfast</li>
           <li>[5%] off if you order food from our restaurant on orders above INR 500.</li>
         </h5>
        </ul>
        <h5><p class="mobile-align">Please Note: This offer is Subject to availability and pre-paid booking only. Prepaid bookings are
        charged in full at the time of booking and are non-refundable in the event of non-arrival. Refer to
        ‘Terms &amp; Conditions’ for cancellation policy.</p></h5>
      </div> <!-- end m-t-30 -->
      <div class="col-md-4">
        <div class="text-center card-box">
          <div class="text-center card-box">
              <div class="member-card">
                  <div class="">
                      <h4 class="m-b-5">PromoCode - IBCH05</h4>
                  </div>
                  <hr/>

                  <h4 class="">
                      5% off Best Available Online Rate
                  </h4>

                  <hr/>
                  <h4 class="">
                      Luxury Welcome Pack
                  </h4>

                  <hr/>
                  <h4 class="">
                      Free Wifi
                  </h4>
                  <hr/>
              </div>
          </div>
      </div>
  </div>
</div>
<div class="col-md-4">
		<img src="assets/images/offers/Reception.jpg" class="img-responsive">
    <div class="text-container">
    		<h3 class="text-center">STAY 3 – 5 NIGHTS – 5% OFF</h3>
    		<center><hr style="width: 50px; height: 3px; background-color: #179d67;"></center>
    		<h5><p>Are you staying between one and five nights? If you prepay you will save up to 5% if you book at ...</p></h5>
    		<center><a href="#/1-5" class="read-more">View Now</a></center>
    		<center><hr style="width: 60px; height: 1px; margin-top: 0px; background-color: #179d67;"></center>
    </div>
</div>
<div class="col-md-4">
		<img src="assets/images/offers/Pool_View.jpg" class="img-responsive">
		<div class="text-container">
					<h3 class="text-center">STAY 7 – 10 NIGHTS – 10% OFF</h3>
					<center><hr style="width: 50px; height: 3px; background-color: #179d67;"></center>
					<h5><p>If you’re coming to the Collingham Apartments for between 7 - 10 nights, book early and save up to 10% on your ...</p></h5>
          	<center><a href="#7-10" class="read-more">View Now</a></center>
					<center><hr style="width: 60px; height: 1px; margin-top: 0px; background-color: #179d67;"></center>
		</div>
</div>
<div class="col-md-4">
		<img src="assets/images/offers/Golf_Course3.jpg" class="img-responsive">
   <div class="text-container">
			<h3 class="text-center">STAY 11 – 28 NIGHTS – 15% OFF</h3>
			<center><hr style="width: 50px; height: 3px; background-color: #179d67;"></center>
			<h5><p>If you’re coming to the Collingham Apartments for between 11 and 28 nights, book early and save up to 15% on your ...</p></h5>
      <center>	<a href="#11-30" class="read-more">View Now</a></center>
			<center><hr style="width: 60px; height: 1px; margin-top: 0px; background-color: #179d67;"></center>
	</div>
</div>
</div>
<script>
  $.datetimepicker.setLocale('en');
  $('.datetimepicker').datetimepicker({
      timepicker: false,
      format:'Y-m-d',
      minDate: 0
  });
</script>
