
 <style type="text/css">
 @media(min-width: 768px) and (max-width: 1024px){
    /* #mt1{
        margin-left: 74px!important;
    } */
 }
     .fc .fc-toolbar > * > :first-child {
    margin-left: 0;
    font-size: 20px;
}
.fc .fc-toolbar > * > * {
    float: left;
    margin-left: .75em;
    font-size: 20px;
}
.TABB{
    background-color: white;
}
.pad{
    margin-top: 50px;
}
.marg{
    float: right;
}
td{
    text-align: center;
}
th{
    text-align: center;
}
.switch {
  position: relative;
  display: inline-block;
  width: 38px;
  height: 18px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #F46969;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 15px;
  width: 15px;
  left: 4px;
  bottom: 2px;
  right: 12px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
  border: 1px solid black;
}

input:checked + .slider {
  background-color: #21f224;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
 </style>
 <script>
document.getElementById("date").innerHTML = Date();
</script>

<div class="content-page">
  <div class="content" style="margin-top: -38px;">
      <div class="container">
          <div class="row" style="margin-top: 72px;">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">House Keeping</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="#/dashboard">IBC</a>
                        </li>
                        <li class="active">
                            <a>House Keeping</a>
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
            </div>

 <div class="col-md-12">
 <div class="row">
 <button type="button" class="btn btn-default">
          <span class="glyphicon glyphicon-chevron-left"></span>
        </button>
        <button type="button" class="btn btn-default">
          <span class="glyphicon glyphicon-chevron-right"></span>
        </button>
        <button type="button" class="btn btn-warning">
          <b> YESTERDAY</b>
        </button>
        <button type="button" class="btn btn-primary ">
          <b> TODAY</b>
        </button>
        <button type="button" class="btn btn-info">
          <b> TOMORROW</b>
        </button>
        <button type="button" class="btn btn-default">
                        <span class="glyphicon glyphicon-calendar"></span>
                        <b> SELECT </b>
                        </button>
                        <button type="button" class="btn btn-default marg">
                        <span class="glyphicon glyphicon-print"></span>
                        <b>PRINT</b>
                        </button>
</div>
<div class="row">
                            <div class="col-sm-12">
                                <div class="card-box pad">

                                    <h4 class="m-t-0 header-title"><b><p id="date"></p></b></h4>
                                    <p class="text-muted font-13">

                                    </p>

                                    <table class="table-bordered tablesaw table m-b-0 TABB" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                        <thead>
                                            <tr>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="3">Unit Number</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Booking Info</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">Guest</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Clean & Ready for next Guest</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td style="color:black !important">D-201</td>
                                                <td style="color:black !important">done</td>
                                                <td style="color:black !important">done</td>
                                                <td><label class="switch">
                                         <input type="checkbox">
                                            <span class="slider round"></span>
                                        </label></td>

                                            </tr>
                                            <tr>
                                                <td style="color:black !important">D-202</td>
                                                <td style="color:black !important">done</td>
                                                <td style="color:black !important">done</td>
                                                <td><label class="switch">
                                         <input type="checkbox">
                                            <span class="slider round"></span>
                                        </label></td>

                                            </tr>
                                            <tr>
                                                <td style="color:black !important">D-203</td>
                                                <td style="color:black !important">done</td>
                                                <td style="color:black !important">done</td>
                                                <td><label class="switch">
                                         <input type="checkbox">
                                            <span class="slider round"></span>
                                        </label></td>

                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

      </div>
  </div>
  <div ng-include="'tmp/footer.php'"></div>

</div>
