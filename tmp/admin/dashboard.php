<div class="content-page">
    <div class="content" style="margin-top: -38px;">
        <div class="container">
            <div class="row" style="margin-top: 72px;">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Dashboard</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="#/dashboard">BOS</a>
                            </li>
                            <li>
                                <a>Dashboard</a>
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="portlet">
                        <!-- <div class="portlet-heading card-box-green">
                            <h3 class="portlet-title">
                                Properties
                            </h3>
                            <a data-toggle="modal" data-target="#propertylist" class="btn w-md btn-rounded btn-custom waves-effect waves-light pull-right"><i class="fa fa-plus-circle fa-lg "></i> &nbsp; ADD NEW PROPERTY</a>
                            <div class="clearfix"></div>
                        </div> -->
                        <div id="bg-primary" class="panel-collapse collapse in">
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row app-green-text">
                                        <div class="col-md-2">PageSize:
                                            <select ng-model="entryLimit" class="form-control">
                                            <option>5</option>
                                            <option>10</option>
                                            <option>20</option>
                                            <option>50</option>
                                            <option>100</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">Filter:
                                            <input type="text" ng-model="search" ng-change="filter()" placeholder="Search" class="form-control" />
                                        </div>
                                        </div>
                                        <br/>
                                        <div class="panel panel-default price-panel">
                                            <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead class="thead-light">
                                                <tr class="pointer">
                                                    <th><span ng-click="sort_by('reservation_no');">reservation no</span></th>
                                                    <th><span ng-click="sort_by('check_in');">check in</span></th>
                                                    <th><span ng-click="sort_by('check_out');">check out</span></th>
                                                    <th><span ng-click="sort_by('country');">no of adults</span></th>
                                                    <th><span ng-click="sort_by('status');">no of children</span></th>
                                                    <th><span ng-click="sort_by('state');">extra bed </span></th>
                                                    <th><span ng-click="sort_by('status');">room type</span></th>
													<th><span ng-click="sort_by('city');">no of rooms</span></th>
                                                    <th><span ng-click="sort_by('state');">price</span></th>
                                                    <th><span ng-click="sort_by('country');">status</span></th>
                                                    <!-- <th>Edit</th> -->
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr ng-repeat="data in filtered = (bookings | filter:search | orderBy : predicate :reverse) | startFrom:(currentPage-1)*entryLimit | limitTo:entryLimit">
                                                    <td align="center">{{data.reservation_no}}</td>
                                                    <td align="center">{{data.check_in | date}}</td>
                                                    <td align="center" >{{data.check_out | date}}</td>
                                                    <td align="center" >{{data.no_of_adults}}</td>
                                                    <td align="center" >{{data.no_of_children}}</td>
                                                    <td align="center" >{{data.extra_beds}}</td>
                                                    <td align="center" >{{data.room_type}}</td>
                                                    <td align="center" >{{data.no_of_rooms}}</td>
                                                    <td align="center" >{{data.price}}</td>
                                                    <td align="center" >{{data.status}}</td>
                                                
                                                    <!-- <td >
                                                        <a data-toggle="modal" data-target="#edit_property_modal"  ng-click="getPropertyDetails(data);" style="font-size: 25px;"><i class="fa fa-pencil"></i></a>
                                                    </td> -->
                                                </tr>
                                                </tbody>
                                            </table>
                                            </div>
                                            <div class="col-md-12" ng-show="filteredItems == 0">
                                                <div class="col-md-12">
                                                    <h4>Not  found</h4>
                                                </div>
                                            </div>
                                            <div class="col-md-12" ng-show="filteredItems > 0">
                                                <div pagination="" page="currentPage" on-select-page="setPage(page)" boundary-links="true" total-items="filteredItems" items-per-page="entryLimit" class="pagination-small" previous-text="&laquo;" next-text="&raquo;">
                                                </div>
                                            </div>
                                        </div> <!-- End of Panel -->
                                    </div>
                                </div>  <!-- End row -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div ng-include="'tmp/footer.php'"></div>
    </div>
</div>

<!--List your property modal-->
<div class="modal fade" id="propertylist" data-backdrop="static" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="col-sm-12">
			<div class="m-t-30 account-pages">
				<div class="text-center account-logo-box header-bar">
					<h3 class="text-uppercase">
						<span>Add New Property</span>
						<button type="button" class="btn btn-default pull-right" data-dismiss="modal">x</button>
					</h3>
				</div>
				<div class="account-content">
					<form class="form-horizontal" name="property_form" enctype="multipart/form-data" method="post">
                        <div class="row">
							<span class="message col-md-10 col-md-offset-1"></span>
							<div class="col-md-5 col-md-offset-1">
								<div class="form-group">
									<label for="field-1" class="control-label">Property Name</label>
									<input type="text" class="form-control" name="property_name" ng-model="property.property_name" required>
									<div ng-show="property_form.property_name.$touched">
										<p class="error" ng-show="property_form.property_name.$error.required">This field is required.</p>
									</div>
								</div>
							</div>
							<div class="col-md-5 col-md-offset-1">
								<div class="form-group">
									<label for="field-1" class="control-label">Type of Property</label>
									<input type="text" class="form-control" name="property_type" ng-model="property.property_type" required>
									<div ng-show="property_form.property_type.$touched">
										<p class="error" ng-show="property_form.property_type.$error.required">This field is required.</p>
									</div>
								</div>
							</div> <!-- End of col-md-4 -->
							<div class="col-md-1">
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="row">
							<div class="col-md-5 col-md-offset-1">
								<div class="form-group">
									<label for="field-1" class="control-label">Adress Line 1</label>
									<input type="text" class="form-control" name="address1" ng-model="property.address1" required>
									<div ng-show="property_form.address1.$touched">
										<p class="error" ng-show="property_form.address1.$error.required">This field is required.</p>
									</div>
								</div>
								<div class="form-group">
									<label for="field-1" class="control-label">Address Line 2</label>
									<input type="text" class="form-control" name="address2" ng-model="property.address2">
								</div>
								<div class="form-group">
									<label for="field-1" class="control-label">City</label>
									<input type="text" class="form-control" name="city" ng-model="property.city" required>
									<div ng-show="property_form.city.$touched">
										<p class="error" ng-show="property_form.city.$error.required">This field is required.</p>
									</div>
								</div>
							</div>
							<div class="col-md-5 col-md-offset-1">
								<div class="form-group">
									<label for="field-1" class="control-label">State</label>
									<input type="text" class="form-control" name="state" ng-model="property.state" required>
									<div ng-show="property_form.state.$touched">
										<p class="error" ng-show="property_form.state.$error.required">This field is required.</p>
									</div>
								</div>
								<div class="form-group demo-section k-content">
									<label for="field-1" class="control-label">Country/Region</label>
									<input type="text" class="cntry-ctrl" kendo-auto-complete name="country_name" ng-model="property.country_name" k-data-source="countryList" required style="width: 100%;">
									<div ng-show="property_form.country_name.$touched">
										<p class="error" ng-show="property_form.country_name.$error.required">This field is required.</p>
									</div>
								</div>
								<div class="form-group">
									<label for="field-1" class="control-label">Zip Code</label>
									<input type="text" class="form-control" name="zipcode" ng-model="property.zipcode" required>
									<div ng-show="property_form.zipcode.$touched">
										<p class="error" ng-show="property_form.zipcode.$error.required">This field is required.</p>
									</div>
								</div>
							</div> <!-- End of col-md-4 -->
							<div class="col-md-1">
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="modal-footer">
							<div class="row pull-right">
								<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
								<button type="submit" class="btn btn-primary waves-effect waves-light" ng-disabled="property_form.$invalid" ng-click="addNewProperty(property);">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div><!-- /.modal-dialog -->
</div><!-- End of modal -->

<!--Edit your property modal-->
<div class="modal fade" id="edit_property_modal" data-backdrop="static" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="col-sm-12">
			<div class="m-t-30 account-pages">
				<div class="text-center account-logo-box header-bar">
					<h3 class="text-uppercase">
						<span>Edit Property</span>
						<button type="button" class="btn btn-default pull-right" data-dismiss="modal">x</button>
					</h3>
				</div>
				<div class="account-content">
					<form class="form-horizontal" name="property_form" enctype="multipart/form-data" method="post" ng-repeat="property in propertyDetails">
                        <div class="row">
                            <span class="message col-md-10 col-md-offset-1"></span>
                            <input type="hidden" name="id" ng-model="property.id">
							<div class="col-md-5 col-md-offset-1">
								<div class="form-group">
									<label for="field-1" class="control-label">Property Name</label>
									<input type="text" class="form-control" name="property_name" ng-model="property.property_name" required>
									<div ng-show="property_form.property_name.$touched">
										<p class="error" ng-show="property_form.property_name.$error.required">This field is required.</p>
									</div>
								</div>
							</div>
							<div class="col-md-5 col-md-offset-1">
								<div class="form-group">
									<label for="field-1" class="control-label">Type of Property</label>
									<input type="text" class="form-control" name="property_type" ng-model="property.property_type" required>
									<div ng-show="property_form.property_type.$touched">
										<p class="error" ng-show="property_form.property_type.$error.required">This field is required.</p>
									</div>
								</div>
							</div> <!-- End of col-md-4 -->
							<div class="col-md-1">
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="row">
							<div class="col-md-5 col-md-offset-1">
								<div class="form-group">
									<label for="field-1" class="control-label">Adress Line 1</label>
									<input type="text" class="form-control" name="address1" ng-model="property.address1" required>
									<div ng-show="property_form.address1.$touched">
										<p class="error" ng-show="property_form.address1.$error.required">This field is required.</p>
									</div>
								</div>
								<div class="form-group">
									<label for="field-1" class="control-label">Address Line 2</label>
									<input type="text" class="form-control" name="address2" ng-model="property.address2">
								</div>
								<div class="form-group">
									<label for="field-1" class="control-label">City</label>
									<input type="text" class="form-control" name="city" ng-model="property.city" required>
									<div ng-show="property_form.city.$touched">
										<p class="error" ng-show="property_form.city.$error.required">This field is required.</p>
									</div>
								</div>
							</div>
							<div class="col-md-5 col-md-offset-1">
								<div class="form-group">
									<label for="field-1" class="control-label">State</label>
									<input type="text" class="form-control" name="state" ng-model="property.state" required>
									<div ng-show="property_form.state.$touched">
										<p class="error" ng-show="property_form.state.$error.required">This field is required.</p>
									</div>
								</div>
								<div class="form-group demo-section k-content">
									<label for="field-1" class="control-label">Country/Region</label>
									<input type="text" class="cntry-ctrl" kendo-auto-complete name="country_name" ng-model="property.country_name" k-data-source="countryList" required style="width: 100%;">
									<div ng-show="property_form.country_name.$touched">
										<p class="error" ng-show="property_form.country_name.$error.required">This field is required.</p>
									</div>
								</div>
								<div class="form-group">
									<label for="field-1" class="control-label">Zip Code</label>
									<input type="text" class="form-control" name="zip" ng-model="property.zip" required>
									<div ng-show="property_form.zip.$touched">
										<p class="error" ng-show="property_form.zip.$error.required">This field is required.</p>
									</div>
								</div>
							</div> <!-- End of col-md-4 -->
							<div class="col-md-1">
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="modal-footer">
							<div class="row pull-right">
								<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
								<button type="submit" class="btn btn-primary waves-effect waves-light" ng-disabled="property_form.$invalid" ng-click="updateProperty(property);">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div><!-- /.modal-dialog -->
</div><!-- End of modal -->
