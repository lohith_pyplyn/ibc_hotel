<div class="content-page">
  <div class="content">
      <div class="container">
          <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Settings</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="#/dashboard">BOS</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">Settings</a>
                        </li>
                        <li class="active">
                            <a>Accommodation</a>
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
          </div>

          <div class="row">
              <div class="col-sm-12">
                <div class="portlet">
                    <div class="portlet-heading card-box-green">
                        <h3 class="portlet-title">
                            Room Types <i class="fa fa-home fa-lg"></i>
                        </h3>
                        <button type="button" class="btn w-md btn-rounded btn-custom waves-effect waves-light pull-right" data-toggle="modal" data-target="#add-room-types-modal"><i class="fa fa-plus"></i> ADD NEW</button>
                        <div class="clearfix"></div>
                    </div>
                    <div id="bg-primary" class="panel-collapse collapse in">
                        <div class="portlet-body">
                          <div class="row">
                              <div class="col-md-12">
                                <div class="panel panel-default price-panel">
                                  <div class="table-responsive">
                                    <table class="table table-bordered text-center">
                                      <thead class="thead-light">
                                        <tr class="pointer">
                                            <th class="text-center">Image</th>
                                            <th class="text-center" ng-click="sort_by1('type');"> Room Type </th>
                                            <th class="text-center" ng-click="sort_by1('description');"> Description </th>
                                            <th class="text-center" ng-click="sort_by1('no_of_rooms');"> Total Rooms </th>
                                            <th class="text-center" ng-click="sort_by1('price');"> Price </th>
                                            <th class="text-center" ng-click="sort_by1('no_of_person_allowed');"> Max Person allowed </th>
                                            <th class="text-center" ng-click="sort_by1('no_of_children_allowed');"> Max Children allowed </th>
                                            <th class="text-center" ng-click="sort_by1('no_of_extra_beds_allowed');"> Max extra beds allowed </th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                          <tr ng-repeat="data in room_types">
                                              <td>
                                                <img ng-src="uploads/room_images/{{data.image}}" ng-show="data.image" class="img-circle" style="width: 40px; height: 40px;"><br>
                                                <!-- <img ng-src="uploads/room_images/no-image.png" ng-show="!data.room_image" class="img-circle" style="width: 40px; height: 40px;"><br> -->
                                              </td>
                                              <td>{{ data.room_type }}</td>
                                              <td>{{ data.description }}</td>
                                              <td>{{ data.no_of_rooms }}</td>
                                              <td>{{ data.price }}</td>
                                              <td>{{ data.no_of_person_allowed }}</td>
                                              <td>{{ data.no_of_children_allowed }}</td>
                                              <td>{{ data.no_of_extra_beds_allowed }}</td>

                                              <td>
                                                <a class="app-green-text" data-toggle="modal" ng-click="getpricingDetails(data.id)" data-target="#edit-extra-modal" style="font-size: 25px; color: red;">
                                                <i class="fa fa-pencil">
                                                </i>
                                                </a>
                                              </td>
                                              <td>
                                                <a class="app-green-text" ng-click="deleteRoomType(data.id)" data-toggle="modal" data-target="#delete-room-type-modal" ><i class="fa fa-trash"></i></a>
                                              </td>
                                          </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                  <div class="col-md-12" ng-show="filteredItems1 == 0">
                                      <div class="col-md-12">
                                          <h4>Not  found</h4>
                                      </div>
                                  </div>
                                  <div class="col-md-12" ng-show="filteredItems1 > 0">
                                      <div pagination="" page="currentPage1" on-select-page="setPage1(page)" boundary-links="true" total-items="filteredItems1" items-per-page="entryLimit1" class="pagination-small" previous-text="&laquo;" next-text="&raquo;">
                                      </div>
                                  </div>
                                </div> <!-- End of Panel -->
                              </div>
                          </div>  <!-- End row -->
                        </div>
                    </div>
                </div>
              </div>
          </div>
      </div> <!-- container -->
      <div ng-include="'tmp/footer.php'"></div>
  </div> <!-- content -->
</div> <!-- content-page -->

<!-- Add Accomodation types modal -->

<!-- Add Room types modal -->
<div class="modal fade" id="add-room-types-modal" data-backdrop="static" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="col-sm-12">
              <div class="m-t-30 account-pages">
                <div class="text-center account-logo-box header-bar">
                    <h3 class="text-uppercase">
                        <span>Add New Room Type</span>
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">x</button>
                    </h3>
                    <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
                </div>
                <div class="account-content">
                    <form class="form-horizontal" name="room_type_form" enctype="multipart/form-data" method="post">
                      <span class="message col-md-10 col-md-offset-1"></span>
                      <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                          <div class="form-group">
                            <label for="field-1" class="control-label">Room Type</label>
                              <input type="text" class="form-control" name="room_type" ng-model="room.room_type" required autofocus>
                              <div ng-show="room_type_form.room_type.$touched">
                                <p class="error" ng-show="room_type_form.room_type.$error.required">This field is required.</p>
                              </div>
                          </div>
                          <div class="form-group">
                            <label for="field-1" class="control-label">Description</label>
                              <textarea type="text" class="form-control" name="desc" ng-model="room.desc"></textarea>
                          </div>
                          <div class="form-group">
                            <label for="field-1" class="control-label">Total Rooms</label>
                              <input type="number" class="form-control" name="no_of_rooms" ng-model="room.no_of_rooms" required>
                              <div ng-show="room_type_form.no_of_rooms.$touched">
                                <p class="error" ng-show="room_type_form.no_of_rooms.$error.required">This field is required.</p>
                              </div>
                          </div>
                          <div class="form-group">
                            <label for="field-1" class="control-label">Price</label>
                              <input type="double" class="form-control" name="price" ng-model="room.price" required>
                              <div ng-show="room_type_form.price.$touched">
                                <p class="error" ng-show="room_type_form.price.$error.required">This field is required.</p>
                              </div>
                          </div>
                          <div class="form-group">
                            <label for="field-1" class="control-label">No. of Person Allowed</label>
                              <input type="number" class="form-control" name="no_of_person_allowed" ng-model="room.no_of_person_allowed" required>
                              <div ng-show="room_type_form.no_of_person_allowed.$touched">
                                <p class="error" ng-show="room_type_form.no_of_person_allowed.$error.required">This field is required.</p>
                              </div>
                          </div>
                          <div class="form-group">
                            <label for="field-1" class="control-label">No. of Children Allowed</label>
                              <input type="number" class="form-control" name="no_of_children_allowed" ng-model="room.no_of_children_allowed">
                          </div>
                          <div class="form-group">
                            <label for="field-1" class="control-label">No. of Extra Beds Allowed</label>
                              <input type="number" class="form-control" name="no_of_extra_beds_allowed" ng-model="room.no_of_extra_beds_allowed">
                          </div>
                          <div class="form-group">
                            <label for="field-1" class="control-label">Upload Image</label>
                            <input type="file" name="room_image" class="form-control" ngf-select="" accept="*" ng-model="room_image" />
                        </div>
                          <div class="row pull-right">
                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary waves-effect waves-light" ng-disabled="room_type_form.$invalid" ng-click="addRoomType(room,room_image);">Save</button>
                          </div>
                        </div> <!-- End of col-md-4 -->
                      </div><!-- End of row -->
                    </form>
                    <div class="clearfix"></div>
                </div>
              </div>
        </div>
    </div><!-- /.modal-dialog -->
</div><!-- End of modal -->

<div class="modal fade" id="edit-extra-modal" data-backdrop="static" role="dialog">
   <div class="modal-dialog modal-md">
       <div class="col-sm-12">
           <div class="m-t-30 account-pages">
               <div class="text-center account-logo-box header-bar">
                   <h3 class="text-uppercase">
                       <span>Edit Price</span>
                       <button type="button" class="btn btn-default pull-right" data-dismiss="modal">x</button>
                   </h3>
                   <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
               </div>
               <div class="account-content">
                 <form class="form-horizontal" name="edit_extra_form" enctype="multipart/form-data" method="post" >
                   <div class="row">
                     <input type="hidden" name="id" ng-model="price.id">
                     <div class="col-md-11 col-md-offset-1">
                       <div class="form-group">
                         <label for="price" class="control-label">Price</label>
                         <input type="text" class="form-control" name="price" ng-model="price.price" ng-pattern="/^(\d+|\d+\.\d+)$/"  required>
                         <div ng-show="room_type_form.price.$touched">
                           <p class="error" ng-show="room_type_form.price.$error.required">This field is required.</p>
                           <p class="error" ng-show="room_type_form.price.$error.pattern">Invalid Pattern.</p>
                         </div>
                       </div>
                     </div>
                     <div class="col-md-1">
                     </div>
                     <div class="">
                       <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Cancel</button>
                       <button type="submit" class="btn btn-primary waves-effect waves-light btn-space" ng-disabled="edit_extra_form.$invalid" ng-click="updateprice(price);" >Save</button>
                     </div>
                   </div> <!-- End of row -->

                 </form>
                 <div class="clearfix"></div>
               </div>
           </div>
           <!-- end wrapper -->

       </div>
   </div><!-- /.modal-dialog -->
</div><!-- End of modal -->
