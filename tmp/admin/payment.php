<div class="content-page">
  <div class="content" style="margin-top: -38px;">
      <div class="container">
          <div class="row" style="margin-top: 72px;">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Payments & Invoice</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="#/dashboard">IBC</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">Payments</a>
                        </li>
                        <li class="active">
                            <a>Payments & Invoice</a>
                        </li>

                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
          </div>
          <div class="row">
              <div class="col-sm-6">
                  <div class="card-box card-box-bg" >
                    <h3>Card payments <i class="fa fa-credit-card"></i></h3>
                      <p class="color-black">Register price changes such as seasonal prices or time-limited special offers.</p>
                      <button type="button" class="btn w-md btn-bordered btn-primary waves-effect waves-light" data-toggle="modal" data-target="#add-accommodation-modal"><i class="mdi mdi-lead-pencil"></i>  EDIT</button>
                  </div>
              </div>
              <div class="col-sm-6">
                  <div class="card-box card-box-bg" >
                    <h3>Invoicing module <i class="fa fa-money"></i></h3>
                      <p class="color-black">Create invoices and cash receipts directly from bookings.</p><br>
                      <button type="button" class="btn w-md btn-bordered btn-primary waves-effect waves-light" data-toggle="modal" data-target="#add-accommodation-modal"><i class="mdi mdi-lead-pencil"></i>  EDIT</button>
                  </div>
              </div>
          </div>
          <!-- End row -->

          <div class="row">
              <div class="col-sm-6">
                  <div class="card-box card-box-bg" style="padding-bottom: 25px;">
                    <h3>Surcharges <i class="fa fa-calculator"></i></h3>
                      <p class="color-black">Create percentage surcharges that will be added on top of stated prices, such as sales or service taxes.</p><br><br>
                      <button type="button" class="btn w-md btn-bordered btn-primary waves-effect waves-light " data-toggle="modal" data-target="#add-accommodation-modal"><i class="mdi mdi-lead-pencil"></i>  EDIT</button>
                </div>
              </div>
              <div class="col-sm-6">
                  <div class="card-box card-box-bg" >
                    <h3>Taxes included <i class="fa fa-bank"></i></h3>
                      <p class="color-black">Specify taxes that are included in the stated prices, e g VAT or GST. This information will be used if you create invoices or use TripAdvisor Instant Booking. After creating tax rates, edit the room types and extra options, and choose the tax rate that applies.</p>
                      <button type="button" class="btn w-md btn-bordered btn-primary waves-effect waves-light " data-toggle="modal" data-target="#add-accommodation-modal"><i class="mdi mdi-lead-pencil"></i>  ADD NEW TAX</button>
                  </div>
                </div>
          </div>
        </div>
      </div>
    </div>
    <div ng-include="'tmp/footer.php'"></div>
