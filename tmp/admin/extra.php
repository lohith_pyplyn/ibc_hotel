<div class="content-page">
 <div class="content" style="margin-top: -38px;">
     <div class="container">
         <div class="row" style="margin-top: 72px;">
           <div class="col-xs-12">
               <div class="page-title-box">
                   <h4 class="page-title">Extras</h4>
                   <ol class="breadcrumb p-0 m-0">
                       <li>
                           <a href="#/dashboard">IBC</a>
                       </li>
                       <li class="active">
                           <a>Extras</a>
                       </li>

                   </ol>
                   <div class="clearfix"></div>
               </div>
           </div>
         </div>
         <div class="row">
             <div class="col-sm-12">
               <div class="portlet">
                 <div class="portlet-heading card-box-green">
                   <h3 class="portlet-title">
                     Extras
                     <i class="fa fa-shopping-cart"></i>
                   </h3>
                   <div class="clearfix">
                   </div>
                 </div>
                 <div id="bg-primary" class="panel-collapse collapse in">
                   <div class="portlet-body">
                     <button type="button" class="btn w-md btn-bordered btn-primary waves-effect waves-light pull-right" data-toggle="modal" data-target="#add-extra-modal">
                       <i class="fa fa-plus"></i> ADD NEW
                     </button>
                     <p>Register extra options that may be selected by the guest when booking, for example breakfast, bed linens, etc. </p>
                   </div> <!-- Portlet body -->
                 </div>
               </div> <!-- Portlet ends -->
             </div>
         </div>
         <div class="row">
           <div class="col-md-12">
             <div class="card-box card-box-bg">
               <div class="row">
                 <div class="row app-green-text">
                     <div class="col-md-2">PageSize:
                         <select ng-model="entryLimit" class="form-control">
                             <option>5</option>
                             <option>10</option>
                             <option>20</option>
                             <option>50</option>
                             <option>100</option>
                         </select>
                     </div>
                     <div class="col-md-3">Filter:
                         <input type="text" ng-model="search" ng-change="filter()" placeholder="Search" class="form-control" />
                     </div>
                 </div>
                 <br/>
                 <div class="table-responsive">
                     <table class="table">
                         <thead>
                             <tr class="text-custom app-green-text pointer">
                                 <th>
                                   <span ng-click="sort_by('id');">ID</span>
                                 </th>
                                 <th>
                                   <span ng-click="sort_by();">Name</span>
                                 </th>
                                 <th>
                                   <span ng-click="sort_by();">
                                   Description</span>
                                 </th>
                                 <th>
                                   <span ng-click="sort_by();">Price</span>
                                 </th>
                                 <th>
                                   <span ng-click="sort_by();">Status</span>
                                 </th>
                                 <th></th>
                                 <th></th>
                                 <th></th>
                             </tr>
                         </thead>
                         <tbody>
                             <tr ng-repeat="data in filtered = (extraOptions | filter:search | orderBy : predicate :reverse) | startFrom:(currentPage-1)*entryLimit | limitTo:entryLimit">
                                 <td class="text-center">{{ data.id }}</td>
                                 <td class="text-center"><strong>{{ data.name }}</strong></td>
                                 <td class="text-center">{{ data.description }}</td>
                                 <td class="text-center">{{ data.price }}</td>
                                 <td class="text-center" ng-if="data.status == 1">Available</td>
                                 <td class="text-center" ng-if="data.status != 1">Not Available</td>
                                 <td class="text-center">
                                   <a class="app-green-text" data-toggle="modal" ng-click="getExtraDetails(data.id)" data-target="#edit-extra-modal" style="font-size: 25px; color: red;">
                                   <i class="fa fa-pencil">
                                   </i>
                                   </a>
                                 </td>
                                 <td>
                                   <a data-toggle="modal" data-target="#delete-extra-modal" ng-click="getExtraDetails(data.id);" style="font-size: 25px; color: red;">
                                     <i class="fa fa-trash"></i>
                                   </a>
                                 </td>
                             </tr>
                         </tbody>
                     </table>
                 </div>
                 <div class="col-md-12" ng-show="filteredItems == 0">
                     <div class="col-md-12">
                         <h4>Not  found</h4>
                     </div>
                 </div>
                 <div class="col-md-12" ng-show="filteredItems > 0">
                     <div pagination="" page="currentPage" on-select-page="setPage(page)" boundary-links="true" total-items="filteredItems" items-per-page="entryLimit" class="pagination-small" previous-text="&laquo;" next-text="&raquo;">
                     </div>
                 </div>
               </div>
           </div>
         </div>
     </div>
 </div>
 <div ng-include="'tmp/footer.php'"></div>
</div>


<!-- Add Extra Option Modal -->
<div class="modal fade" id="add-extra-modal" data-backdrop="static" role="dialog">
   <div class="modal-dialog modal-lg">
       <div class="col-sm-12">
           <div class="m-t-30 account-pages">
               <div class="text-center account-logo-box header-bar">
                   <h3 class="text-uppercase" style="color: white;">
                       <span>Add Extra Option</span>
                       <button type="button" class="btn btn-default pull-right" data-dismiss="modal">x</button>
                   </h3>
               </div>
               <div class="account-content">
                 <form class="form-horizontal" name="add_extra_form" enctype="multipart/form-data" method="post">
                   <div class="row">
                     <div class="col-md-4 col-md-offset-1">
                       <div class="form-group">
                         <label for="name">Name</label>
                         <input type="text" class="form-control" name="name" ng-model="extra.name" placeholder="Name" required>
                         <div ng-show="add_extra_form.name.$touched">
                           <p class="error" ng-show="add_extra_form.name.$error.required">This field is required.</p>
                         </div>
                       </div>
                       <div class="form-group">
                         <label for="description">
                           Description
                         </label>
                         <textarea class="form-control" name="description" ng-model="extra.description" placeholder="Description">
                         </textarea>
                       </div>
                       <div class="form-group">
                         <label for="price" class="control-label">
                           Price
                         </label>
                         <input type="text" class="form-control" name="price" ng-model="extra.price" ng-pattern="/^(\d+|\d+\.\d+)$/" placeholder="Price" required>
                         <div ng-show="add_extra_form.price.$touched">
                           <p class="error" ng-show="add_extra_form.price.$error.required">This field is required.</p>
                           <p class="error" ng-show="add_extra_form.price.$error.pattern">Invalid Pattern.</p>
                         </div>
                       </div>
                       <div class="form-group">
                         <label for="status" class="control-label">Status</label>
                         <select class="form-control" name="sort_order" ng-model="extra.status">
                           <option value="">Select</option>
                           <option value="1">Available</option>
                           <option value="0">Not Available</option>
                         </select>
                       </div>
                     </div>
                     <div class="col-md-1">
                     </div>
                     <div class="col-md-6">
                       <div class="card-box card-box-bg">
                         <h1>
                           <i class=" mdi mdi-help-circle app-green-text"></i>
                         </h1>
                         <p>
                           Add extra options such as breakfast, bed linens, fruit baskets etc.
                         </p>
                         <p>
                           State unit price and how the suggested amount should be calculated. Choose if the suggested amount should be editable or fixed and indicate if the availability is limited by time or amount.
                         </p>
                         <p>
                           The field Object type makes sure that the object is described correctly in the booking process.
                         </p>
                         <p>
                           Enter text for all supported languages​​. Basic translation can be done for free at
                           <a href="https://translate.google.com/" target="_blank"> translate.google.com</a>.
                           The English text will be used whenever a translation is missing.
                         </p>
                       </div>
                     </div>
                   </div>
                   <div class="col-md-4 col-md-offset-3 btn-top-space">
                       <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">
                         Cancel
                       </button>
                       <button type="submit" class="btn btn-primary waves-effect waves-light btn-space" ng-disabled="add_extra_form.$invalid" ng-click="addNewExtra(extra)">
                         Save
                       </button>
                   </div>
                 </form>
                 <div class="clearfix"></div>
               </div>
           </div>
           <!-- end wrapper -->
       </div>
   </div><!-- /.modal-dialog -->
</div><!-- End of modal -->

<!-- Edit Extra Option Modal -->
<div class="modal fade" id="edit-extra-modal" data-backdrop="static" role="dialog">
   <div class="modal-dialog modal-lg">
       <div class="col-sm-12">
           <div class="m-t-30 account-pages">
               <div class="text-center account-logo-box header-bar">
                   <h3 class="text-uppercase">
                       <span>Edit Extra Option</span>
                       <button type="button" class="btn btn-default pull-right" data-dismiss="modal">x</button>
                   </h3>
                   <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
               </div>
               <div class="account-content">
                 <form class="form-horizontal" name="edit_extra_form" enctype="multipart/form-data" method="post" ng-repeat="option in extraDetails">
                   <div class="row">
                     <input type="hidden" name="id" ng-model="option.id">
                     <div class="col-md-4 col-md-offset-1">
                       <div class="form-group">
                         <label for="name" class="control-label">Name</label>
                         <input type="text" class="form-control" name="name" ng-model="option.name" placeholder="" required>
                         <div ng-show="edit_extra_form.name.$touched">
                           <p class="error" ng-show="edit_extra_form.name.$error.required">This field is required.</p>
                         </div>
                       </div>
                       <div class="form-group">
                           <label for="description" class="control-label">Description</label>
                           <textarea class="form-control" name="description" ng-model="option.description" ></textarea>
                       </div>
                       <div class="form-group">
                         <label for="price" class="control-label">Price</label>
                         <input type="text" class="form-control" name="price" ng-model="option.price" ng-pattern="/^(\d+|\d+\.\d+)$/" placeholder="Ex: 0.00" required>
                         <div ng-show="edit_extra_form.price.$touched">
                           <p class="error" ng-show="edit_extra_form.price.$error.required">This field is required.</p>
                           <p class="error" ng-show="edit_extra_form.price.$error.pattern">Invalid Pattern.</p>
                         </div>
                       </div>
                       <div class="form-group">
                         <label for="status">Status</label>
                         <select class="form-control" name="status" ng-model="option.status">
                           <option value="">Select</option>
                           <option value="1">Available</option>
                           <option value="0">Not Available</option>
                         </select>
                       </div>
                     </div>
                     <div class="col-md-1">
                     </div>
                     <div class="col-md-6">
                       <div class="card-box card-box-bg">
                         <h1>
                           <i class=" mdi mdi-help-circle app-green-text"></i>
                         </h1>
                         <p>
                           Edit extra options such as breakfast, bed linens, fruit baskets etc.
                         </p>
                         <p>
                           State unit price and how the suggested amount should be calculated. Choose if the suggested amount should be editable or fixed and indicate if the availability is limited by time or amount.
                         </p>
                         <p>
                           The field Object type makes sure that the object is described correctly in the booking process.
                         </p>
                         <p>
                           Enter text for all supported languages​​. Basic translation can be done for free at
                           <a href="https://translate.google.com/" target="_blank"> translate.google.com</a>.
                           The English text will be used whenever a translation is missing.
                         </p>
                       </div>
                     </div>
                     <div class="col-md-3 col-md-offset-4 btn-top-space">
                       <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Cancel</button>
                       <button type="submit" class="btn btn-primary waves-effect waves-light btn-space" ng-disabled="edit_extra_form.$invalid" ng-click="updateExtraOption(option);" >Save</button>
                     </div>
                   </div> <!-- End of row -->

                 </form>
                 <div class="clearfix"></div>
               </div>
           </div>
           <!-- end wrapper -->

       </div>
   </div><!-- /.modal-dialog -->
</div><!-- End of modal -->

<!-- Delete Extra Option Modal -->
<div id="delete-extra-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
 <div class="modal-dialog">
   <div class="modal-content">
     <div class="modal-body">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
       <p class="text-center" style="font-size:80px;"><i class="fa fa-exclamation-triangle"></i></p>
         <div class="row text-center">
             <div class="col-md-12">
                 <span class="col-sm-12 success text-center"></span>
                 <span class="col-sm-12 error text-center"></span>
                 <h3 class="color-black">Do you want to delete this extra option?</h3>
             </div>
         </div>
         <div class="modal-footer">
           <div class="col-md-10 col-md-offset-2" style="top: 10px;">
             <!-- <button type="button" class="btn w-md btn-bordered btn-primary waves-effect waves-light"><i class="mdi mdi-lead-pencil"></i> &nbsp;&nbsp;EDIT</button> -->
             <button type="button" class="btn w-md btn-bordered btn-primary waves-effect waves-light" ng-click="deleteExtraOption(extraId)"></i> &nbsp;&nbsp;OK</button>
             <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
           </div>
         </div>
     </div>
   </div>
 </div>
</div>
