<div ng-controller="HeaderController">
  <div class="topbar">
      <div class="topbar-left">
          <a href="#/" class="logo">
              <span>
                  <img src="assets/images/logo.png" alt="" style="width: 100%; height: 65px;">
              </span>
              <i>
                  <img src="assets/images/logo.png" alt="" style="width: 100%;" >
              </i>
          </a>
      </div>
      <div class="navbar navbar-default" role="navigation">
          <div class="container">
              <ul class="nav navbar-nav navbar-left">
                  <li>
                      <button class="button-menu-mobile open-left waves-effect">
                          <i class="mdi mdi-menu"></i>
                      </button>
                  </li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                  <li class="dropdown user-box">
                      <a href="#" class="dropdown-toggle waves-effect user-link" data-toggle="dropdown" aria-expanded="true">
                          <img ng-src="assets/images/users/avatars/avatar_default2.png" alt="user-img" class="img-circle user-img">
                      </a>

                      <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list" >
                          <li>
                              <h5>Hi, Admin</h5>
                          </li>
                          <li><a href="#/profile"><i class="glyphicon glyphicon-user m-r-5"></i> Profile</a></li>
                          <li><a href="#/change-password"><i class="glyphicon glyphicon-cog"></i> Change Password</a></li>
                          <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0);" ng-click="logout();" <i class="glyphicon glyphicon-log-out"> </i> Logout</a></li>
                      </ul>
                  </li>
              </ul>

          </div>
      </div>
  </div>
  <!-- Top Bar End -->

  <!-- ========== Left Sidebar Start ========== -->
  <div class="left side-menu">
      <div class="sidebar-inner slimscrollleft">
          <div id="sidebar-menu">
              <div class="user-details">
                  <div class="overlay"></div>
                  <div class="text-center">
                      <img ng-src="assets/images/users/avatars/avatar_default2.png" alt="user-img" class="thumb-md img-circle" style="width:75px; height: 75px; margin-bottom: -10px;">
                  </div>
                  <div class="user-info">
                      <div>
                          <a href="#setting-dropdown" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">{{first_name}} {{last_name}}<span class="mdi mdi-menu-down"></span></a>
                      </div>
                  </div>
              </div>

              <ul>
                  <li class="menu-title">Navigation</li>
                  <li class="has_sub">
                      <a href="#/accomodation" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Accomodation </span> </a>
                  </li>

                  <li class="has_sub">
                      <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-settings"></i> <span>Settings</span> <span class="menu-arrow"></span></a>
                      <ul class="list-unstyled">
                          <li><a href="#/profile">Profile</a></li>
                          <li><a href="#/change-password">Change Password</a></li>
                      </ul>
                  </li>

                  <li>
                      <a href="javascript:void(0);" class="waves-effect" ng-click="logout();"><i class="mdi mdi-logout"></i> Logout</a>
                  </li>
              </ul>

              <!-- Sidebar -->
              <div class="clearfix"></div>

              <!-- <div class="help-box">
                  <h5 class="text-muted m-t-0">For Help ?</h5>
                  <p class=""><span class="text-custom">Email:</span> <br/> lohithkumar17@gmail.com</p>
                  <p class="m-b-0"><span class="text-custom">Call:</span> <br/>+91 - 9164800684</p>
              </div> -->
          </div>

      </div>
  </div>

</div>
