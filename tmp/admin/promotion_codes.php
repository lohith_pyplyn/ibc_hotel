<div class="content-page">
  <div class="content"  style="margin-top: -38px;">
      <div class="container">
          <div class="row" style="margin-top: 72px;">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Promotion Codes</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="#/dashboard">IBC</a>
                        </li>

                        <li class="active">
                            <a>Promoion Codes</a>
                        </li>

                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
          </div> <!-- end of row -->
          <div class="row">
            <div class="col-sm-12">
              <div class="row">
                <div class="card-box card-box-bg">
                  <h3> Promotion & discount codes <i class="fa fa-ticket"></i>
                    <button type="button" class="btn w-md btn-bordered btn-primary waves-effect waves-light pull-right" data-toggle="modal" data-target="#promotion-modal" ng-click="generatePromotionCode();"><i class="fa fa-plus"></i> CREATE NEW CODE</button>
                  </h3>
                  <p class="color-black">Give your guests a discount with promotion codes.</p>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="row app-green-text">
                          <div class="col-md-2">PageSize:
                              <select name="entryLimit" ng-model="entryLimit" class="form-control">
                                  <option>5</option>
                                  <option>10</option>
                                  <option>20</option>
                                  <option>50</option>
                                  <option>100</option>
                              </select>
                          </div>
                          <div class="col-md-3">Filter:
                              <input type="text" ng-model="search1" ng-change="filter()" placeholder="Search" class="form-control" />
                          </div>
                      </div>
                      <br/>
                      <div class="panel panel-default price-panel">
                        <div class="table-responsive">
                          <table class="table table-bordered text-center">
                            <thead class="thead-light">
                              <tr class="pointer">
                                  <th class="text-center" ng-click="sort_by('promotion_code');"> Promotion Code </th>
                                  <th class="text-center"> Description </th>
                                  <th class="text-center"> Period </th>
                                  <th class="text-center"> Minimum booking price</th>
                                  <th class="text-center"> Offer Discount Price</th>
                                  <th class="text-center"></th>
                              </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="data in filtered = (promotionCodes | filter:search1 | orderBy : predicate1 :reverse1) | startFrom:(currentPage1-1)*entryLimit1 | limitTo:entryLimit1">
                                    <td>{{ data.promotion_code }}</td>
                                    <td>{{ data.description }}</td>
                                    <td>{{ data.start_date | date : "dd/MM/y" }} - {{ data.end_date | date : "dd/MM/y" }}</td>
                                    <td>{{ data.minimum_booking_price}}</td>
                                    <td>{{ data.offer_discount_price}}</td>
                                    <td>
                                      <a class="app-green-text" data-toggle="modal" ng-click="storePromotionID(data.id)" data-target="#delete-promotion-modal"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                          </table>
                        </div>
                        <div class="col-md-12" ng-show="filteredItems1 == 0">
                            <div class="col-md-12">
                                <h4>Not  found</h4>
                            </div>
                        </div>
                        <div class="col-md-12" ng-show="filteredItems1 > 0">
                            <div pagination="" page="currentPage1" on-select-page="setPage1(page)" boundary-links="true" total-items="filteredItems1" items-per-page="entryLimit1" class="pagination-small" previous-text="&laquo;" next-text="&raquo;">
                            </div>
                        </div>
                      </div> <!-- End of Panel -->
                    </div>
                  </div>

                </div> <!-- End of card -->
              </div>
            </div>
            <div ng-include="'tmp/footer.php'"></div>

        </div><!-- End row -->
        <div class="modal fade" id="promotion-modal" data-backdrop="static" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="col-sm-12">
            <div class="m-t-30 account-pages">
                <div class="text-center account-logo-box header-bar">
                    <h3 class="text-uppercase">
                        <span>Promotion and Discount codes</span>
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">x</button>
                    </h3>
                    <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
                </div>
                <div class="account-content">
                  <form class="form-horizontal" name="promotion_form" enctype="multipart/form-data" method="post">
                    <div class="row">
                      <span class="message col-sm-10 col-sm-offset-1"></span>
                      <div class="col-md-12">
                        <div class="col-md-5 col-md-offset-1">
                          <div class="form-group">
                            <label for="promotion_name">Promotion code</span></label>
                            <input type="text" id="promotion_code" class="form-control" name="promotion_code" ng-model="promotion_code" required>
                            <div ng-show="promotion_form.promotion_code.$touched">
                              <p class="error" ng-show="promotion_form.promotion_code.$error.required">This field is required.</p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="description">Description</span></label>
                            <input type="text" class="form-control" name="description" ng-model="promotion.description" required>
                            <div ng-show="promotion_form.description.$touched">
                              <p class="error" ng-show="promotion_form.description.$error.required">This field is required.</p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="Minimum_booking_price">Minimum booking price </span></label>
                            <input type="text" class="form-control" name="Minimum_booking_price" ng-model="promotion.Minimum_booking_price" required>
                            <div ng-show="promotion_form.Minimum_booking_price.$touched">
                              <p class="error" ng-show="promotion_form.Minimum_booking_price.$error.required">This field is required.</p>
                            </div>
                          </div>
                        </div> <!-- End of col-md-5 -->
                        <div class="col-md-5 col-md-offset-1">
                          <div class="form-group">
                            <label for="start_from">From</label>
                            <input type="text" class="form-control" name="start_from" pickadate ng-model="promotion.start_from" required >
                            <div ng-show="promotion_form.start_from.$touched">
                              <p class="error" ng-show="promotion_form.start_from.$error.required">This field is required.</p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="end">To</label>
                            <input type="text" class="form-control" name="end" pickadate ng-model="promotion.end" required >
                            <div ng-show="promotion_form.end.$touched">
                              <p class="error" ng-show="promotion_form.end.$error.required">This field is required.</p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="Offer_Discount_Price">Offer discount price (Upto)</span></label>
                            <input type="text" class="form-control" name="Offer_Discount" ng-model="promotion.Offer_Discount_Price" required>
                            <div ng-show="promotion_form.Offer_Discount_Price.$touched">
                              <p class="error" ng-show="promotion_form.Offer_Discount_Price.$error.required">This field is required.</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <br>


                    <br><br>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">
                        Cancel
                      </button>
                      <button type="submit" class="btn btn-primary waves-effect waves-light" ng-disabled="promotion_form.$invalid" ng-click="addNewPromotionCode(promotion)">
                        Save
                      </button>
                    </div>
                  </form>
                  <div class="clearfix"></div>
                </div>
            </div>
            <!-- end wrapper -->

        </div>
    </div><!-- /.modal-dialog -->
</div><!-- End of modal -->
<div id="delete-promotion-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <p class="text-center" style="font-size:80px;"><i class="fa fa-exclamation-triangle"></i></p>
          <div class="row text-center">
              <div class="col-md-12">
                  <span class="col-sm-12 message text-center"></span>
                  <h3 class="color-black">Do you want to delete this promotion code?</h3>
              </div>
          </div>
          <div class="modal-footer">
            <div class="col-md-10 col-md-offset-2" style="top: 10px;">
              <button type="button" class="btn w-md btn-bordered btn-primary waves-effect waves-light" ng-click="deletePromotionCode(selectedPromotionID)"></i> &nbsp;&nbsp;OK</button>
              <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
