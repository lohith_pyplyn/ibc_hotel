<div class="content-page">
  <div class="content">
      <div class="container">
          <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Profile</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="#/dashboard">BOS</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">Settings</a>
                        </li>
                        <li class="active">
                            <a>Profile</a>
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
          </div>
        <div class="row">
            <div class="col-md-12">
                  <div class="card-box">
                    <ul class="nav nav-tabs tabs-bordered">
                        <li class="active">
                            <a data-target="#home-b2" data-toggle="tab" aria-expanded="false">
                                <span class="visible-xs"><i class="fa fa-user"></i></span>
                                <span class="hidden-xs">Profile</span>
                            </a>
                        </li>
                        <!-- <li class="">
                            <a data-target="#profile-b2" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs"><i class="fa fa-credit-card"></i></span>
                                <span class="hidden-xs">Card Details</span>
                            </a>
                        </li> -->
                        <li class="">
                            <a data-target="#messages-b2" data-toggle="tab" aria-expanded="false">
                                <span class="visible-xs"><i class="fa fa-key"></i></span>
                                <span class="hidden-xs">Password</span>
                            </a>
                        </li>
                        <!-- <li class="">
                            <a data-target="#settings-b2" data-toggle="tab" aria-expanded="false">
                                <span class="visible-xs"><i class="fa fa-sign-in"></i></span>
                                <span class="hidden-xs">Socia Connections</span>
                            </a>
                        </li> -->
                    </ul>
                    <div class="tab-content">
                      <div class="tab-pane active" id="home-b2">
                        <div class="portlet">
                          <div id="bg-primary" class="panel-collapse collapse in">
                            <div class="portlet-body">
                             <span class="message col-sm-6 col-sm-offset-3"></span>
                              <form class="form-horizontal" name="add_agent_form" method="post">
                                <div class="row">
                                  <div class="col-md-3 col-md-offset-1">
                                    <div class="form-group">
                                        <img ng-src="userprofile.jpg" alt="" class="img-circle" width="100%" height="80%"><br><br><br>
                                        <label for="field-1" class="control-label">Profile Image</label>
                                        <input type="file" name="profile_pic" class="form-control" ngf-select="" accept="*" ng-model="profile_pic" />
                                    </div>
                                  </div>
                                  <div class="col-md-4 col-md-offset-2">
                                    <div class="form-group">
                                        <label for="field-1" class="control-label">First Name</label>
                                        <input type="text" name="first_name" ng-model="agent.first_name" class="form-control" required>
                                        <div ng-show="add_agent_form.first_name.$touched">
                                          <p class="error" ng-show="add_agent_form.first_name.$error.required">First name is required.</p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                      <label for="field-1" class="control-label">Last Name</label>
                                      <input type="text" name="last_name" ng-model="agent.last_name" class="form-control">
                                    </div>
                                    <div class="form-group">
                                      <label for="field-1" class="control-label">Phone</label>
                                      <input type="text" name="phone" ng-model="agent.phone" class="form-control" ng-pattern="/^[0-9]+$/">
                                      <div ng-show="add_agent_form.phone.$touched">
                                        <p class="error" ng-show="add_agent_form.phone.$error.pattern">Invalid Phone number.</p>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="field-1" class="control-label">Address</label>
                                        <textarea type="text" name="address" ng-model="agent.address" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">
                                      <div class="col-md-4">
                                        <label for="field-1" class="control-label">City</label>
                                        <input type="text" name="city" ng-model="agent.city" class="form-control">
                                      </div>
                                      <div class="col-md-4">
                                          <label for="field-1" class="control-label">State</label>
                                          <input type="text" name="state" ng-model="agent.state" class="form-control">
                                      </div>
                                      <div class="col-md-4">
                                          <label for="field-1" class="control-label">Country</label>
                                          <input type="text" name="country" ng-model="agent.country" class="form-control">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label for="field-1" class="control-label">Pincode</label>
                                      <input type="text" name="pin_code" ng-model="agent.pin_code" ng-pattern="/^[0-9]+$/" class="form-control">
                                      <div ng-show="add_agent_form.pin_code.$touched">
                                        <p class="error" ng-show="add_agent_form.pin_code.$error.pattern">Invalid Pincode.</p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="modal-footer">
                                  <button type="submit" class="btn btn-primary waves-effect waves-light" ng-disabled="add_agent_form.$invalid" ng-click="updateProfile(agent,profile_pic);">Update</button>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane" id="profile-b2">
                        <button type="button" class="btn w-md btn-rounded pswbtn waves-effect waves-light pull-right" data-toggle="modal" data-target="#add-accommodation-type-modal"><i class="pswcolor fa fa-plus"></i>&nbsp;Add New Card </button><br/ >
                        <br/ >
                        <div class="row" style="margin-top:10px;">
                            <div class="col-sm-12">
                                <div class="panel panel-default price-panel table-responsive">
                                    <table class="table  table-bordered text-center">
                                        <thead class="thead-light">
                                            <tr>
                                                <th class="text-center col-sm-2 bgcolor">ID</th>
                                                <th class="text-center col-sm-1 bgcolor">Owner</th>
                                                <th class="text-center col-sm-2 bgcolor">Card Number </th>
                                                <th class="text-center col-sm-2 bgcolor">CVV </th>
                                                <th class="text-center col-sm-2 bgcolor">Expiration Date </th>
                                                <!-- <th class="text-center col-sm-2 bgcolor">Booking Number </th>
                                                <th class="text-center col-sm-1 bgcolor">Amount </th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>User name</td>
                                                <td>12345678912345</td>
                                                <td>232</td>
                                                <td>8-2018</td>
                                                <!-- <td>4</td>
                                                <td>5</td> -->
                                            </tr>
                                        </tbody>
                                    </table> <!-- End of first table -->
                                </div> <!-- End of pannel -->
                            </div>
                        </div> <!-- row -->
                      </div>
                      <!-- Add Accomodation types modal -->
                      <div class="modal fade" id="add-accommodation-type-modal" data-backdrop="static" role="dialog">
                          <div class="modal-dialog modal-md">
                              <div class="col-sm-12">
                                    <div class="m-t-30 account-pages">
                                      <div class="text-center account-logo-box header-bar">
                                          <h3 class="text-uppercase">
                                              <span>ADD CARD DETAILS</span>
                                              <button type="button" class="btn btn-default pull-right" data-dismiss="modal">x</button>
                                          </h3>
                                          <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
                                      </div>
                                      <div class="account-content">
                                          <form class="form-horizontal" name="accommodation_type_form" enctype="multipart/form-data" method="post">
                                            <span class="message col-md-10 col-md-offset-1"></span>
                                            <!-- <div class="row"> -->
                                              <div class="col-md-10 col-md-offset-1">
                                                <!-- <div class="creditCardForm"> -->
                                                  <div class="heading">
                                                      <!-- <h1>ADD CARD DETAILS</h1> -->
                                                  </div>
                                                  <div class="payment">
                                                      <form>
                                                          <div class="form-group owner">
                                                              <label for="owner">Owner</label>
                                                              <input type="text" class="form-control" id="owner">
                                                          </div>
                                                          <div class="form-group CVV">
                                                              <label for="cvv">CVV</label>
                                                              <input type="text" class="form-control" id="cvv">
                                                          </div>
                                                          <div class="form-group" id="card-number-field">
                                                              <label for="cardNumber">Card Number</label>
                                                              <input type="text" class="form-control" id="cardNumber">
                                                          </div>
                                                          <div class="form-group" id="expiration-date">
                                                              <label>Expiration Date</label>
                                                              <select>
                                                                  <option value="01">January</option>
                                                                  <option value="02">February </option>
                                                                  <option value="03">March</option>
                                                                  <option value="04">April</option>
                                                                  <option value="05">May</option>
                                                                  <option value="06">June</option>
                                                                  <option value="07">July</option>
                                                                  <option value="08">August</option>
                                                                  <option value="09">September</option>
                                                                  <option value="10">October</option>
                                                                  <option value="11">November</option>
                                                                  <option value="12">December</option>
                                                              </select>
                                                              <select>
                                                                  <option value="16"> 2016</option>
                                                                  <option value="17"> 2017</option>
                                                                  <option value="18"> 2018</option>
                                                                  <option value="19"> 2019</option>
                                                                  <option value="20"> 2020</option>
                                                                  <option value="21"> 2021</option>
                                                              </select>
                                                          </div>
                                                          <div class="form-group" id="credit_cards">
                                                              <img src="assets/images/visa.jpg" id="visa">
                                                              <img src="assets/images/mastercard.jpg" id="mastercard">
                                                              <img src="assets/images/amex.jpg" id="amex">
                                                          </div>
                                                          <!-- <div class="form-group" id="pay-now">
                                                              <button type="submit" class="btn btn-default" id="confirm-purchase">Confirm</button>
                                                          </div> -->
                                                      </form>
                                                  </div>
                                              <!-- </div> -->
                                                <div class="row pull-right">
                                                  <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                                  <button type="submit" class="btn btn-primary waves-effect waves-light" ng-disabled="accommodation_type_form.$invalid" ng-click="addAccommodationType(accommodation);">Save</button>
                                                </div>
                                              </div> <!-- End of col-md-4 -->
                                            <!-- </div> End of row -->
                                          </form>
                                          <div class="clearfix"></div>
                                      </div>
                                    </div>
                              </div>
                          </div><!-- /.modal-dialog -->
                      </div><!-- End of modal -->
                        <div class="tab-pane" id="messages-b2">
                          <div class="card-box">
                            <br/ >
                            <div class="row">
                              <div class="col-md-2">
                              <h3 class="col-md-3">PASSWORD:</h3>
                              </div>
                              <div class="col-md-3">
                              <button type="button" class="btn w-md btn-rounded pswbtn waves-effect waves-light pull-left" data-toggle="modal" data-target="#add-password-type-modal"><i class="pswcolor"></i>Change Password </button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="modal fade" id="add-password-type-modal" data-backdrop="static" role="dialog">
                            <div class="modal-dialog modal-md">
                                <div class="col-sm-12">
                                      <div class="m-t-30 account-pages">
                                        <div class="text-center account-logo-box header-bar">
                                            <h3 class="text-uppercase">
                                                <span>Change Password</span>
                                                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">x</button>
                                            </h3>
                                            <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
                                        </div>
                                        <div class="account-content">
                                            <form class="form-horizontal" name="accommodation_type_form" enctype="multipart/form-data" method="post">
                                              <span class="message col-md-10 col-md-offset-1"></span>
                                              <!-- <div class="row"> -->
                                                <div class="col-md-10 col-md-offset-1">
                                                  <!-- <div class="creditCardForm"> -->
                                                    <div class="heading">
                                                        <!-- <h1>ADD CARD DETAILS</h1> -->
                                                    </div>
                                                    <div class="row">
                                                      <div class="">
                                                        <div class="form-group">
                                                          <label for="field-1" class="control-label">Current Password</label>
                                                          <input type="password" name="current_password" ng-model="user.current_password" ng-password="user_master.current_password" class="form-control" required>
                                                          <div ng-show="password_form.current_password.$touched">
                                                            <p class="error" ng-show="password_form.current_password.$error.required">Please enter current password!</p>
                                                            <p class="error" ng-show="password_form.current_password.$error.password">Wrong password entered!</p>
                                                          </div>
                                                        </div>
                                                        <div class="form-group">
                                                          <label for="field-1" class="control-label">New Password</label>
                                                          <input type="password" name="new_password" ng-model="user.new_password" class="form-control" required>
                                                          <div ng-show="password_form.new_password.$touched">
                                                            <p class="error" ng-show="password_form.new_password.$error.required">New Password is required.</p>
                                                          </div>
                                                        </div>
                                                        <div class="form-group">
                                                          <label for="field-1" class="control-label">Confirm Password</label>
                                                          <input type="password" name="confirm_new_password" ng-model="user.confirm_new_password" class="form-control" ng-pattern="user.new_password" required>
                                                          <div ng-show="password_form.confirm_new_password.$touched">
                                                            <p class="error" ng-show="password_form.confirm_new_password.$error.required">Confirm Password is required.</p>
                                                            <p class="error" ng-show="password_form.confirm_new_password.$error.pattern">Password doesn't match.</p>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                <!-- </div> -->
                                                  <div class="row pull-right">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" class="btn btn-primary waves-effect waves-light" ng-disabled="accommodation_type_form.$invalid" ng-click="addAccommodationType(accommodation);">Save</button>
                                                  </div>
                                                </div> <!-- End of col-md-4 -->
                                              <!-- </div> End of row -->
                                            </form>
                                            <div class="clearfix"></div>
                                        </div>
                                      </div>
                                </div>
                            </div><!-- /.modal-dialog -->
                        </div><!-- End of modal -->
                      <div class="tab-pane" id="settings-b2">
                        <h3>Your Networks</h3>
                        <p>Sign in with just 1 click when you link to Facebook or Google</p>
                        <button class="loginBtn loginBtn--facebook">
                          Login with Facebook
                        </button>
                        <button style="width: 207px;" class="loginBtn loginBtn--google">
                          Login with Google
                        </button>
                      </div>
                  </div>
                </div>
            </div>
          </div> <!-- end col -->
      </div>
  </div>
</div>
<div ng-include="'tmp/admin/footer.php'"></div>
