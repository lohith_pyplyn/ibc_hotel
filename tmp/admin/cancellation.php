<div class="content-page">
  <div class="content" style="margin-top: -38px;">
      <div class="container">
        <div class="row" style="margin-top: 72px;">
          <div class="col-xs-12">
              <div class="page-title-box">
                  <h4 class="page-title">Cancellation Policy</h4>
                  <ol class="breadcrumb p-0 m-0">
                      <li>
                          <a href="#/dashboard">IBC</a>
                      </li>
                      <li>
                          <a href="javascript:void(0);">Payments</a>
                      </li>
                      <li class="active">
                          <a>Cancellation Policy</a>
                      </li>

                  </ol>
                  <div class="clearfix"></div>
              </div>
          </div>
        </div>
      </div>
        <div class="row">
        <div class="card-box card-box-bg">
          <h3>Cancellation policies <i style="font-size: 20px;" class="fa fa-times-circle"></i>
          <!-- <button type="button" class="btn w-md btn-bordered btn-primary waves-effect waves-light pull-right" data-toggle="modal" data-target="#add-policy-modal"><i class="fa fa-plus"></i> ADD NEW POLICY</button> --> </h3> 
          <p class="color-black">Create cancellation policies to be associated with different room types.</p>
          <div class="row">
            <div class="col-md-12">
              <div class="row app-green-text">
                  <div class="col-md-2">PageSize:
                      <select name="entryLimit" ng-model="entryLimit" class="form-control">
                          <option>5</option>
                          <option>10</option>
                          <option>20</option>
                          <option>50</option>
                          <option>100</option>
                      </select>
                  </div>
                  <div class="col-md-3">Filter:
                      <input type="text" ng-model="search1" ng-change="filter()" placeholder="Search" class="form-control" />
                  </div>
              </div>
              <br/>
              <div class="panel panel-default price-panel">
                <div class="table-responsive">
                  <table class="table table-bordered text-center">
                    <thead class="thead-light">
                      <tr class="pointer">
                          <th class="text-center" ng-click="sort_by('policy_type');"> Policy type </th>
                          <th class="text-center"> Description </th>
                          <th class="text-center"></th>
                      </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="data in filtered = (policies | filter:search1 | orderBy : predicate1 :reverse1) | startFrom:(currentPage1-1)*entryLimit1 | limitTo:entryLimit1">
                            <td>
                              <span ng-if="data.policy_type == 1"> Fully refundable </span>
                              <span ng-if="data.policy_type == 2"> Partially refundable </span>
                            </td>
                            <td>{{ data.description }}</td>
                            <td>
                              <a class="app-green-text" ng-click="deleteCancellationPolicy(data.id)"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    </tbody>
                  </table>
                </div>
                <div class="col-md-12" ng-show="filteredItems1 == 0">
                    <div class="col-md-12">
                        <h4>Not  found</h4>
                    </div>
                </div>
                <div class="col-md-12" ng-show="filteredItems1 > 0">
                    <div pagination="" page="currentPage1" on-select-page="setPage1(page)" boundary-links="true" total-items="filteredItems1" items-per-page="entryLimit1" class="pagination-small" previous-text="&laquo;" next-text="&raquo;">
                    </div>
                </div>
              </div> <!-- End of Panel -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <div ng-include="'tmp/footer.php'"></div>
  </div>
<div class="modal fade" id="restrict-period-modal" data-backdrop="static" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="col-sm-12">
            <div class="m-t-30 account-pages">
                <div class="text-center account-logo-box header-bar">
                    <h3 class="text-uppercase">
                        <span>Restrict bookings over a certain period</span>
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">x</button>
                    </h3>
                </div>
                <div class="account-content">
                    <form class="form-horizontal" name="restrict_booking_form" method="post">
                      <span class="message col-sm-10 col-sm-offset-1"></span>
                        <div class="row">
                          <div class="col-md-4 col-md-offset-1">
                            <div class="form-group">
                              <label for="field-1" class="control-label">Name</label>
                              <input type="text" class="form-control" name="name" ng-model="restrict.name" placeholder="Only show Internally" required>
                              <div ng-show="restrict_booking_form.name.$touched">
                                <p class="error" ng-show="restrict_booking_form.name.$error.required">This field is required.</p>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="field-2" class="control-label">Unavailable from</label>
                              <input type="text" name="start_from" ng-model="restrict.start_from" class="form-control datetimepicker" required>
                              <div ng-show="restrict_booking_form.start_from.$touched">
                                <p class="error" ng-show="restrict_booking_form.start_from.$error.required">This field is required.</p>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="field-3" class="control-label">Available from</label>
                              <input type="text" class="form-control datetimepicker" name="end" ng-model="restrict.end" required>
                              <div ng-show="restrict_booking_form.end.$touched">
                                <p class="error" ng-show="restrict_booking_form.end.$error.required">This field is required.</p>
                              </div>
                            </div>
                            <div class="form-group">
                              <div ng-repeat="allow in allowRestriction" class="medium-checkbox">
                                <input type="checkbox" data-checklist-model="restrict.allowRestriction" data-checklist-value="allow" class="margin_10px"> {{allow.name}}<br>
                              </div>
                            </div>
                          </div> <!-- End of col-md-5 -->
                          <div class="col-md-1"></div>
                          <div class="col-md-4 col-md-offset-1">
                            <div class="form-group">
                              <label for="field-4" class="control-label">Valid for Accommodation</label>
                              <select class="form-control" name="accommodation_name" ng-model="restrict.accommodation_name">
                                <option value="">All </option>
                                <option ng-repeat="accommodation in accommodationTypes" ng-value="accommodation.id">{{ accommodation.name}}</option>
                              </select>
                              <div ng-show="restrict_booking_form.accommodation_name.$touched">
                                <p class="error" ng-show="restrict_booking_form.accommodation_name.$error.required">This field is required.</p>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="field-5" class="control-label">Blocked days</label>
                              <select class="form-control" name="blocked_days" ng-model="restrict.blocked_days">
                                <option value="">All days of week</option>
                                <option value="1">Only specific days</option>
                              </select>
                              <div ng-show="restrict_booking_form.blocked_days.$touched">
                                <p class="error" ng-show="restrict_booking_form.blocked_days.$error.required">This field is required.</p>
                              </div>
                            </div>
                            <div class="form-group" ng-if="restrict.blocked_days == 1">
                              <div ng-repeat="days in weekDays" class="medium-checkbox">
                                <input type="checkbox" data-checklist-model="restrict.weekDays" data-checklist-value="days" class="margin_10px"> {{days.day}}<br>
                              </div>
                            </div>
                          </div> <!-- col-md-5 -->
                        </div> <!-- End of row -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary waves-effect waves-light" ng-disabled="restrict_booking_form.$invalid" ng-click="addNewRestriction(restrict)">Save</button>
                        </div><br>
                        <!-- ******* -->
                        <div class="row">
                            <div class="card-box card-box-bg">
                                <h2><i class=" mdi mdi-help-circle app-green-text"></i></h2>
                                <p>The facility can be closed for a certain time period – eg. holidays, renovations, low season or similar. (Alternatively accommodation units can be closed separately, see each unit page.)</p>
                                <p>Please note that bookings may already have been made for the selected time period.</p>
                                <p>The restriction can be set for all or only specific days of the week during the period. Choose whether only new check-ins should be blocked or also stays that overlap the specified dates.</p>
                            </div>
                        </div>  <!-- End of row -->
                    </form>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- end wrapper -->

        </div>
    </div><!-- /.modal-dialog -->
</div><!-- End of modal -->

<div class="modal fade" id="edit-restrict-period-modal" data-backdrop="static" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="col-sm-12">
            <div class="m-t-30 account-pages">
                <div class="text-center account-logo-box header-bar">
                    <h3 class="text-uppercase">
                        <span>Restrict bookings over a certain period</span>
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">x</button>
                    </h3>
                </div>
                <div class="account-content">
                    <form class="form-horizontal" name="restrict_booking_form" method="post" ng-repeat="restrict in restrictionDetails">
                      <span class="message col-sm-10 col-sm-offset-1"></span>
                        <div class="row">
                          <div class="col-md-4 col-md-offset-1">
                            <div class="form-group">
                              <label for="field-1" class="control-label">Name</label>
                              <input type="text" class="form-control" name="name" ng-model="restrict.name" placeholder="Only show Internally" required>
                              <div ng-show="restrict_booking_form.name.$touched">
                                <p class="error" ng-show="restrict_booking_form.name.$error.required">This field is required.</p>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="field-2" class="control-label">Unavailable from</label>
                              <input type="text" name="start_date" ng-model="restrict.start_date" class="form-control datetimepicker" required>
                              <div ng-show="restrict_booking_form.start_date.$touched">
                                <p class="error" ng-show="restrict_booking_form.start_date.$error.required">This field is required.</p>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="field-3" class="control-label">Available from</label>
                              <input type="text" class="form-control datetimepicker" name="end_date" ng-model="restrict.end_date" required>
                              <div ng-show="restrict_booking_form.end_date.$touched">
                                <p class="error" ng-show="restrict_booking_form.end_date.$error.required">This field is required.</p>
                              </div>
                            </div>
                            <div class="form-group">
                              <div ng-repeat="allow in allowRestriction" class="medium-checkbox">
                                <input type="checkbox" data-checklist-model="tempallows.allowRestriction" data-checklist-value="allow" class="margin_10px"> {{allow.name}}<br>
                              </div>
                            </div>
                          </div> <!-- End of col-md-5 -->
                          <div class="col-md-1"></div>
                          <div class="col-md-4 col-md-offset-1">
                            <div class="form-group">
                              <label for="field-4" class="control-label">Valid for Accommodation</label>
                              <input type="hidden" name="accommodation_id" ng-model="restrict.accommodation">
                              <select class="form-control" name="acc_name" ng-model="restrict.acc_name">
                                <option value="">Select</option>
                                <option value="all">All </option>
                                <option ng-repeat="accommodation in accommodationTypes" ng-value="accommodation.id">{{ accommodation.name}}</option>
                              </select>
                              <div ng-show="restrict_booking_form.acc_name.$touched">
                                <p class="error" ng-show="restrict_booking_form.acc_name.$error.required">This field is required.</p>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="field-5" class="control-label">Blocked days</label>
                              <select class="form-control" name="blocked_days" ng-model="restrict.blocked_days">
                                <option value="">All days of week</option>
                                <option value="1">Only specific days</option>
                              </select>
                              <div ng-show="restrict_booking_form.blocked_days.$touched">
                                <p class="error" ng-show="restrict_booking_form.blocked_days.$error.required">This field is required.</p>
                              </div>
                            </div>
                            <div class="form-group" ng-if="restrict.blocked_days == 1">
                              <div ng-repeat="days in weekDays" class="medium-checkbox">
                                <input type="checkbox" data-checklist-model="tempdays.weekDays" data-checklist-value="days" class="margin_10px"> {{days.day}}<br>
                              </div>
                            </div>
                          </div> <!-- col-md-5 -->
                        </div> <!-- End of row -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary waves-effect waves-light" ng-disabled="restrict_booking_form.$invalid" ng-click="UpdateRestriction(restrict,days)">Save</button>
                        </div><br>
                        <!-- ******* -->
                        <div class="row">
                            <div class="card-box card-box-bg">
                                <h2><i class=" mdi mdi-help-circle app-green-text"></i></h2>
                                <p>The facility can be closed for a certain time period – eg. holidays, renovations, low season or similar. (Alternatively accommodation units can be closed separately, see each unit page.)</p>
                                <p>Please note that bookings may already have been made for the selected time period.</p>
                                <p>The restriction can be set for all or only specific days of the week during the period. Choose whether only new check-ins should be blocked or also stays that overlap the specified dates.</p>
                            </div>
                        </div>  <!-- End of row -->
                    </form>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- end wrapper -->

        </div>
    </div><!-- /.modal-dialog -->
</div><!-- End of modal -->
<div class="modal fade" id="add-policy-modal" data-backdrop="static" role="dialog">
    <div class="modal-dialog">
        <div class="col-sm-12">
            <div class="m-t-30 account-pages">
                <div class="text-center account-logo-box header-bar">
                    <h3 class="text-uppercase">
                        <span>Add Policy</span>
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">x</button>
                    </h3>
                    <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
                </div>
                <div class="account-content">
                    <form class="form-horizontal" name="add_policy_form" enctype="multipart/form-data" method="post">
                      <span class="message col-sm-10 col-sm-offset-1"></span>
                        <div class="row">
                            <div class="col-md-11 move-right">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Description</label>
                                    <input class="form-control autogrow" name="description" ng-model="policy.description" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="field-2" class="control-label">Policy type</label>
                                    <select class="form-control" name="policy_type" ng-model="policy.policy_type">
                                        <option value="">Select type </option>
                                        <option value="1">Fully refundable </option>
                                        <option value="2">Partially refundable</option>
                                    </select>
                                </div>
                                <div class="form-group" ng-if="policy.policy_type == 1">
                                    <span>Full refund if cancelled up to &nbsp;
                                    <select class="form-control select-width inlinedisplay bordered-input inputsmall" name="full_refund_days" ng-model="policy.full_refund_days" ng-options="number for number in numbers">
                                    </select>
                                    &nbsp; days prior to arrival. If cancelled later or in case of no-show, the first night will be charged.</span>
                                </div>
                                <div class="form-group" ng-if="policy.policy_type == 2">
                                    <span>Cancellation fee of &nbsp;
                                    <input type="number" class="form-control select-width inputsmall inlinedisplay" name="cancel_percent" ng-model="policy.cancel_percent"> % if cancelled up to &nbsp;
                                    <select class="form-control select-width inlinedisplay bordered-input inputsmall" name="partial_refund_days" ng-model="policy.partial_refund_days" ng-options="number for number in numbers">
                                    </select>  days prior to arrival.  </span>
                                    <span>
                                      If cancelled later or in case of no-show &nbsp;
                                      <input type="number" class="form-control select-width inputsmall inlinedisplay" name="charge_percent" ng-model="policy.charge_percent">  % will be charged
                                    </span>
                                </div>
                                <p class="gap">Create cancellation policies to be associated with different room types.</p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary waves-effect waves-light" ng-disabled="add_policy_form.$invalid" ng-click="addCancellationPolicy(policy);">Ok</button>
                        </div>
                    </form>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- end wrapper -->

        </div>
    </div><!-- /.modal-dialog -->
</div>
<div id="delete-restriction-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <p class="text-center" style="font-size:80px;"><i class="fa fa-exclamation-triangle"></i></p>
          <div class="row text-center">
              <div class="col-md-12">
                  <span class="col-sm-12 message text-center"></span>
                  <h3 class="color-black">Do you really want to delete this restriction?</h3>
              </div>
          </div>
          <div class="modal-footer">
            <div class="col-md-10 col-md-offset-2" style="top: 10px;">
              <button type="button" class="btn w-md btn-bordered btn-primary waves-effect waves-light" ng-click="deleteRestriction(selectedRestrictionID)"></i> &nbsp;&nbsp;OK</button>
              <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
