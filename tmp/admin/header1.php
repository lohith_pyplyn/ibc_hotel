<div ng-controller="NavigationController">
  <div id="menuLoaderDiv" ng-show="isLoggedIn" style="display: none;">
    <div ng-controller="HeaderController">
      <div class="topbar">
        <div class="topbar-left">
          <a href="#/" class="logo">
            <span>
                <img src="assets/images/logo.png" alt="" style="width: 100%; height: 65px;">
            </span>
            <i>
                <img src="assets/images/logo.png" alt="" style="width: 100%;" >
            </i>
          </a>
        </div>
        <div class="navbar navbar-default" role="navigation">
          <div class="container">
            <ul class="nav navbar-nav navbar-left">
              <li>
                <button class="button-menu-mobile open-left waves-effect">
                  <i class="mdi mdi-menu"></i>
                </button>
              </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown user-box">
                <a href="#" class="dropdown-toggle waves-effect user-link" data-toggle="dropdown" aria-expanded="true">
                    <img ng-src="assets/images/users/avatars/avatar_default2.png" alt="user-img" class="img-circle user-img">
                </a>
                <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list" >
                    <li>
                        <h5>Hi, Admin</h5>
                    </li>
                    <li><a href="#/profile"><i class="glyphicon glyphicon-user m-r-5"></i> Profile</a></li>
                    <li><a href="#/change-password"><i class="glyphicon glyphicon-cog"></i> Change Password</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0);" ng-click="logout();" <i class="glyphicon glyphicon-log-out"> </i> Logout</a></li>
                  </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <!-- Top Bar End -->
    </div>
    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu margin-top">
        <div class="sidebar-inner slimscrollleft">
            <div id="sidebar-menu">
                <ul>
                    <li class="has_sub">
                        <a href="#/dashboard" class="waves-effect"><i class="fa fa-tachometer app-green-text"></i> <span>Dashboard</span> </a>
                    </li>
                    <li class="has_sub">
                        <a href="#/accomodation" class="waves-effect"><i class="fa fa-bed app-green-text"></i> <span>Accommodation</span> </a>
                    </li>
                    <li class="has_sub">
                        <a href="#/extra" class="waves-effect"><i class="fa fa-plus-circle fa-lg app-green-text"></i> <span>Extras</span> </a>
                    </li>
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money app-green-text"></i> <span>Payments</span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="#/cancellation"><span>Cancellation Policy</span> </a></li>
                            <li><a href="#/payment"><span>Payment & Invoice</span> </a></li>
                        </ul>
                    </li>
                    <li class="has_sub">
                        <a href="#/user_account" class="waves-effect"><i class="fa fa-group app-green-text"></i> <span>User Management</span> </span></a>
                        <!-- <ul class="list-unstyled">
                            <li><a href="#/expense-category">Expense Types</a></li>
                            <li><a href="#/expense-vouchers">Expense Vouchers</a></li>
                        </ul> -->
                    </li>
                    <li class="has_sub">
                        <a href="#/house_keeping" class="waves-effect"><i class="fa fa-bed app-green-text"></i> <span>House Keeping</span> </span></a>
                        <!-- <ul class="list-unstyled">
                            <li><a href="#/service" class="waves-effect"> <span> Add Service </span> </a></li>
                            <li><a href="#/book-service" class="waves-effect"><span> Book Service </span> </a></li>
                        </ul> -->
                    </li>
                    <li class="has_sub">
                      <a href="#/promotion_codes" class="waves-effect"><i class="fa fa-file app-green-text"></i><span>Promotion Codes</span></span> </a>
                        <!-- <ul class="list-unstyled">
                            <li><a href="#/customer-report" class="waves-effect"> <span> Customer </span> </a></li> -->
                            <!-- <li><a href="#/book-service" class="waves-effect"><span> Book Service </span> </a></li> -->
                        <!-- </ul> -->
                    </li>
                    <li class="has_sub">
                        <a href="#/room_facility" class="waves-effect"><i class="fa fa-building app-green-text"></i><span>Room Facilities</span> </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="waves-effect" ng-click="logout();"><i class="mdi mdi-logout app-green-text"></i> Logout</a>
                    </li>
                </ul>
                <!-- Sidebar -->
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
  </div>
</div>
