app
.config(['$routeProvider', '$httpProvider', '$locationProvider', function ($routeProvider, $httpProvider, $locationProvider) {
    $locationProvider.hashPrefix('');
    $routeProvider
        .when('/', {
            controller: 'HomeController',
            templateUrl: BASE_URL+'tmp/user/home.php'
        })
        .when('/aboutus', {
            controller: 'AboutController',
            templateUrl: BASE_URL+'tmp/user/aboutus.php'
        })
        .when('/fianlbooking', {
            controller: 'fianlbookingController',
            templateUrl: BASE_URL+'tmp/user/fianlbooking.php'
        })
        .when('/offers', {
            controller: 'OfferController',
            templateUrl: BASE_URL+'tmp/user/offers.php'
        })
        .when('/1-5', {
            controller: 'one_fiveController',
            templateUrl: BASE_URL+'tmp/user/1-5.php'
        })
        .when('/7-10', {
            controller: 'seven_tenController',
            templateUrl: BASE_URL+'tmp/user/7-10.php'
        })
        .when('/11-30', {
            controller: 'eleven_thirtyController',
            templateUrl: BASE_URL+'tmp/user/11-30.php'
        })
        .when('/apartment', {
            controller: 'ApartmentController',
            templateUrl: BASE_URL+'tmp/user/apartment.php'
        })
        .when('/contact', {
            controller: 'ContactController',
            templateUrl: BASE_URL+'tmp/user/contact.php'
        })
        .when('/corporate', {
            controller: 'CorporateController',
            templateUrl: BASE_URL+'tmp/user/corporate.php'
        })
        .when('/hotel_policy', {
            controller: 'HotelPolicyController',
            templateUrl: BASE_URL+'tmp/user/hotel_policy.php'
        })
        .when('/feedback', {
            controller: 'FeedbackController',
            templateUrl: BASE_URL+'tmp/user/feedback.php'
        })
        .when('/login', {
            controller: 'LoginController',
            templateUrl: BASE_URL+'tmp/user/login.php'
        })
        .when('/dashboard', {
            controller: 'DashboardController',
            templateUrl: BASE_URL+'tmp/user/dashboard.php'
        })
        .when('/user_account', {
            controller: 'UserController',
            templateUrl: BASE_URL+'tmp/user/user_account.php'
        })
        .when('/customer', {
            controller: 'customerController',
            templateUrl: BASE_URL+'tmp/user/customer.php'
        })
        .when('/booking', {
            controller: 'bookingIndexController',
            templateUrl: BASE_URL+'tmp/user/booking.php'
        })
        .when('/availability', {
            controller: 'availabilityController',
            templateUrl: BASE_URL+'tmp/user/availability.php'
        })
        .when('/apartment_one', {
            controller: 'apartmentoneController',
            templateUrl: BASE_URL+'tmp/user/apartment_one.php'
        })
        .when('/apartment_two', {
            controller: 'apartment_twoConeController',
            templateUrl: BASE_URL+'tmp/user/apartment_two.php'
        })
        .when('/studio_apartment', {
            controller: 'studio_apartmentController',
            templateUrl: BASE_URL+'tmp/user/studio_apartment.php'
        })
        .when('/apartment_Three', {
            controller: 'apartment_ThreeController',
            templateUrl: BASE_URL+'tmp/user/apartment_Three.php'
        })
        .when('/apartment_four', {
            controller: 'apartment_fourController',
            templateUrl: BASE_URL+'tmp/user/apartment_four.php'
        })
        .when('/add_booking', {
            controller: 'bookingController',
            templateUrl: BASE_URL+'tmp/user/add_booking.php'
        })
        .when('/book-service',{
         controller: 'bookServcieController',
          templateUrl: BASE_URL+'tmp/user/book_service.php'
        })
        .when('/upcomingbooking',{
         controller: 'upcomingbookingController',
          templateUrl: BASE_URL+'tmp/user/upcomingbooking.php'
        })
        .when('/view_booking/:id', {
            controller: 'viewbookingController',
            templateUrl: BASE_URL+'tmp/user/view_booking.php'
        })
        .when('/profile', {
            controller: 'profileController',
            templateUrl: BASE_URL+'tmp/user/profile.php'
        })
        .when('/booking-payment', {
            controller: 'bookingPaymentController',
            templateUrl: BASE_URL+'tmp/user/booking_payment.php'
        })
        .when('/bookinghistory', {
            controller: 'bookinghistoryController',
            templateUrl: BASE_URL+'tmp/user/bookinghistory.php'
        })
        .when('/user-profile', {
            controller: 'userprofileController',
            templateUrl: BASE_URL+'tmp/user/userprofile.php'
        })
        .when('/reports', {
            controller: 'ReportsController',
            templateUrl: BASE_URL+'tmp/user/reports.php'
        })
        .when('/error', {
            controller: 'ErrorController',
            templateUrl: BASE_URL+'tmp/error.php'
        })
        .when('/privacypolicy', {
            controller: 'PrivacyPolicyController',
            templateUrl: BASE_URL+'tmp/user/privacy_policy.php'
        })
        .when('/TermsAndConditions', {
            controller: 'PrivacyPolicyController',
            templateUrl: BASE_URL+'tmp/user/TermsAndConditions.php'
        })
        
        .otherwise({ redirectTo: 'error' });
}])

.run(['$rootScope', '$location', '$cookieStore', '$http', '$route',
    function ($rootScope, $location, $cookieStore, $http, $route, currentRoute) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
          // if ($location.path() == '/aboutus' && !$rootScope.globals.currentUser) {
          //     $location.path('/aboutus');
          // }
          // if ($location.path() == '/apartment') {
          //     $location.path('/apartment');
          // }
          //  redirect to login page if not logged in
            // if (!$rootScope.globals.currentUser) {
            //   $rootScope.isDashboard = false;
            //     // $location.path('/');
            // }
            if($location.path() == '/'){
              $location.path('/');
            } else if($location.path() == '/apartment'){
              $location.path('/apartment');
            } else if($location.path() == '/aboutus'){
              $location.path('/aboutus');
            } else if($location.path() == '/offers'){
              $location.path('/offers');
            } else if($location.path() == '/corporate'){
              $location.path('/corporate');
            }
          // Redirect to dashboard page if user already logged in
            if ($location.path() == '/dashboard' || $location.path() == '/user-profile' || $location.path() == '/upcomingbooking' || $location.path() == '/bookinghistory' && $rootScope.globals.currentUser) {
               $rootScope.isDashboard = true;
                // $location.path('/dashboard');
            } else {
              $rootScope.isDashboard = false;
              //$location.path('/');
            }
          //
            if ($location.path() == '/' && $rootScope.globals.currentUser) {
                $rootScope.isDashboard = false;
                $location.path('/');
            }

        });
    }]);
