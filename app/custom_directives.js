app
// .directive('ngUnique', ['$http', function ($http) {
//     return {
//     require: 'ngModel',
//     link: function (scope, elem, attrs, ctrl) {
//
//       elem.on('blur', function (evt) {
//       scope.$apply(function () {
//         $http({
//         type: 'json',
//         method: 'POST',
//         url: BASE_URL+'user/isUniqueValue',
//         data: {
//           username:elem.val(),
//           dbField:attrs.ngUnique
//         }
//         }).success(function(data) {
//         ctrl.$setValidity('unique', data.status);
//        });
//       });
//       });
//     }
//     }
//   }
// ])

.directive('ngPassword', ['$http', function ($http) {
    return {
    require: 'ngModel',
    link: function (scope, elem, attrs, ctrl) {

      elem.on('blur', function (evt) {
      scope.$apply(function () {
        $http({
        type: 'json',
        method: 'POST',
        url: BASE_URL+'hotel/isPassNotMatch',
        data: {
          current_password:elem.val(),
          dbField:attrs.ngPassword
        }
      }).then(function(data) {
        ctrl.$setValidity('password', data.status);
       });
      });
      });
    }
    }
  }
])
.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});
;
