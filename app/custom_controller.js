app
.controller('LoginController', ['$scope', '$rootScope', '$location', 'AuthenticationService',
    function ($scope, $rootScope, $location, AuthenticationService) {
        $scope.pre_loader = false;
        $scope.login = function (user) {
            AuthenticationService.Login(user, function (response) {
                if (response.data.success) {
                    AuthenticationService.SetCredentials(user);
                    $('#success').show();
                    $('#success').html(response.data.success);
                    location.reload();
                    setTimeout(function(){
                      $location.path('/');
                    }, 2000);
                    $('#success').hide();
                } else {
                  $('#error').show();
                  $('#error').html(response.data.error);
                  setTimeout(function() {
                    $('#error').hide();
                  }, 3000);
                }
            });
        };
        $scope.logout = function () {
          AuthenticationService.ClearCredentials();
        };
    }
])
.controller('ForgotPasswordController', ['$scope', '$rootScope', '$location',
    function ($scope, $rootScope, $location) {
    }
])
.controller('NavigationController', ['$http','$scope', '$rootScope', '$location', 'AuthenticationService',
    function ($http, $scope, $rootScope, $location, AuthenticationService) {
      $scope.isLoggedIn = $rootScope.globals.currentUser;
      if($scope.isLoggedIn) {
        $scope.user = $rootScope.globals.currentUser.username;
          $("#menuLoaderDiv").css("display","block");
      }
      $scope.pre_loader = false;
      $scope.login = function (user) {
          AuthenticationService.Login(user, function (response) {
              if (response.data.success) {
                  AuthenticationService.SetCredentials(user);
                  $('#success').show();
                  $('#success').html(response.data.success);
                  location.reload();
                  setTimeout(function(){
                    $location.path('/');
                  }, 2000);
                  $('#success').hide();
                  console.log(response.data.user_data);
              } else {
                $('#error').show();
                $('#error').html(response.data.error);
                setTimeout(function() {
                  $('#error').hide();
                }, 3000);
              }
          });
      };

      $scope.logout = function () {
        AuthenticationService.ClearCredentials();
      };


      //Register User//
       $scope.registerUser = function(user){
           $http.post(BASE_URL+"user_controller/register_user",user).then(function(result){
              if(result.data.success) {
                   var message = '<div class="alert alert-info fade in"><strong>'+result.data.success+'</strong></div>';
                   $('.message').html(message);
                   $('.message').show();
                   // console.log(result.data.success);
                   setTimeout(function() {
                       location.reload();
                       // window.location.href = '#/';
                   }, 3000);
               } else {
                   var message = '<div class="alert alert-icon alert-danger alert-dismissible fade in"><strong>'+result.data.error+'</strong></div>';
                   $('.message').html(message);
                   $('.message').show();
                   setTimeout(function() {
                       $('.message').hide();
                   }, 3000);
                   // console.log(result.data.error);
               }
               $scope.isLoggedIn = $rootScope.globals.currentUser;
               if($scope.isLoggedIn) {
                 $http.get(BASE_URL+'user/get_user_name').then(function(rslt){
                   $scope.user_name = rslt.data[0].username;
                 });

                 // $http.get(BASE_URL+'agent/get_user_details').then(function(rslt){
                 //     $scope.profile_image = rslt.data[0].image;
                 // });
               }

               $scope.logout = function () {
                 AuthenticationService.ClearCredentials();
               };
           });
       }
    }
])
.controller('HeaderController', ['$scope', '$rootScope', '$location', 'AuthenticationService',
    function ($scope, $rootScope, $location, AuthenticationService) {
      //
      // console.log($rootScope.globals.currentUser);
        $scope.logout = function () {
          AuthenticationService.ClearCredentials();
        };
    }
])
.controller('HomeController', ['$scope', '$http', '$window','$location', '$route', '$rootScope', '$timeout', 'AuthenticationService',
    function ($scope, $http, $window, $rootScope, $timeout, $location,$route, AuthenticationService) {
      // AuthenticationService.isLoggedIn();
        // $("#sidebar").css("display","none");



      $scope.userFeedback = function(property) {

        $http.post(BASE_URL+'User_controller/user_feedback',property).then(function(result) {
          console.log(property);

          if(result.data.success){
            var message = '<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><i class="mdi mdi-check-all"></i><strong>Well Done!  </strong> ' + result.data.success + '</div>';
            $('.message').show();
            $('.message').html(message);
              setTimeout(function() {
                $('.message').hide();
                location.reload();
              }, 3000);
          } else {
            var message = '<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><i class="mdi mdi-block-helper"></i><strong>Oh snap!  </strong> ' + result.data.error + '</div>';
            // console.log(result.data.error);
            $('.message').show();
            $('.message').html(message);
            setTimeout(function() {
              $('.message').hide();
            }, 3000);
          }
        });
      }

        sessionStorage.setItem("grand_total", 0);
        sessionStorage.removeItem("promocode_applied");
        $scope.isDashboard = false;
        $scope.availability = function(availability){
            sessionStorage.setItem("check_in_date", availability.check_in);
            sessionStorage.setItem("check_out_date", availability.check_out);
            // console.log(availability); return;
          $http.post(BASE_URL+'user_controller/availability_search',availability).then(function(rslt){
            console.log(rslt.data);

                var availability = JSON.stringify(rslt.data);
               sessionStorage.setItem("available_rooms", availability);
               window.location.href = '#/availability';

          });

        }
        
    }
])



.controller('AboutController', ['$scope', '$http', '$rootScope', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $timeout, AuthenticationService) {
      // AuthenticationService.isLoggedIn();

    }
])

.controller('PrivacyPolicyController', ['$scope', '$http', '$rootScope', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $timeout, AuthenticationService) {
      // AuthenticationService.isLoggedIn();

      $scope.privacypolicy = function(){
        window.location.href = '#/privacypolicy';
      }
    }
])


.controller('PrivacyPolicyController', ['$scope', '$http', '$rootScope', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $timeout, AuthenticationService) {
      // AuthenticationService.isLoggedIn();

      $scope.TermsAndConditions = function(){
        window.location.href = '#/TermsAndConditions';
      }
    }
])
.controller('OfferController', ['$scope', '$http', '$rootScope', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $timeout, AuthenticationService) {
      // AuthenticationService.isLoggedIn();
      sessionStorage.setItem("grand_total", 0);
      $scope.availability = function(availability){
          sessionStorage.setItem("check_in_date", availability.check_in);
          sessionStorage.setItem("check_out_date", availability.check_out);
          // console.log(availability); return;
        $http.post(BASE_URL+'user_controller/availability_search',availability).then(function(rslt){
          console.log(rslt.data);

              var availability = JSON.stringify(rslt.data);
             sessionStorage.setItem("available_rooms", availability);
             window.location.href = '#/availability';

        });

      }

    }
])
.controller('ApartmentController', ['$scope', '$http', '$rootScope', '$window', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $window, $location, $timeout, AuthenticationService) {
      // AuthenticationService.isLoggedIn();
    //  $window.location.href = '#/apartment';
      $scope.isDashboard = false;
      $scope.apartment_one = function(){
        window.location.href = '#/apartment_one';
      }
      sessionStorage.setItem("grand_total", 0);
      $scope.availability = function(availability){
          sessionStorage.setItem("check_in_date", availability.check_in);
          sessionStorage.setItem("check_out_date", availability.check_out);
          // console.log(availability); return;
        $http.post(BASE_URL+'user_controller/availability_search',availability).then(function(rslt){
          console.log(rslt.data);

              var availability = JSON.stringify(rslt.data);
             sessionStorage.setItem("available_rooms", availability);
             window.location.href = '#/availability';

        });

      }
      $http.get(BASE_URL+'accommodation/get_room_types').then(function(rslt){
        $scope.result = rslt.data;
        for(var i = 0; i < $scope.result.length; i++) {
          if($scope.result[i].id == 1) {
            $scope.studio_price = $scope.result[i].price;
          }
          if($scope.result[i].id == 2) {
            $scope.one_bedroom_apartment_price = $scope.result[i].price;
          }
          if($scope.result[i].id == 3) {
            $scope.two_bedroom_apartment_price = $scope.result[i].price;
          }
          if($scope.result[i].id == 4) {
            $scope.three_bedroom_apartment_price = $scope.result[i].price;
          }
          if($scope.result[i].id == 5) {
            $scope.penthouse__price = $scope.result[i].price;
          }
        }
      });
    }
])


.controller('studio_apartmentController', ['$scope', '$http', '$rootScope', '$window', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $window, $location, $timeout, AuthenticationService) {
      // AuthenticationService.isLoggedIn();
    //  $window.location.href = '#/apartment';
      $scope.isDashboard = false;
      $scope.studio_apartment = function(){
        window.location.href = '#/studio_apartment';
      }
    }
])


.controller('apartmentoneController', ['$scope', '$http', '$rootScope', '$window', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $window, $location, $timeout, AuthenticationService) {
      // AuthenticationService.isLoggedIn();
    //  $window.location.href = '#/apartment';
      $scope.isDashboard = false;
      $scope.apartment_one = function(){
        window.location.href = '#/apartment_one';
      }
    }
])

.controller('apartment_twoConeController', ['$scope', '$http', '$rootScope', '$window', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $window, $location, $timeout, AuthenticationService) {
      // AuthenticationService.isLoggedIn();
    //  $window.location.href = '#/apartment';
      $scope.isDashboard = false;
      $scope.apartment_two = function(){
        window.location.href = '#/apartment_two';
      }
    }
])

.controller('apartment_ThreeController', ['$scope', '$http', '$rootScope', '$window', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $window, $location, $timeout, AuthenticationService) {
      // AuthenticationService.isLoggedIn();
    //  $window.location.href = '#/apartment';
      $scope.isDashboard = false;
      $scope.apartment_Three = function(){
        window.location.href = '#/apartment_Three';
      }
    }
])
.controller('apartment_fourController', ['$scope', '$http', '$rootScope', '$window', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $window, $location, $timeout, AuthenticationService) {
      // AuthenticationService.isLoggedIn();
    //  $window.location.href = '#/apartment';
      $scope.isDashboard = false;
      $scope.apartment_four = function(){
        window.location.href = '#/apartment_four';
      }
    }
])

.controller('AmenitiesController', ['$scope', '$http', '$rootScope', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $window, $location, $timeout, AuthenticationService) {
      // AuthenticationService.isLoggedIn();
      $window.location.href = '#/apartment';
    }
])
.controller('CorporateController', ['$scope', '$http', '$rootScope', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $window, $location, $timeout, AuthenticationService) {
      // AuthenticationService.isLoggedIn();
      // $window.location.href = '#/apartment';
      sessionStorage.setItem("grand_total", 0);
      $scope.availability = function(availability){
          sessionStorage.setItem("check_in_date", availability.check_in);
          sessionStorage.setItem("check_out_date", availability.check_out);
          // console.log(availability); return;
        $http.post(BASE_URL+'user_controller/availability_search',availability).then(function(rslt){
          console.log(rslt.data);

              var availability = JSON.stringify(rslt.data);
             sessionStorage.setItem("available_rooms", availability);
             window.location.href = '#/availability';

        });

      }
    }
])
.controller('one_fiveController', ['$scope', '$http', '$rootScope', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $window, $location, $timeout, AuthenticationService) {
      // AuthenticationService.isLoggedIn();
      // $window.location.href = '#/apartment';
      sessionStorage.setItem("grand_total", 0);
      $scope.availability = function(availability){
          sessionStorage.setItem("check_in_date", availability.check_in);
          sessionStorage.setItem("check_out_date", availability.check_out);
          // console.log(availability); return;
        $http.post(BASE_URL+'user_controller/availability_search',availability).then(function(rslt){
          console.log(rslt.data);

              var availability = JSON.stringify(rslt.data);
             sessionStorage.setItem("available_rooms", availability);
             window.location.href = '#/availability';

        });

      }
    }
])
.controller('seven_tenController', ['$scope', '$http', '$rootScope', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $window, $location, $timeout, AuthenticationService) {
      // AuthenticationService.isLoggedIn();
      // $window.location.href = '#/apartment';
      sessionStorage.setItem("grand_total", 0);
      $scope.availability = function(availability){
          sessionStorage.setItem("check_in_date", availability.check_in);
          sessionStorage.setItem("check_out_date", availability.check_out);
          // console.log(availability); return;
        $http.post(BASE_URL+'user_controller/availability_search',availability).then(function(rslt){
          console.log(rslt.data);

              var availability = JSON.stringify(rslt.data);
             sessionStorage.setItem("available_rooms", availability);
             window.location.href = '#/availability';

        });

      }
    }
]).controller('eleven_thirtyController', ['$scope', '$http', '$rootScope', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $window, $location, $timeout, AuthenticationService) {
      // AuthenticationService.isLoggedIn();
      // $window.location.href = '#/apartment';
      sessionStorage.setItem("grand_total", 0);
      $scope.availability = function(availability){
          sessionStorage.setItem("check_in_date", availability.check_in);
          sessionStorage.setItem("check_out_date", availability.check_out);
          // console.log(availability); return;
        $http.post(BASE_URL+'user_controller/availability_search',availability).then(function(rslt){
          console.log(rslt.data);

              var availability = JSON.stringify(rslt.data);
             sessionStorage.setItem("available_rooms", availability);
             window.location.href = '#/availability';

        });

      }
    }
])
.controller('HotelPolicyController', ['$scope', '$http', '$rootScope', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $window, $location, $timeout, AuthenticationService) {
      // AuthenticationService.isLoggedIn();
      $window.location.href = '#/apartment';
    }
])
.controller('FeedbackController', ['$scope', '$http', '$rootScope', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $window, $location, $timeout, AuthenticationService) {
      // AuthenticationService.isLoggedIn();
      $window.location.href = '#/apartment';
    }
])
.controller('fianlbookingController', ['$scope', '$http', '$rootScope', '$window', '$location', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $window, $location, $timeout, AuthenticationService) {
      AuthenticationService.isLoggedIn();

      $scope.check_in = sessionStorage.getItem("check_in_date");
      $scope.check_out = sessionStorage.getItem("check_out_date");
      $scope.stay_details = JSON.parse(sessionStorage.getItem('available_rooms'));
      $scope.grand_total = sessionStorage.getItem("grand_total");

    }
])

.controller('availabilityController', ['$scope', '$http', '$window', '$rootScope', '$location', '$route', '$timeout', 'AuthenticationService',
    function ($scope, $http, $window, $rootScope, $location, $route, timeout, AuthenticationService) {

      $scope.get_availability = function(availability){
          sessionStorage.setItem("grand_total", 0);
          sessionStorage.setItem("promocode_applied", '');
          sessionStorage.setItem("final_price", 0);
          sessionStorage.setItem("tax_price", 0);
          sessionStorage.setItem("check_in_date", availability.check_in);
          sessionStorage.setItem("check_out_date", availability.check_out);
          $http.post(BASE_URL+'user_controller/availability_search',availability).then(function(rslt){
              var availability = JSON.stringify(rslt.data);
              sessionStorage.setItem("available_rooms", availability);
          });
          location.reload();
      }

      $scope.date_diff = function(date1, date2) {
        var date1 = new Date(date1);
        var date2 = new Date(date2);
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
        return diffDays;
      }

      $scope.check_in = sessionStorage.getItem("check_in_date");
      $scope.check_out = sessionStorage.getItem("check_out_date");

      if(sessionStorage.getItem("promocode_applied") != undefined && sessionStorage.getItem("promocode_applied") != '') {
        $scope.discounted_price = sessionStorage.getItem("promocode_discount_price");
        $scope.discount_percent = sessionStorage.getItem("promocode_discount_percent");
        $scope.promocode_applied = true; 
      } else {
        $scope.promocode_applied = false; 
      }

      $scope.applyPromoCode = function(promo_code) {
        var price = sessionStorage.getItem("grand_total");
        var check_in = sessionStorage.getItem("check_in_date");
        $http.get(BASE_URL+'priceChanges/get_promocode_by_name/'+promo_code+'/'+price+'/'+check_in).then(function(result){
            $scope.promocode_details = result.data;
            if(result.data != 'false') {
              var discount_percent = result.data[0].offer_discount_price;

              sessionStorage.setItem("promocode_applied", promo_code);
              sessionStorage.setItem("promocode_discount_percent", discount_percent);
              $scope.discount_percent = discount_percent;
              var discounted_price = price / 100 * discount_percent;
              $scope.discounted_price = Math.round(discounted_price);
              sessionStorage.setItem("promocode_discount_price", discounted_price);
              var total = price - discounted_price; 
              var tax = total / 100 * 12;
              sessionStorage.setItem("tax_price", tax);
              $scope.tax_price = Math.round(tax);
              $scope.final_price = Math.round(total + +tax);
              sessionStorage.setItem("final_price", $scope.final_price);
              $scope.promocode_applied = true;

              var message = '<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><i class="mdi mdi-check-all"></i><strong>Well Done!  </strong> Congrats! Promo code applied successfully...</div>';
              $('.message').show();
              $('.message').html(message);
              setTimeout(function() {
                $('.message').hide();
                $('#promocode-modal').modal('hide');
              }, 3000);
              
            } else {
              var date_diff = $scope.date_diff($scope.check_in, $scope.check_out); 
              if(promo_code == 'IBCH05' && date_diff >= 3 && date_diff <= 6) {
                sessionStorage.setItem("promocode_applied", promo_code);
                sessionStorage.setItem("promocode_discount_percent", 5);
                $scope.discount_percent = 5;
                var discounted_price = price / 100 * 5;
                $scope.discounted_price = Math.round(discounted_price);
                sessionStorage.setItem("promocode_discount_price", discounted_price);
                var total = price - discounted_price; 
                var tax = total / 100 * 12;
                $scope.tax_price = Math.round(tax);
                sessionStorage.setItem("tax_price", tax);
                $scope.final_price = Math.round(total + +tax);
                sessionStorage.setItem("final_price", $scope.final_price);
                $scope.promocode_applied = true;

                var message = '<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><i class="mdi mdi-check-all"></i><strong>Well Done!  </strong> Congrats! Promo code applied successfully...</div>';
                $('.message').show();
                $('.message').html(message);
                setTimeout(function() {
                  $('.message').hide();
                  $('#promocode-modal').modal('hide');
                }, 3000);
      
              } else if(promo_code == 'IBCH10' && date_diff >= 7 && date_diff <= 10) {
                sessionStorage.setItem("promocode_applied", promo_code);
                sessionStorage.setItem("promocode_discount_percent", 10);
                $scope.discount_percent = 10;
                var discounted_price = price / 100 * 10;
                $scope.discounted_price = Math.round(discounted_price);
                sessionStorage.setItem("promocode_discount_price", discounted_price);
                var total = price - discounted_price; 
                var tax = total / 100 * 12;
                $scope.tax_price = Math.round(tax);
                $scope.final_price = Math.round(total + +tax);
                sessionStorage.setItem("tax_price", tax);
                sessionStorage.setItem("final_price", $scope.final_price);
                $scope.promocode_applied = true;

                var message = '<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><i class="mdi mdi-check-all"></i><strong>Well Done!  </strong> Congrats! Promo code applied successfully...</div>';
                $('.message').show();
                $('.message').html(message);
                setTimeout(function() {
                  $('.message').hide();
                  $('#promocode-modal').modal('hide');
                }, 3000);

              } else if(promo_code == 'IBCH15' && date_diff >= 11) {
                sessionStorage.setItem("promocode_applied", promo_code);
                sessionStorage.setItem("promocode_discount_percent", 15);
                $scope.discount_percent = 15;
                var discounted_price = price / 100 * 15;
                $scope.discounted_price = Math.round(discounted_price);
                sessionStorage.setItem("promocode_discount_price", discounted_price);
                var total = price - discounted_price; 
                var tax = total / 100 * 12;
                $scope.tax_price = Math.round(tax);
                $scope.final_price = Math.round(total + +tax);
                sessionStorage.setItem("tax_price", tax);
                sessionStorage.setItem("final_price", $scope.final_price);
                $scope.promocode_applied = true;

                var message = '<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><i class="mdi mdi-check-all"></i><strong>Well Done!  </strong> Congrats! Promo code applied successfully...</div>';
                $('.message').show();
                $('.message').html(message);
                setTimeout(function() {
                  $('.message').hide();
                  $('#promocode-modal').modal('hide');
                }, 3000);

              } else {
                  var message = '<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><i class="mdi mdi-block-helper"></i><strong>Oh snap!  </strong> Sorry, Invalid Promo code ! </div>';
                  $('.message').show();
                  $('.message').html(message);
                  setTimeout(function() {
                    $('.message').hide();
                  }, 3000);
              }
            }
        });
      }

      $scope.confirmBooking = function(){
        //window.location.href = '#/fianlbooking';
        var data = JSON.parse(sessionStorage.getItem("available_rooms"));
        $scope.fianl_data = [];
        for(var i = 0; i < data.length; i++) {
          if(data[i].is_room_added) {
            $scope.fianl_data.push(data[i]);
          }
        }
        $scope.pay_amount = sessionStorage.getItem("grand_total");
        alert("Thank you for using our system...");
      }

      if(sessionStorage.getItem("terms_conditions") != undefined) {
        if(sessionStorage.getItem("terms_conditions")) {
          //console.log(sessionStorage.getItem("terms_conditions"));
        }
      }

      $scope.termsConditions = function(terms) {
        if(terms == true) {
          sessionStorage.setItem("terms_conditions", true);
        } else {
          sessionStorage.setItem("terms_conditions", false);
        }
      }

        $scope.availability = JSON.parse(sessionStorage.getItem("available_rooms"));

        // $scope.no_of_rooms = [];
        for(var k = 0; k < $scope.availability.length; k++){
          $scope.availability[k].available_rooms_range = [];
          for(var i = 0; i < $scope.availability[k].available_rooms ; i++){

            var numbers = {
              'id' : i+1
            };
            $scope.availability[k].available_rooms_range.push(numbers);
        }
        }
        for(var k = 0; k < $scope.availability.length; k++){
          $scope.availability[k].no_of_adults = [];
          for(var i = 0; i < $scope.availability[k].no_of_person_allowed ; i++){

            var numbers = {
              'id' : i+1
            };
            $scope.availability[k].no_of_adults.push(numbers);
        }
        }
        for(var k = 0; k < $scope.availability.length; k++){
          $scope.availability[k].no_of_children = [];
          for(var i = 0; i < $scope.availability[k].no_of_children_allowed ; i++){

            var numbers = {
              'id' : i+1
            };
            $scope.availability[k].no_of_children.push(numbers);
        }
        }
        for(var k = 0; k < $scope.availability.length; k++){
          $scope.availability[k].no_of_extras = [];
          for(var i = 0; i < $scope.availability[k].no_of_extra_beds_allowed ; i++){

            var numbers = {
              'id' : i+1
            };
            $scope.availability[k].no_of_extras.push(numbers);
          }
        }

        if(sessionStorage.getItem("grand_total") == undefined) {
          $scope.grand_total = 0;
        } else {
          $scope.grand_total = sessionStorage.getItem("grand_total");
        }

        if(sessionStorage.getItem("tax_price") == undefined) {
          $scope.tax_price = 0;
        } else {
          $scope.tax_price = sessionStorage.getItem("tax_price");
        }

        if(sessionStorage.getItem("final_price") == undefined) {
          $scope.final_price = 0;
        } else {
          $scope.final_price = sessionStorage.getItem("final_price");
        }

        $scope.saveRooms = function(room) {
          var total_price = $scope.update_price(room.selected_no_of_rooms,room.extra_beds1);
          $scope.grand_total = 0;
          for(var i= 0; i < $scope.availability.length; i++){
            if($scope.availability[i].id == room.id){
              $scope.availability[i].selected_price = total_price;
              $scope.availability[i].guest_name = room.guest_name;
              $scope.availability[i].selected_no_of_rooms = room.selected_no_of_rooms;
              $scope.availability[i].selected_no_of_adults = room.no_of_adults1;
              $scope.availability[i].selected_no_of_children = room.no_of_children1;
              $scope.availability[i].selected_extra_beds = room.extra_beds1;
              $scope.availability[i].selected_adults = room.no_of_adults1;
              $scope.availability[i].selected_children = room.no_of_children1;
              $scope.availability[i].is_room_added = true;
            }
            if($scope.availability[i].selected_price != undefined) {
              $scope.grand_total = $scope.grand_total + +$scope.availability[i].selected_price;
            }
          }
          sessionStorage.setItem("grand_total", $scope.grand_total);
          if(sessionStorage.getItem("promocode_applied") != undefined && sessionStorage.getItem("promocode_applied") != '') {
            var discount_percent = sessionStorage.getItem("promocode_discount_percent");
            var discounted_price = $scope.grand_total / 100 * discount_percent;
            $scope.discounted_price = Math.round(discounted_price);
            sessionStorage.setItem("promocode_discount_price", discounted_price);
            var total = $scope.grand_total - discounted_price; 
            $scope.tax_price = Math.round(total / 100 * 12);
            sessionStorage.setItem("tax_price", $scope.tax_price);
          } else {
            var total = $scope.grand_total;
            $scope.tax_price = $scope.grand_total / 100 * 12;
            sessionStorage.setItem("tax_price", $scope.tax_price);
          }
          
          $scope.final_price = Math.round(total + +$scope.tax_price);
          sessionStorage.setItem("final_price", $scope.final_price);
          var availability = JSON.stringify($scope.availability);
          sessionStorage.setItem("available_rooms", availability);
          $('#userbooking-modal').modal('hide');
        }

        $scope.remove_room = function(id) {
          $scope.grand_total = 0;
          for(var i= 0; i < $scope.availability.length; i++){
            if($scope.availability[i].id == id){
              $scope.availability[i].guest_name = '';
              $scope.availability[i].selected_price = 0;
              $scope.availability[i].selected_no_of_rooms = 0;
              $scope.availability[i].selected_extra_beds = 0;
              $scope.availability[i].selected_adults = 0;
              $scope.availability[i].selected_children = 0;
              $scope.availability[i].is_room_added = false;
              $scope.grand_total = $scope.grand_total - $scope.availability[i].selected_price;
            } else {
              if($scope.availability[i].selected_price != undefined) {
                $scope.grand_total = $scope.grand_total + +$scope.availability[i].selected_price;
              }
            }
          }
          sessionStorage.setItem("grand_total", $scope.grand_total);
          if(sessionStorage.getItem("promocode_applied") != undefined && sessionStorage.getItem("promocode_applied") != '') {
            var discount_percent = sessionStorage.getItem("promocode_discount_percent");
            var discounted_price = $scope.grand_total / 100 * discount_percent;
            $scope.discounted_price = Math.round(discounted_price);
            sessionStorage.setItem("promocode_discount_price", discounted_price);
            var total = $scope.grand_total - discounted_price; 
            $scope.tax_price = Math.round(total / 100 * 12);
            sessionStorage.setItem("tax_price", $scope.tax_price);
          } else {
            var total = $scope.grand_total;
            $scope.tax_price = $scope.grand_total / 100 * 12;
            sessionStorage.setItem("tax_price", $scope.tax_price);
          }

          $scope.final_price = Math.round(total + +$scope.tax_price);
          sessionStorage.setItem("final_price", $scope.final_price);
          var availability = JSON.stringify($scope.availability);
          sessionStorage.setItem("available_rooms", availability);
        }

        $scope.storeBookingTempData = function(data){
          //console.log(data); return;
          $scope.isLoggedIn = $rootScope.globals.currentUser;
          if($scope.isLoggedIn) {
            $("#userbooking-modal").modal();
          }
          else {
              $("#loginSignup").modal();
          }

        $scope.addroom = {};
        $scope.addroom.id = data.id;
        $scope.addroom.price = data.price;
        $scope.addroom.availability = data.availability;
        $scope.addroom.extra_beds = data.extra_beds;
        $scope.addroom.old_price = data.old_price;
        $scope.addroom.price1 = data.price;
        $rootScope.old_price = data.price;

        // $scope.addroom.selected_no_of_rooms = data.available_rooms;
        // $scope.addroom.available_rooms = parseInt(data.available_rooms);

        $scope.available_rooms = [];
        for(var i = 1; i <= data.available_rooms; i++){
          $scope.available_rooms.push(i);
        }

        $scope.addroom.extra_beds = parseInt(data.no_of_extra_beds_allowed);
        $scope.addroom.no_of_children = parseInt(data.no_of_children_allowed);
        $scope.addroom.no_of_adults = parseInt(data.no_of_person_allowed);

      }

      $http.get('userbooking/getpricingfor_extrabed').then(function(result){
        $scope.one_bed_price = result.data[0].price;
      });

      $scope.update_price = function(no_of_rooms,no_extra_bed){
          var total_extrabed_price = 0;
          if(typeof no_extra_bed !== 'undefined' && no_extra_bed != "" && no_extra_bed != 0) {
            total_extrabed_price = parseFloat(no_extra_bed) * parseFloat($scope.one_bed_price).toFixed(2);
          }

          if(typeof no_of_rooms !== 'undefined' && no_of_rooms != "") {
            var total_room_price = no_of_rooms * $rootScope.old_price;
          } else {
            var total_room_price = 1 * $rootScope.old_price;
          }
          $scope.final_price = +parseFloat(total_room_price) + +parseFloat(total_extrabed_price);
          $scope.addroom.selected_price = $scope.final_price;
          return $scope.addroom.selected_price;
          // console.log($scope.final_price);
      }

      $scope.get_room_max_counts = function(room_type_id, no_of_rooms) {

        var extra_beds = $scope.addroom.extra_beds * no_of_rooms;
        $scope.no_of_extras = [];
        for(var i = 1; i <= extra_beds; i++){
          var numbers = {
            'id' : i
          };
          $scope.no_of_extras.push(numbers);
        }

        var no_of_children = $scope.addroom.no_of_children * no_of_rooms;
        $scope.no_of_children = [];
        for(var i = 0; i < no_of_children; i++){
          var numbers = {
            'id' : i+1
          };
          $scope.no_of_children.push(numbers);
        }

        var no_of_adults = $scope.addroom.no_of_adults * no_of_rooms;
        $scope.no_of_adults = [];
        for(var i = 0; i < no_of_adults; i++){
          var numbers = {
            'id' : i+1
          };
          $scope.no_of_adults.push(numbers);
        }
      }

      function generateCode() {
        var text = "IBC";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 7; i++)
          text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
      }

      $scope.confirmBooking = function(amount) {
        var arr = {
          'amount' : amount
        }
        
        $http.post(BASE_URL+'paymentController/store_amount', arr).then(function(rslt){
        });
        $window.location.href = BASE_URL+'payment';


        // var RequestData = {
        //     key: '6s6phkfQ',
        //     txnid: '123456789',
        //     hash: 'defdfaadgerhetiwerer',
        //     amount: amount,
        //     firstname: 'IBC Hotels and resorts pvt ltd',
        //     email: 'govindaraju@ibchotels.co.in',
        //     phone: '9739003100',
        //     productinfo: 'Booking',
        //     surl : 'https://sucess-url.in',
        //     furl: 'https://fail-url.in',
        //     mode:'dropout'// non-mandatory for Customized Response Handling
        // }

        // var Handler = {
        //       responseHandler: function(BOLT){
        //         // your payment response Code goes here, BOLT is the response object
        //       },
        //       catchException: function(BOLT){
        //         // the code you use to handle the integration errors goes here
        //       }
        // }
        
        // bolt.launch( RequestData , Handler ); 
        
      }


    }
])
.controller('ContactController', ['$scope', '$http', '$rootScope', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $timeout, AuthenticationService) {
      // AuthenticationService.isLoggedIn();

    }
])
.controller('DashboardController', ['$scope', '$http', '$rootScope', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $timeout, AuthenticationService) {
      $scope.isDashboard = true;
      // AuthenticationService.isLoggedIn();
      $("#sidebar").css("display","block");
      $(".dashboard").css("display","block");
      // $("#topbar").css("display","block");
      console.log("user dash");
      $http.get(BASE_URL+'userbooking/get_all_bookings').then(function(result){
        console.log(result.data);
        $rootScope.room_type_id = result.data.room_type_id;
        $scope.booking_details       = result.data;
        $scope.currentPage2   = 1; //current page
        $scope.entryLimit2    = 5; //max no of items to display in a page
        $scope.filteredItems = $scope.booking_details.length; //Initially for no filter
        $scope.totalItems    = $scope.booking_details.length;
    });
      var room_type = $rootScope.room_type_id;
    $http.post(BASE_URL+'userbooking/get_room_type/'+room_type).then(function(result){
      console.log(result.data);
    });
    $scope.setPage = function(pageNo) {
        $scope.currentPage2 = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() {
            $scope.filteredItems = $scope.filtered.length;
        }, 5);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    }

    }

])

.controller('reviewController', ['$scope', '$http', '$rootScope', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $timeout, AuthenticationService) {
      $scope.isDashboard = true;
      // AuthenticationService.isLoggedIn();
    }
])
.controller('upcomingbookingController', ['$scope', '$http', '$rootScope', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $timeout, AuthenticationService) {
      $scope.isDashboard = true;
      AuthenticationService.isLoggedIn();
    }
])
.controller('bookinghistoryController', ['$scope', '$http', '$rootScope', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $timeout, AuthenticationService) {
      $scope.isDashboard = true;
      AuthenticationService.isLoggedIn();
    }
])

.controller('userprofileController', ['$scope', '$http', '$rootScope', '$timeout', '$window', 'AuthenticationService', 'Upload',
    function ($scope, $http, $rootScope, $timeout, $window, AuthenticationService, Upload) {
        AuthenticationService.isLoggedIn();

        $http.get(BASE_URL+'profile/get_user_details').then(function(rslt){
            $scope.agent_details = rslt.data;
          // console.log(rslt.data);
        });

        $scope.updateProfile = function(agent,files) {
          Upload.upload({
              url: 'profile/update_profile',
              method: 'POST',
              headers: {'Content-Type': 'application/json'},
              fields: {
                'first_name'       :     agent.first_name,
                'last_name'       :     agent.last_name,
                'mobile_no'       :     agent.mobile_no,
                'address'         :     agent.address,
                'city'            :     agent.city,
                'state'           :     agent.state,
                'country'         :     agent.country,
                'pin_code'        :     agent.pin_code
              },
              file: files
          }).then(function(result) {
            $window.scrollTo(0, 0);
            if(result.data.success){
              var message = '<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><i class="mdi mdi-check-all"></i><strong>Well Done!  </strong> ' + result.data.success + '</div>';
              $('.message').show();
              $('.message').html(message);
                setTimeout(function() {
                  $('.message').hide();
                  location.reload();
                }, 3000);
            } else {
              var message = '<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><i class="mdi mdi-block-helper"></i><strong>Oh snap!  </strong> ' + result.data.error + '</div>';
              $('.message').show();
              $('.message').html(message);
              setTimeout(function() {
                $('.message').hide();
              }, 3000);
            }
          });
        }
    }
])
;
