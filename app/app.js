'use strict';

var app = angular.module('bos', [
    'ngRoute',
    'ui.bootstrap',
    'ngCookies',
    'ngDialog',
    'ngFileUpload',
    'angularFileUpload',
    'ngDialog',
    'pickadate'
]);

app.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});
app.filter("asDate", function () {
    return function (input) {
        return new Date(input);
    }
});
function wsloader(isloading){
	if(isloading){
		$('#wsloader').modal('show');
	}else{
		$('#wsloader').modal('hide');
	}
}
