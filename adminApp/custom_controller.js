app
.controller('LoginController', ['$scope', '$rootScope', '$location', 'AuthenticationService',
    function ($scope, $rootScope, $location, AuthenticationService) {

        $scope.pre_loader = false;
        $scope.login = function (user) {
            AuthenticationService.Login(user, function (response) {
                if (response.data.success) {
                    AuthenticationService.SetCredentials(user);
                    $('#success').show();
                    $('#success').html(response.data.success);
                    location.reload();
                    setTimeout(function(){
                      $location.path('/');
                    }, 2000);
                    $('#success').hide();
                } else {
                  $('#error').show();
                  $('#error').html(response.data.error);
                  setTimeout(function() {
                    $('#error').hide();
                  }, 3000);
                }
            });
        };

        $scope.logout = function () {
          AuthenticationService.ClearCredentials();
        };
    }
])

.controller('ForgotPasswordController', ['$scope', '$rootScope', '$location',
    function ($scope, $rootScope, $location) {

    }
])

.controller('HomeController', ['$scope', '$http', '$rootScope', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $timeout, AuthenticationService) {
      // AuthenticationService.isLoggedIn();

    }
])

.controller('AccomController', ['$scope', '$http', '$rootScope', '$timeout', 'AuthenticationService','Upload',
    function ($scope, $http, $rootScope, $timeout, AuthenticationService, Upload) {
      // AuthenticationService.isLoggedIn();
      $http.get(BASE_URL+'accommodation/get_room_types').then(function(rslt){
          $scope.room_types  = rslt.data;
          // console.log($scope.room_types);
      });

      $scope.storeRoomTypeId = function(id) {
        $scope.roomTypeId = id;
      }
      $scope.deleteRoomType = function(id){
          $http.post('accommodation/delete_room_type/'+id).then(function(result){
          if(result.data.success){
            location.reload();
          } else {
            alert(result.data.success);
          }
        });
      }
      $scope.getpricingDetails = function(id) {
          $http.get('accommodation/getpricingDetails/'+id).then(function(result){
            $scope.price = result.data[0];
            // console.log(result.data[0]);
        });
      }
      $scope.updateprice = function(price){
        $http.post('accommodation/updateprice',price).then(function(result){
        if(result.data.success){
          location.reload();
        } else {
          // alert(result.data.success);
        }
      });
      }
      $scope.addRoomType = function(room,files) {
        console.log(room);
        Upload.upload({
          url: 'accommodation/add_room_type',
          method: 'POST',
          headers: {'Content-Type': 'application/json'},
          fields: {
            'room_type'               :  room.room_type,
            'desc'                    :  room.desc,
            'no_of_rooms'             :  room.no_of_rooms,
            'price'                   :  room.price,
            'no_of_person_allowed'    :  room.no_of_person_allowed, 
            'no_of_children_allowed'  :  room.no_of_children_allowed,
            'no_of_extra_beds_allowed':  room.no_of_extra_beds_allowed
          },
          file: files
      }).then(function(result){
          if(result.data.success){
            var message = '<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><i class="mdi mdi-check-all"></i><strong>Well Done!  </strong> ' +result.data.success+ '</div>';
            $('.message').show();
            $('.message').html(message);
              setTimeout(function() {
                $('.message').hide();
                location.reload();
              }, 3000);
          } else {
            var message = '<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><i class="mdi mdi-block-helper"></i><strong>Oh snap!  </strong> ' +result.data.error+ '</div>';
            $('.message').show();
            $('.message').html(message);
            setTimeout(function() {
              $('.message').hide();
            }, 3000);
          }
        });
      }
    }
])
.controller('dashboardController', ['$scope', '$http', '$rootScope', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $timeout, AuthenticationService) {
      // AuthenticationService.isLoggedIn();
   
      $http.get(BASE_URL+'userbooking/get_all_bookings').then(function(result){
          // console.log(result.data);
         
          $scope.bookings       = result.data;
          $scope.currentPage   = 1; //current page
          $scope.entryLimit    = 5; //max no of items to display in a page
          $scope.filteredItems = $scope.bookings.length; //Initially for no filter
          $scope.totalItems    = $scope.bookings.length;
      });

      $scope.setPage = function(pageNo) {
          $scope.currentPage = pageNo;
      };
      $scope.filter = function() {
          $timeout(function() {
              $scope.filteredItems = $scope.filtered.length;
          }, 5);
      };
      $scope.sort_by = function(predicate) {
          $scope.predicate = predicate;
          $scope.reverse = !$scope.reverse;
      }

    }
])
.controller('userprofileController', ['$scope', '$http', '$rootScope', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $timeout, AuthenticationService) {
      // AuthenticationService.isLoggedIn();

    }
])
.controller('ExtrasController', ['$scope', '$http', '$rootScope', '$timeout', '$window', 'AuthenticationService', 'Upload',
    function ($scope, $http, $rootScope, $timeout, $window, AuthenticationService, Upload) {
        AuthenticationService.isLoggedIn();

        $http.get(BASE_URL+'extras/get_extra_options').then(function(rslt){
            $scope.extraOptions  = rslt.data;
            $scope.currentPage   = 1; //current page
            $scope.entryLimit    = 5; //max no of items to display in a page
            $scope.filteredItems = $scope.extraOptions.length; //Initially for no filter
            $scope.totalItems    = $scope.extraOptions.length;
        });
        $scope.filter_room_types = {
            selected:{}
        };

        $scope.getExtraDetails = function(id) {
          $http.get(BASE_URL+'extras/get_extra_option_by_id/'+id).then(function(rslt){
              $scope.extraDetails  = rslt.data;
              $scope.extraId = rslt.data[0].id;
          });
        }

        $scope.setPage = function(pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.filter = function() {
            $timeout(function() {
                $scope.filteredItems = $scope.filtered.length;
            }, 5);
        };
        $scope.sort_by = function(predicate) {
            $scope.predicate = predicate;
            $scope.reverse = !$scope.reverse;
        }

        $scope.addNewExtra = function(extra) {
          $http.post(BASE_URL+'extras/add_extra_option', extra).then(function(result) {
            if(result.data.success){
              $('.success').show();
              $('.success').html(result.data.success);
                setTimeout(function() {
                  $('.success').hide();
                  location.reload();
                }, 3000);
            } else {
              $('.error').show();
              $('.error').html(result.data.error);
              setTimeout(function() {
                $('.error').hide();
              }, 3000);
            }
          });
        }

        $scope.updateExtraOption = function(extra) {
          // console.log(extra);
          $http.post(BASE_URL+'extras/get_extra_option_by_id', extra).then(function(result) {
            if(result.data.success){
              $('.success').show();
              $('.success').html(result.data.success);
                setTimeout(function() {
                  $('.success').hide();
                  location.reload();
                }, 3000);
            } else {
              $('.error').show();
              $('.error').html(result.data.error);
              setTimeout(function() {
                $('.error').hide();
              }, 3000);
            }
          });
        }



        $scope.deleteExtraOption = function(extraId) {
          // console.log(extraId);
          $http.post(BASE_URL+'extras/delete_extra_option/'+extraId).then(function(result) {
            if(result.data.success){
              $('.success').show();
              $('.success').html(result.data.success);
                setTimeout(function() {
                  $('.success').hide();
                  location.reload();
                }, 3000);
            } else {
              $('.error').show();
              $('.error').html(result.data.error);
              setTimeout(function() {
                $('.error').hide();
              }, 3000);
            }
          });
        }

        $scope.removeImage = function(id) {
          var YesRemove = confirm('Are you Sure you want to Remove this image ?');
          if (YesRemove) {
            $http.post('extras/remove_extra_image/'+id).then(function(result){
              // console.log(result);
            if(result.data.success){
              location.reload();
            } else {
              //alert(result.data.error);
            }
          });
          }
        }

    }
])
.controller('CancelController', ['$scope', '$http', '$window', '$rootScope', '$location', '$timeout', 'AuthenticationService',
    function($scope, $http, $window, $rootScope, $location, $timeout, AuthenticationService) {
      AuthenticationService.isLoggedIn();


      $scope.numbers = [];
      for(var i = 1; i <= 180; i++) {
        $scope.numbers.push(i);
      }

      $http.get(BASE_URL+'restrictions/get_cancellation_policies').then(function(rslt){
        // console.log(rslt.data);
          $scope.policies       = rslt.data;
          $scope.currentPage1   = 1; //current page
          $scope.entryLimit1    = 5; //max no of items to display in a page
          $scope.filteredItems1 = $scope.policies.length; //Initially for no filter
          $scope.totalItems1    = $scope.policies.length;
      });

      $scope.setPage1 = function(pageNo) {
          $scope.currentPage1 = pageNo;
      };
      $scope.filter = function() {
          $timeout(function() {
              $scope.filteredItems1 = $scope.filtered.length;
          }, 5);
      };
      $scope.sort_by1 = function(predicate) {
          $scope.predicate1 = predicate;
          $scope.reverse1 = !$scope.reverse;
      }


      $scope.addCancellationPolicy = function(policy) {
        $http.post(BASE_URL+'restrictions/add_cancellation_policy',policy).then(function(result) {
          $window.scrollTo(0, 0);
          // console.log(result.data);
          if(result.data.success){
            var message = '<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><i class="mdi mdi-check-all"></i><strong>Well Done!  </strong> ' + result.data.success + '</div>';
            $('.message').show();
            $('.message').html(message);
              setTimeout(function() {
                $('.message').hide();
                location.reload();
              }, 3000);
          } else {
            var message = '<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><i class="mdi mdi-block-helper"></i><strong>Oh snap!  </strong> ' + result.data.error + '</div>';
            // console.log(result.data.error);
            $('.message').show();
            $('.message').html(message);
            setTimeout(function() {
              $('.message').hide();
            }, 3000);
          }
        });
      }

      $scope.deleteCancellationPolicy = function(id) {
        $http.post(BASE_URL+'restrictions/delete_cancellation_policy/'+ id).then(function(result) {
          if(result.data.success){
            location.reload();
          } else {
            alert(result.data.error);
          }
        });
      }


    }// end of function

])
.controller('PaymentController', ['$scope', '$http', '$rootScope', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $timeout, AuthenticationService) {
      // AuthenticationService.isLoggedIn();

    }
])
.controller('UserController', ['$scope', '$http', '$rootScope', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $timeout, AuthenticationService) {
      AuthenticationService.isLoggedIn();
          $http.get(BASE_URL+'admin/get_user_accounts').then(function(rslt){
            $scope.userAccounts  = rslt.data;
          })
              // $scope.userAccounts  = rslt.data;
              // $scope.currentPage   = 1; //current page
              // $scope.entryLimit    = 5; //max no of items to display in a page
              // $scope.filteredItems = $scope.userAccounts.length; //Initially for no filter
              // $scope.totalItems    = $scope.userAccounts.length;


        // $scope.setPage = function(pageNo) {
        //     $scope.currentPage = pageNo;
        // };
        // $scope.filter = function() {
        //     $timeout(function() {
        //         $scope.filteredItems = $scope.filtered.length;
        //     }, 5);
        // };
        // $scope.sort_by = function(predicate) {
        //     $scope.predicate = predicate;
        //     $scope.reverse = !$scope.reverse;
        // }
        // $scope.addNewUser = function(user) {
        //   $http.post(BASE_URL+'userAccounts/add_new_user', user).then(function(result) {
        //     if(result.data.success){
        //       var message = '<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><i class="mdi mdi-check-all"></i><strong>Well Done!  </strong> ' + result.data.success + '</div>';
        //       $('.message').show();
        //       $('.message').html(message);
        //         setTimeout(function() {
        //           $('.message').hide();
        //           location.reload();
        //         }, 3000);
        //     } else {
        //       var message = '<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><i class="mdi mdi-block-helper"></i><strong>Oh snap!  </strong> ' + result.data.error + '</div>';
        //       $('.message').show();
        //       $('.message').html(message);
        //       setTimeout(function() {
        //         $('.message').hide();
        //       }, 3000);
        //     }
        //   });
        // }

    }
])
.controller('HouseController', ['$scope', '$http', '$rootScope', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $timeout, AuthenticationService) {
      // AuthenticationService.isLoggedIn();

    }
])
.controller('PriceChangesController', ['$scope', '$http', '$window', '$rootScope', '$location', '$timeout', 'AuthenticationService',
    function($scope, $http, $window, $rootScope, $location, $timeout, AuthenticationService) {
      AuthenticationService.isLoggedIn();

      $http.get(BASE_URL+'priceChanges/get_promotion_codes').then(function(rslt){
          $scope.promotionCodes  = rslt.data;
          $scope.currentPage1   = 1; //current page
          $scope.entryLimit1    = 5; //max no of items to display in a page
          $scope.filteredItems1 = $scope.promotionCodes.length; //Initially for no filter
          $scope.totalItems1    = $scope.promotionCodes.length;
      });

      $scope.setPage1 = function(pageNo) {
          $scope.currentPage1 = pageNo;
      };
      $scope.filter = function() {
          $timeout(function() {
              $scope.filteredItems1 = $scope.filtered.length;
          }, 5);
      };
      $scope.sort_by1 = function(predicate) {
          $scope.predicate1 = predicate;
          $scope.reverse1 = !$scope.reverse;
      }

      $scope.addNewPromotionCode = function(promotion) {
        promotion.promotion_code = $scope.promotion_code;
        $http.post(BASE_URL+'priceChanges/add_promotion_code',promotion).then(function(result) {
          $window.scrollTo(0, 0);
          if(result.data.success){
            var message = '<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><i class="mdi mdi-check-all"></i><strong>Well Done!  </strong> ' + result.data.success + '</div>';
            $('.message').show();
            $('.message').html(message);
              setTimeout(function() {
                $('.message').hide();
                location.reload();
              }, 3000);
          } else {
            var message = '<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><i class="mdi mdi-block-helper"></i><strong>Oh snap!  </strong> ' + result.data.error + '</div>';
            $('.message').show();
            $('.message').html(message);
            setTimeout(function() {
              $('.message').hide();
            }, 3000);
          }
        });
      };
      $scope.storePromotionID = function(id) {
        $scope.selectedPromotionID = id;
      }
      $scope.deletePromotionCode = function(id) {
        $http.post(BASE_URL+'priceChanges/delete_promotion_code/'+ id).then(function(result) {
          if(result.data.success){
            var message = '<div class="alert alert-icon alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><i class="mdi mdi-check-all"></i><strong>Well Done!  </strong> ' + result.data.success + '</div>';
            $('.message').show();
            $('.message').html(message);
              setTimeout(function() {
                $('.message').hide();
                location.reload();
              }, 3000);
          } else {
            var message = '<div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><i class="mdi mdi-block-helper"></i><strong>Oh snap!  </strong> ' + result.data.error + '</div>';
            $('.message').show();
            $('.message').html(message);
            setTimeout(function() {
              $('.message').hide();
            }, 3000);
          }
        });
      }

      $scope.percentages = [];
      for(var i = 1; i <= 100; i++) {
        $scope.percentages.push(i);
      }

      $scope.generatePromotionCode = function() {
        var code = generateCode();
        $http.get(BASE_URL+'priceChanges/is_code_exist/'+code).then(function(result){
            if(!result.data){
              $scope.promotion_code  = code;
              $('#promotion_code').attr('disabled', true);
            } else {
              $scope.generatePromotionCode();
            }
        });
      }

      function generateCode() {
        var text = "IBC";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 7; i++)
          text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
      }

    }
])

.controller('RoomController', ['$scope', '$http', '$rootScope', '$timeout', 'AuthenticationService',
    function ($scope, $http, $rootScope, $timeout, AuthenticationService) {
      // AuthenticationService.isLoggedIn();

    }
])

.controller('NavigationController', ['$scope', '$rootScope', '$location', 'AuthenticationService',
    function ($scope, $rootScope, $location, AuthenticationService) {
      $scope.isLoggedIn = $rootScope.globals.currentUser;
      if($scope.isLoggedIn) {
          $("#menuLoaderDiv").css("display","block");
      }
    }
])

.controller('ErrorController', ['$scope', '$rootScope', '$location', 'AuthenticationService',
    function ($scope, $rootScope, $location, AuthenticationService) {
      AuthenticationService.isLoggedIn();
    }
])


.controller('HeaderController', ['$scope', '$rootScope', '$location', 'AuthenticationService',
    function ($scope, $rootScope, $location, AuthenticationService) {
        $scope.logout = function () {
          AuthenticationService.ClearCredentials();
        };
    }
])


;
