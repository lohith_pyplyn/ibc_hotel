app
.config(['$routeProvider', '$httpProvider', '$locationProvider', function ($routeProvider, $httpProvider, $locationProvider) {
    $locationProvider.hashPrefix('');
    $routeProvider
        .when('/login', {
            controller: 'LoginController',
            templateUrl: BASE_URL+'tmp/admin/login.php'
        })
        .when('/dashboard', {
            controller: 'dashboardController',
            templateUrl: BASE_URL+'tmp/admin/dashboard.php'
        })
        .when('/accomodation', {
            controller: 'AccomController',
            templateUrl: BASE_URL+'tmp/admin/accomodation.php'
        })
        .when('/userprofile', {
            controller: 'userprofileController',
            templateUrl: BASE_URL+'tmp/admin/userprofile.php'
        })
        .when('/extra', {
            controller: 'ExtrasController',
            templateUrl: BASE_URL+'tmp/admin/extra.php'
        })
        .when('/cancellation', {
            controller: 'CancelController',
            templateUrl: BASE_URL+'tmp/admin/cancellation.php'
        })
        .when('/payment', {
            controller: 'PaymentController',
            templateUrl: BASE_URL+'tmp/admin/payment.php'
        })
        .when('/user_account', {
            controller: 'UserController',
            templateUrl: BASE_URL+'tmp/admin/user_account.php'
        })
        .when('/house_keeping', {
            controller: 'HouseController',
            templateUrl: BASE_URL+'tmp/admin/house_keeping.php'
        })
        .when('/promotion_codes', {
            controller: 'PriceChangesController',
            templateUrl: BASE_URL+'tmp/admin/promotion_codes.php'
        })
        .when('/room_facility', {
            controller: 'RoomController',
            templateUrl: BASE_URL+'tmp/admin/room_facility.php'
        })
        .when('/add-new-hotel', {
            controller: 'AddHotelController',
            templateUrl: BASE_URL+'tmp/admin/add_new_hotel.php'
        })
        .when('/edit-hotel/:id', {
            controller: 'EditHotelController',
            templateUrl: BASE_URL+'tmp/admin/edit_hotel.php'
        })

        .when('/error', {
            controller: 'ErrorController',
            templateUrl: BASE_URL+'tmp/error.php'
        })

        .otherwise({ redirectTo: 'error' });
}])

.run(['$rootScope', '$location', '$cookieStore', '$http', '$route',
    function ($rootScope, $location, $cookieStore, $http, $route, currentRoute) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in
            if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
                $location.path('/login');
            }
            // Redirect to dashboard page if user already logged in
            if ($location.path() == '/login' && $rootScope.globals.currentUser) {
                $location.path('/dashboard');
            }
        });
    }]);
