<?php
header('Content-Type: text/html');
class Userbooking extends CI_Controller
{

	function __construct() {
    parent::__construct();
	}
	function getpricingfor_extrabed(){
		header('Content-Type: application/json');
		$this->load->model('Availability_model');
		$price = $this->Availability_model->getpricingfor_extrabed();
		echo json_encode($price);
	}
	function get_room_type($room_type_id){
		header('Content-Type: application/json');
		$this->load->model('Mdl_common');
		$room_type = $this->Mdl_common->get_room_type($room_type_id);
		echo json_encode($room_type);
	}
  function userbooking_details(){
    $_POST = json_decode(file_get_contents("php://input"),true);
		// print_r($_POST);
		// return;
		$lastBookingNo = $this->mdl_common->getLastBookingNo();
		// print_r($lastBookingNo);
				if(!$lastBookingNo){
					$booking_no_series = '100100';
				} else {
					$booking_no = $lastBookingNo->resrervation_no;
					$series = explode("-",$booking_no);
					$series[1]++;
					$booking_no_series = $series[1];
				}

				$user_id= $this->session->userdata('user_id');


    $arrayinsert = array(
			// 'user_id'							=>	$user_id,
       // 'resrervation_no'    =>  "IBC-".$booking_no_series,
       // 'check_in'     	 	  =>  $_POST['check_in'],
       // 'check_out'        	=>  $_POST['check_out'],
       // 'no_of_adults'     	=>  $_POST['no_of_adults1'],
       // 'no_of_children'			=>  $_POST['no_of_children1'],
       // 'room_type_id'				=>  $_POST['room_type_id'],
       'no_of_rooms'				=>  $_POST['available_rooms1'],
       // 'promo_code'		  		=>  $_POST['promo_code'],
			 'extra_beds'		  		=>  $_POST['extra_beds'],
			 'price'		  				=>  $_POST['price']

       );
    // $this->load->model('Authentication_model');
    // $user_data = $this->Authentication_model->register_user($_POST);
    // $this->db->insert('booking', $arrayinsert);
     if($this->db->insert('booking', $arrayinsert)){
       $data = array("success"=>"Registration completed successfully...");
      }else{
        $data = array("error"=>"Sorry! Something went wrong.");
      }
    echo json_encode($data);
  }
	// function get_extra_bed_count_by_id($room_type_id){
	// 	$this->load->model('availability_model');
	// 	$room_type_row_data = $this->availability_model->get_room_type_by_id($room_type_id);
	// 	echo json_encode($room_type_row_data);
	//
  // }



	public function get_all_bookings() {
    // $hotel_id = $this->session->userdata('hotel_id');
    $this->load->model('booking_model');

		$result = $this->booking_model->get_all_bookings();
		echo json_encode($result);
	}



}
