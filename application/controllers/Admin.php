<?php
header('Content-Type: text/html');
class Admin extends CI_Controller
{

	function __construct() {
		parent::__construct();
	}

  function get_promotion_code() {
    header('Content-Type: application/json');
  $this->load->model('Admin_model');
  $isCodeExist = $this->Admin_model->get_promotion_code();
  echo json_encode($isCodeExist);

  }
	function get_user_accounts(){
  header('Content-Type: application/json');
  $this->load->model('admin_model');
  $data = $this->admin_model->get_user_accounts();
  echo json_encode($data);

	}
}
