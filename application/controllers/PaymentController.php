<?php

class PaymentController extends CI_Controller
{

	function __construct() {
		parent::__construct();
		$this->load->library('session');
    }
    
    function index() {
        $data['amount'] = $this->session->userdata('pay_amount');
        $this->load->view('payment', $data);
    }

    function success() {
        $this->load->view('success');
    }

    function failure() {
        $this->load->view('failure');
    }

    function store_amount() {
        $_POST = json_decode(file_get_contents("php://input"),true);
        $amount = $_POST['amount'];
        $this->session->set_userdata('pay_amount', $amount);
    }


}