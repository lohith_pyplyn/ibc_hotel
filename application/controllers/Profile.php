<?php
header('Content-Type: text/html');
class Profile extends CI_Controller
{

	function __construct() {
		parent::__construct();
		$this->load->library('session');
	}

	function change_password() {
		$_POST = json_decode(file_get_contents("php://input"),true);
		$this->load->model('agent_model');

		if(isset($_POST['current_password']) && !empty($_POST['current_password'])){
			$password = $this->agent_model->userPassword();
			if($password != md5($_POST['current_password'])) {
				$data = array("error"=>"Current password is incorrect !");
				echo json_encode($data);
				return  ;
			}
		} else {
			$data = array("error"=>"Current password is required!");
			echo json_encode($data);
			return  ;
		}

		if(isset($_POST['new_password']) && !empty($_POST['new_password'])){
		}else{
			$data = array("error"=>"New Password is required !");
			echo json_encode($data);
			return  ;
		}

		if(isset($_POST['confirm_new_password']) && !empty($_POST['confirm_new_password'])){
				if($_POST['new_password'] != $_POST['confirm_new_password']) {
					$data = array("error"=>"Password doesn't match !");
					echo json_encode($data);
					return  ;
				}
		}else{
			$data = array("error"=>"Confirm Password is required !");
			echo json_encode($data);
			return  ;
		}
		$userId = $this->session->userdata('id');
		if($this->db->update('user_master', array('password'=>md5($_POST['new_password'])), array('id'=>$userId))) {
			$data = array("success"=>"Your password has been changed...");

		} else {
			$data = array("error"=>"Sorry! Password not changed!");
		}
		echo json_encode($data);
	}

	function isPassNotMatch() {
		$data = array('status' => true);
		$value  = json_decode(file_get_contents("php://input"),true);
		$this->load->model('agent_model');
		$pass = $this->agent_model->userPassword();

		if($pass != md5($value['current_password'])){
			$data['status'] = false;
		}
		echo json_encode($data , JSON_FORCE_OBJECT);
	}


	function get_user_details() {
		header('Content-Type: application/json');
        $this->load->model('mdl_common');
		$user_details = $this->mdl_common->get_user_details();
		echo json_encode($user_details);
	}

	function update_profile() {
		$updateArray = array(
			'first_name'       => $_POST['first_name'],
			'last_name'       => $_POST['last_name'],
			'mobile_no'  => $_POST['mobile_no'],
			'address'    => $_POST['address'],
			'city'       => $_POST['city'],
			'state'      => $_POST['state'],
			'country'    => $_POST['country'],
			'pin_code'        => $_POST['pin_code']
		);
		$agentId = $this->session->userdata('user_id');
		if($this->db->update('user', $updateArray, array('user_id'=>$agentId))) {
			if(!empty($_FILES)) {
				$date = date("ymdHis");
				$imageFiles = $_FILES[ 'file' ][ 'name' ];
				$image = 'agent_'.$agentId.'_'.$date.'.png';

				$configVideo = array(
					'upload_path' => 'assets/images/users',
					'max_size' => '8000240',
					'allowed_types' => 'png|gif|jpg|jpeg',
					'overwrite'=> FALSE,
					'remove_spaces' => TRUE,
					'file_name'=> $image
				);

				$this->load->library('upload', $configVideo);
				$this->upload->initialize($configVideo);
				if (!$this->upload->do_upload('file')) {
					$data = array('error' => 'Not a valid files, only png|gif|jpg|jpeg ');
					echo json_encode($data , JSON_FORCE_OBJECT);
				} else {
					if(!$this->db->update('user', array('profile_pic'=>$image), array('user_id'=>$agentId))){
						$data = array('error' => "Error in image uploading");
						echo json_encode($data , JSON_FORCE_OBJECT);
						return;
					} else {
						$data = array("success" => "Image has been updated successfully...");
						echo json_encode($data , JSON_FORCE_OBJECT);
						return;
					}
				}
			}
			$data = array("success" => "Profile has been updated successfully...");
		} else {
			$data = array("error" => "Sorry, Something went wrong!");
		}
		echo json_encode($data);
	}


}
