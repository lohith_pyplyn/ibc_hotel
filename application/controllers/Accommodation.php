<?php
header('Content-Type: text/html');
class Accommodation extends CI_Controller
{

	function __construct() {
		parent::__construct();
		$this->load->library('session');
	}

	function add_room_type() {
		if(isset($_POST['room_type']) && !empty($_POST['room_type'])) {
		} else {
			$data = array("error"=>"Room type is required !");
			echo json_encode($data);
			return  ;
		}
		if(isset($_POST['no_of_person_allowed']) && !empty($_POST['no_of_person_allowed'])) {
		} else {
			$data = array("error"=>"No. of person field is required !");
			echo json_encode($data);
			return  ;
		}
		if(isset($_POST['no_of_children_allowed']) && !empty($_POST['no_of_children_allowed'])) {
		} else {
			$_POST['no_of_children_allowed'] = 0;
		}
		if(isset($_POST['desc']) && !empty($_POST['desc'])) {
		} else {
			$_POST['desc'] = '';
		}
		$room_types_array = array(
		//	'hotel_id'				=> $hotel_id,
			'room_type' 			   => $_POST['room_type'],
			'no_of_person_allowed' 	   => $_POST['no_of_person_allowed'],
			'no_of_rooms' 			   => $_POST['no_of_rooms'],
			'price' 	     		   => $_POST['price'],
			'no_of_children_allowed'   => $_POST['no_of_children_allowed'],
			'no_of_extra_beds_allowed' => $_POST['no_of_extra_beds_allowed'],
			'description' 			   => $_POST['desc']
		);
		if($this->db->insert('room_types', $room_types_array)) {
			$last_id =  $this->db->insert_id();
			if(!empty($_FILES)) {
				$date = date("ymdHis");
				$imageFiles = $_FILES[ 'file' ][ 'name' ];
				$image = 'room_type_'.$date.'.png';

				$configVideo = array(
					'upload_path' => 'uploads/room_images',
					// 'max_size' => '800000000240',
					'allowed_types' => 'png|gif|jpg|jpeg',
					'overwrite'=> FALSE,
					'remove_spaces' => TRUE,
					'file_name'=> $image
				);

				$this->load->library('upload', $configVideo);
				$this->upload->initialize($configVideo);
				if (!$this->upload->do_upload('file')) {
					$data = array('error' => 'Not a valid files, only png|gif|jpg|jpeg'.$this->upload->display_errors());
					echo json_encode($data , JSON_FORCE_OBJECT);
					return;
				} else {
					if(!$this->db->update('room_types', array('image'=>$image), array('id'=>$last_id))){
						$data = array('error' => $this->db->_error_message());
						echo json_encode($data , JSON_FORCE_OBJECT);
						return;
					} else {
						$data = array("success" => "Room type added Successfully...");
						echo json_encode($data , JSON_FORCE_OBJECT);
						return;
					}
				}
			}
			$data = array("success"=>"Room type added Successfully...");
		} else {
			$data = array("error"=>"Sorry. Room type not added!");
		}
		echo json_encode($data);
	}



	function add_accommodation_type() {
		$_POST = json_decode(file_get_contents("php://input"),true);
		if(isset($_POST['acc_type']) && !empty($_POST['acc_type'])) {
		} else {
			$data = array("error"=>"Accommodation type is required !");
			echo json_encode($data);
			return  ;
		}
		$hotel_id = $this->session->userdata('hotel_id');
		$accomodation_types_array = array(
			'hotel_id'		=> $hotel_id,
			'object_type' => $_POST['acc_type']
		);
		if($this->db->insert('accommodation_types', $accomodation_types_array)) {
			$data = array("success"=>"Accommodation type added Successfully...");
		} else {
			$data = array("error"=>"Sorry. Accommodation type not added!");
		}
		echo json_encode($data);
	}


	function getpricingDetails($id){
		header('Content-Type: application/json');
		$this->load->model('admin_model');
		$price= $this->admin_model->getpricingDetails($id);
		echo json_encode($price);
	}
	function updateprice(){
			$_POST = json_decode(file_get_contents("php://input"),true);
			// $this->load->model('admin_model');
			// $result = $this->admin_model->updateprice($_POST);
			// if($result) {
			// 	$data = array("success"=>"price updated Successfully...");
			// } else {
			// 	$data = array("error"=>"Sorry.failed to update price!");
			// }
			// echo json_encode($data);
			$updateArray = array(
				'price'					=>	$_POST['price']

			);
			if($this->db->update('room_types', $updateArray, array('id'=>$_POST['id']))) {
				$data = array("success" => " Successfully...");
			} else {
				$data = array("error" => "Sorry... Something went wrong, Please try again later!");
			}
			echo json_encode($data);
	}
	function add_new_unit() {
		$_POST = json_decode(file_get_contents("php://input"),true);
		if(isset($_POST['unit_name']) && !empty($_POST['unit_name'])) {
		} else {
			$data = array("error"=>"Room name required !");
			echo json_encode($data);
			return  ;
		}

		if(isset($_POST['price']) && !empty($_POST['price'])) {
			if($_POST['price'] < 1) {
				$data = array("error"=>"Price should be greater than 0 !");
				echo json_encode($data);
				return  ;
			}
		} else {
			$data = array("error"=>"Price is required !");
			echo json_encode($data);
			return  ;
		}

		$this->load->model('admin_model');
		$result = $this->admin_model->add_new_unit($_POST);
		if($result) {
			$data = array("success"=>"Room added Successfully...");
		} else {
			$data = array("error"=>"Sorry. Room not added!");
		}
		echo json_encode($data);
	}

	function remove_room_image($id) {
		$this->load->model('admin_model');
    	$image_name = $this->admin_model->get_accommodation_image($id);
		if($this->db->update('accommodation', array('image'=>''), array('id'=>$id))) {
			$path = './uploads/room_images/'.$image_name;
			unlink($path);
			$data = array("success"=>"Image removed Successfully...");
		} else {
			$data = array("error"=>"Sorry. Image not removed!");
		}
		echo json_encode($data);
	}

	function delete_room($id) {
		if($this->db->delete('rooms', array('id'=>$id))) {
			$data = array("success"=>"Room deleted Successfully...");
		} else {
			$data = array("error"=>"Sorry. Room not deleted!");
		}
		echo json_encode($data);
	}

	function delete_room_type($id) {
		if($this->db->delete('room_types', array('id'=>$id))) {
			// $this->db->delete('rooms', array('room_type'=>$id));
			$data = array("success"=>"Room type deleted Successfully...");
		} else {
			$data = array("error"=>"Sorry. Room type not deleted!");
		}
		echo json_encode($data);
	}

	function delete_accommodation_type($id) {
		if($this->db->delete('accommodation_types', array('id'=>$id))) {
			$data = array("success"=>"Accommodation type deleted Successfully...");
		} else {
			$data = array("error"=>"Sorry. Accommodation type not deleted!");
		}
		echo json_encode($data);
	}

	function delete_accommodation($id) {
		$this->load->model('admin_model');
    	$image_name = $this->admin_model->get_accommodation_image($id);
		if($this->db->delete('accommodation', array('id'=>$id))) {
			if(!empty($image_name)) {
				$path = './uploads/room_images/'.$image_name;
				unlink($path);
			}
			if($this->db->delete('rooms', array('accommodation_id'=>$id))) {
			} else {
				$data = array("error"=>"Sorry. Rooms not deleted!");
			}
			$data = array("success"=>"Accommodation deleted Successfully...");
		} else {
			$data = array("error"=>"Sorry. Accommodation not deleted!");
		}
		echo json_encode($data);
	}

	function get_accommodation_details_by_id($id) {
		header('Content-Type: application/json');
		$this->load->model('admin_model');
		$room_details = $this->admin_model->get_accommodation_details_by_id($id);
		echo json_encode($room_details);
	}

	function get_room1_types() {
		header('Content-Type: application/json');
		$this->load->model('admin_model');
		$room1_types = $this->admin_model->get_room1_types();
		echo json_encode($room1_types);
	}

	function get_selected_room_types() {
		header('Content-Type: application/json');
		$this->load->model('admin_model');
		$selected_room_types = $this->admin_model->get_selected_room_types();
		echo json_encode($selected_room_types);
	}

	function get_accommodation_types() {
		header('Content-Type: application/json');
		$this->load->model('admin_model');
		$accommodation_types = $this->admin_model->get_accommodation_types();
		echo json_encode($accommodation_types);
	}

	function get_accommodations() {
		header('Content-Type: application/json');
		$this->load->model('admin_model');
		$accommodations= $this->admin_model->get_accommodations();
		echo json_encode($accommodations);
	}

	function add_new_accomodation() {
		if(isset($_POST['accommodation_type']) && !empty($_POST['accommodation_type'])){
		}else{
			$data = array("error"=>"Accommodation type required !");
			echo json_encode($data);
			return  ;
		}
		if(isset($_POST['name']) && !empty($_POST['name'])){
		}else{
			$data = array("error"=>"Accommodation Name required !");
			echo json_encode($data);
			return  ;
		}
		if(isset($_POST['description']) && !empty($_POST['description'])){
		}else{
			$_POST['description'] = '';
		}
		if(isset($_POST['price']) && !empty($_POST['price'])){
			if($_POST['price'] < 1) {
				$data = array("error"=>"Price should be greater than 0 !");
				echo json_encode($data);
				return  ;
			}
		}else{
			$data = array("error"=>"Price is required !");
			echo json_encode($data);
			return  ;
		}

		$insertArray = array(
			'hotel_id' 					=> $this->session->userdata('hotel_id'),
			'name'						=>	$_POST['name'],
			'accommodation_type' 		=> $_POST['accommodation_type'],
			'description'				=> $_POST['description'],
			'price'						=>	$_POST['price']

		);
		if($this->db->insert('accommodation', $insertArray)) {
			$last_id =  $this->db->insert_id();
			if(!empty($_FILES)) {
				$date = date("ymdHis");
				$imageFiles = $_FILES[ 'file' ][ 'name' ];
				$image = 'accommodation_'.$date.'.png';

				$configVideo = array(
					'upload_path' => './uploads/room_images',
					//'max_size' => '800000000240',
					'allowed_types' => 'png|gif|jpg|jpeg',
					'overwrite'=> FALSE,
					'remove_spaces' => TRUE,
					'file_name'=> $image
				);

				$this->load->library('upload', $configVideo);
				$this->upload->initialize($configVideo);
				if (!$this->upload->do_upload('file')) {
					$data = array('error' => 'Not a valid files, only png|gif|jpg|jpeg');
					echo json_encode($data , JSON_FORCE_OBJECT);
					return;
				} else {
					if(!$this->db->update('accommodation', array('image'=>$image), array('id'=>$last_id))){
						$data = array('error' => $this->db->_error_message());
						echo json_encode($data , JSON_FORCE_OBJECT);
						return;
					} else {
						$data = array("success" => "Your Details Added Successfully...");
						echo json_encode($data , JSON_FORCE_OBJECT);
						return;
					}
				}
			}
			$data = array("success" => "Your Details Added Successfully...");
		} else {
			$data = array("error" => "Sorry... Something went wrong, Please try again later!");
		}
		echo json_encode($data);
	}


	function update_accommodation() {

		if(isset($_POST['description']) && !empty($_POST['description'])){
		}else{
			$_POST['description'] = '';
		}
		if(isset($_POST['price']) && !empty($_POST['price'])){
			if($_POST['price'] < 1) {
				$data = array("error"=>"Price should be greater than 0 !");
				echo json_encode($data);
				return  ;
			}
		}else{
			$data = array("error"=>"Price is required !");
			echo json_encode($data);
			return  ;
		}

		$insertArray = array(
			'description'				 => $_POST['description'],
			'price'							 =>	$_POST['price']
		);
		if($this->db->update('accommodation', $insertArray, array('id'=>$_POST['id']))) {
			if(!empty($_FILES)) {
				$date = date("ymdHis");
				$imageFiles = $_FILES[ 'file' ][ 'name' ];
				$image = 'accomodation_'.$date.'.png';

				$configVideo = array(
					'upload_path' => './uploads/room_images',
					//'max_size' => '8000240',
					'allowed_types' => 'png|gif|jpg|jpeg',
					'overwrite'=> FALSE,
					'remove_spaces' => TRUE,
					'file_name'=> $image
				);

				$this->load->library('upload', $configVideo);
				$this->upload->initialize($configVideo);
				if (!$this->upload->do_upload('file')) {
					$data = array('error' => 'Not a valid files, only png|gif|jpg|jpeg ');
					echo json_encode($data , JSON_FORCE_OBJECT);
					return;
				} else {
					if(!$this->db->update('accommodation', array('image'=>$image), array('id'=>$_POST['id']))){
						$data = array('error' => $this->db->_error_message());
						echo json_encode($data , JSON_FORCE_OBJECT);
						return;
					} else {
						$data = array("success" => "Your Details updated Successfully...");
						echo json_encode($data , JSON_FORCE_OBJECT);
						return;
					}
				}
			}
			$data = array("success" => "Your Details updated Successfully...");
		} else {
			$data = array("error" => "Sorry... Something went wrong, Please try again later!");
		}
		echo json_encode($data);
	}
	function getUnitDetails($id) {
		header('Content-Type: application/json');
		$this->load->model('admin_model');
		$room1_details = $this->admin_model->getUnitDetails($id);
		echo json_encode($room1_details);
	}
	function get_accommodation_list(){
		header('Content-Type: application/json');
		$this->load->model('admin_model');
		$ac_list = $this->admin_model->get_accommodation_list();
		echo json_encode($ac_list);
	}
	function getRoomList($id) {
		header('Content-Type: application/json');
		$this->load->model('admin_model');
		$rmList = $this->admin_model->getRoomList($id);
		echo json_encode($rmList);
	}
	function get_room_types() {
		header('Content-Type: application/json');
		$this->load->model('admin_model');
		$room_types = $this->admin_model->getRoomTypes();
		echo json_encode($room_types);
	}
	function updateRoomDetails() {
		$_POST = json_decode(file_get_contents("php://input"),true);
		if(isset($_POST['room_number']) && !empty($_POST['room_number'])){
		}else{
			$data = array("error"=>"Room Number is required !");
			echo json_encode($data);
			return  ;
		}
		if(isset($_POST['room_type']) && !empty($_POST['room_type'])){
		}else{
			$data = array("error"=>"Room Type is required !");
			echo json_encode($data);
			return  ;
		}
		if(isset($_POST['price']) && !empty($_POST['price'])){
			if($_POST['price'] < 1) {
				$data = array("error"=>"Price should be greater than 0 !");
				echo json_encode($data);
				return  ;
			}
		}else{
			$data = array("error"=>"Room price is required !");
			echo json_encode($data);
			return  ;
		}
		$room_array = array(
			'room_number' => $_POST['room_number'],
			'room_type' => $_POST['room_type'],
			//'no_of_beds' => $_POST['no_of_beds'],
			'price' => $_POST['price']
		);
		if($this->db->update('rooms', $room_array, array('id' => $_POST['id']))){
			$data = array("success" => "Your Details updated Successfully...");
		} else {
			$data = array("error" => "Sorry... Something went wrong, Please try again later!");
		}
		echo json_encode($data);
	}

}
