<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registeruser extends CI_Controller {

public function __construct()
{
    parent::__construct();
    $this->load->library('session');
}
//Register : Add New User
public function register_user(){

       //validations
       $_POST = json_decode(file_get_contents("php://input"),true);

       // if(isset($_POST['username']) && !empty($_POST['username'])){
       //     $this->load->model('Mdl_common');
       //     $isUserExist = $this->Mdl_common->isUserExist($_POST['username']);
       //     if($isUserExist == true) {
       //         $data = array("error" => "Username already taken!");
       //         echo json_encode($data);
       //
       //         return  ;
       //     }
       // }else{
       //     $data = array("error"=>"User Name required !");
       //     echo json_encode($data);
       //     return  ;
       // }
       //
       // if(isset($_POST['email']) && !empty($_POST['email'])){
       //     $this->load->model('UserLoginModel');
       //     $isEmailExist = $this->UserLoginModel->isEmailExist($_POST['email']);
       //     if($isEmailExist == true) {
       //         $data = array("error" => "Email already Exist!");
       //         echo json_encode($data);
       //
       //         return  ;
       //     }
       // }else{
       //     $data = array("error"=>"Email required !");
       //     echo json_encode($data);
       //     return  ;
       // }

       if(isset($_POST['password']) && !empty($_POST['password'])){
       }else{
           $data = array("error"=>"Password required !");
           echo json_encode($data);
           return  ;
       }
       if(isset($_POST['confirmpassword']) && !empty($_POST['confirmpassword'])){
           if(($_POST['confirmpassword']) != ($_POST['password']) ){
               $data = array("error"=>"Password doesn't match !");
               echo json_encode($data);
               return  ;
           }
       }else{
           $data = array("error"=>"Confirm Password required!");
           echo json_encode($data);
           return  ;
       }


       //insertion in db


       //select plan is there in plan table
       $this->load->model('Authentication_model');
       $user_data = $this->Authentication_model->register_user($_POST);
       if($user_data)
       {
          $data = array("success"=>"Registration completed successfully...");
       }else{
         $data = array("error"=>"Sorry! Something went wrong.");
       }
       echo json_encode($data);
   }
}
?>
