<?php
header('Content-Type: text/html');
class Agent_admin_controller extends CI_Controller
{
  function __construct() {
    parent::__construct();
    $this->load->library('session');
  }

  function get_agent_number() {
    $this->load->model('admin_model');
    $lastAgentNumber = $this->admin_model->getLastAgentNumber();
    if(!$lastAgentNumber){
      $agent_number = '00001';
    } else {
      $agent_no = $lastAgentNumber->agent_number;
      $series = explode("-",$agent_no);
      $series[1]++;
      $agent_number = $series[1];
    }
    $agent_number = sprintf("%05d", $series[1]);
    $agent_number = "AG-".$agent_number;
    $data['agent_number'] = $agent_number;
    echo json_encode($data);
  }

  function get_agent_renewal_history($id) {
    $history = $this->mdl_common->get_agent_renewal_history($id);
    $historyData = array();
    if(!empty($history)) {
      foreach($history as $value) {
        $date = date_create($value['created_at']);
        $renewed_date = date_format($date, 'Y-m-d');
        $value['created_at'] = $renewed_date;
        $renewed_date = date_create($renewed_date);
        $expiry_date = date_create($value['expiry_date']);
        $diff = date_diff($renewed_date,$expiry_date);
        $diff = $diff->format("%a");
        $value['no_of_days'] = $diff;
        $historyData[] = $value;
      }
    }
    echo json_encode($historyData);
  }

  function renew_agent_account() {
    $_POST = json_decode(file_get_contents("php://input"),true);
    if(isset($_POST['received_amount']) && !empty($_POST['received_amount'])){
    }else{
        $data = array("message"=>"Received amount is required !");
        echo json_encode($data);
        return  ;
    }
    if(isset($_POST['expire_date']) && !empty($_POST['expire_date'])){
    }else{
        $data = array("message"=>"Expiry date is required !");
        echo json_encode($data);
        return  ;
    }

    if(isset($_POST['received_amount']) && !empty($_POST['received_amount'])){
    }else{
        $data = array("message"=>"Recevied amount is required !");
        echo json_encode($data);
        return  ;
    }

    if(isset($_POST['charge_amount']) && !empty($_POST['charge_amount'])){
    }else{
        $data = array("message"=>"Charge amount is required !");
        echo json_encode($data);
        return  ;
    }

    if($_POST['charge_amount'] >= $_POST['received_amount']) {
      $balance_amount = $_POST['charge_amount'] - $_POST['received_amount'];
    } else {
      $balance_amount = 0;
    }

    $insertArray = array(
      'user_id'         => $_POST['id'],
      'received_amount' => $_POST['received_amount'],
      'charge_amount'   => $_POST['charge_amount'],
      'balance_amount'  => $balance_amount,
      'expiry_date'     => $_POST['expire_date']
    );
    if($this->db->insert('agent_account_renewal', $insertArray)) {
      $this->db->update('agent', array('expire_date'=>$_POST['expire_date']), array('user_id'=>$_POST['id']));
      $data = array("success"=>"Agent account renewed successully...");
    } else {
      $data = array("success"=>"Sorry. Something went wrong. Please try again later!");
    }
    echo json_encode($data);
  }

  public function registerNewUser() {
    $_POST = json_decode(file_get_contents("php://input"),true);
    if(isset($_POST['name']) && !empty($_POST['name'])){
    }else{
        $data = array("message"=>"Name required !");
        echo json_encode($data);
        return  ;
    }
    if(isset($_POST['username']) && !empty($_POST['username'])){
    }else{
        $data = array("message"=>"User Name required !");
        echo json_encode($data);
        return  ;
    }

    if(isset($_POST['email']) && !empty($_POST['email'])){
    }else{
        $data = array("message"=>"Email required !");
        echo json_encode($data);
        return  ;
    }
    if(isset($_POST['mobile_no']) && !empty($_POST['mobile_no'])){
    }else{
        $data = array("message"=>"Mobile Number required !");
        echo json_encode($data);
        return  ;
    }

    if(isset($_POST['password']) && !empty($_POST['password'])){
    }else{
        $data = array("message"=>"Password required !");
        echo json_encode($data);
        return  ;
    }

    if(isset($_POST['received_amount']) && !empty($_POST['received_amount'])){
    }else{
        $data = array("message"=>"Received amount is required !");
        echo json_encode($data);
        return  ;
    }

    if(isset($_POST['charge_amount']) && !empty($_POST['charge_amount'])){
    }else{
      $data = array("message"=>"Charge amount is required !");
      echo json_encode($data);
      return  ;
    }

    $isUserExist = $this->mdl_common->isUserExist($_POST['username']);
    if($isUserExist == true) {

        $insertUserArray = array(
            "username"     => $_POST['username'],
            "email"         => $_POST['email'],
            "password" => md5($_POST['password'])
        );
        $user_type = $this->session->userdata('user_type');

        if($this->db->insert('user', $insertUserArray)){
          $user_id = $this->mdl_common->getLatUser($_POST['username'],md5($_POST['password']));
          if($user_type == "admin"){
            $insertAgentArray = array(
              'agent_number' => $_POST['agent_number'],
              'user_id' => $user_id->user_id,
              'name' => $_POST['name'],
              'mobile_no' => $_POST['mobile_no'],
              'address' => $_POST['address'],
              'expire_date' => $_POST['expire_date'],
            );
          } else {
            $insertAgentArray = array(
              'agent_number' => $_POST['agent_number'],
              'user_id' => $user_id->user_id,
              'name' => $_POST['name'],
              'mobile_no' => $_POST['mobile_no'],
              'address' => $_POST['address'],
              'expire_date' => $_POST['expire_date'],
            );
          }
          if($this->db->insert('agent',$insertAgentArray)) {
            $last_id = $this->db->insert_id();
            if($_POST['charge_amount'] >= $_POST['received_amount']) {
              $balance_amount = $_POST['charge_amount'] - $_POST['received_amount'];
            } else {
              $balance_amount = 0;
            }
            $insertAgentDetails = array(
              'user_id'         => $user_id->user_id,
              'received_amount' => $_POST['received_amount'],
              'charge_amount'   => $_POST['charge_amount'],
              'balance_amount'  => $balance_amount,
              'expiry_date'     => $_POST['expire_date']
            );
            $this->db->insert('agent_account_renewal', $insertAgentDetails);
          }
          $data = array("message" => "User Registered Successfully...");
        } else {
          $data = array("message" => "Error in registering user. Please try again!");
        }
    } else {
        $data = array("message" => "Username already taken!");
    }
    echo json_encode($data);
  }

  function getAllAgents(){
    header('Content-Type: text/html');
    $allAgents = $this->mdl_common->getAllAgents();
    $agentDetails = array();
		if(!empty($allAgents)) {
			$date = date('Y-m-d');
			$today = date_create($date);
			foreach ($allAgents as $value) {
				$expiry_date = $value['expire_date'];
				$dt = new DateTime($expiry_date);
				$expiry_date = date_create($dt->format('Y-m-d'));
				$diff = date_diff($today,$expiry_date);
				$diff = $diff->format("%R%a");
				if($diff < 0) {
          $value['bgColor'] = 'red';
					$value['expired'] = true;
				} else {
          $value['bgColor'] = '';
					$value['expired'] = false;
				}
				$agentDetails[] = $value;
			}
		}
    echo json_encode($agentDetails);
  }
  function getAgentById($id) {
    header('Content-Type: text/html');
    $agent = $this->mdl_common->getAgentById($id);
    echo json_encode($agent);
  }

  function updateAgent(){
    $_POST = json_decode(file_get_contents("php://input"),true);
    if(isset($_POST['name']) && !empty($_POST['name'])){
    }else{
        $data = array("message"=>"Name required !");
        echo json_encode($data);
        return  ;
    }
    $updateArray2 = array(
      'name' => $_POST['name'],
      'mobile_no' => $_POST['mobile_no'],
      'address' => $_POST['address']
    );
    if($this->db->update('agent',$updateArray2,array('user_id' => $_POST['user_id']))){
      $data = array("success" => "Agent Updated Successfully..");
    } else {
      $data = array("error" => "Sorry... Something went wrong, Please try again later!");
    }
    echo json_encode($data);
  }

  function deleteAgent($user_id){
    if($this->db->delete('user', array('user_id' => $user_id ))) {
      $this->db->delete('agent', array('user_id' => $user_id ));
      $this->db->delete('agent_account_renewal', array('user_id' => $user_id));
      $data = array("success" => "Agent Deleted Successfully");
    } else {
      $data = array("error" => "Sorry Agent Not Deleted!");
    }
    echo json_encode($data);
  }
}
