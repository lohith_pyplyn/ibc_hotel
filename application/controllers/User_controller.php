<?php
header('Content-Type: text/html');
class User_controller extends CI_Controller
{

	function __construct() {
        parent::__construct();

	}
	function availability_search(){
		$_POST = json_decode(file_get_contents("php://input"),true);
		$this->load->model('availability_model');
		$available_rooms = $this->availability_model->get_available_rooms($_POST['check_in'],$_POST['check_out']);
		echo json_encode($available_rooms);
	}
  	public function register_user(){

         $_POST = json_decode(file_get_contents("php://input"),true);
         if(isset($_POST['password']) && !empty($_POST['password'])){
         }else{
             $data = array("error"=>"Password required !");
             echo json_encode($data);
             return  ;
         }
         if(isset($_POST['confirmpassword']) && !empty($_POST['confirmpassword'])){
             if(($_POST['confirmpassword']) != ($_POST['password']) ){
                 $data = array("error"=>"Password doesn't match !");
                 echo json_encode($data);
                 return  ;
             }
         }else{
             $data = array("error"=>"Confirm Password required!");
             echo json_encode($data);
             return  ;
         }
         $arrayinsert = array(
            'username'     	  =>  $_POST['username'],
            'password'        =>  md5($_POST['password']),
            'email'         	=>  $_POST['email'],
            'user_type'			  =>  'user',
            'user_status'			=>  'active'
            );
         // $this->load->model('Authentication_model');
         // $user_data = $this->Authentication_model->register_user($_POST);
         if($this->db->insert('user_master', $arrayinsert))
         {
           $insertArray = array(
              'user_id'           => $this->db->insert_id(),
              'first_name'      	=>  $_POST['firstname'],
              'last_name'     	  =>  $_POST['lastname'],
              'mobile_no'     		=>  $_POST['phoneno'],
              'email'         	  =>  $_POST['email'],
              'address'           => '',
              'city'              => '',
              'state'             => '',
              'country'           => '',
              'pincode'           => ''
           );
            $this->db->insert('user', $insertArray);
            $data = array("success"=>"Registration completed successfully...");
         }else{
           $data = array("error"=>"Sorry! Something went wrong.");
         }
         echo json_encode($data);
     }



     public function user_feedback(){

        $_POST = json_decode(file_get_contents("php://input"),true);
        if(isset($_POST['full_name']) && !empty($_POST['full_name'])){
        }else{
            $data = array("error"=>"full_name required !");
            echo json_encode($data);
            return  ;
        }
        if(isset($_POST['email']) && !empty($_POST['email'])){
        }else{
            $data = array("error"=>"email required !");
            echo json_encode($data);
            return  ;
        }
        if(isset($_POST['phone_number']) && !empty($_POST['phone_number'])){
        }else{
            $data = array("error"=>"phone_number required !");
            echo json_encode($data);
            return  ;
        }
        if(isset($_POST['Apartment_Number']) && !empty($_POST['Apartment_Number'])){
        }else{
            $data = array("error"=>"Apartment_Number required !");
            echo json_encode($data);
            return  ;
        }
        if(isset($_POST['start_from']) && !empty($_POST['start_from'])){
        }else{
            $data = array("error"=>"Password required !");
            echo json_encode($data);
            return  ;
        }
        if(isset($_POST['end']) && !empty($_POST['end'])){
        }else{
            $data = array("error"=>"end required !");
            echo json_encode($data);
            return  ;
        }
        if(isset($_POST['Timely_Response']) && !empty($_POST['Timely_Response'])){
        }else{
            $data = array("error"=>"Timely_Response required !");
            echo json_encode($data);
            return  ;
        }
        if(isset($_POST['Our_Support']) && !empty($_POST['Our_Support'])){
        }else{
            $data = array("error"=>"Our_Support required !");
            echo json_encode($data);
            return  ;
        }
        if(isset($_POST['Overall_Satisfaction']) && !empty($_POST['Overall_Satisfaction'])){
        }else{
            $data = array("error"=>"Overall_Satisfaction required !");
            echo json_encode($data);
            return  ;
        }

        if(isset($_POST['anything_tell_us']) && !empty($_POST['anything_tell_us'])){
        }else{
            $data = array("error"=>"anything_tell_us required !");
            echo json_encode($data);
            return  ;
        }


          $this->load->model('Authentication_model');
         $user_data = $this->Authentication_model->user_feedback_model($_POST);
        $insertArr = array(
            'full_name'   		     => $_POST['full_name'],
            'email'  		       	 	 => $_POST['email'],
            'phone_number' 		     => $_POST['phone_number'],
            'Apartment_Number'	   => $_POST['Apartment_Number'],
            'start_from'           => $_POST['start_from'],
            'end' 		             => $_POST['end'],
            'Timely_Response'      => $_POST['Timely_Response'],
            'Our_Support' 		     => $_POST['Our_Support'],
            'Overall_Satisfaction' => $_POST['Overall_Satisfaction'],
            'anything_tell_us'     => $_POST['anything_tell_us'],
        );
		if($this->db->insert('user_feedback_table', $insertArr)) {
			$data = array("success" => "Submitted successfully...");
		} else {
			$data = array("error" => "Sorry... Something went wrong, Please try again later!");
		}
		echo json_encode($data);
	}
}
