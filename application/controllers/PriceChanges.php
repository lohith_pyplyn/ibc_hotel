<?php
header('Content-Type: text/html');
class PriceChanges extends CI_Controller
{

	function __construct() {
		parent::__construct();
		$this->load->library('session');
	}

	function get_promocode_by_name($promocode, $price, $check_in) {
		$this->load->model('price_change_model');
		$promocode_details = $this->price_change_model->get_promocode_by_name($promocode, $price, $check_in);
		echo json_encode($promocode_details);
	}

	function is_code_exist($code) {
		header('Content-Type: application/json');
		$this->load->model('price_change_model');
		$isCodeExist = $this->price_change_model->is_code_exist($code);
		echo json_encode($isCodeExist);
	}

	function get_temp_prices() {
		header('Content-Type: application/json');
		$this->load->model('price_change_model');
		$temp_prices = $this->price_change_model->get_temp_prices();
		echo json_encode($temp_prices);
	}

	function get_promotion_codes() {
		header('Content-Type: application/json');
		$this->load->model('price_change_model');
		$promotion_codes = $this->price_change_model->get_promotion_codes();
		echo json_encode($promotion_codes);
	}

	function get_temp_price_details($id) {
		header('Content-Type: application/json');
		$this->load->model('price_change_model');
		$temp_price_details = $this->price_change_model->get_temp_price_details($id);
		echo json_encode($temp_price_details);
	}

  function get_week_days() {
    header('Content-Type: application/json');
		$this->load->model('price_change_model');
		$week_days= $this->price_change_model->get_week_days();
		echo json_encode($week_days);
  }

	function add_temp_price() {
		$_POST = json_decode(file_get_contents("php://input"),true);

		if(isset($_POST['name']) && !empty($_POST['name'])) {
		} else {
			$data = array("error"=>"Name field is required !");
			echo json_encode($data);
			return  ;
		}
		if(isset($_POST['start_from']) && !empty($_POST['start_from'])) {
		} else {
			$data = array("error"=>"Prices from date is required !");
			echo json_encode($data);
			return  ;
		}
		if(isset($_POST['end']) && !empty($_POST['end'])) {
		} else {
			$data = array("error"=>"Prices end date is required !");
			echo json_encode($data);
			return  ;
		}
		if(strtotime($_POST['start_from']) > strtotime($_POST['end'])) {
			$data = array("error"=>"End date cannot be earlier than start date!");
			echo json_encode($data);
			return  ;
		}
		$days = array();
		if(!empty($_POST['weekDays'])) {
			asort($_POST['weekDays']);
			foreach ($_POST['weekDays'] as $value) {
				$days[] = $value['short_name'];
			}
			$week_days = implode(', ', $days);
		} else {
			$data = array("error"=>"At least one day must be selected !");
			echo json_encode($data);
			return  ;
		}

		$insertArray = array(
			// 'hotel_id' 	=> $this->session->userdata('hotel_id'),
			'name'			=> $_POST['name'],
			'start_date'=> $_POST['start_from'],
			'end_date'	=> $_POST['end'],
			'days'			=> $week_days
		);

		if($this->db->insert('temp_price_changes', $insertArray)) {
				$last_id = $this->db->insert_id();
				foreach ($_POST['accommodations'] as $value) {
						$new_price = $value['price'] + $value['new_price'];
						$insertAccommodations = array(
							'price_change_id'		=> $last_id,
							'accommodation_id'	=> $value['id'],
							'discount'					=> $value['new_price'],
							'price'							=> $new_price
						);
						$this->db->insert('temp_price_changes_for_accommodation', $insertAccommodations);
				}
				$data = array("success" => "New temporary price added successfully...");
		} else {
			$data = array("error" => "Sorry... Something went wrong, Please try again later!");
		}
		echo json_encode($data);
	}

	function update_temp_price() {
		$_POST = json_decode(file_get_contents("php://input"),true);

		if(isset($_POST['name']) && !empty($_POST['name'])) {
		} else {
			$data = array("error"=>"Name field is required !");
			echo json_encode($data);
			return  ;
		}
		if(isset($_POST['start_date']) && !empty($_POST['start_date'])) {
		} else {
			$data = array("error"=>"Prices from date is required !");
			echo json_encode($data);
			return  ;
		}
		if(isset($_POST['end_date']) && !empty($_POST['end_date'])) {
		} else {
			$data = array("error"=>"Prices end date is required !");
			echo json_encode($data);
			return  ;
		}
		if(strtotime($_POST['start_date']) > strtotime($_POST['end_date'])) {
			$data = array("error"=>"End date cannot be earlier than start date!");
			echo json_encode($data);
			return  ;
		}
		$days = array();
		if(!empty($_POST['weekDays'])) {
			foreach ($_POST['weekDays'] as $value) {
				$days[] = $value['short_name'];
			}
			$week_days = implode(', ', $days);
		} else {
			$data = array("error"=>"At least one day must be selected !");
			echo json_encode($data);
			return  ;
		}

		$updateArray = array(
			'name'			=> $_POST['name'],
			'start_date'=> $_POST['start_date'],
			'end_date'	=> $_POST['end_date'],
			'days'			=> $week_days
		);

		if($this->db->update('temp_price_changes', $updateArray, array('id'=>$_POST['id']))) {
				foreach ($_POST['accommodations'] as $value) {
					$new_price = $value['regular_price'] + $value['discount'];
					if(!empty($value['temp_price_id'])) {
						$updateAccommodations = array(
							'discount'					=> $value['discount'],
							'price'							=> $new_price
						);
						$this->db->update('temp_price_changes_for_accommodation', $updateAccommodations, array('id'=>$value['temp_price_id']));
					} else {
						$insertAccommodation = array(
							'price_change_id'		=> $_POST['id'],
							'accommodation_id'	=> $value['id'],
							'discount'					=> $value['discount'],
							'price'							=> $new_price
						);
						$this->db->insert('temp_price_changes_for_accommodation', $insertAccommodation);
					}
				}
				$data = array("success" => "Temporary price updated successfully...");
		} else {
			$data = array("error" => "Sorry... Something went wrong, Please try again later!");
		}
		echo json_encode($data);
	}

	function delete_temp_price($id) {
		if($this->db->delete('temp_price_changes', array('id'=>$id))) {
			if($this->db->delete('temp_price_changes_for_accommodation', array('price_change_id'=>$id))) {
				$data = array("success"=>"Price change deleted Successfully...");
			} else {
				$data = array("error"=>"Sorry. Price change not deleted!");
			}
		} else {
			$data = array("error"=>"Sorry. Price change not deleted!");
		}
		echo json_encode($data);
	}

	function add_promotion_code() {
		$_POST = json_decode(file_get_contents("php://input"),true);

		if(isset($_POST['promotion_code']) && !empty($_POST['promotion_code'])) {
		} else {
			$data = array("error"=>"Promotion code is required !");
			echo json_encode($data);
			return  ;
		}
		if(isset($_POST['description']) && !empty($_POST['description'])) {
		} else {
			$data = array("error"=>"Description is required !");
			echo json_encode($data);
			return  ;
		}
		if(isset($_POST['Minimum_booking_price']) && !empty($_POST['Minimum_booking_price'])) {
		} else {
			$data = array("error"=>"Minimum_booking_price is required !");
			echo json_encode($data);
			return  ;
		}
		if(isset($_POST['Offer_Discount_Price']) && !empty($_POST['Offer_Discount_Price'])) {
		} else {
			$data = array("error"=>"Offer_Discount_Price is required !");
			echo json_encode($data);
			return  ;
		}
		if(isset($_POST['start_from']) && !empty($_POST['start_from'])) {
		} else {
			$data = array("error"=>"Prices from date is required !");
			echo json_encode($data);
			return  ;
		}
		if(isset($_POST['end']) && !empty($_POST['end'])) {
		} else {
			$data = array("error"=>"Prices end date is required !");
			echo json_encode($data);
			return  ;
		}
		if(strtotime($_POST['start_from']) > strtotime($_POST['end'])) {
			$data = array("error"=>"End date cannot be earlier than start date!");
			echo json_encode($data);
			return  ;
		}


		$insertArray = array(
			// 'hotel_id' 						=> $this->session->userdata('hotel_id'),
			'promotion_code'			=> $_POST['promotion_code'],
			'description'					=> $_POST['description'],
			'Minimum_booking_price'			=> $_POST['Minimum_booking_price'],
			'Offer_Discount_Price'			=> $_POST['Offer_Discount_Price'],
			'start_date'					=> $_POST['start_from'],
			'end_date'						=> $_POST['end'],
			//'discount_type'				=> $_POST['discount_type']
		);
    // $this->db->insert('promotion_discounts', $insertArray);
		if($this->db->insert('promotion_discounts', $insertArray)) {
				$data = array("success" => "New Promotion code added successfully...");
		} else {
			$data = array("error" => "Sorry... Something went wrong, Please try again later!");
		}
		echo json_encode($data);
	}

	function delete_promotion_code($id) {
		if($this->db->delete('promotion_discounts', array('id'=>$id))) {
				$data = array("success"=>"Promotion code deleted Successfully...");
		} else {
			$data = array("error"=>"Sorry. Promotion code not deleted!");
		}
		echo json_encode($data);
	}


}
