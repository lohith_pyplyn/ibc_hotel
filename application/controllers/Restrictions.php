<?php
header('Content-Type: text/html');
class Restrictions extends CI_Controller
{

	function __construct() {
		parent::__construct();
		$this->load->library('session');
	}

	function get_cancellation_policies() {
		header('Content-Type: application/json');
		$this->load->model('restriction_model');
		$policies = $this->restriction_model->get_cancellation_policies();
		echo json_encode($policies);
	}

	function delete_cancellation_policy($id) {
		if($this->db->delete('cancellation_policy', array('id'=>$id))) {
			$data = array("success"=>"Cancellation policy deleted Successfully...");
		} else {
			$data = array("error"=>"Sorry. Cancellation policy not deleted!");
		}
		echo json_encode($data);
	}


	function add_cancellation_policy() {
		$_POST = json_decode(file_get_contents("php://input"),true);

		if(isset($_POST['description']) && !empty($_POST['description'])) {
		} else {
			$data = array("error"=>"Description is required !");
			echo json_encode($data);
			return  ;
		}

		if(isset($_POST['policy_type']) && !empty($_POST['policy_type'])) {
		} else {
			$_POST['policy_type'] = 1;
		}
        $full_refund_days= 0;
        $partial_refund_days = 0;
        $cancel_percent = 0;
        $charge_percent = 0;
		if($_POST['policy_type'] == 1) {
			if(isset($_POST['full_refund_days']) && !empty($_POST['full_refund_days'])) {
                $full_refund_days= $_POST['full_refund_days'];
			} else {
				$full_refund_days = 0;
			}
		} else {
			if(isset($_POST['cancel_percent']) && !empty($_POST['cancel_percent'])) {
                $cancel_percent = $_POST['cancel_percent'];
			} else {
				$cancel_percent = 0;
			}
			if(isset($_POST['partial_refund_days']) && !empty($_POST['partial_refund_days'])) {
                $partial_refund_days = $_POST['partial_refund_days'];
			} else {
				$partial_refund_days = 0;
			}
			if(isset($_POST['charge_percent']) && !empty($_POST['charge_percent'])) {
                $charge_percent = $_POST['charge_percent'];
			} else {
                $charge_percent = 0;
			}
		}

        $insertArray = array(
            'description'   		=> $_POST['description'],
            'policy_type'  			=> $_POST['policy_type'],
            'full_refund_days' 		=> $full_refund_days,
            'cancel_fee'			=> $cancel_percent,
            'partial_refund_days'   => $partial_refund_days,
            'charge_percent' 		=> $charge_percent
        );

		if($this->db->insert('cancellation_policy', $insertArray)) {
			$data = array("success" => "New policy added successfully...");
		} else {
			$data = array("error" => "Sorry... Something went wrong, Please try again later!");
		}
		echo json_encode($data);
	}

}
