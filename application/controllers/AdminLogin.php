<?php
header('Content-Type: text/html');
class AdminLogin extends CI_Controller
{
  
	function __construct() {
		parent::__construct();
	}

  /**
   * Function : Login for admin
   * @author : Lohith
   * @param : Input array username and password 
   * @return : JSON encoded result array
   */
	function login() {
    header('Content-Type: application/json');
    $_POST = json_decode(file_get_contents("php://input"),true);
    if(isset($_POST['username']) && !empty($_POST['username']) && isset($_POST['password']) && !empty($_POST['password'])){
          $user_details = array(
            'username'  => $_POST['username'],
            'password'  => $_POST['password'],
            'user_type' => 'admin'
          );
          $check_login = $this->authentication_model->login_user($user_details);
          if ($check_login == true ) {
            $data = array("success" => "Logged in successfully...");
          } else {
            $data = array("error" => "Invalid Credentials!");
          }
     }else{
         $data = array("error" => "Incomplete Form!");
     }
     echo json_encode($data);
	}

	function checkLoginStatus() {
    header('Content-Type: application/json');
    $user_type = $this->session->userdata('user_type');
    if(isset($user_type)) {
      if($user_type != 'admin') {
        $data = array("logged_in" => false);
        echo json_encode($data);
        return;
      } 
    } else {
      $user_type = 'admin';
    }
    $is_logged_in = $this->authentication_model->check_login($user_type);
    $data = array("logged_in" => $is_logged_in);
    echo json_encode($data);
	}


  function logout() {
    @session_destroy();
    $this->session->sess_destroy();
  }

}
