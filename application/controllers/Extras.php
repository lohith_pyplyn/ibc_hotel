<?php
header('Content-Type: text/html');
class Extras extends CI_Controller
{

	function __construct() {
		parent::__construct();
		$this->load->library('session');
	}

  function get_extra_options() {
    header('Content-Type: application/json');
		$this->load->model('admin_model');
		$extra_options= $this->admin_model->get_extra_options();
		echo json_encode($extra_options);
  }

  function get_extra_option_by_id($id) {
    header('Content-Type: application/json');
		$this->load->model('admin_model');
		$extra_details = $this->admin_model->get_extra_option_by_id($id);
		echo json_encode($extra_details);
  }

  function delete_extra_option($id) {
    if($this->db->delete('extras', array('id'=>$id))) {
			$data = array("success"=>"Extra option deleted Successfully...");
		} else {
			$data = array("error"=>"Sorry. Extra option not deleted!");
		}
		echo json_encode($data);
  }

  function remove_extra_image($id) {
    $this->load->model('admin_model');
    $image_name = $this->admin_model->get_extra_image($id);
		if($this->db->update('extras', array('image'=>''), array('id'=>$id))) {
      $path = './uploads/extra_images/'.$image_name;
      unlink($path);
			$data = array("success"=>"Image removed Successfully...");
		} else {
			$data = array("error"=>"Sorry. Image not removed!");
		}
		echo json_encode($data);
	}

	function add_extra_option() {
		$_POST = json_decode(file_get_contents("php://input"),true);
      if(isset($_POST['name']) && !empty($_POST['name'])){
      }else{
          $data = array("error"=>"Name field is required !");
          echo json_encode($data);
          return  ;
      }

			if(isset($_POST['description']) && !empty($_POST['description'])){
      }else{
          $_POST['description'] = '';
      }

			if(isset($_POST['price']) && !empty($_POST['price'])){
				if($_POST['price'] < 1) {
					$data = array("error"=>"Price should be greater than 0 !");
					echo json_encode($data);
					return  ;
				}
      }else{
				$data = array("error"=>"Price is required !");
				echo json_encode($data);
				return  ;
      }

			if(isset($_POST['status']) && !empty($_POST['status'])){
      }else{
          $_POST['status'] = 0;
      }

			$insertArray = array(
				// 'hotel_id'			=>  $this->session->userdata('hotel_id'),
				'name'					=>	$_POST['name'],
				'description'		=> 	$_POST['description'],
				'price'					=>	$_POST['price'],
				'status'				=>	$_POST['status']
			);
			if($this->db->insert('extras', $insertArray)) {
				$data = array("success" => "Your extra details added Successfully...");
			} else {
				$data = array("error" => "Sorry... Something went wrong, Please try again later!");
			}
      echo json_encode($data);
  }

  function update_extra_option() {
		$_POST = json_decode(file_get_contents("php://input"),true);
			if(isset($_POST['name']) && !empty($_POST['name'])){
			}else{
					$data = array("error"=>"Name field is required !");
					echo json_encode($data);
					return  ;
			}

			if(isset($_POST['description']) && !empty($_POST['description'])){
			}else{
					$_POST['description'] = '';
			}

			if(isset($_POST['price']) && !empty($_POST['price'])){
				if($_POST['price'] < 1) {
					$data = array("error"=>"Price should be greater than 0 !");
					echo json_encode($data);
					return  ;
				}
			}else{
				$data = array("error"=>"Price is required !");
				echo json_encode($data);
				return  ;
			}

			if(isset($_POST['status']) && !empty($_POST['status'])){
			}else{
					$_POST['status'] = 0;
			}

			$updateArray = array(
				'name'					=>	$_POST['name'],
				'description'		=> 	$_POST['description'],
				'price'					=>	$_POST['price'],
				'status'				=>	$_POST['status']
			);
			if($this->db->update('extras', $updateArray, array('id'=>$_POST['id']))) {
				$data = array("success" => "Your extra details updated Successfully...");
			} else {
				$data = array("error" => "Sorry... Something went wrong, Please try again later!");
			}
			echo json_encode($data);
  }



}
