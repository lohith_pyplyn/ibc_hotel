<?php
header('Content-Type: text/html');
class Agent_controller extends CI_Controller
{

	function __construct() {
		parent::__construct();
		$this->load->library('session');
	}
	function getHotelByUser(){
		header('Content-Type: application/json');
		$user_id = $this->session->userdata('user_id');
		$Hcount = $this->mdl_common->getHotelByUser($user_id);
		echo json_encode($Hcount);
	}
	function getSubAgentById(){
	header('Content-Type: application/json');
	$user_id = $this->session->userdata('user_id');
	$AllSubAgent = $this->mdl_common->getAllSubAgentById($user_id);
	echo json_encode($AllSubAgent);
}



function completeTask($id) {
  header('Content-Type: application/json');

  $arr = array(
    'status' => 'paid'
  );

  if($this->db->update('bookings', $arr, array('id' => $id))){
    $data = array("message"=>"Task Updated Succesfully...");
    echo json_encode($data);
  }else{
    $data = array("error"=>"Sorry... Task Not Updated !");
    echo json_encode($data);
  }

}


function add_supplier(){
	$_POST = json_decode(file_get_contents("php://input"),true);
	if(isset($_POST['supplier_type']) && !empty($_POST['supplier_type'])){
	}else{
		$data = array("error"=>"Supplier Type field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['supplier_name']) && !empty($_POST['supplier_name'])){
	}else{
		$data = array("error"=>"Supplier Name field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['email']) && !empty($_POST['email'])){
	}else{
		$data = array("error"=>"Supplier Email field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['address']) && !empty($_POST['address'])){
	}else{
		$data = array("error"=>"Supplier address field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['city']) && !empty($_POST['city'])){
	}else{
		$data = array("error"=>"Supplier city field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['state']) && !empty($_POST['state'])){
	}else{
		$data = array("error"=>"Supplier state field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['country']) && !empty($_POST['country'])){
	}else{
		$data = array("error"=>"Supplier country field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['zipcode']) && !empty($_POST['zipcode'])){
	}else{
		$data = array("error"=>"Supplier zipcode field is required !");
		echo json_encode($data);
		return  ;
	}
	$sup_array = array(
		'supplier_type' => $_POST['supplier_type'],
		'supplier_name' => $_POST['supplier_name'],
		'email'					=> $_POST['email'],
		'phone'					=> $_POST['address'],
		'address'				=> $_POST['address'],
		'city'					=> $_POST['city'],
		'state'					=> $_POST['state'],
		'country'				=> $_POST['country'],
		'zipcode'				=> $_POST['zipcode'],
		'user_id'				=> $this->session->userdata('user_id')
	);
	if($this->db->insert('supplier', $sup_array)){
		$data = array("message" => "Supplier Added Successfully");
	} else {
		$data = array("message" => "Error in adding Supplier");
	}
	echo json_encode($data);
}
function getAllSuppliers(){
	header('Content-Type: application/json');
	$allSuppliers = $this->mdl_common->get_all_suppliers();
	echo json_encode($allSuppliers);
}
function get_supplier_by_id($id){
	header('Content-Type: application/json');
	$supplierData = $this->mdl_common->get_supplier_by_id($id);
	echo json_encode($supplierData);

}
function update_supplier(){
	$_POST = json_decode(file_get_contents("php://input"),true);
	if(isset($_POST['supplier_type']) && !empty($_POST['supplier_type'])){
	}else{
		$data = array("error"=>"Supplier Type field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['supplier_name']) && !empty($_POST['supplier_name'])){
	}else{
		$data = array("error"=>"Supplier Name field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['email']) && !empty($_POST['email'])){
	}else{
		$data = array("error"=>"Supplier Email field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['address']) && !empty($_POST['address'])){
	}else{
		$data = array("error"=>"Supplier address field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['city']) && !empty($_POST['city'])){
	}else{
		$data = array("error"=>"Supplier city field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['state']) && !empty($_POST['state'])){
	}else{
		$data = array("error"=>"Supplier state field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['country']) && !empty($_POST['country'])){
	}else{
		$data = array("error"=>"Supplier country field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['zipcode']) && !empty($_POST['zipcode'])){
	}else{
		$data = array("error"=>"Supplier zipcode field is required !");
		echo json_encode($data);
		return  ;
	}
	$sup_array = array(
		'supplier_type' => $_POST['supplier_type'],
		'supplier_name' => $_POST['supplier_name'],
		'email'					=> $_POST['email'],
		'phone'					=> $_POST['address'],
		'address'				=> $_POST['address'],
		'city'					=> $_POST['city'],
		'state'					=> $_POST['state'],
		'country'				=> $_POST['country'],
		'zipcode'				=> $_POST['zipcode']
	);
	if($this->db->update('supplier',$sup_array,array('id' => $_POST['id']))){
		$data = array("message"=>"Supplier Updated Succesfully...");
	} else {
		$data = array("error"=>"Error in updating supplier...");
	}
	echo json_encode($data);
}
function delete_supplier($id){
	if($this->db->delete('supplier', array('id' => $id))){
		$data = array("success" => "Supplier Deleted Successfully");
	} else {
		$data = array("error" => "Sorry supplier Not Deleted!");
	}
	echo json_encode($data);
}
function add_hotel(){
//	$_POST = json_decode(file_get_contents("php://input"),true);
	if(isset($_POST['name']) && !empty($_POST['name'])){
	}else{
		$data = array("error"=>"Hotel Name field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['email']) && !empty($_POST['email'])){
	}else{
		$data = array("error"=>"Hotel email field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['phone']) && !empty($_POST['phone'])){
	}else{
		$data = array("error"=>"Hotel phone field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['address']) && !empty($_POST['address'])){
	}else{
		$data = array("error"=>"Hotel address field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['city']) && !empty($_POST['city'])){
	}else{
		$data = array("error"=>"HOtel city field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['state']) && !empty($_POST['state'])){
	}else{
		$data = array("error"=>"Hotel state field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['country']) && !empty($_POST['country'])){
	}else{
		$data = array("error"=>"Hotel country field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['zipcode']) && !empty($_POST['zipcode'])){
	}else{
		$data = array("error"=>"Supplier zipcode field is required !");
		echo json_encode($data);
		return  ;
	}
	$hotel_array = array(
		'name' 					=> $_POST['name'],
		'email'					=> $_POST['email'],
		'phone'					=> $_POST['phone'],
		'address'				=> $_POST['address'],
		'city'					=> $_POST['city'],
		'state'					=> $_POST['state'],
		'country'				=> $_POST['country'],
		'zipcode'				=> $_POST['zipcode'],
		'user_id'				=> $this->session->userdata('user_id')
	);
	if($this->db->insert('hotel', $hotel_array)){
		$last_id = $this->db->insert_id();
		if(!empty($_FILES)) {
			$date = date("ymdHis");
			$imageFiles = $_FILES[ 'file' ][ 'name' ];
			$image = 'hotel_'.$imageFiles;

			$config = array(
				'upload_path' => './uploads/hotel_images',
				'max_size' => '8000240',
				'allowed_types' => 'png|gif|jpg|jpeg',
				'overwrite'=> FALSE,
				'remove_spaces' => TRUE,
				'file_name'=> $image
			);
			$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('file')) {
					$data = array('error' => 'Not a valid files, only png|gif|jpg|jpeg ');
					echo json_encode($data , JSON_FORCE_OBJECT);
				} else {
					if(!$this->db->update('hotel', array('image'=>$image), array('id'=>$last_id))){
						$data = array('error' => $this->db->_error_message());
						echo json_encode($data , JSON_FORCE_OBJECT);
						return;
					} else {
						$data = array("message" => "Your Details Added Successfully...");
						echo json_encode($data , JSON_FORCE_OBJECT);
						return;
					}
				}
			}
			$data = array("message" => "Your Details Added Successfully...");
		} else {
			$data = array("error" => "Sorry... Something went wrong, Please try again later!");
		}
		echo json_encode($data);
}
function remove_hotel_image($id) {
    $image_name = $this->mdl_common->get_hotel_image($id);
		if($this->db->update('hotel', array('image'=>''), array('id'=>$id))) {
      $path = './uploads/hotel_images/'.$image_name->image;
      unlink($path);
			$data = array("success"=>"Image removed Successfully...");
		} else {
			$data = array("error"=>"Sorry. Image not removed!");
		}
		echo json_encode($data);
	}

function getAllHotels(){
	header('Content-Type: application/json');
	$allHotels = $this->mdl_common->get_all_hotels();
	echo json_encode($allHotels);
}
function get_hotel_by_id($id){
	header('Content-Type: application/json');
	$hotelData = $this->mdl_common->get_hotel_by_id($id);
	echo json_encode($hotelData);

}
function update_hotel(){
	// $_POST = json_decode(file_get_contents("php://input"),true);
	if(isset($_POST['name']) && !empty($_POST['name'])){
	}else{
		$data = array("error"=>"Hotel Name field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['email']) && !empty($_POST['email'])){
	}else{
		$data = array("error"=>"Hotel email field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['phone']) && !empty($_POST['phone'])){
	}else{
		$data = array("error"=>"Hotel phone field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['address']) && !empty($_POST['address'])){
	}else{
		$data = array("error"=>"Hotel address field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['city']) && !empty($_POST['city'])){
	}else{
		$data = array("error"=>"HOtel city field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['state']) && !empty($_POST['state'])){
	}else{
		$data = array("error"=>"Hotel state field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['country']) && !empty($_POST['country'])){
	}else{
		$data = array("error"=>"Hotel country field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['zipcode']) && !empty($_POST['zipcode'])){
	}else{
		$data = array("error"=>"Supplier zipcode field is required !");
		echo json_encode($data);
		return  ;
	}
	$hotel_array = array(
		'name' 					=> $_POST['name'],
		'email'					=> $_POST['email'],
		'phone'					=> $_POST['phone'],
		'address'				=> $_POST['address'],
		'city'					=> $_POST['city'],
		'state'					=> $_POST['state'],
		'country'				=> $_POST['country'],
		'zipcode'				=> $_POST['zipcode'],
	);
	if($this->db->update('hotel',$hotel_array,array('id' => $_POST['id']))){
		//$last_id = $this->db->insert_id();
		if(!empty($_FILES)) {
			$date = date("ymdHis");
			$imageFiles = $_FILES[ 'file' ][ 'name' ];
			$image = 'hotel_'.$imageFiles;

			$config = array(
				'upload_path' => './uploads/hotel_images',
				'max_size' => '8000240',
				'allowed_types' => 'png|gif|jpg|jpeg|PNG|GIF|JPG|JPEG',
				'overwrite'=> FALSE,
				'remove_spaces' => TRUE,
				'file_name'=> $image
			);
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('file')) {
				$data = array('error' => 'Not a valid files, only png|gif|jpg|jpeg ');
				echo json_encode($data , JSON_FORCE_OBJECT);
			} else {
				if(!$this->db->update('hotel', array('image'=>$image), array('id'=>$_POST['id']))){
					$data = array('error' => $this->db->_error_message());
					echo json_encode($data , JSON_FORCE_OBJECT);
					return;
				} else {
					$data = array("message" => "Your Details Updated Successfully...");
					echo json_encode($data , JSON_FORCE_OBJECT);
					return;
				}
			}
		}
		$data = array("message" => "Your Details Updated Successfully...");
	} else {
		$data = array("error" => "Sorry... Something went wrong, Please try again later!");
	}
	echo json_encode($data);
}
function delete_hotel($id){
	$image = $this->mdl_common->getHotelById($id);
	$image = $image->image;
	if($this->db->delete('hotel', array('id' => $id))){
		if(!empty($image)) {
				$path = './uploads/hotel_images/'.$image;
				unlink($path);
			}
		$data = array("success" => "Hotel Deleted Successfully");
	} else {
		$data = array("error" => "Sorry hotel Not Deleted!");
	}
	echo json_encode($data);
}
function add_roomtypes(){
	$_POST = json_decode(file_get_contents("php://input"),true);
	if(isset($_POST['name']) && !empty($_POST['name'])){
	}else{
		$data = array("error"=>"RoomType Name field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['no_of_pax']) && !empty($_POST['no_of_pax'])){
	}else{
		$data = array("error"=>"No. of Pax field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['description']) && !empty($_POST['description'])){
	}else{
		$data = array("error"=>"Description field is required !");
		echo json_encode($data);
		return  ;
	}
	$rt_array = array(
		'room_name' 				=> $_POST['name'],
		'no_of_pax'					=> $_POST['no_of_pax'],
		'purchase_price'		=> $_POST['purchase_price'],
		'sale_price'				=> $_POST['sale_price'],
		'tax_price'					=> $_POST['tax_price'],
		'description'				=> $_POST['description'],
		'user_id'						=> $this->session->userdata('user_id')
	);
	if($this->db->insert('room_types', $rt_array)){
		$data = array("message" => "Room Types Added Successfully");
	} else {
		$data = array("message" => "Error in adding Room Types");
	}
	echo json_encode($data);
}
function getAllRoomTypes(){
	header('Content-Type: application/json');
	$allRoomTypes = $this->mdl_common->get_all_roomtypes();
	echo json_encode($allRoomTypes);
}
function get_roomtype_by_id($id){
	header('Content-Type: application/json');
	$roomData = $this->mdl_common->get_roomtype_by_id($id);
	echo json_encode($roomData);

}
function update_roomtype(){
	$_POST = json_decode(file_get_contents("php://input"),true);
	if(isset($_POST['room_name']) && !empty($_POST['room_name'])){
	}else{
		$data = array("error"=>"RoomType Name field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['no_of_pax']) && !empty($_POST['no_of_pax'])){
	}else{
		$data = array("error"=>"No. of Pax field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['description']) && !empty($_POST['description'])){
	}else{
		$data = array("error"=>"Description field is required !");
		echo json_encode($data);
		return  ;
	}
	$rt_array = array(
		'room_name' 				=> $_POST['room_name'],
		'no_of_pax'					=> $_POST['no_of_pax'],
		'purchase_price'		=> $_POST['purchase_price'],
		'sale_price'				=> $_POST['sale_price'],
		'tax_price'					=> $_POST['tax_price'],
		'description'				=> $_POST['description'],
	);
	if($this->db->update('room_types',$rt_array,array('id' => $_POST['id']))){
		$data = array("message"=>"Room Type Updated Succesfully...");
	} else {
		$data = array("error"=>"Error in updating Room Type...");
	}
	echo json_encode($data);
}
function delete_roomtype($id){
	if($this->db->delete('room_types', array('id' => $id))){
		$data = array("success" => "Room Type Deleted Successfully");
	} else {
		$data = array("error" => "Sorry Room Type Not Deleted!");
	}
	echo json_encode($data);
}
function add_customer(){
	$_POST = json_decode(file_get_contents("php://input"),true);
	if(isset($_POST['customer_type']) && !empty($_POST['customer_type'])){
	}else{
		$data = array("error"=>"Customer Type field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['name']) && !empty($_POST['name'])){
	}else{
		$data = array("error"=>"RoomType Name field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['email']) && !empty($_POST['email'])){
	}else{
		$data = array("error"=>"Customer Email field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['phone']) && !empty($_POST['phone'])){
	}else{
		$data = array("error"=>"Customer Phone Number field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['proof_id']) && !empty($_POST['proof_id'])){
	}else{
		$data = array("error"=>"Customer Proof Id field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['address']) && !empty($_POST['address'])){
	}else{
		$data = array("error"=>"Customer Address field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['city']) && !empty($_POST['city'])){
	}else{
		$data = array("error"=>"Customer City field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['state']) && !empty($_POST['state'])){
	}else{
		$data = array("error"=>"Customer State field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['country']) && !empty($_POST['country'])){
	}else{
		$data = array("error"=>"Customer Country field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['zipcode']) && !empty($_POST['zipcode'])){
	}else{
		$data = array("error"=>"Zipcode field is required !");
		echo json_encode($data);
		return  ;
	}
	$cust_array = array(
		'name'						=> $_POST['name'],
		'customer_type' 	=> $_POST['customer_type'],
		'email'						=> $_POST['email'],
		'phone' 					=> $_POST['phone'],
		'proof_id'				=> $_POST['proof_id'],
		'address'					=> $_POST['address'],
		'city'						=> $_POST['city'],
		'state'						=> $_POST['state'],
		'country' 				=> $_POST['country'],
		'zipcode'					=> $_POST['zipcode'],
		'credit_limit'		=> $_POST['credit_limit'],
		'user_id'					=> $this->session->userdata('user_id')
	);
	if($this->db->insert('sub_agent', $cust_array)){
		$data = array("message" => "Customer Added Successfully");
	} else {
		$data = array("message" => "Error in adding customer");
	}
	echo json_encode($data);
}
function getAllCustomers(){
	header('Content-Type: application/json');
	$allCustomer = $this->mdl_common->get_all_customer();
	echo json_encode($allCustomer);
}
function get_customer_by_id($id){
	header('Content-Type: application/json');
	$customerData = $this->mdl_common->get_customer_by_id($id);
	echo json_encode($customerData);

}
function update_customer(){
	$_POST = json_decode(file_get_contents("php://input"),true);
	if(isset($_POST['customer_type']) && !empty($_POST['customer_type'])){
	}else{
		$data = array("error"=>"Customer Type field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['name']) && !empty($_POST['name'])){
	}else{
		$data = array("error"=>"RoomType Name field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['email']) && !empty($_POST['email'])){
	}else{
		$data = array("error"=>"Customer Email field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['phone']) && !empty($_POST['phone'])){
	}else{
		$data = array("error"=>"Customer Phone Number field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['proof_id']) && !empty($_POST['proof_id'])){
	}else{
		$data = array("error"=>"Customer Proof Id field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['address']) && !empty($_POST['address'])){
	}else{
		$data = array("error"=>"Customer Address field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['city']) && !empty($_POST['city'])){
	}else{
		$data = array("error"=>"Customer City field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['state']) && !empty($_POST['state'])){
	}else{
		$data = array("error"=>"Customer State field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['country']) && !empty($_POST['country'])){
	}else{
		$data = array("error"=>"Customer Country field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['zipcode']) && !empty($_POST['zipcode'])){
	}else{
		$data = array("error"=>"Zipcode field is required !");
		echo json_encode($data);
		return  ;
	}
	$cust_array = array(
		'name'						=> $_POST['name'],
		'customer_type' 	=> $_POST['customer_type'],
		'email'						=> $_POST['email'],
		'phone' 					=> $_POST['phone'],
		'proof_id'				=> $_POST['proof_id'],
		'address'					=> $_POST['address'],
		'city'						=> $_POST['city'],
		'state'						=> $_POST['state'],
		'country' 				=> $_POST['country'],
		'zipcode'					=> $_POST['zipcode'],
		'credit_limit'		=> $_POST['credit_limit']
	);
	if($this->db->update('sub_agent',$cust_array,array('id' => $_POST['id']))){
		$data = array("message"=>"Customer Updated Succesfully...");
	} else {
		$data = array("error"=>"Error in updating Customer...");
	}
	echo json_encode($data);
}
function delete_customer($id){
	if($this->db->delete('customer', array('id' => $id))){
		$data = array("success" => "Customer Deleted Successfully");
	} else {
		$data = array("error" => "Sorry Customer Not Deleted!");
	}
	echo json_encode($data);
}
function add_meal(){
	$_POST = json_decode(file_get_contents("php://input"),true);
	if(isset($_POST['name']) && !empty($_POST['name'])){
	}else{
		$data = array("error"=>"Meal Name field is required !");
		echo json_encode($data);
		return  ;
	}
	$meal_array = array(
		'name' 								=> $_POST['name'],
		'purchase_price'			=> $_POST['purchase_price'],
		'sale_price'					=> $_POST['sale_price'],
		'tax_price'						=> $_POST['tax_price'],
		'description'					=> $_POST['description'],
		'user_id'							=> $this->session->userdata('user_id')
	);
	if($this->db->insert('meal', $meal_array)){
		$data = array("message" => "Meal Added Successfully");
	} else {
		$data = array("message" => "Error in adding Meal");
	}
	echo json_encode($data);
}
function getAllMeals(){
	header('Content-Type: application/json');
	$allMeal = $this->mdl_common->get_all_meal();
	echo json_encode($allMeal);
}
function get_meal_by_id($id){
	header('Content-Type: application/json');
	$mealData = $this->mdl_common->get_meal_by_id($id);
	echo json_encode($mealData);

}
function update_meal(){
	$_POST = json_decode(file_get_contents("php://input"),true);
	if(isset($_POST['name']) && !empty($_POST['name'])){
	}else{
		$data = array("error"=>"RoomType Name field is required !");
		echo json_encode($data);
		return  ;
	}
	$meal_array = array(
		'name' 								=> $_POST['name'],
		'purchase_price'			=> $_POST['purchase_price'],
		'sale_price'					=> $_POST['sale_price'],
		'tax_price'						=> $_POST['tax_price'],
		'description'					=> $_POST['description'],
		'user_id'							=> $this->session->userdata('user_id')
	);
	if($this->db->update('meal',$meal_array,array('id' => $_POST['id']))){
		$data = array("message"=>"Meal Updated Succesfully...");
	} else {
		$data = array("error"=>"Error in updating Meal...");
	}
	echo json_encode($data);
}
function delete_meal($id){
	if($this->db->delete('meal', array('id' => $id))){
		$data = array("success" => "Meal Deleted Successfully");
	} else {
		$data = array("error" => "Sorry Meal Not Deleted!");
	}
	echo json_encode($data);
}
function add_service(){
	$_POST = json_decode(file_get_contents("php://input"),true);
	if(isset($_POST['name']) && !empty($_POST['name'])){
	}else{
		$data = array("error"=>"Service Name field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['route_from']) && !empty($_POST['route_from'])){
	}else{
		$data = array("error"=>"Route From field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['route_to']) && !empty($_POST['route_to'])){
	}else{
		$data = array("error"=>"Route To field is required !");
		echo json_encode($data);
		return  ;
	}
	$service_array = array(
		'name' 							=> $_POST['name'],
		'route_from'					=> $_POST['route_from'],
		'route_to'						=> $_POST['route_to'],
		'user_id'						=> $this->session->userdata('user_id')
	);
	if($this->db->insert('service', $service_array)){
		$data = array("message" => "Service Added Successfully");
	} else {
		$data = array("message" => "Error in adding service"." ".$this->db->_error_message());
	}
	echo json_encode($data);
}
function getAllService(){
	header('Content-Type: application/json');
	$allService = $this->mdl_common->get_all_service();
	echo json_encode($allService);
}
function get_service_by_id($id){
	header('Content-Type: application/json');
	$serviceData = $this->mdl_common->get_service_by_id($id);
	echo json_encode($serviceData);

}
function update_service(){
	$_POST = json_decode(file_get_contents("php://input"),true);
	if(isset($_POST['name']) && !empty($_POST['name'])){
	}else{
		$data = array("error"=>"Service Name field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['route_from']) && !empty($_POST['route_from'])){
	}else{
		$data = array("error"=>"Route From field is required !");
		echo json_encode($data);
		return  ;
	}
	if(isset($_POST['route_to']) && !empty($_POST['route_to'])){
	}else{
		$data = array("error"=>"Route To field is required !");
		echo json_encode($data);
		return  ;
	}
	$service_array = array(
		'name' 			=> $_POST['name'],
		'route_from'	=> $_POST['route_from'],
		'route_to'		=> $_POST['route_to'],
		'user_id'		=> $this->session->userdata('user_id')
	);
	if($this->db->update('service',$service_array,array('id' => $_POST['id']))){
		$data = array("message"=>"Service Updated Succesfully...");
	} else {
		$data = array("error"=>"Error in updating service...");
	}
	echo json_encode($data);
}
function delete_service($id){
	if($this->db->delete('service', array('id' => $id))){
		$data = array("success" => "Service Deleted Successfully");
	} else {
		$data = array("error" => "Sorry Service Not Deleted!");
	}
	echo json_encode($data);
}
function getAllBookings(){
	header('Content-Type: application/json');
	$allBooking = $this->mdl_common->get_all_bookings();
	$today = date('Y-m-d');
	$date1 = date_create($today);
	$bookings = array();
	foreach($allBooking as $value) {
		$date2 = date_create($value['check_out']);
		if($date1 == $date2) {
			$value['extend'] = true;
		} else {
			$value['extend'] = false;
		}

		if($date1 > $date2) {
			$value['expired'] = true;
		} else {
			$value['expired'] = false;
		}

		$check_in = date_create($value['check_in']);
		$tomorrow = date('Y-m-d',strtotime($value['check_in'] . "+1 days"));
		$tomorrow = date_create($tomorrow);
		if($date1 == $check_in || $tomorrow == $date1 ) {
			$value['check_in_available'] = true;
		} else {
			$value['check_in_available'] = false;
		}

		if($value['expired'] != true && $value['stay_status'] != 'checked_out') {
			$value['is_editable'] = true;
		} else {
			$value['is_editable'] = false;
		}


		$bookings[] = $value;
	}
	echo json_encode($bookings);
}
function get_booking_by_id($id){
	header('Content-Type: application/json');
	$book = $this->mdl_common->get_booking_by_id($id);
	$moved_room_history = $this->mdl_common->get_exteded_history($id);
	$extended_checkout_history = $this->mdl_common->get_checkout_extended_history($id);
	$book->moved_room_history = $moved_room_history;
	$book->extended_checkout_history = $extended_checkout_history;
	echo json_encode($book);
}
function get_agent_by_type($id){
	header('Content-Type: application/json');
	$agent = $this->mdl_common->get_agent_by_type($id);
	echo json_encode($agent);
}
function get_no_of_pax_by_id($id){
header('Content-Type: application/json');
$pax_no = $this->mdl_common->get_no_of_pax_by_id($id);
echo json_encode($pax_no->no_of_pax);
}

function getdatediff($check_in,$check_out){
	header('Content-Type: application/json');
	$check_in = date_create($check_in);
	$check_out = date_create($check_out);
	$diff  	 = date_diff($check_in,$check_out);
	$no_of_days = $diff->format("%a");
	if($no_of_days == 0) {
		$no_of_days = 1;
	}
	echo json_encode($no_of_days);
}

function isSubAgentAvailable(){
	$agent1 = $this->mdl_common->getAgent(1);
	$agent2 = $this->mdl_common->getAgent(2);
	$agent3 = $this->mdl_common->getAgent(3);
	$agent4 = $this->mdl_common->getAgent(4);
	$agent_array = array(
		'agent' 					=> $agent1,
		'overseas_agent'	=> $agent2,
		'online_agent'		=> $agent3,
		'travel_agent'		=> $agent4
	);
	echo json_encode($agent_array);
}
function get_all_services(){
	header('Content-Type: application/json');
	$all_service = $this->mdl_common->get_all_services();
	echo json_encode($all_service);
}

}
