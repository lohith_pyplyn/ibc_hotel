<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model {

	public function __construct(){
      parent::__construct();
      $this->load->library('session');
  	}
		function get_promotion_code() {
			$query = $this->db->query('SELECT * FROM promotion_code');
			return $query->row();
		}
		function get_user_accounts() {
			$query = $this->db->query('SELECT * FROM user');
			return $query->result_array();
		}
  	public function getLastAgentNumber(){
		$query = $this->db->query('SELECT * FROM agent ORDER BY agent_number DESC LIMIT 1');
		return $query->row();
	}

	public function get_accommodation_image($id) {
		$result = $this->allSelects("SELECT * FROM accommodation WHERE id=".$id);
		if(!empty($result)) {
			return $result[0]['image'];
		} else {
			return false;
		}
	}
	public function get_accommodation_list(){
		$query = $this->db->get('accommodation');
		return $query->result();
	}
	function getpricingDetails($id){
		$result =$this->allSelects('SELECT id, price FROM room_types WHERE id='.$id);
		if(!empty($result)) {
			return $result;
		} else {
			return false;
		}
	}
	function updateprice($id){
		$result =$this->allSelects('UPDATE room_types SET price = FROM room_types WHERE id='.$id);
		if(!empty($result)) {
			return $result;
		} else {
			return false;
		}
	}


	public function getRoomList($id){
		// $this->db->where('hotel_id', $this->session->userdata('hotel_id'));
		// $this->db->where('accommodation_id', $id);
		// $query = $this->db->get('rooms');
		// return $query->result();
		$result = $this->allSelects("select ac.price as acc_price, r.* from accommodation as ac, rooms as r where r.hotel_id='".$this->session->userdata('hotel_id')."' and r.accommodation_id = '".$id."' and r.accommodation_id = ac.id");
		if(!empty($result)){
			return $result;
		} else {
			return false;
		}
	}
	public function get_extra_image($id) {
		$result = $this->allSelects("SELECT * FROM extras WHERE id=".$id);
		if(!empty($result)) {
			return $result[0]['image'];
		} else {
			return false;
		}
	}

	public function get_extra_option_by_id($id) {
		$result = $this->allSelects("SELECT * FROM extras WHERE id=".$id);
		if(!empty($result)) {
			return $result;
		} else {
			return false;
		}
	}

	public function get_extra_options() {
		$query = $this->db->get('extras');
		return $query->result();
	}

	public function add_new_unit($insertArray) {
		// $insertArray = array(
		// 	'accommodation_id' 	=> $unit['id'],
		// 	'unit_name'					=> $unit['unit_name']
		// );
		if($this->db->insert('rooms', $insertArray)) {
			return true;
		} else {
			return false;
		}
	}

	public function get_accommodation_details_by_id($id) {
		$result = $this->allSelects("SELECT a.*, b.object_type as name FROM accommodation a LEFT JOIN accommodation_types b on a.accommodation_type = b.id WHERE a.id=".$id);
		if(!empty($result)) {
			return $result;
		} else {
			return false;
		}
	}

	public function get_room1_types() {
		$query = $this->db->get('room_types');
		return $query->result();
	}

	public function get_selected_room_types() {
		$result = $this->allSelects("SELECT a.object_type, r.* FROM accommodation a LEFT JOIN room_types r on a.object_type = r.id group by a.object_type");
		return $result;
	}

	public function get_accommodation_types() {
		$query = $this->db->get('accommodation_types');
		return $query->result();
	}

	public function get_accommodations() {
		$result = $this->allSelects("SELECT a.*, t.object_type FROM accommodation a LEFT JOIN accommodation_types t on a.accommodation_type = t.id");
		if(!empty($result)) {
				foreach ($result as $value) {
					$value['rooms'] = $this->get_rooms($value['id']);
					$result_array[] = $value;
				}
				return $result_array;
		} else {
			return false;
		}
	}

	public function get_rooms($id) {
		$result = $this->allSelects("SELECT * FROM rooms WHERE accommodation_id =".$id." AND hotel_id=".$this->session->userdata('hotel_id'));
		if(!empty($result)) {
			return $result;
		} else {
			return false;
		}
	}

	public function getUnitDetails($id){
		$result = $this->allSelects("SELECT * from rooms WHERE id = ".$id);
		if(!empty($result)) {
			return $result;
		} else {
			return false;
		}
	}
	public function getRoomTypes() {
		$query = $this->db->get('room_types');
		return  $query->result();
	}
	#====================================================================================================
			#Des : To execute given query and return result in form of array
 	#====================================================================================================
	public function allSelects($sqlquery){
		$query = $this->db->query($sqlquery);
		return $query->result_array();
	}

}
?>
