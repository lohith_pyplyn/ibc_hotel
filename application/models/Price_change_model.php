<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Price_change_model extends CI_Model {

	public function __construct(){
      parent::__construct();
      $this->load->library('session');
  }

  	public function get_promocode_by_name($promocode, $price, $check_in) {
		  $result = $this->allSelects("SELECT * FROM promotion_discounts WHERE promotion_code = '$promocode' AND minimum_booking_price <= $price AND start_date <= '$check_in' AND end_date >= '$check_in'");
		  if(!empty($result)) {
			  return $result;
		  } else {
			  return false;
		  }
	  }

	public function is_code_exist($code) {
		$result = $this->allSelects("SELECT * FROM promotion_discounts WHERE promotion_code = '$code'");
		if(!empty($result)) {
			return true;
		} else {
			return false;
		}
	}

	public function get_temp_prices() {
		$hotel_id = $this->session->userdata('hotel_id');
		$result = $this->allSelects("SELECT * FROM temp_price_changes WHERE hotel_id = ".$hotel_id);
		return $result;
	}

	public function get_promotion_codes() {
		// $hotel_id = $this->session->userdata('hotel_id');
		$result = $this->allSelects("SELECT * FROM promotion_discounts ORDER BY id DESC");
		return $result;
	}

	public function get_temp_price_details($id) {
		$result = $this->allSelects("SELECT * FROM temp_price_changes WHERE id= ".$id);
		$details = array();
		foreach ($result as $value) {
			$value['accommodations'] 	= $this->getAccommodationPrices($value['id']);
			//$value['weekDays'] 				= $this->getWeekDays($value['days']);
			$details[] 								= $value;
		}
		return $details;
	}

	function getWeekDays($days) {
		$arr = explode(', ', $days);
		$days = array();
		foreach ($arr as $value) {
			$days[] = $this->allSelects("SELECT * FROM week_days WHERE short_name = '".$value."'");
		}
		return $days;
	}

	function getAccommodationPrices($id) {
		$hotel_id = $this->session->userdata('hotel_id');
		$result = $this->allSelects("SELECT id, name, price as regular_price FROM accommodation WHERE hotel_id=$hotel_id");
		if(!empty($result)) {
				foreach ($result as $value) {
					$array = $this->get_temp_prices_for_accommodation($value['id'], $id);
					if(!empty($array)){
						foreach ($array as  $value1) {
							$value['temp_price_id'] = $value1['temp_price_id'];
							$value['discount'] = $value1['discount'];
							$value['price']    = $value1['new_price'];
						}
					} else {
						$value['price_change_id'] = 0;
						$value['discount'] = 0;
						$value['price']    = 0;
					}
					$result_array[] = $value;
				}
				return $result_array;
		} else {
			return false;
		}
	}

	function get_temp_prices_for_accommodation($acc_id, $price_change_id) {
		$result = $this->allSelects("SELECT id as temp_price_id, discount, price as new_price FROM temp_price_changes_for_accommodation WHERE price_change_id = $price_change_id AND accommodation_id = $acc_id");
		return $result;
	}

	function getAccommodationPrices1($id) {
		$result = $this->allSelects("SELECT a.name, a.price as regular_price, t.* FROM temp_price_changes_for_accommodation t RIGHT JOIN accommodation a on t.accommodation_id = a.id WHERE price_change_id = ".$id);
		return $result;
	}

  public function get_week_days() {
    $week_days = $this->db->get('week_days');
    return $week_days->result_array();
  }

	#====================================================================================================
			#Des : To execute given query and return result in form of array
 	#====================================================================================================
	public function allSelects($sqlquery){

		//var_dump($sqlquery);
		//die();
		$query = $this->db->query($sqlquery);
		//var_dump($query);die();
		return $query->result_array();
	}


}
?>
