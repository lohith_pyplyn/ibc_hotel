<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Restriction_model extends CI_Model {

	public function __construct(){
      parent::__construct();
      $this->load->library('session');
  }

	public function get_cancellation_policies() {
		// $hotel_id = $this->session->userdata('hotel_id');
		$result = $this->allSelects("SELECT * FROM cancellation_policy ORDER BY id DESC");
		return $result;
	}

	#====================================================================================================
			#Des : To execute given query and return result in form of array
 	#====================================================================================================
	public function allSelects($sqlquery){

		//var_dump($sqlquery);
		//die();
		$query = $this->db->query($sqlquery);
		//var_dump($query);die();
		return $query->result_array();
	}


}
?>
