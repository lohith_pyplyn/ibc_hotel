<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_common extends CI_Model {

	public function __construct(){
        parent::__construct();
        $this->load->library('session');
	}
 function get_room_type($room_type_id){
	 $this->db->where('id', $room_type_id);
	 $query = $this->db->get('room_types');
	 return $query->row();
 }
	public function get_agent_renewal_history($id) {
		$result = $this->allSelects("SELECT * FROM agent_account_renewal WHERE user_id = $id ORDER BY expiry_date DESC");
		return $result;
	}

	function get_banks() {
		$user_id = $this->session->userdata('user_id');
		$result = $this->allSelects("SELECT * FROM banks WHERE user_id = $user_id");
		return $result;
	}

	function get_user_details() {
		$user_id = $this->session->userdata('user_id');
		$result = $this->allSelects("SELECT * FROM user WHERE user_id = $user_id");
		return $result;
	}
// 	public function getLastBookingNo(){
// 	$query = $this->db->query('SELECT * FROM booking ORDER BY resrervation_no DESC LIMIT 1');
// 	return $query->row();
// }

    function changeStayStatus($email) {
    	$mail_body = '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>New User Request</title>
        </head>

        <body>
            Hello '.$email.',<br />
                <div>&nbsp;&nbsp;&nbsp;Your Check-In status updated.</div><br /><br />
            Thanks,<br />
        </body>
        </html>
        ';
        return $mail_body;
    }

	#====================================================================================================
	#Des : for exist user
 	#====================================================================================================
	function isUserExist($where){
		$query = "SELECT user_id FROM user WHERE username = '".$where."'";
		$result = $this->db->query($query);
		$data = $result->result_array();

		if(isset($data) && !empty($data)){
			return false;
		}else{
			return true;
		}
	}

	#====================================================================================================
			#Des : To execute given query and return result in form of array
 	#====================================================================================================
	public function allSelects($sqlquery){

		//var_dump($sqlquery);
		//die();
		$query = $this->db->query($sqlquery);
		//var_dump($query);die();
		return $query->result_array();
	}
	function userPassword($where){
		$query = "SELECT password FROM user WHERE user_id = '".$where."'";
		$result = $this->db->query($query);
		$data = $result->result_array();

			foreach ($data as $key => $value) {
				return $value['password'];
			}
	}
	public function add_booking($arr){
		if($this->db->insert('bookings', $arr)) {
			return true;
		} else {
			return false;
		}
	}
	public function getGrossTotal($id){
		$query = "Select sum(sale_cost) as total FROM bookings WHERE user_id=$id";
		$result = $this->db->query($query);
		$data = $result->row();
		return $data;
	}

	public function getTotalPending($id){
		$query = "Select sum(balance) as total FROM bookings WHERE status='pending' AND user_id=$id";
		$result = $this->db->query($query);
		$data = $result->row();
		return $data;
	}

	public function getTotalPaid($id){
		$query = "Select sum(received_amount) as total FROM bookings WHERE status='paid' AND user_id=$id";
		$result = $this->db->query($query);
		$data = $result->row();
		return $data;
	}
	public function getAllAgents(){
		$query = $this->allSelects("select a.*, u.email, u.username from agent a LEFT JOIN user u on a.user_id = u.user_id ORDER BY a.agent_number DESC");
		return $query;
	}

	// public function getAllAgents() {
	// 	$query = $this->allSelects("select a.*, b.received_amount from agent a left join (select user_id, sum(sale_sub_total) as received_amount from booking_details WHERE booking_status = 2 group by user_id) b on a.user_id = b.user_id");
	// 	return $query;
	// }

	public function getLatUser($id1,$id2){
		$this->db->where('username', $id1);
		$this->db->where('password', $id2);
		$query = $this->db->get('user');
		return $query->row();
	}
	public function getAgentById($id){
		$query = $this->allSelects("select a.*,b.email,b.username from agent a left join user b on a.user_id = b.user_id where a.user_id = $id");
		// $query = $this->db->get('agent');
		return $query;
	}
	public function getAllSubAgentById($id){
	//	$query = $this->allSelects("select a.*,b.email,b.username from agent a left join user b on a.user_id = b.user_id where a.created_by = $id");
		// $query = $this->db->get('agent');
		$this->db->where('created_by',$id);
		$query = $this->db->get('agent');

		return count($query->result());
	}
	public function getSubAgents($id){
		$query = $this->allSelects("select a.*,b.email,b.username from agent a left join user b on a.user_id = b.user_id where a.created_by = $id");
		// $query = $this->db->get('agent');
		return $query;
	}
	public function getHotels($id){
		$this->db->where('user_id', $id);
		$query = $this->db->get('hotel');
		return $query->result();
	}
	public function getHotelById($id){
		$this->db->where('id', $id);
		$query = $this->db->get('hotel');
		return $query->row();
	}
	public function getRoomTypes(){
		$query = $this->db->get('room_types');
		return $query->result();
	}
	public function getRooms($id){
		// $this->db->where('user_id', $id);
		// $query = $this->db->get('room');
		$query = $this->allSelects('select a.*,b.room_type,c.name as hotel_name from room a left join room_types b on a.room_type = b.id left join hotel c on a.hotel_id = c.id where a.user_id="'.$id.'";');
		return $query;
	}
	public function getRoomDetailsById($id){
		// $this->db->where('id', $id);
		// $query = $this->db->get('room');
		$query = $this->allSelects('select a.id, a.price_per_room as per_room_price,a.no_of_rooms as no_of_rooms,a.room_type,a.hotel_id as hotel,c.name from room a left join room_types b on a.room_type = b.id left join hotel c on a.hotel_id = c.id where a.id="'.$id.'";');

		return $query;
	}
	public function getHotelByUser($id){
		// $this->db->select('count(*)');
		$this->db->where('user_id', $id);
		$query = $this->db->get('hotel');
		return count($query->result());
	}
	public function get_all_suppliers(){
		$query = $this->db->get('supplier');
		return $query->result();
	}
	public function get_supplier_by_id($id){
		$this->db->where('id',$id);
		$query = $this->db->get('supplier');
		return $query->row();
	}
	public function get_all_hotels(){
		$this->db->where('user_id', $this->session->userdata('user_id'));
		$query = $this->db->get('hotel');
		return $query->result();
	}
	public function get_hotel_by_id($id){
		$this->db->where('id',$id);
		$query = $this->db->get('hotel');
		return $query->row();
	}
	public function get_all_roomtypes(){
		$this->db->where('user_id', $this->session->userdata('user_id'));
		$query = $this->db->get('room_types');
		return $query->result();
	}
	public function get_roomtype_by_id($id){
		$this->db->where('id',$id);
		$query = $this->db->get('room_types');
		return $query->row();
	}
	public function get_all_customer(){
			$this->db->where('user_id', $this->session->userdata('user_id'));
		$query = $this->db->get('sub_agent');
		return $query->result();
	}
	public function get_customer_by_id($id){
		$this->db->where('id',$id);
		$query = $this->db->get('sub_agent');
		return $query->row();
	}
	public function get_all_meal(){
		$this->db->where('user_id', $this->session->userdata('user_id'));
		$query = $this->db->get('meal');
		return $query->result();
	}
	public function get_meal_by_id($id){
		$this->db->where('id',$id);
		$query = $this->db->get('meal');
		return $query->row();
	}
	public function get_all_service(){
			$this->db->where('user_id', $this->session->userdata('user_id'));
		$query = $this->db->get('service');
		return $query->result();
	}
	public function get_service_by_id($id){
		$this->db->where('id',$id);
		$query = $this->db->get('service');
		return $query->row();
	}
	public function get_hotel_image($id){
		$this->db->select('image');
		$this->db->where('id', $id);
		$query = $this->db->get('hotel');
		return $query->row();
	}
	public function get_all_bookings(){
		$user_id = $this->session->userdata('user_id');
		$query = "SELECT b.id as booking_id,b.subagent_id,c.id as customer_id, c.name, c.phone, c.email, b.*, r.room_name as room_type, h.name as hotel_name,ss.id as stay_status_id, ss.hotel_reservation_no,ss.room_numbers,ss.stay_status,p.id as payment_id FROM hotel h RIGHT JOIN booking_details b on h.id = b.hotel LEFT JOIN customer c on b.customer_id = c.id LEFT JOIN room_types r on r.id = b.room_type LEFT JOIN payment_details p on b.customer_id = p.customer_id LEFT JOIN stay_status ss on b.id = ss.booking_id where b.user_id = $user_id";
		$result = $this->db->query($query);
		$data = $result->result_array();
		return $data;
	}
	public function get_booking_by_id($id){
		$user_id = $this->session->userdata('user_id');
		$query = "SELECT c.id as custome_id,b.id as booking_id, p.id as payment_id, c.*, b.*,p.*,m.name as meal_name,s.supplier_name, r.room_name, h.name as hotel_name,ss.id as stay_status_id,ss.room_numbers,ss.stay_status,ss.created_at as allocated_date,mr.id as moved_rooms_id,mr.room_numbers as moved_rooms,mr.created_at as moved_date,ec.previous_date,ec.extended_date,ec.created_at as extended_on FROM hotel h RIGHT JOIN booking_details b on h.id = b.hotel LEFT JOIN customer c on b.customer_id = c.id LEFT JOIN room_types r on r.id = b.room_type LEFT JOIN payment_details p on b.customer_id = p.customer_id LEFT JOIN meal m on b.meal_type = m.id LEFT JOIN supplier s on b.supplier = s.id LEFT JOIN stay_status ss ON ss.booking_id = b.id LEFT JOIN moved_rooms mr ON mr.stay_status_id= ss.id LEFT JOIN extend_checkout ec ON ec.booking_id = b.id where b.id = $id and b.user_id = $user_id";
		$result = $this->db->query($query);
		$data = $result->row();
		return $data;
	}
	public function get_exteded_history($id){
		$user_id = $this->session->userdata('user_id');
		$query = $this->db->query("SELECT a.* FROM moved_rooms a LEFT JOIN stay_status b ON b.id = a.stay_status_id WHERE b.booking_id = $id AND a.user_id = $user_id");
		return $query->result();
	}
	public function get_checkout_extended_history($id){
		$user_id = $this->session->userdata('user_id');
		$query = $this->db->query("SELECT a.* FROM extend_checkout a LEFT JOIN booking_details b ON b.id = a.booking_id WHERE a.booking_id = $id");
		return $query->result();
	}
	public function get_agent_by_type($id){
		$user_id = $this->session->userdata('user_id');
		$this->db->where('customer_type', $id);
		$this->db->where('user_id', $user_id);
		$query = $this->db->get('sub_agent');
		return $query->result();
	}
	public function get_no_of_pax_by_id($id){
		$user_id = $this->session->userdata('user_id');
		$this->db->where('id', $id);
		$this->db->where('user_id', $user_id);
		$query = $this->db->get('room_types');
		return $query->row();
	}
	public function getLastBookingNo(){
		$query = $this->db->query('SELECT * FROM booking ORDER BY resrervation_no DESC LIMIT 1');
		return $query->row();
	}
	public function getLastVoucherNo(){
		$query = $this->db->query('SELECT * FROM booking_payments ORDER BY id DESC LIMIT 1');
		return $query->row();
	}
	public function getLastExpenseVoucherNo(){
		$query = $this->db->query('SELECT * FROM expense_vouchers ORDER BY id DESC LIMIT 1');
		return $query->row();
	}
	public function getAgent($id){
		$user_id = $this->session->userdata('user_id');
		$this->db->where('customer_type',$id);
		$this->db->where('user_id',$user_id);
		$query = $this->db->get('sub_agent');
		$result = $query->result_array();
		if(!empty($result)) {
			return true;
		} else {
			return false;
		}
	}
	public function get_all_services(){
		$user_id = $this->session->userdata('user_id');
		$this->db->where('user_id', $user_id);
		$query = $this->db->get('service');
		return $query->result();
	}
}
?>
