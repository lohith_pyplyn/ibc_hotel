<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Availability_model extends CI_Model {

  function get_available_rooms($check_in,$check_out) {
    $query = $this->allSelects("SELECT *, id as room_type_id FROM room_types");
    $temp_arr = array();
    foreach ($query as $value) {
      $booked_rooms = $this->get_available_rooms_by_dates($value['id'],$check_in,$check_out);
      $value['available_rooms'] = $value['no_of_rooms'] - $booked_rooms;
      $temp_arr[] = $value;
    }
    return $temp_arr;
  }

  function get_available_rooms_by_dates($id,$check_in,$check_out) {
    $result = $this->allSelects("SELECT * FROM booking WHERE room_type_id = $id AND (check_in BETWEEN '$check_in' AND '$check_out' OR check_out between '$check_in' AND '$check_out')");
    $total = 0;
    foreach ($result as $value) {
      $total += $value['no_of_rooms'];
    }
    return $total;
  }
  function get_all_rooms(){
    $result = $this->allSelects("SELECT *, id as room_type_id FROM room_types ");
		return $result;
  }
  function get_rooms_three(){
    $result = $this->allSelects("SELECT *, id as room_type_id FROM room_types WHERE no_of_person_allowed > 1");
    return $result;
  }
  function get_rooms_four(){
    $result = $this->allSelects("SELECT *, id as room_type_id FROM room_types WHERE no_of_person_allowed > 3");
    return $result;
  }
  function get_room_type_by_id($room_type_id){
    $this->db->where('id', $room_type_id);
    $query = $this->db->get('room_types');
    return $query->row();
  }
  function getpricingfor_extrabed(){
    $result = $this->allSelects("SELECT price FROM extras WHERE name = 'bed'");
    return $result;
  }
  #====================================================================================================
			#Des : To execute given query and return result in form of array
 	#====================================================================================================
	public function allSelects($sqlquery){
		$query = $this->db->query($sqlquery);
		return $query->result_array();
	}
}
