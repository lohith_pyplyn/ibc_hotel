<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Booking_model extends CI_Model {

	public function __construct(){
      parent::__construct();
      $this->load->library('session');
	}

	public function get_all_bookings() {
		$user_id = $this->session->userdata('user_id');
		$result = $this->allSelects("SELECT a.*, b.room_type as room_type_name FROM booking a LEFT JOIN room_types b on a.room_type_id = b.id WHERE a.user_id = $user_id");
		return $result;
	}

	public function get_payments_for_booking($id,$voucher_type) {
		$result = $this->allSelects("SELECT * FROM booking_payments WHERE booking_id = $id AND voucher_type = '$voucher_type'");
		return $result;
	}

	public function get_payments_for_service($id) {
		$result = $this->allSelects("SELECT * FROM service_payments WHERE service_id = $id");
		return $result;
	}

	public function check_pending_amount($booking_id,$amount,$voucher_type) {
		$total_booking_amount 	=	$this->get_booking_total_amount($booking_id, $voucher_type);
		$total_paid_amount		= 	$this->get_booking_paid_amount($booking_id, $voucher_type) + $amount;
		$total 					= 	$total_booking_amount - $total_paid_amount;
		if($total == 0) {
			return 0;
		} else if($total < 0) {
			return -1;
		} else {
			return 1;
		}
	}

	public function check_pending_amount_for_service($service_id,$amount) {
		$total_service_sale_amount 	=	$this->get_service_sale_total_amount($service_id);
		$total_paid_amount			= 	$this->get_service_paid_amount($service_id) + $amount;
		$total 						= 	$total_service_sale_amount - $total_paid_amount;
		if($total == 0) {
			return 0;
		} else if($total < 0) {
			return -1;
		} else {
			return 1;
		}
	}

	public function get_service_sale_total_amount($service_id) {
		$result = $this->allSelects("SELECT * FROM booked_services WHERE id = $service_id");
		if(!empty($result)) {
			foreach ($result as $value) {
				return $value['total_price'];
			}
		} else {
			return 0;
		}
	}

	public function get_service_paid_amount($service_id) {
		$query = "Select sum(amount) as total FROM service_payments WHERE service_id = $service_id";
		$result = $this->db->query($query);
		$data = $result->result_array();
		if(!empty($data)) {
			foreach($data as $value){
				return $value['total'];
			}
		} else {
			return 0;
		}
	}

	public function get_booking_total_amount($booking_id, $voucher_type) {
		if($voucher_type == 'rv') {
			$result = $this->allSelects("SELECT * FROM booking_details WHERE id = $booking_id");
			if(!empty($result)) {
				foreach ($result as $value) {
					return $value['sale_sub_total'];
				}
			} else {
				return 0;
			}
		} else if($voucher_type == 'pv') {
			$result = $this->allSelects("SELECT * FROM booking_details WHERE id = $booking_id");
			if(!empty($result)) {
				foreach ($result as $value) {
					return $value['purchase_sub_total'];
				}
			} else {
				return 0;
			}
		}
	}

	public function get_booking_paid_amount($booking_id, $voucher_type) {
		if($voucher_type == 'rv') {
			$query = "Select sum(amount) as total FROM booking_payments WHERE booking_id = $booking_id AND voucher_type = 'rv'";
			$result = $this->db->query($query);
			$data = $result->result_array();
			if(!empty($data)) {
				foreach($data as $value){
					return $value['total'];
				}
			} else {
				return 0;
			}
		} else if($voucher_type == 'pv') {
			$query = "Select sum(amount) as total FROM booking_payments WHERE booking_id = $booking_id AND voucher_type = 'pv'";
			$result = $this->db->query($query);
			$data = $result->result_array();
			if(!empty($data)) {
				foreach($data as $value){
					return $value['total'];
				}
			} else {
				return 0;
			}
		}
	}

	public function get_booking_purchase_total_amount($booking_id) {
		$result = $this->allSelects("SELECT * FROM booking_details WHERE id = $booking_id");
		if(!empty($result)) {
			foreach ($result as $value) {
				return $value['purchase_sub_total'];
			}
		} else {
			return 0;
		}
	}
	public function get_booking_sale_total_amount($booking_id) {
		$result = $this->allSelects("SELECT * FROM booking_details WHERE id = $booking_id");
		if(!empty($result)) {
			foreach ($result as $value) {
				return $value['sale_sub_total'];
			}
		} else {
			return 0;
		}
	}

	public function get_booking_sale_paid_amount($booking_id) {
		$query = "Select sum(amount) as total FROM booking_payments WHERE booking_id = $booking_id AND voucher_type = 'rv'";
		$result = $this->db->query($query);
		$data = $result->result_array();
		if(!empty($data)) {
			foreach($data as $value){
				return $value['total'];
			}
		} else {
			return 0;
		}
	}

	public function get_booking_purchase_paid_amount($booking_id) {
		$query = "Select sum(amount) as total FROM booking_payments WHERE booking_id = $booking_id AND voucher_type = 'pv'";
		$result = $this->db->query($query);
		$data = $result->result_array();
		if(!empty($data)) {
			foreach($data as $value){
				return $value['total'];
			}
		} else {
			return 0;
		}
	}

	public function get_booking_refund_paid_amount($booking_id) {
		$result = $this->allSelects("SELECT * FROM booking_payments WHERE booking_id = $booking_id AND voucher_type = 'refund'");
		return $result;
	}

	public function get_booking_by_id($booking_id) {
		$result = $this->allSelects("SELECT * FROM booking_details WHERE id = $booking_id");
		return $result;
	}

	public function get_customers() {
		$user_id = $this->session->userdata('user_id');
		$result = $this->allSelects("SELECT * FROM customer WHERE user_id = $user_id");
		return $result;
	}

  public function get_room_types() {
		$user_id = $this->session->userdata('user_id');
		$result = $this->allSelects("SELECT * FROM room_types WHERE user_id = $user_id");
		return $result;
	}

	public function get_meal_types() {
		$user_id = $this->session->userdata('user_id');
		$result = $this->allSelects("SELECT * FROM meal WHERE user_id = $user_id");
		return $result;
	}

  	public function get_suppliers($id) {
		$user_id = $this->session->userdata('user_id');
		$this->db->where('supplier_type',$id);
		$this->db->where('user_id',$user_id);
		$query = $this->db->get('supplier');
		return $query->result();
		// $result = $this->allSelects("SELECT * FROM supplier WHERE supplier_type=".$id." AND user_id = $user_id");
		// return $result;
	}

	public function get_hotels() {
		$user_id = $this->session->userdata('user_id');
		$result = $this->allSelects("SELECT * FROM hotel WHERE user_id = $user_id");
		return $result;
	}

  public function get_credit_amount($id) {
		$user_id = $this->session->userdata('user_id');
		$result = $this->allSelects("SELECT * FROM sub_agent WHERE user_id = $user_id AND id=$id");
		if(!empty($result)) {
			foreach ($result as $value) {
				return $value['credit_limit'];
			}
		} else {
		return 0;
		}
	}
	public function get_hotel_by_id($id){
		$user_id = $this->session->userdata('user_id');
		$this->db->where('id', $id);
		$this->db->where('user_id', $user_id);
		$query = $this->db->get('hotel');
		return $query->row();
	}
	public function get_extedend_history($id){
		$user_id = $this->session->userdata('user_id');

	}
	public function get_stay_status_data($id){
		$user_id = $this->session->userdata('user_id');
		$this->db->where('booking_id', $id);
		$this->db->where('user_id', $user_id);
		$query = $this->db->get('stay_status');
		return $query->row();
	}
	public function is_room_extended($status_id){
		// $this->db->where('id',$status_id);
		// $query = $this->db->get('moved_rooms');
		// return $query->row();
		$query = $this->db->query("SELECT a.id, a.booking_id, a.hotel_reservation_no, a.reservation_no, a.no_of_rooms, b.room_numbers FROM stay_status a RIGHT JOIN moved_rooms b ON a.id = b.stay_status_id WHERE a.id = $status_id ORDER BY b.created_at DESC");
		return $query->result();
	}
	public function get_sub_agent_data($id){
		$this->db->where('id', $id);
		$query = $this->db->get('sub_agent');
		return $query->row();
	}
	public function get_all_booked_services(){
		$user_id = $this->session->userdata('user_id');

		$query = $this->allSelects("select a.* ,b.name as service_name from booked_services a left join service b on a.service_id = b.id where a.user_id = $user_id");
		return $query;
	}
	public function get_booked_service_by_id($id){
		$user_id = $this->session->userdata('user_id');

		$query = $this->allSelects("select a.* ,b.name as service_name,c.name as agent_name from booked_services a left join service b on a.service_id = b.id left join sub_agent c on a.agent_id = c.id where b.user_id=$user_id and a.id = $id");
		return $query;
	}
	public function get_allotties(){
		$res = $this->db->get('allotty');
		return $res->result();
		// $result = $this->allSelects("SELECT * FROM allotty");
		// return $result;
	}
	public function get_all_allotties(){
		$res = $this->allSelects("SELECT id as allotty_id, supplier_type_name FROM allotty");
		return $res;
		// $result = $this->allSelects("SELECT * FROM allotty");
		// return $result;
	}
	public function get_currency_type(){
		$currency_type = $this->allSelects("SELECT * FROM currency_type");
		return $currency_type;
	}
	#====================================================================================================
			#Des : To execute given query and return result in form of array
 	#====================================================================================================
	public function allSelects($sqlquery){
		$query = $this->db->query($sqlquery);
		return $query->result_array();
	}

}
?>
