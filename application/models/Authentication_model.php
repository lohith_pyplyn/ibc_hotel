<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Authentication_model extends CI_Model {

	public function __construct(){
      parent::__construct();
      $this->load->library('session');
  }

	public function get_user_details($id) {
		$result = $this->allSelects("SELECT * FROM user WHERE user_id = $id");
		return $result;
	}

	public function login_user($user_details)
	{
    $this->db->where('username',$user_details['username']);
 		$this->db->where('password',md5($user_details['password']));
		$this->db->where('user_type',$user_details['user_type']);
		$rs	= $this->db->get('user_master');
		$user_info = (array)$rs->first_row();
		if(!empty($user_info))
		{

			$session_info = $user_info;
			$this->session->set_userdata($session_info);
			$this->session->set_userdata('is_login', 'true');
			return true;
		}else{
      return false;
		}
  }

	function check_login($user_type) {
		$session = $this->session->userdata('username');
		if($session) {
			return true;
		} else {
			return false;
		}
	}
	function register_user($insertArray){
			$insertArray = array(
					'first_name'     	=>  $_POST['firstname'],
					'last_name'     	=>  $_POST['lastname'],
					'phone'     			=>  $_POST['phoneno'],
					'email'         	=>  $_POST['email']

			);
			$arrayinsert = array(

					'username'     	=>  $_POST['username'],
					'password'      =>  md5($_POST['password']),
					'email'         	=>  $_POST['email'],
					'user_type'			=>  'user'

			);
			if($this->db->insert('user_master',$arrayinsert)) {
				$this->db->insert('customers',$insertArray);
				return true;
			}
			else{
				return false;
			}
		}
	#====================================================================================================
	#Des : for exist user
 	#====================================================================================================
	function isUserExist($where){
		$query = "SELECT user_id FROM user_master WHERE user_name = '".$where."'";
		$result = $this->db->query($query);
		$data = $result->result_array();

		if(isset($data) && !empty($data)){
			return false;
		}else{
			return true;
		}
	}

	#====================================================================================================
			#Des : To execute given query and return result in form of array
 	#====================================================================================================
	public function allSelects($sqlquery){
		$query = $this->db->query($sqlquery);
		return $query->result_array();
	}
 


	public function user_feedback_model() {
		// $hotel_id = $this->session->userdata('hotel_id');
		$result = $this->allSelects("SELECT * FROM cancellation_policy ORDER BY id DESC");
		return $result;
	}



}
?>
