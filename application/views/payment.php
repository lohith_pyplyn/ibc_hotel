<?php
 
// Merchant key here as provided by Payu
$MERCHANT_KEY = "gtKFFx";
 
// Merchant Salt as provided by Payu
$SALT = "eCwWELxi";
 
// End point - change to https://secure.payu.in for LIVE mode
$PAYU_BASE_URL = "https://test.payu.in";
// $PAYU_BASE_URL = "https://secure.payu.in";
 
$action = '';

$pay_amount = $amount;

$fistname = "test";
$email = "test@test.com";
$phone = "9164800684";
$product_info = "Hotel Booking";
 
$posted = array();
if(!empty($_POST)) {
    //print_r($_POST);
  foreach($_POST as $key => $value) {    
    $posted[$key] = $value; 
 
  }
}
 
$formError = 0;
 
if(empty($posted['txnid'])) {
  // Generate random transaction id
  $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
} else {
  $txnid = $posted['txnid'];
}
 
$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if(empty($posted['hash']) && sizeof($posted) > 0) {
  if(
          empty($posted['key'])
          || empty($posted['txnid'])
          || empty($posted['amount'])
          || empty($posted['firstname'])
          || empty($posted['email'])
          || empty($posted['phone'])
          || empty($posted['productinfo'])
          || empty($posted['surl'])
          || empty($posted['furl'])
 
  ) {
    $formError = 1;
  } else {
    //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
    $hashVarsSeq = explode('|', $hashSequence);
    $hash_string = ''; 
    foreach($hashVarsSeq as $hash_var) {
        $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
        $hash_string .= '|';
    }

    $hash_string .= $SALT; 
 
    $hash = strtolower(hash('sha512', $hash_string));
    $action = $PAYU_BASE_URL . '/_payment';
  }
} elseif(!empty($posted['hash'])) {
  $hash = $posted['hash'];
  $action = $PAYU_BASE_URL . '/_payment';
}
?>
<html>
  <head>
    <style>
        
        .lds-ripple {
            display: inline-block;
            position: relative;
            width: 64px;
            height: 64px;
            top: 50%;
            left: 50%;
            margin-top: 200px;
            margin-left: -50px;
            }
            .lds-ripple div {
            position: absolute;
            border: 4px solid #000;
            opacity: 1;
            border-radius: 50%;
            animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
            }
            .lds-ripple div:nth-child(2) {
            animation-delay: -0.5s;
            }
            @keyframes lds-ripple {
            0% {
                top: 28px;
                left: 28px;
                width: 0;
                height: 0;
                opacity: 1;
            }
            100% {
                top: -1px;
                left: -1px;
                width: 58px;
                height: 58px;
                opacity: 0;
            }
        }

    </style>
    <script>
        var auto = setTimeout(function(){ 
            document.forms["payuForm"].submit();    
        }, 100);
    </script>
  </head>
  <body onload="submitPayuForm()">
  <div class="lds-ripple"><div></div><div></div></div>
    <form action="<?php echo $action; ?>" method="post" name="payuForm">
        <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
        <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
        <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
        <input type="hidden" name="amount" value="<?php echo $pay_amount ?>">
        <input type="hidden" name="firstname" id="firstname" value="<?php echo $fistname ?>" />
        <input type="hidden" name="email" id="email" value="<?php echo $email ?>" />
        <input type="hidden" name="phone" value="<?php echo $phone ?>" />
        <input type="hidden" name="productinfo" value="<?php echo $product_info ?>">
        <!-- Success URI:  -->
        <input type="hidden" name="surl" value="http://localhost/ibc_hotel/success" size="64" />
        <!-- Failure URI:  -->
        <input type="hidden" name="furl" value="http://localhost/ibc_hotel/failure" size="64" />

         <!-- <input type="hidden" name="service_provider" value="payu_paisa" size="64" />-->
        <?php if(!$hash) { ?>
        <!-- <input type="submit" value="Submit" /> -->
        <?php } ?>
    </form>
  </body>
</html>
