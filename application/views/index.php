<!DOCTYPE html>
<html ng-app="bos">
<head>
  <meta charset="utf-8">
  <title>IBC Hotel & Resort</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" >
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
  <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon"/>

  <!--calendar css-->
  <link href="plugins/fullcalendar/css/fullcalendar.min.css" rel="stylesheet" />

  <link href="plugins/jquery.filer/css/jquery.filer.css" rel="stylesheet" />
  <link href="plugins/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css" rel="stylesheet" />

  <!-- Plugins css-->
  <link href="plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css" rel="stylesheet" />
  <link href="plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
  <link href="plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
  <link href="plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
  <link href="plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
  <link href="plugins/bx-slider/jquery.bxslider.css" rel="stylesheet" type="text/css" />

  <link href="plugins/jquery.steps/css/jquery.steps.css" rel="stylesheet" type="text/css" />

  <!-- App css -->
  <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

  <link rel="stylesheet" href="assets/css/treeGrid.css">
  <link rel="stylesheet" type="text/css" href="assets/css/jquery.datetimepicker.css"/>
  <link rel="stylesheet" href="bower_components/angular-pickadate/angular-pickadate.min.css"/>
  <link href="assets/css/ionicons.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="plugins/switchery/switchery.min.css">

  <link rel="stylesheet" type="text/css" href="assets/source/jquery.fancybox.css?v=2.1.5" media="screen" />
  <link rel="stylesheet" type="text/css" href="assets/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
  <link rel="stylesheet" type="text/css" href="assets/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
  <link rel="stylesheet" href="bower_components/angular-dialog/ngDialog.css"/>
  <link rel="stylesheet" href="bower_components/angular-dialog/ngDialog-custom-width.css"/>
  <link rel="stylesheet" href="bower_components/angular-dialog/ngDialog-theme-default.css"/>
  <link rel="stylesheet" href="bower_components/angular-dialog/ngDialog-theme-plain.css"/>

  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

  <!-- Custom CSS -->
  <link rel="stylesheet" href="assets/css/custom_styles.css">
  <link rel="stylesheet" href="assets/css/login.css">
  <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">

  </style>
  <script src="assets/js/modernizr.min.js"></script>

  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-83057131-1', 'auto');
  ga('send', 'pageview');

  </script>
  <style>
  .affix {
    top: 0 !important;
    width: 100% !important;
    z-index: 9999 !important;
  }
  </style>

  <script type="text/javascript">
  var BASE_URL = "<?php $this->config->item('base_url'); ?>";
  </script>
</head>


<body class="fixed-left">
  <!-- <div ng-view></div> -->
  <div class="container-fluid" id="wrapper" style="padding: 0;">
    <!-- <div class="" ng-include="'tmp/user/header1.php'"></div> -->
    <div ng-controller="NavigationController">
      <div id="menuLoaderDiv">
        <div ng-controller="HeaderController">
          <header id="" ng-hide="isDashboard">
            <nav class="navbar topHeader" >
              <div class="container-fluid" style="position:fixed;background-color: white; width:100%;">
                <div class=" animated fadeInLeft" id="myNavbar" style="background-color: white";>
                  <ul class="nav navbar-nav display-hide icon-size" style="display: flex; justify-content: center;">
                  <li><a href="#"><i class="fa fa-facebook-square fa-lg" style="color: #337ab7"></i></a></li>
                  <li><a href="#"><i class="fa fa-instagram fa-lg" style="color: deeppink"></i></a></li>
                 <li><a href="#"><i class="fa fa-twitter-square fa-lg" style="color: #1DA1F2"></i></a></li>
                  </ul>
                  <ul class="nav navbar-nav HeaderTop">
                    <li><h4 style=" padding-top:6px;"> <a class="display-hide" style="color: #000000; padding-top:6px;" href="#" ><i class="fa fa-envelope display-hide "></i>&nbsp;&nbsp;reservations@ibchotels.co.in &nbsp;</a></h4></li>
                    <li><h4 style=" padding-top:6px;"> <a  class="displayhide" style="color: #000000;" href="#"> &nbsp;&nbsp;<i class="fa fa-whatsapp fa-1x  displayhide"></i> 08041670733</a></h4></li>
                    <li><a class="display-hide" href="#"  style="color: #000000;">English</a></li>
                    <li ng-show="!isLoggedIn"><a href="" style="color: #000000;" data-toggle="modal" data-target="#loginSignup">Member Login</a></li>
                    <li ng-show="!isLoggedIn"><a href="" style="color: #000000;" data-toggle="modal" data-target="#Signup">Sign Up</a></li>
                    <li ng-show="isLoggedIn">
                      <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown user-box align-box">
                            <a href="" class="dropdown-toggle waves-effect user-link" data-toggle="dropdown" aria-expanded="true" style="padding:0px!important;">
                                <img ng-src="assets/images/users/avatars/avatar_default2.png" alt="" class="img-circle user-img img-aligs" style="margin-top: 5px!important;">
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list" >
                                <li>
                                    <h5>Hi, {{user}}</h5>
                                </li>
                                <li ng-if="!isDashboard"><a href="#/dashboard"><i class="glyphicon glyphicon-user"></i>&nbsp;Dashboard</a></li>
                                <li ng-if="isDashboard"><a href="#/"><i class="glyphicon glyphicon-user"></i>&nbsp;Home</a></li>
                                <li><a href="#/user-profile"><i class="glyphicon glyphicon-cog"></i>&nbsp;User Profile</a></li>
                                <li><a href="javascript:void(0);" ng-click="logout();"><i class="glyphicon glyphicon-log-out"></i>&nbsp;Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                    </li>
                  </ul>
                </div>
              </div>
            </nav>

            <nav class="navbar navbar-default navbar-inverse sec_navbar headerIBC skewHeaderCorner">
              <div class="container-fluid ">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar1">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand imagepadding" href="#"><img class="img-responsive img-align myNavbar2"  src="./assets/images/ibc_title.gif" alt="IBC Hotel"></img></a>
                  <!-- <ul class="nav navbar-nav navbar-left">
                    <li>
                        <button class="button-menu-mobile open-left waves-effect" style="margin-top:-35px">
                            <i class="mdi mdi-menu"></i>
                        </button>
                    </li>
                </ul> -->
                </div>
                <div class="collapse navbar-collapse active myNavbar2"  data-toggle="collapse" data-target=".navbar-collapse.in" id="myNavbar1" role="navigation" >
                  <div class="center">
                  <ul class="nav navbar-nav navbar-right colour-change"style="color:white !important;">
                    <li class="home"><a
                    style="color:white" href="#">Home</a></li>
                    <li class=""><a style="color:white" href="#/apartment">Apartment</a></li>
                    <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" style="color:white" href="#">Facilities<span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="#" data-toggle="modal" data-target="#Dining">Dining</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#gym">GYM</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#Business_Facilities">Business Facilities</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#Conference_Hall">banquet/conference Hall</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#Airport_Transfers">Airport transfers</a></li>
                      </ul>
                    </li>
                    <!-- <li class=""><a style="color:white;" href="#/planevent">Plan an event</a></li> -->
                    <li class=""><a style="color:white" href="#/corporate">Corporate Travel</a></li>
                    <li class=""><a style="color:white" href="#/offers">Offers</a></li>

                    <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" style="color:white" href="#">About<span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="#/aboutus">About Us</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#Hotel_policy">Hotel Policy</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#feedbackform">Feedback</a></li>
                        <li><a href="#/contact">Contact Us</a></li>
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a class="dropdown-toggle" data-toggle="dropdown" style="color:white" href="#">Love Bangalore<span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a  href="#" data-toggle="modal" data-target="#Tourist_Attraction">Tourist Attraction</a></li>
                        <li><a  href="#" data-toggle="modal" data-target="#Dining_and_Entertainment">Dining & Entertainment</a></li>
                        <li><a  href="#" data-toggle="modal" data-target="#Shopping_in_Bangalore">Shopping</a></li>
                        <li><a  href="#" data-toggle="modal" data-target="#Medical_Tourism">Medical Tourism</a></li>
                        <li><a  href="#">Blog</a></li>
                      </ul>
                    </li>
                  </ul>
                </div>
              </div>
              </div>
            </nav>
          </header>
          <div class="topbar ng-cloak dashboard topbar-position" style="display:none;" ng-hide="!isDashboard" >
            <div class="topbar-left">
              <a href="#/" class="logo">
                <span>
                    <img src="assets/images/ibc_title.gif" alt="" style="width: 100%; height: 70px;">
                </span>
                <i>
                    <img src="assets/images/ibc_title.gif" alt="" style="width: 100%;" >
                </i>
              </a>
            </div>
            <div class="navbar navbar-default" role="navigation" >
              <div class="container">
                <ul class="nav navbar-nav navbar-left">
                  <li>
                    <button class="button-menu-mobile open-left waves-effect">
                      <i class="mdi mdi-menu"></i>
                    </button>
                  </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                  <li class="dropdown user-box">
                    <a href="#" class="dropdown-toggle waves-effect user-link" data-toggle="dropdown" aria-expanded="true">
                      <img ng-src="assets/images/users/avatars/avatar_default2.png" alt="user-img" class="img-circle user-img img-aligns">
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list" >
                      <li>
                          <h5>Hi, {{user}}</h5>
                      </li>
                      <li><a href="#/user-profile"><i class="glyphicon glyphicon-user m-r-5"></i>&nbsp;Profile</a></li>
                      <li><a href="#/change-password"><i class="glyphicon glyphicon-cog"></i>&nbsp;Change Password</a></li>
                      <li><a href="javascript:void(0);" ng-click="logout();" ><i class="glyphicon glyphicon-log-out"></i>&nbsp;Logout</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>


        <div class="left side-menu margin-top topbar-position" style="display: none;" id="sidebar" ng-if="isDashboard" data-toggle="collapse" data-target=".side-menu.in">
          <div class="sidebar-inner slimscrollleft">
            <div id="sidebar-menu">
              <ul>
                <li ng-if="isDashboard"><a href="#/" class="waves-effect"><i class="glyphicon glyphicon-home "></i>&nbsp; Home</a></li>
                <li class="has_sub">
                    <a href="#/dashboard" class="waves-effect"><i class="fa fa-tachometer"></i> <span> Dashboard </span> </a>
                </li>
                <li class="has_sub">
                    <a href="#/upcomingbooking" class="waves-effect"><i class="fa fa-bookmark"></i> <span> Upcoming Booking </span> </a>
                </li>
                <li class="has_sub">
                  <a href="#/bookinghistory" class="waves-effect"><i class="fa fa-history"></i><span> Booking History </span> </a>
                </li>
                <li>
                  <a href="javascript:void(0);" class="waves-effect" ng-click="logout();"><i class="mdi mdi-logout"></i>&nbsp; Logout</a>
                </li>
              </ul>
                <!-- Sidebar -->
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>


      <!-- Modal -->
      <div class="modal fade" id="loginSignup" role="dialog">
        <!-- <div class="modal-dialog"> -->
          <!-- Modal content-->
          <!-- <div class="modal-content"> -->
            <div class="modal-body">
              <div class="row">
                  <div class="col-sm-12">
                      <div class="wrapper-page">
                          <div class="m-t-40 account-pages">
                              <div class="text-center account-logo-box" style="background:#000000;">
                                  <h2 class="text-uppercase" style="color:#ffffff!important;">
                                    login
                                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">&times;</button>
                                  </h2>
                              </div>
                              <div class="account-content" style="border-radius: 0 0 5px 5px">
                                  <form class="form-horizontal" name="loginUserForm">
                                      <span id="success" class="col-sm-12 success"></span>
                                      <span id="error" class="col-sm-12 error"></span>
                                      <div class="form-group ">
                                        <div class="col-xs-12">
                                          <input class="form-control" type="text" required="" placeholder="Username" name="username" ng-model="user.username">
                                          <div ng-show="loginUserForm.username.$touched">
                                            <p class="error" ng-show="loginUserForm.username.$error.required">This field is required.</p>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="form-group">
                                          <div class="col-xs-12">
                                              <input class="form-control" type="password" name="password" ng-model="user.password" required="" placeholder="Password">
                                              <div ng-show="loginUserForm.password.$touched">
                                                <p class="error" ng-show="loginUserForm.password.$error.required">This field is required.</p>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="form-group text-center m-t-30">
                                          <div class="col-sm-12">
                                              <a id="forgotPass" class="text-muted" data-toggle="modal" data-target="#forgotModal"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
                                          </div>
                                      </div>
                                      <div class="form-group text-center m-t-10">
                                          <div class="col-xs-12">
                                              <button class="btn w-md btn-bordered btn-primary" type="submit"  ng-click="login(user);">Log In</button>
                                          </div>
                                      </div>
                                  </form>
                                  <div class="clearfix"></div>
                              </div>
                          </div>
                          <!-- end card-box-->
                      </div>
                      <!-- end wrapper -->
                  </div>
              </div>
            </div>
            <!-- <div class="modal-footer">
              <div class="col-md-12">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div> -->
          <!-- </div> -->
        <!-- </div> -->
      </div>
      <div class="modal fade" id="Signup" role="dialog">
        <div class="modal-dialog modal-lg">
          <!-- Modal content-->
          <div class="row">
            <div class="col-sm-12">
              <!-- <div class="wrapper-page"> -->
              <div class=" account-pages">
                <div class="text-center account-logo-box" style="background:black;">
                  <h2 class="text-uppercase"style="color:white;">
                    Register
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">x</button>
                  </h2>
                </div>
                <div class="account-content" style="border-radius: 0 0 5px 5px">
                  <div class="account-content">
                    <form class="form-horizontal" name="user_form" enctype="multipart/form-data" method="post">
                      <div class="row">
                        <span class="message col-md-10 col-md-offset-1"></span>
                        <div class="col-md-5 col-md-offset-1">
                          <div class="form-group">
                            <label for="field-1" class="control-label">First Name</label>
                            <input type="text" class="form-control" name="firstname" ng-model="user.firstname" required>
                            <div ng-show="user_form.firstname.$touched">
                              <p class="error" ng-show="user_form.firstname.$error.required">This field is required.</p>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                          <div class="form-group">
                            <label for="field-1" class="control-label">Username</label>
                            <input type="text" class="form-control" name="username" ng-model="user.username" required>
                            <div ng-show="user_form.username.$touched">
                              <p class="error" ng-show="user_form.username.$error.required">This field is required.</p>
                            </div>
                          </div>

                        </div> <!-- End of col-md-4 -->
                        <div class="col-md-1">
                        </div>
                        <div class="clearfix"></div>
                      </div>
                      <div class="row">
                        <div class="col-md-5 col-md-offset-1">
                          <div class="form-group">
                            <label for="field-1" class="control-label">Last Name</label>
                            <input type="text" class="form-control" name="lastname" ng-model="user.lastname" required>
                            <div ng-show="user_form.lastname.$touched">
                              <p class="error" ng-show="user_form.lastname.$error.required">This field is required.</p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="field-1" class="control-label">E-Mail</label>
                            <input type="text" class="form-control" name="email" ng-model="user.email" required>
                            <div ng-show="user_form.email.$touched">
                              <p class="error" ng-show="user_form.email.$error.required">This field is required.</p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="field-1" class="control-label">Phone Number</label>
                            <input type="text" class="form-control" name="phoneno" ng-model="user.phoneno" minlength="10" maxlength="15" required>
                            <div ng-show="user_form.phoneno.$touched">
                              <p class="error" ng-show="user_form.phoneno.$error.required">This field is required.</p>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                          <div class="form-group">
                            <label  for="field-1" class="control-label">Password</label>
                            <input type="password" class="form-control" name="password" ng-model="user.password" required id="pass">
                            <div ng-show="user_form.password.$touched">
                              <p class="error" ng-show="user_form.password.$error.required">This field is required.</p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="field-1" class="control-label">Confirm Password</label>
                            <input type="password" class="form-control" name="confirmpassword" ng-model="user.confirmpassword" required ng-pattern="user.password" id="cpass">
                            <div ng-show="user_form.confirmpassword.$touched">
                              <p class="error" ng-show="user_form.confirmpassword.$error.required">Password is Required.</p>
                              <p class="error" ng-show="user_form.confirmpassword.$error.pattern">Password do not match.</p>
                            </div>
                          </div>
                        </div> <!-- End of col-md-4 -->
                        <div class="col-md-1">
                        </div>
                        <div class="clearfix"></div>
                      </div>
                      <div class="modal-footer">
                        <div class="row pull-right">
                          <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                          <button type="submit" class="btn btn-primary waves-effect waves-light" ng-disabled="user_form.$invalid" ng-click="registerUser(user);">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <!-- end card-box-->
              <!-- </div> -->
              <!-- end wrapper -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <div ng-view></div>
    <div class="" ng-include="'tmp/user/footer.php'" ng-hide="isDashboard">

    </div>
  </div>

  <!--result model -->
  <div class="modal fade" id="resultModal" tabindex="-1" role="dialog" aria-labelledby="resultModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="resultModal">Result Message</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <div id="result-msg-by-id" class="result-msg"></div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <!--===========================================Ws Loader======================================-->
  <div class="modal fade bs-example-modal-sm"  id="wsloader" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="margin-top:100px; background-color: unset;">
    <div class="modal-dialog modal-sm">
      <div class="modal-content" style="background-color: transparent; box-shadow: 0px 0px 0px 0px rgb(255, 255, 255); border: medium none; border-radius: 0px;" align="center">
        <img ng-src="assets/images/loader.gif" style="width:100px !important;"/>
      </div>
    </div>
  </div>
  <script>
  var resizefunc = [];
  </script>

  <!-- jQuery  -->
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="assets/js/detect.js"></script>
  <script src="assets/js/fastclick.js"></script>
  <script src="assets/js/jquery.blockUI.js"></script>
  <script src="assets/js/waves.js"></script>
  <script src="assets/js/jquery.slimscroll.js"></script>
  <script src="assets/js/jquery.scrollTo.min.js"></script>
  <script src="plugins/switchery/switchery.min.js"></script>


  <!-- Jquery-Ui -->
  <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
  <script src="plugins/bx-slider/jquery.bxslider.min.js"></script>



  <script src="plugins/moment/moment.js"></script>
  <script src='plugins/fullcalendar/js/fullcalendar.min.js'></script>
  <script src="assets/pages/jquery.fullcalendar.js"></script>


  <!-- Counter js  -->
  <script src="plugins/waypoints/jquery.waypoints.min.js"></script>
  <script src="plugins/counterup/jquery.counterup.min.js"></script>



  <!-- Percentage Loader -->
  <script src="assets/js/jquery.percentageloader-0.1.min.js"></script>

  <!-- Angular  -->
  <script src="bower_components/angular/angular.js"></script>
  <script src="bower_components/angular-route/angular-route.js"></script>
  <script src="bower_components/angular-aria/angular-aria.min.js"></script>
  <script src="bower_components/angular-cookies/angular-cookies.js"></script>
  <script src="bower_components/angular-animate/angular-animate.js"></script>
  <script src="bower_components/angular-messages/angular-messages.min.js"></script>
  <script src="bower_components/svg-assets-cache/svg-assets-cache.js"></script>
  <script src="bower_components/angular-material/angular-material.js"></script>
  <script src="bower_components/angular-bootstrap/ui-bootstrap-tpls.js"></script>
  <script type="text/javascript" src="bower_components/angular-pickadate/angular-pickadate.min.js"></script>
  <script type="text/javascript" src="assets/js/jquery.datetimepicker.full.js"></script>
  <script type="text/javascript" src="bower_components/angular-scroll/angular-smooth-scroll.js"></script>

  <script src="bower_components/angular-file-upload/angular-file-upload.min.js" type="text/javascript"></script>
  <script src="bower_components/angular-file-upload/ng-file-upload.min.js"></script>
  <script src="assets/js/signature_pad.js"></script>
  <script src="assets/js/ng-signature-pad.js"></script>

  <script type='text/javascript' src='bower_components/angular-dialog/ngDialog.js'></script>
  <script type='text/javascript' src='bower_components/angular-youtube/angular-youtube-embed.js'></script>


  <!-- Custom application JS -->
  <script src="app/app.js"></script>
  <script src="app/url.js"></script>
  <script src="app/custom_controller.js"></script>
  <script src="app/custom_directives.js"></script>
  <script src="app/custom_services.js"></script>

  <!-- Plugin Js -->
  <script src="plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
  <script src="plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>
  <script src="plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js"></script>
  <script type="text/javascript" src="plugins/multiselect/js/jquery.multi-select.js"></script>
  <script type="text/javascript" src="plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
  <!-- <script src="plugins/select2/js/select2.min.js" type="text/javascript"></script> -->
  <script src="plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
  <script src="plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
  <script src="plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
  <script src="plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>

  <!-- Jquery filer js -->
  <script src="plugins/jquery.filer/js/jquery.filer.min.js"></script>

  <!-- page specific js -->
  <script src="assets/pages/jquery.fileuploads.init.js"></script>


  <!-- Charts -->
  <!-- Flot chart js -->
  <script src="plugins/flot-chart/jquery.flot.min.js"></script>
  <script src="plugins/flot-chart/jquery.flot.time.js"></script>
  <script src="plugins/flot-chart/jquery.flot.tooltip.min.js"></script>
  <script src="plugins/flot-chart/jquery.flot.resize.js"></script>
  <script src="plugins/flot-chart/jquery.flot.pie.js"></script>
  <script src="plugins/flot-chart/jquery.flot.selection.js"></script>
  <script src="plugins/flot-chart/jquery.flot.crosshair.js"></script>
  <script src="plugins/counterup/jquery.counterup.min.js"></script>

  <!-- App js -->
  <script src="assets/js/jquery.core.js"></script>
  <script src="assets/js/jquery.app.js"></script>
  <!-- Google Tree Chart Js -->
  <script src="assets/js/loader.js"></script>
  <!-- endbuild -->


</body>
</html>
