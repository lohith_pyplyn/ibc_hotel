<!DOCTYPE html>
<html ng-app="bos">
    <head>
      <meta charset="utf-8">
      <title>IBC Hotels & Resorts</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon"/>
      <link href="plugins/jquery.filer/css/jquery.filer.css" rel="stylesheet" />
        <link href="plugins/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css" rel="stylesheet" />

        <!-- Plugins css-->
        <link href="plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css" rel="stylesheet" />
        <link href="plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
        <link href="plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
        <link href="plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />

        <link href="plugins/jquery.steps/css/jquery.steps.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <link rel="stylesheet" href="assets/css/treeGrid.css">
        <link rel="stylesheet" type="text/css" href="assets/css/jquery.datetimepicker.css"/>
        <link rel="stylesheet" href="bower_components/angular-pickadate/angular-pickadate.min.css"/>
        <link href="assets/css/ionicons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="plugins/switchery/switchery.min.css">



        <link rel="stylesheet" type="text/css" href="assets/source/jquery.fancybox.css?v=2.1.5" media="screen" />
        <link rel="stylesheet" type="text/css" href="assets/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
        <link rel="stylesheet" type="text/css" href="assets/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
        <link rel="stylesheet" href="bower_components/angular-dialog/ngDialog.css"/>
        <link rel="stylesheet" href="bower_components/angular-dialog/ngDialog-custom-width.css"/>
        <link rel="stylesheet" href="bower_components/angular-dialog/ngDialog-theme-default.css"/>
        <link rel="stylesheet" href="bower_components/angular-dialog/ngDialog-theme-plain.css"/>
        <!-- <link rel="stylesheet" href="assets/css/angularjs-datetime-picker.css"/> -->

        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="assets/css/custom_styles.css">
        <link rel="stylesheet" href="assets/css/login.css">
        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">

        </style>
        <script src="assets/js/modernizr.min.js"></script>
      <style>
        .affix {
          top: 0 !important;
          width: 100% !important;
          z-index: 9999 !important;
        }
      </style>

      <script type="text/javascript">
          var BASE_URL = "<?php $this->config->item('base_url'); ?>";
      </script>
    </head>
    <body class="fixed-left">
        <!-- <div ng-view></div> -->
        <div class="container-fluid" id="wrapper" style="padding: 0;">
            <div ng-controller="NavigationController">
              <div id="menuLoaderDiv" ng-show="isLoggedIn" style="display: none;">
                <div ng-controller="HeaderController">
                  <div class="topbar">
                    <div class="topbar-left">
                      <a href="#/" class="logo">
                        <span>
                            <img src="assets/images/ibc_title.gif" alt="" style="width: 100%; height: 65px;">
                        </span>
                        <i>
                            <img src="assets/images/ibc_title.gif" alt="" style="width: 100%;" >
                        </i>
                      </a>
                    </div>
                    <div class="navbar navbar-default" role="navigation">
                      <div class="container">
                        <ul class="nav navbar-nav navbar-left">
                          <li>
                            <button class="button-menu-mobile open-left waves-effect">
                              <i class="mdi mdi-menu"></i>
                            </button>
                          </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                          <li class="dropdown user-box">
                            <a href="#" class="dropdown-toggle waves-effect user-link" data-toggle="dropdown" aria-expanded="true">
                                <img ng-src="assets/images/users/avatars/avatar_default2.png" alt="user-img" class="img-circle user-img">
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list" >
                                <li>
                                    <h5>Hi, Admin</h5>
                                </li>
                                <li><a href="#/userprofile"><i class="glyphicon glyphicon-user m-r-5"></i> Profile</a></li>
                                <li><a href="#/change-password"><i class="glyphicon glyphicon-cog"></i> Change Password</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0);" ng-click="logout();" <i class="glyphicon glyphicon-log-out"> </i> Logout</a></li>
                              </ul>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <!-- Top Bar End -->
                <!-- ========== Left Sidebar Start ========== -->
                <div class="left side-menu margin-top">
                    <div class="sidebar-inner slimscrollleft">
                        <div id="sidebar-menu">
                            <ul>
                                <li class="has_sub">
                                    <a href="#/dashboard" class="waves-effect"><i class="fa fa-tachometer app-green-text"></i> <span>Dashboard</span> </a>
                                </li>
                                <li class="has_sub">
                                    <a href="#/accomodation" class="waves-effect"><i class="fa fa-bed app-green-text"></i> <span>Accommodation</span> </a>
                                </li>
                                <li class="has_sub">
                                    <a href="#/extra" class="waves-effect"><i class="fa fa-plus-circle fa-lg app-green-text"></i> <span>Extras</span> </a>
                                </li>
                                <li class="has_sub">
                                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money app-green-text"></i> <span>Payments</span> <span class="menu-arrow"></span></a>
                                    <ul class="list-unstyled">
                                        <li><a href="#/cancellation"><span>Cancellation Policy</span> </a></li>
                                        <!-- <li><a href="#/payment"><span>Payment & Invoice</span> </a></li> -->
                                    </ul>
                                </li>
                                <li class="has_sub">
                                    <a href="#/user_account" class="waves-effect"><i class="fa fa-group app-green-text"></i> <span>User Management</span> </span></a>
                                    <!-- <ul class="list-unstyled">
                                        <li><a href="#/expense-category">Expense Types</a></li>
                                        <li><a href="#/expense-vouchers">Expense Vouchers</a></li>
                                    </ul> -->
                                </li>
                                <li class="has_sub">
                                <a href="#/promotion_codes" class="waves-effect"><i class="fa fa-file app-green-text"></i><span>Promotion Codes</span></span> </a>
                                    <!-- <ul class="list-unstyled">
                                        <li><a href="#/customer-report" class="waves-effect"> <span> Customer </span> </a></li> -->
                                        <!-- <li><a href="#/book-service" class="waves-effect"><span> Book Service </span> </a></li> -->
                                    <!-- </ul> -->
                                </li>
                                <!-- <li class="has_sub">
                                    <a href="#/room_facility" class="waves-effect"><i class="fa fa-building app-green-text"></i><span>Facilities</span> </a>
                                </li> -->
                                <li>
                                    <a href="javascript:void(0);" class="waves-effect" ng-click="logout();"><i class="mdi mdi-logout app-green-text"></i> Logout</a>
                                </li>
                            </ul>
                            <!-- Sidebar -->
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                </div>
              </div>
            </div>
            <div ng-view></div>
        </div>

        <!--result model -->
        <div class="modal fade" id="resultModal" tabindex="-1" role="dialog" aria-labelledby="resultModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="resultModal">Result Message</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div id="result-msg-by-id" class="result-msg"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--===========================================Ws Loader======================================-->
        <div class="modal fade bs-example-modal-sm"  id="wsloader" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="margin-top:100px; background-color: unset;">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" style="background-color: transparent; box-shadow: 0px 0px 0px 0px rgb(255, 255, 255); border: medium none; border-radius: 0px;" align="center">
                    <img ng-src="assets/images/loader.gif" style="width:100px !important;"/>
                </div>
            </div>
        </div>
        <script>
            var resizefunc = [];
        </script>
        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        <script src="plugins/switchery/switchery.min.js"></script>

        <!-- Counter js  -->
        <script src="plugins/waypoints/jquery.waypoints.min.js"></script>
        <script src="plugins/counterup/jquery.counterup.min.js"></script>

        <script src="plugins/moment/moment.js"></script>
        <!-- Percentage Loader -->
        <script src="assets/js/jquery.percentageloader-0.1.min.js"></script>

        <!-- Angular  -->
        <script src="bower_components/angular/angular.js"></script>
        <script src="bower_components/angular-route/angular-route.js"></script>
        <script src="bower_components/angular-aria/angular-aria.min.js"></script>
        <script src="bower_components/angular-cookies/angular-cookies.js"></script>
        <script src="bower_components/angular-animate/angular-animate.js"></script>
        <script src="bower_components/angular-messages/angular-messages.min.js"></script>
        <script src="bower_components/svg-assets-cache/svg-assets-cache.js"></script>
        <script src="bower_components/angular-material/angular-material.js"></script>
        <script src="bower_components/angular-bootstrap/ui-bootstrap-tpls.js"></script>
        <script type="text/javascript" src="bower_components/angular-pickadate/angular-pickadate.min.js"></script>
        <script type="text/javascript" src="bower_components/angular-datetime/angularjs-datetime-picker.min.js"></script>
        <script type="text/javascript" src="assets/js/jquery.datetimepicker.full.js"></script>
        <script type="text/javascript" src="bower_components/angular-scroll/angular-smooth-scroll.js"></script>

        <script src="bower_components/angular-file-upload/angular-file-upload.min.js" type="text/javascript"></script>
        <script src="bower_components/angular-file-upload/ng-file-upload.min.js"></script>
        <script src="assets/js/signature_pad.js"></script>
        <script src="assets/js/ng-signature-pad.js"></script>

        <script type='text/javascript' src='bower_components/angular-dialog/ngDialog.js'></script>
        <script type='text/javascript' src='bower_components/angular-youtube/angular-youtube-embed.js'></script>



        <!-- Custom application JS -->
        <script src="adminApp/app.js"></script>
        <script src="adminApp/url.js"></script>
        <script src="adminApp/custom_controller.js"></script>
        <script src="adminApp/custom_directives.js"></script>
        <script src="adminApp/custom_services.js"></script>

        <!-- Plugin Js -->
        <script src="plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
        <script src="plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>
        <script src="plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js"></script>
        <script type="text/javascript" src="plugins/multiselect/js/jquery.multi-select.js"></script>
        <script type="text/javascript" src="plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
        <!-- <script src="plugins/select2/js/select2.min.js" type="text/javascript"></script> -->
        <script src="plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
        <script src="plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
        <script src="plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
        <script src="plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>

        <!-- Jquery filer js -->
        <script src="plugins/jquery.filer/js/jquery.filer.min.js"></script>

        <!-- page specific js -->
        <script src="assets/pages/jquery.fileuploads.init.js"></script>


        <!-- Charts -->
        <!-- Flot chart js -->
        <script src="plugins/flot-chart/jquery.flot.min.js"></script>
        <script src="plugins/flot-chart/jquery.flot.time.js"></script>
        <script src="plugins/flot-chart/jquery.flot.tooltip.min.js"></script>
        <script src="plugins/flot-chart/jquery.flot.resize.js"></script>
        <script src="plugins/flot-chart/jquery.flot.pie.js"></script>
        <script src="plugins/flot-chart/jquery.flot.selection.js"></script>
        <script src="plugins/flot-chart/jquery.flot.crosshair.js"></script>
        <script src="plugins/counterup/jquery.counterup.min.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

        <!-- Google Tree Chart Js -->
        <script src="assets/js/loader.js"></script>

        <!-- endbuild -->

    </body>
</html>
